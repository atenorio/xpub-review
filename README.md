# Hindawi Peer Review Platform

[Review](review.hindawi.com) is a system that allows authors to submit manuscripts and editors to manage the peer review for a particular journal on Hindawi.
For more details about Hindawi, please visit: https://www.hindawi.com/

## Structure

This repository is a monorepo containing Pubsweet components used and created by Hindawi.
The app entry point is `packages/app`, the place where the `xpub-review` setup and configurations are stored.

## Installing

In the root directory, run `yarn` to install all the dependencies.

## Configuration

xPub-review is using external services as AWS, MTS-FTP, Publons, ORCID. In order to run the app locally a `.env` file is mandatory with keys and settings for each service. For that, copy `packages/app/.env.example` to `packages/app/.env`. In order to create an admin account with app startup, set the `EMAIL_SENDER` property in the `.env` file to a valid email address.

Contact us at technology@hindawi.com for help getting setup.

## Running the app

1. Open Docker engine.
2. Start services with `yarn startdb`. This will create a local Postgres database inside a docker container.
3. Start app with `yarn start` in root. The migration are started by an npm script pre hook and run before the app actually starts.
4. Login with your `EMAIL_SENDER` (from `.env`) + admin (eg. john.smith+admin@hindawi.com). The password will be `Password1!` and can be changed.

## Community

Join [the Mattermost channel](https://mattermost.coko.foundation/coko/channels/xpub) for discussion of xpub.

## Migrations

1. use folder `./packages/app/migrations`
2. add file named `${timestamp}-${name}(.sql/.js)` e.g. `1560160800-user-add-name-field.sql`

## Scripts

Run a specific script with `yarn script_name`. Current scripts in the application:

- `yarn migrate` - Run all migrations manually (note: successful migrations will not run twice, unless deleted from migrations table)
- `yarn cleandb` - Cleans the database of all the tables, including pgboss jobs and runs migrations
- `yarn addusers [name]` - Will add users with `EMAIL_SENDER` (from `.env`) + `name` (eg. john.smith+ae@hindawi.com). Multiple users can be added by separating names trough space (eg. `yarn addusers ce ae a`) and if no user is specified, the default users will be added (ea, ce, ae, a1, r1, r2). The passwords will be `Password1!` and can be changed.
- `yarn obfuscatedb` - Obfuscates sensitive data (usually used for a production replica, but without any real user data)
