import React from 'react'
import { Formik } from 'formik'
import { DragDropContext } from 'react-dnd'
import { theme } from '@hindawi/ui'
import { render } from '@testing-library/react'
import TestBackend from 'react-dnd-test-backend'
import { ThemeProvider } from 'styled-components'

export const renderWithDragAndFormik = (
  Component,
  { formProps, compProps },
) => {
  const C = DragDropContext(TestBackend)(Component)
  return render(
    <ThemeProvider theme={theme}>
      <Formik {...formProps}>{() => <C {...compProps} />}</Formik>
    </ThemeProvider>,
  )
}

export const withDragContext = Component =>
  DragDropContext(TestBackend)(Component)

export const renderWithDrag = ui => {
  const C = DragDropContext(TestBackend)(() => (
    <ThemeProvider theme={theme}>{ui}</ThemeProvider>
  ))
  return render(<C />)
}
