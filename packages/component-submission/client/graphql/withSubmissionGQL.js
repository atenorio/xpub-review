import { get } from 'lodash'
import { compose } from 'recompose'
import { graphql } from 'react-apollo'

import { queries as dashboardQueries } from 'component-dashboard/client/graphql'
import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getManuscriptAndActiveJournals, {
    options: props => ({
      variables: {
        manuscriptId: get(props, 'match.params.manuscriptId'),
      },
    }),
  }),
  graphql(mutations.createDraftManuscript, {
    name: 'createDraft',
    options: props => ({
      refetchQueries: [
        {
          query: dashboardQueries.getManuscripts,
        },
      ],
    }),
  }),
  graphql(mutations.updateDraftManuscript, {
    name: 'updateDraft',
    options: props => ({
      refetchQueries: [
        {
          query: dashboardQueries.getManuscripts,
        },
      ],
    }),
  }),
  graphql(mutations.addAuthorToManuscript, {
    name: 'addAuthorToManuscript',
  }),
  graphql(mutations.updateAutosave, {
    name: 'updateAutosave',
  }),
  graphql(mutations.removeAuthorFromManuscript, {
    name: 'removeAuthor',
  }),
  graphql(mutations.editAuthorFromManuscript, {
    name: 'editAuthor',
  }),
  graphql(mutations.addFileToManuscript, {
    name: 'addFileToManuscript',
  }),
  graphql(mutations.removeFileFromManuscript, {
    name: 'removeFileFromManuscript',
  }),
  graphql(mutations.getFileSignedUrl, { name: 'getFileSignedUrl' }),
  graphql(mutations.updateManuscriptFile, {
    name: 'updateManuscriptFile',
    options: props => ({
      refetchQueries: [
        {
          query: queries.getManuscriptAndActiveJournals,
          variables: {
            manuscriptId: get(props, 'match.params.manuscriptId'),
          },
        },
      ],
    }),
  }),
  graphql(mutations.submitManuscript, {
    name: 'submitManuscript',
    options: props => ({
      variables: {
        manuscriptId: get(props, 'match.params.manuscriptId'),
      },
      refetchQueries: [
        {
          query: dashboardQueries.getManuscripts,
        },
      ],
    }),
  }),
)
