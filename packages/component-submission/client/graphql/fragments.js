import gql from 'graphql-tag'
import { fragments as fileFragments } from 'component-files/client'

export const teamMember = gql`
  fragment teamMember on TeamMember {
    id
    isSubmitting
    isCorresponding
    invited
    responded
    reviewerNumber
    user {
      id
    }
    status
    alias {
      aff
      email
      country
      name {
        surname
        givenNames
      }
    }
  }
`
export const specialIssue = gql`
  fragment specialIssue on SpecialIssue {
    id
    name
    callForPapers
  }
`
export const draftManuscriptDetails = gql`
  fragment draftManuscriptDetails on Manuscript {
    id
    journalId
    submissionId
    meta {
      abstract
      title
      agreeTc
      articleTypeId
      dataAvailability
      fundingStatement
      conflictOfInterest
    }
    status
    authors {
      ...teamMember
    }
    files {
      ...fileDetails
    }
    specialIssue {
      ...specialIssue
    }
    section {
      id
      name
    }
  }
  ${specialIssue}
  ${teamMember}
  ${fileFragments.fileDetails}
`

export const activeJournals = gql`
  fragment activeJournals on Journal {
    id
    name
    code
    apc
    articleTypes {
      id
      name
    }
    specialIssues {
      ...specialIssue
    }
    sections {
      id
      name
      specialIssues {
        ...specialIssue
      }
    }
  }
  ${specialIssue}
`
