import React, { Fragment } from 'react'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { Row, Text, Label, Icon } from '@hindawi/ui'

import { WizardFiles } from './'

const WizardStepFour = ({
  formValues,
  wizardErrors,
  onUploadFile,
  onDeleteFile,
  onChangeList,
}) => (
  <Fragment>
    <H2>4. Manuscript Files Upload</H2>
    <Row>
      <Text
        align="center"
        display="inline-block"
        mb={1 / 2}
        mt={1 / 2}
        secondary
      >
        Drag & Drop files in the specific section or click{' '}
        <Icon
          bold
          color="colorSecondary"
          fontSize="10px"
          icon="expand"
          mr={1}
        />
        to upload.
      </Text>
    </Row>

    <Row mb={4}>
      <Text secondary>
        Use the{' '}
        <Icon color="colorSecondary" fontSize="16px" icon="move" mr={1} />
        icon to reorder or move files to a different type.
      </Text>
    </Row>

    <Row justify="flex-start">
      <Label mb={2} mt={2}>
        Files
      </Label>
    </Row>

    <WizardFiles
      files={get(formValues, 'files', [])}
      onChangeList={onChangeList}
      onDeleteFile={onDeleteFile}
      onUploadFile={onUploadFile}
    />

    {get(wizardErrors, 'fileError', false) && (
      <Row justify="flex-start" mt={2}>
        <Text error>{get(wizardErrors, 'fileError', '')}</Text>
      </Row>
    )}
  </Fragment>
)

export default WizardStepFour
