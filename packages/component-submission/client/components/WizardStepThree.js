import React, { Fragment } from 'react'
import { compose, withHandlers, withProps } from 'recompose'
import { camelCase, get } from 'lodash'
import {
  ActionLink,
  Row,
  Text,
  Menu,
  Item,
  Label,
  Textarea,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'
import { H2, TextField, YesOrNo } from '@pubsweet/ui'

import { WizardAuthors } from './'
import withVisibleDeclarations from '../withVisibleDeclarations'

const WizardStepThree = ({
  journal,
  questions,
  formValues,
  isAuthorEdit,
  declarations,
  setFieldValue,
  articleTypes = [],
  getTooltipContent,
  setWizardEditMode,
  isConflictsVisible,
  resetQuestionValues,
  isDataAvailabilityVisible,
  isFundingStatementVisible,
  isDataAvailabilityRequired,
  ...rest
}) => (
  <Fragment>
    <Row alignItems="center">
      <H2>3. Manuscript & Author Details</H2>
    </Row>
    <Row flexDirection="column" mb={6}>
      <Text align="center" mb={2} mt={2} secondary>
        Please provide the details of all the authors of this manuscript, in the
        order that they appear on the manuscript.
      </Text>
      <Text align="center" secondary>
        Your details have been prefilled as the submitting author.
      </Text>
    </Row>

    <Row mb={2}>
      <Item data-test-id="submission-title" flex={3} mr={2} vertical>
        <Label required>Manuscript Title</Label>
        <ValidatedFormField
          component={TextField}
          inline
          name="meta.title"
          validate={[validators.required]}
        />
      </Item>
      <Item data-test-id="submission-type" vertical>
        <Label required>Manuscript Type</Label>
        <ValidatedFormField
          component={Menu}
          name="meta.articleTypeId"
          onChange={resetQuestionValues}
          options={articleTypes}
          placeholder="Please select"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row mb={4}>
      <Item data-test-id="submission-abstract" vertical>
        <Label required>Abstract</Label>
        <ValidatedFormField
          component={Textarea}
          minHeight={30}
          name="meta.abstract"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <WizardAuthors
      formValues={formValues}
      journal={journal}
      setWizardEditMode={setWizardEditMode}
      {...rest}
    />
    {formValues.meta.articleTypeId && (
      <Fragment>
        {isConflictsVisible && (
          <Row mt={6}>
            <Item vertical>
              <Label mb={2} required={isConflictsVisible}>
                {questions.conflictOfInterest.title}
              </Label>
              <Text display="inline" mb={2} secondary>
                {questions.conflictOfInterest.subtitle}{' '}
                <ActionLink
                  display="inline"
                  secondary
                  to="https://www.hindawi.com/ethics/#coi"
                >
                  here.
                </ActionLink>
              </Text>
              <ValidatedFormField
                component={YesOrNo}
                name="meta.hasConflictOfInterest"
                onChange={() =>
                  setFieldValue('meta', {
                    ...formValues.meta,
                    conflictOfInterest: '',
                  })
                }
                validate={[validators.required]}
              />
              {get(formValues, 'meta.hasConflictOfInterest') === 'yes' && (
                <ValidatedFormField
                  component={Textarea}
                  name="meta.conflictOfInterest"
                  placeholder={questions.conflictOfInterest.placeholder}
                  validate={[validators.required]}
                />
              )}
            </Item>
          </Row>
        )}

        {isDataAvailabilityVisible && (
          <Row>
            <Item vertical>
              <Label mb={2} required={isDataAvailabilityRequired}>
                {questions.dataAvailability.title}
              </Label>
              <Text display="inline" mb={2} secondary>
                {questions.dataAvailability.subtitle}{' '}
                <ActionLink
                  display="inline"
                  secondary
                  to="https://www.hindawi.com/research.data/#statement"
                >
                  here.
                </ActionLink>
              </Text>

              <ValidatedFormField
                component={Textarea}
                minHeight={18}
                name="meta.dataAvailability"
                placeholder={questions.dataAvailability.placeholder}
                validate={
                  isDataAvailabilityRequired ? [validators.required] : []
                }
              />
            </Item>
          </Row>
        )}

        {isFundingStatementVisible && (
          <Row>
            <Item vertical>
              <Label mb={2} required>
                {questions.fundingStatement.title}
              </Label>
              <Text mb={2} secondary>
                {questions.fundingStatement.subtitle}
              </Text>
              <ValidatedFormField
                component={Textarea}
                minHeight={18}
                name="meta.fundingStatement"
                placeholder={questions.fundingStatement.placeholder}
                validate={[validators.required]}
              />
            </Item>
          </Row>
        )}
      </Fragment>
    )}
  </Fragment>
)

export default compose(
  withVisibleDeclarations,
  withProps(({ journal, formValues }) => {
    const isSpecialIssue = formValues.issueType === 'specialIssue'
    const articleTypes = isSpecialIssue
      ? journal.articleTypes
          .map(articleType => ({
            label: articleType.name,
            value: articleType.id,
          }))
          .filter(articleType =>
            ['Editorial', 'Research Article', 'Review Article'].includes(
              articleType.label,
            ),
          )
      : journal.articleTypes.map(articleType => ({
          label: articleType.name,
          value: articleType.id,
        }))

    return { articleTypes }
  }),
  withHandlers({
    getArticleTypeName: ({ articleTypes }) => id => {
      const articleLabel = articleTypes.find(at => at.value === id).label
      return camelCase(articleLabel)
    },
    setWizardEditMode: ({ setFieldValue }) => value => {
      setFieldValue('isEditing', value)
    },
    resetQuestionValues: ({ setFieldValue, formValues }) => () => {
      setFieldValue('meta', {
        ...formValues.meta,
        conflictOfInterest: '',
        dataAvailability: '',
        fundingStatement: '',
      })
    },
  }),
  withProps(({ declarations, getArticleTypeName, formValues }) => {
    if (formValues.meta.articleTypeId) {
      const articleTypeName = getArticleTypeName(formValues.meta.articleTypeId)
      return {
        isConflictsVisible:
          declarations[articleTypeName].conflictOfInterest.isVisible,
        isDataAvailabilityVisible:
          declarations[articleTypeName].dataAvailability.isVisible,
        isFundingStatementVisible:
          declarations[articleTypeName].fundingStatement.isVisible,
        isDataAvailabilityRequired:
          declarations[articleTypeName].dataAvailability.isRequired,
      }
    }
    return {}
  }),
)(WizardStepThree)
