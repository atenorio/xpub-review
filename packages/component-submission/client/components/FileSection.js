import React from 'react'
import { isNumber } from 'lodash'
import { FieldArray } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { FilePicker, Spinner } from '@pubsweet/ui'
import {
  File,
  withNativeFileDrop,
  withFileSectionDrop,
  getSupportedFileFormats,
} from 'component-files/client'
import { compose, setDisplayName, withHandlers, withProps } from 'recompose'
import {
  Row,
  Text,
  Item,
  Icon,
  Label,
  ActionLink,
  DragHandle,
  SortableList,
  withFetching,
  radiusHelpers,
} from '@hindawi/ui'

export const Files = ({
  files,
  title,
  remove,
  isLast,
  isFirst,
  compact,
  required,
  listName,
  onDelete,
  onUpload,
  maxFiles,
  moveFile,
  onPreview,
  isFetching,
  isFileOver,
  fetchingError,
  isFileItemOver,
  connectFileDrop,
  canDropFileItem,
  supportedFormats,
  connectDropTarget,
  allowedFileExtensions,
}) => (
  <Root
    compact={compact}
    data-test-id={`add-file-${listName}`}
    isFileItemOver={isFileOver || (isFileItemOver && canDropFileItem)}
    isFirst={isFirst}
    isLast={isLast}
    ref={instance => {
      connectFileDrop(instance)
      connectDropTarget(instance)
    }}
  >
    <Row alignItems="center" mb={2}>
      <Item alignItems="center">
        <Label required={required}>{title}</Label>
        {isFetching ? (
          <Spinner size={4} />
        ) : (
          <FilePicker
            allowedFileExtensions={allowedFileExtensions}
            disabled={files.length >= maxFiles}
            onUpload={onUpload}
          >
            <ActionLink
              disabled={files.length >= maxFiles}
              fontSize="12px"
              fontWeight="bold"
              size="small"
            >
              <Icon
                bold
                color="colorWarning"
                fontSize="10px"
                icon="expand"
                ml={2}
                mr={2}
                mt={2}
              />
              UPLOAD FILE
            </ActionLink>
          </FilePicker>
        )}
      </Item>
      {supportedFormats && (
        <Item justify="flex-end">
          <Text fontSize="13px" mt={2} secondary>
            Supported file formats: {supportedFormats}
          </Text>
        </Item>
      )}
    </Row>

    <SortableList
      beginDragProps={[
        'id',
        'index',
        'mimeType',
        'originalName',
        'listName',
        'remove',
      ]}
      dragHandle={DragHandle}
      hasDelete
      items={files}
      listItem={File}
      listName={listName}
      mb={2}
      moveItem={moveFile}
      onDelete={onDelete}
      onPreview={onPreview}
      remove={remove}
      shadow
    />

    {fetchingError && (
      <Row mb={4}>
        <Text error>{fetchingError}</Text>
      </Row>
    )}
  </Root>
)

export const EnhancedFiles = compose(
  withFetching,
  setDisplayName('FileSection'),
  withProps(({ allowedFileExtensions = [], name, values }) => ({
    supportedFormats: getSupportedFileFormats(allowedFileExtensions),
  })),
  withHandlers({
    moveFile: ({ move }) => (startIndex, endIndex) => {
      if (isNumber(startIndex) && isNumber(endIndex)) {
        move(startIndex, endIndex)
      }
    },
    onDelete: ({
      remove,
      setError,
      clearError,
      setFetching,
      onDeleteFile,
      files = [],
    }) => file => {
      clearError()
      onDeleteFile(file, {
        remove,
        setError,
        setFetching,
        index: files.findIndex(f => f.id === file.id),
      })
    },
    onUpload: ({
      push,
      listName,
      setError,
      clearError,
      setFetching,
      onUploadFile,
    }) => file => {
      clearError()
      if (typeof onUploadFile === 'function') {
        onUploadFile(file, { type: listName, push, setError, setFetching })
      }
    },
  }),
  withProps(({ onUpload }) => ({ onNativeFileDrop: onUpload })),
  withNativeFileDrop,
  withFileSectionDrop,
)(Files)

const FileSection = ({ listName, ...rest }) => (
  <FieldArray name={`files.${listName}`}>
    {({ push, remove, move }) => (
      <EnhancedFiles
        listName={listName}
        move={move}
        push={push}
        remove={remove}
        {...rest}
      />
    )}
  </FieldArray>
)

export default FileSection

// #region styles
const Root = styled.div`
  background: ${props =>
    props.isFileItemOver ? th('colorFurniture') : th('colorBackground')};
  min-height: calc(${th('gridUnit')} * ${({ compact }) => (compact ? 30 : 40)});
  padding: 0 calc(${th('gridUnit')} * 2);
  width: 100%;

  ${radiusHelpers};
  border-bottom: ${props => (!props.isLast ? '1px dashed #dbdbdb' : 'none')};
`
// #endregion
