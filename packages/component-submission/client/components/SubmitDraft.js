import React from 'react'
import { Button } from '@pubsweet/ui'
import { chain } from 'lodash'
import { Mutation } from 'react-apollo'
import { withRouter } from 'react-router-dom'
import { compose, withHandlers, withProps } from 'recompose'

import { mutations } from '../graphql'

const SubmitDraft = ({ label = 'Submit', createDraft, isConfirmed }) => (
  <Mutation
    mutation={mutations.createDraftManuscript}
    variables={{ input: { journalId: null } }}
  >
    {mutateFn => (
      <Button
        data-test-id="manuscript-submit"
        disabled={!isConfirmed}
        mr={4}
        onClick={createDraft(mutateFn)}
        primary
        xs
      >
        {label}
      </Button>
    )}
  </Mutation>
)

export default compose(
  withRouter,
  withHandlers({
    createDraft: ({ history }) => createDraft => () => {
      createDraft().then(({ data: { createDraftManuscript } }) => {
        history.push(
          `/submit/${createDraftManuscript.submissionId}/${createDraftManuscript.id}`,
        )
      })
    },
  }),
  withProps(({ currentUser }) => ({
    isConfirmed: chain(currentUser)
      .get('identities', [])
      .find(i => i.__typename === 'Local')
      .get('isConfirmed', '')
      .value(),
  })),
)(SubmitDraft)
