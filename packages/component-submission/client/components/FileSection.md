File section used in the submission wizard.

```js
const { Formik } = require('formik')

;<Formik
  initialValues={{
    files: {
      testFiles: [
        { id: 'file1', name: 'file1.pdf', size: 123 },
        { id: 'file2', name: 'file2.pdf', size: 45677 },
      ],
    },
  }}
>
  {({ values }) => (
    <Files
      listName="testFiles"
      onDeleteFile={(file, props) => console.log('delete the file', file)}
      files={values.files['testFiles']}
    />
  )}
</Formik>
```
