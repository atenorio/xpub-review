import React, { Fragment } from 'react'
import { FieldArray } from 'formik'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { isNumber, get, isString } from 'lodash'
import { compose, setDisplayName, withHandlers } from 'recompose'
import {
  Row,
  Item,
  Text,
  Label,
  ActionLink,
  AuthorCard,
  DragHandle,
  SortableList,
  Icon,
} from '@hindawi/ui'

export const WizardAuthors = ({
  onEdit,
  addAuthor,
  moveAuthor,
  formValues,
  journalCode,
  onSaveAuthor,
  wizardErrors,
  onCancelEdit,
  onDeleteAuthor,
}) => (
  <FieldArray name="authors">
    {({ form, move, push, remove, replace, unshift }) => {
      const authors = get(form, 'values.authors', [])
      return (
        <Fragment>
          <Row alignItems="center" justify="flex-start" mb={2}>
            <Item alignItems="center">
              <Label mr={2}>Authors</Label>
              <ActionLink
                data-test-id="submission-add-author"
                disabled={get(formValues, 'isEditing', false)}
                fontSize="12px"
                fontWeight="bold"
                onClick={addAuthor(unshift)}
              >
                <Icon bold fontSize="10px" icon="expand" mr={2} />
                ADD AUTHOR
              </ActionLink>
            </Item>
            <Item justify="flex-end">
              <ActionLink
                iconPosition="right"
                to={`https://www.hindawi.com/journals/${journalCode}/guidelines/`}
              >
                Journal Author Submission Guidelines
                <Icon icon="link" ml={2} />
              </ActionLink>
            </Item>
          </Row>
          {authors.length > 0 && (
            <Root data-test-id="author-list">
              <SortableList
                authorsLength={authors.length}
                dragHandle={DragHandle}
                isEditingForm={get(form, 'values.isEditing', false)}
                items={authors}
                listItem={AuthorCard}
                moveItem={moveAuthor(move)}
                onCancelEdit={onCancelEdit(authors, remove)}
                onDeleteAuthor={onDeleteAuthor({ remove })}
                onEdit={onEdit}
                onSaveAuthor={onSaveAuthor({ push, remove, replace })}
              />
              {get(wizardErrors, 'isEditing', false) && (
                <Row justify="flex-start">
                  <Text error>You have an unsaved author.</Text>
                </Row>
              )}
            </Root>
          )}

          {get(wizardErrors, 'authors', false) &&
            isString(wizardErrors.authors) && (
              <Row justify="flex-start" mb={4}>
                <Text error>{wizardErrors.authors}</Text>
              </Row>
            )}
        </Fragment>
      )
    }}
  </FieldArray>
)

export default compose(
  setDisplayName('WizardAuthors'),
  withHandlers({
    addAuthor: ({ setWizardEditMode }) => arrayAdderFn => () => {
      setWizardEditMode(true)
      arrayAdderFn({
        id: 'unsaved-author',
        email: '',
        country: '',
        lastName: '',
        firstName: '',
        affiliation: '',
      })
    },
    moveAuthor: () => formMoveFn => (startIndex, endIndex) => {
      if (isNumber(startIndex) && isNumber(endIndex)) {
        formMoveFn(startIndex, endIndex)
      }
    },
    onCancelEdit: ({ setWizardEditMode }) => (
      authors,
      arrayRemoverFn,
    ) => authorIndex => {
      if (authors[authorIndex].id === 'unsaved-author') {
        arrayRemoverFn(authorIndex)
      }
      setWizardEditMode(false)
    },
    onEdit: ({ setWizardEditMode }) => () => {
      setWizardEditMode(true)
    },
    onDeleteAuthor: ({ onDeleteAuthor, setWizardEditMode }) => formFns => (
      author,
      props,
    ) => {
      onDeleteAuthor(author, {
        ...props,
        formFns,
        setWizardEditMode,
      })
    },
    onSaveAuthor: ({
      onSaveAuthor,
      onEditAuthor,
      setWizardEditMode,
    }) => formFns => (author, { setEditMode, ...props }) => {
      const setEdit = () => {
        setWizardEditMode(false)
        setEditMode(false)
      }
      if (author.id === 'unsaved-author') {
        onSaveAuthor(author, {
          ...props,
          formFns,
          setWizardEditMode: setEdit,
        })
      } else {
        onEditAuthor(author, {
          ...props,
          formFns,
          setWizardEditMode: setEdit,
        })
      }
    },
  }),
)(WizardAuthors)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackground')};
  border-radius: ${th('gridUnit')};
  margin-bottom: calc(${th('gridUnit')} * 4);
  padding: calc(${th('gridUnit')} * 2);
`
// #endregion
