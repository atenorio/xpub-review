The submission wizard buttons.

```js
<WizardButtons
  prevStep={() => console.log('Go to prev step or back')}
  handleSubmit={() => console.log('Go to the next step or submit the draft')}
/>
```

The wizard buttons on the last submission step.

```js
<WizardButtons
  isLast
  prevStep={() => console.log('Go to prev step or back')}
  handleSubmit={() => console.log('Go to the next step or submit the draft')}
/>
```

The submission wizard buttons while request is pending.

```js
<WizardButtons
  isFetching
  prevStep={() => console.log('Go to prev step or back')}
  handleSubmit={() => console.log('Go to the next step or submit the draft')}
/>
```
