const models = require('@pubsweet/models')
const { sendPackage } = require('component-mts-package')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')

const useCases = require('./use-cases')
const notificationService = require('../notifications/notification')
const events = require('component-events')

const resolvers = {
  Mutation: {
    async createDraftManuscript(_, { input }, ctx) {
      return useCases.createDraftManuscriptUseCase
        .initialize(models)
        .execute({ input, userId: ctx.user })
    },
    async updateDraftManuscript(_, { manuscriptId, autosaveInput }, ctx) {
      return useCases.updateDraftManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, autosaveInput })
    },
    async addAuthorToManuscript(_, { manuscriptId, authorInput }, ctx) {
      const sso = process.env.KEYCLOAK_SERVER_URL
        ? require('component-sso')
        : null
      return useCases.addAuthorToManuscriptUseCase
        .initialize({ models, notificationService, logEvent, sso })
        .execute({ manuscriptId, authorInput, userId: ctx.user })
    },
    async removeAuthorFromManuscript(
      _,
      { manuscriptId, authorTeamMemberId },
      ctx,
    ) {
      return useCases.removeAuthorFromManuscriptUseCase
        .initialize({ models, logEvent })
        .execute({ manuscriptId, authorTeamMemberId, userId: ctx.user })
    },
    async editAuthorFromManuscript(_, params, ctx) {
      return useCases.editAuthorFromManuscriptUseCase
        .initialize({ models, logEvent })
        .execute({ params, userId: ctx.user })
    },
    async updateManuscriptFile(_, params, ctx) {
      return useCases.updateManuscriptFileUseCase
        .initialize(models)
        .execute(params)
    },
    async submitManuscript(_, { manuscriptId }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.submitManuscriptUseCase
        .initialize({
          models,
          logEvent,
          sendPackage,
          eventsService,
          notificationService,
        })
        .execute({ manuscriptId, userId: ctx.user })
    },
  },
  Query: {
    async getActiveJournals(_, { input }, ctx) {
      return useCases.getActiveJournalsUseCase
        .initialize(models)
        .execute({ userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
