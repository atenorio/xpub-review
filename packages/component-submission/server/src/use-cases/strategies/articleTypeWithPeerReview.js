const logger = require('@pubsweet/logger')
const { get } = require('lodash')

module.exports = {
  execute: async ({
    Team,
    journal,
    manuscript,
    authorTeam,
    sendPackage,
    eventsService,
    editorialAssistant,
    notificationService,
  }) => {
    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members.jobs',
    })
    const admin = adminTeam.members[0]
    await manuscript.submitManuscript()

    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} before saving`,
    )

    if (!manuscript.technicalCheckToken) {
      throw new ConflictError('Something went wrong. Please try again.')
    }

    await manuscript.save()

    logger.info(
      `Manuscript ${manuscript.id} has technicalCheckToken ${manuscript.technicalCheckToken} after saving`,
    )
    await sendPackage({ manuscript })

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    const submittingAuthor = authorTeam.members.find(
      author => author.isSubmitting,
    )
    const confirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => get(author, 'user.identities.0.isConfirmed'))

    const unconfirmedCoAuthors = authorTeam.members
      .filter(author => !author.isSubmitting)
      .filter(author => !get(author, 'user.identities.0.isConfirmed'))

    confirmedCoAuthors.forEach(author =>
      notificationService.sendToConfirmedAuthors(author, {
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
      }),
    )

    unconfirmedCoAuthors.forEach(author =>
      notificationService.sendToUnconfirmedAuthors(author, {
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
      }),
    )
    notificationService.sendEQSEmail({
      admin,
      journal,
      manuscript,
      editorialAssistant,
    })
  },
}
