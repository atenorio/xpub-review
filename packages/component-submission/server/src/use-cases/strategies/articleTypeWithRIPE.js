module.exports = {
  execute: async ({
    Team,
    journal,
    manuscript,
    Manuscript,
    TeamMember,
    triageEditor,
    eventsService,
    submittingAuthor,
    editorialAssistant,
    notificationService,
  }) => {
    manuscript.updateProperties({
      status: Manuscript.Statuses.makeDecision,
    })

    await manuscript.save()

    const RIPEGlobalTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.researchIntegrityPublishingEditor,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
      },
      eagerLoadRelations: 'members.user.identities',
    })
    const RIPEUser = RIPEGlobalTeam && RIPEGlobalTeam.members[0].user

    const RIPETeam = new Team({
      role: Team.Role.researchIntegrityPublishingEditor,
      manuscriptId: manuscript.id,
    })
    await RIPETeam.save()
    const RIPETeamMember = RIPETeam.addMember(RIPEUser, {
      status: TeamMember.Statuses.accepted,
    })
    await RIPETeamMember.save()

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionSubmitted',
    })

    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
      editor: RIPETeamMember,
    })
    notificationService.notifyEditorsWhenNewManuscriptSubmitted({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
      editor: triageEditor,
    })
  },
}
