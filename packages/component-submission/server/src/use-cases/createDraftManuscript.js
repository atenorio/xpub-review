const generateCustomId = () =>
  Date.now()
    .toString()
    .slice(-7)

const initialize = ({ Manuscript, Team, TeamMember, User }) => ({
  async execute({ input: { journalId, customId }, userId }) {
    const author = await User.find(userId, '[identities, teamMemberships.team]')

    const hasStaffRole = author.teamMemberships.some(t =>
      Team.StaffRoles.includes(t.team.role),
    )

    const manuscript = new Manuscript({
      journalId,
      customId: customId || generateCustomId(),
    })
    await manuscript.save()
    if (!hasStaffRole) {
      const authorTeam = new Team({
        role: Team.Role.author,
        manuscriptId: manuscript.id,
      })
      manuscript.assignTeam(authorTeam)
      await authorTeam.save()

      const authorTeamMember = authorTeam.addMember(author, {
        isSubmitting: true,
        isCorresponding: true,
      })
      authorTeamMember.userId = author.id
      authorTeamMember.teamId = authorTeam.id
      await authorTeamMember.save()
    }

    await manuscript.save()
    return manuscript.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
