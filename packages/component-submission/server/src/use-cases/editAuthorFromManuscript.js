const { findIndex, sortBy } = require('lodash')

const initialize = ({ models: { Manuscript, Team }, logEvent }) => ({
  execute: async ({
    params: {
      manuscriptId,
      authorTeamMemberId,
      authorInput: { isCorresponding, ...alias },
    },
    userId,
  }) => {
    const authorTeam = await Team.findOneBy({
      queryObject: { manuscriptId, role: Team.Role.author },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const authorIndex = findIndex(authorTeam.members, [
      'id',
      authorTeamMemberId,
    ])

    if (isCorresponding !== undefined) {
      authorTeam.members.forEach(member => (member.isCorresponding = false))
      if (isCorresponding === true) {
        authorTeam.members[authorIndex].isCorresponding = true
      } else {
        const submittingAuthorIndex = findIndex(authorTeam.members, [
          'isSubmitting',
          true,
        ])
        authorTeam.members[submittingAuthorIndex].isCorresponding = true
      }
    }

    authorTeam.members[authorIndex].updateProperties({
      alias,
    })
    await authorTeam.saveRecursively()
    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId,
        action: logEvent.actions.author_edited,
        objectType: logEvent.objectType.user,
        objectId: authorTeam.members[0].user.id,
      })
    }

    return sortBy(authorTeam.members, 'position').map(a => a.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
