const { includes } = require('lodash')
const articleTypeWithPeerReview = require('./strategies/articleTypeWithPeerReview')
const articleTypeWithRIPE = require('./strategies/articleTypeWithRIPE')

const initialize = ({
  logEvent,
  sendPackage,
  eventsService,
  notificationService,
  models: { Journal, Manuscript, Team, TeamMember, ArticleType },
}) => ({
  execute: async ({ manuscriptId, userId }) => {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[articleType, files, teams.members, journal, section, specialIssue.section]',
    )

    const journal = await Journal.find(manuscript.journalId, 'teams.members')
    const editorialAssistant = journal.getCorrespondingEditorialAssistant()
    const triageEditor = journal.getTriageEditor()

    const { articleType: currentArticleType } = manuscript

    const authorTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId,
        role: Team.Role.author,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const submittingAuthor = authorTeam.members.find(
      author => author.isSubmitting,
    )

    const strategy = includes(
      ArticleType.TypesWithRIPE,
      currentArticleType.name,
    )
      ? articleTypeWithRIPE
      : articleTypeWithPeerReview

    await strategy.execute({
      Team,
      journal,
      manuscript,
      TeamMember,
      Manuscript,
      authorTeam,
      sendPackage,
      triageEditor,
      eventsService,
      submittingAuthor,
      editorialAssistant,
      notificationService,
    })

    notificationService.sendSubmittingAuthorConfirmation({
      journal,
      manuscript,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.manuscript_submitted,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
