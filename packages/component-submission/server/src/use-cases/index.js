const submitManuscriptUseCase = require('./submitManuscript')
const updateManuscriptFileUseCase = require('./updateManuscriptFile')
const addAuthorToManuscriptUseCase = require('./addAuthorToManuscript')
const createDraftManuscriptUseCase = require('./createDraftManuscript')
const updateDraftManuscriptUseCase = require('./updateDraftManuscript')
const editAuthorFromManuscriptUseCase = require('./editAuthorFromManuscript')
const removeAuthorFromManuscriptUseCase = require('./removeAuthorFromManuscript')
const getActiveJournalsUseCase = require('./getActiveJournals')

module.exports = {
  submitManuscriptUseCase,
  updateManuscriptFileUseCase,
  createDraftManuscriptUseCase,
  updateDraftManuscriptUseCase,
  addAuthorToManuscriptUseCase,
  editAuthorFromManuscriptUseCase,
  removeAuthorFromManuscriptUseCase,
  getActiveJournalsUseCase,
}
