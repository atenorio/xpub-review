const { isEqual, sortBy } = require('lodash')

const initialize = ({ Section, Manuscript, Team, SpecialIssue }) => ({
  execute: async ({ manuscriptId, autosaveInput }) => {
    const manuscript = await Manuscript.find(manuscriptId, [
      'files',
      'teams.[members.[user.[identities]]]',
    ])

    if (
      manuscript.status === Manuscript.Statuses.accepted ||
      manuscript.status === Manuscript.Statuses.rejected
    ) {
      throw new AuthorizationError('Operation not permitted')
    }

    const options = {
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    }

    let authorTeam = {}
    if (autosaveInput.authors.length > 0) {
      authorTeam = await Team.findOrCreate({
        queryObject: options,
        options,
        eagerLoadRelations: 'members',
      })
    }

    const existingAuthorsEmails = sortBy(authorTeam.members, 'position').map(
      member => member.alias.email,
    )

    const inputAuthorsEmails = autosaveInput.authors.map(author => author.email)

    if (!isEqual(existingAuthorsEmails, inputAuthorsEmails)) {
      await Promise.all(
        inputAuthorsEmails.map(async (email, index) => {
          authorTeam.members = authorTeam.members || []
          const teamMember = authorTeam.members.find(
            member => member.alias.email === email,
          )
          if (!teamMember) return

          teamMember.updateProperties({ position: index })
          await teamMember.save()
        }),
      )
    }

    if (manuscript.files.length > 1) {
      const existingFilesByType = sortBy(manuscript.files, [
        'type',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))
      const inputFilesByType = sortBy(autosaveInput.files, [
        'fileType',
        'position',
      ]).map(file => ({ id: file.id, type: file.type }))

      if (!isEqual(existingFilesByType, inputFilesByType)) {
        const parsedFiles = inputFilesByType.reduce(
          (acc, current) => ({
            ...acc,
            [current.type]: [...acc[current.type], { id: current.id }],
          }),
          {
            manuscript: [],
            supplementary: [],
            coverLetter: [],
            figure: [],
          },
        )
        await Promise.all(
          Object.values(parsedFiles).map(files =>
            files.map(async (file, index) => {
              const matchingFile = manuscript.files.find(
                mFile => mFile.id === file.id,
              )
              matchingFile.updateProperties({ position: index })
              await matchingFile.save()
            }),
          ),
        )
      }
    }

    const { journalId, sectionId, specialIssueId } = autosaveInput

    manuscript.updateProperties({
      ...autosaveInput.meta,
      journalId,
      sectionId,
      specialIssueId,
    })
    await manuscript.save()

    if (sectionId) {
      manuscript.section = await Section.findOneBy({
        queryObject: { id: sectionId },
      })
    }
    if (specialIssueId) {
      manuscript.specialIssue = await SpecialIssue.findOneBy({
        queryObject: { id: specialIssueId },
      })
    }

    return manuscript.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
