const { get } = require('lodash')

const initialize = ({ Journal, Team, User, ArticleType }) => ({
  execute: async ({ userId }) => {
    const journals = await Journal.findAll({
      queryObject: { isActive: true },
      orderByField: 'name',
      order: 'asc',
      eagerLoadRelations: [
        'specialIssues',
        'sections.specialIssues',
        'journalArticleTypes.articleType',
      ],
    })

    let filteredJournals
    const user = await User.find(userId, 'teamMemberships.team')
    const staffTeamMember =
      user.getTeamMemberByRole(Team.Role.admin) ||
      user.getTeamMemberByRole(Team.Role.researchIntegrityPublishingEditor) ||
      user.getTeamMemberByRole(Team.Role.editorialAssistant)

    if (staffTeamMember) {
      filteredJournals = journals
    } else {
      filteredJournals = journals.map(journal => {
        const teamMember = user.getTeamMemberForJournal(journal)
        const role = get(teamMember, 'team.role')

        if ([...Team.JournalRoles].includes(role)) {
          journal.journalArticleTypes = journal.journalArticleTypes.filter(
            journalArticleType => {
              const articleTypeName = journalArticleType.articleType.name
              return !ArticleType.TypesWithRIPE.includes(articleTypeName)
            },
          )

          return journal
        }

        journal.journalArticleTypes = journal.journalArticleTypes.filter(
          journalArticleType => {
            const articleTypeName = journalArticleType.articleType.name
            return (
              !ArticleType.EditorialTypes.includes(articleTypeName) &&
              !ArticleType.TypesWithRIPE.includes(articleTypeName)
            )
          },
        )
        return journal
      })
    }

    filteredJournals = filteredJournals.map(journal => {
      if (journal.specialIssues.length > 0) {
        journal.specialIssues = journal.specialIssues.filter(
          si => si.isActive && !si.isCancelled,
        )
      }
      if (journal.sections.length > 0) {
        journal.sections = journal.sections.map(section => {
          if (section.specialIssues.length === 0) return section

          section.specialIssues = section.specialIssues.filter(
            si => si.isActive,
          )
          return section
        })
      }

      return journal
    })

    return filteredJournals.map(journal => journal.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
