const { getActiveJournalsUseCase } = require('../src/use-cases')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const Chance = require('chance')

const chance = new Chance()

describe('Get active journals use case', () => {
  const { Journal, Team, SpecialIssue } = models
  const journals = [
    {
      name: chance.company(),
      activationDate: Date.now() + 3454,
      isActive: false,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
    {
      name: chance.company(),
      activationDate: new Date().toISOString(),
      isActive: true,
    },
  ]
  journals.map(journal =>
    fixtures.generateJournal({ properties: journal, Journal }),
  )
  const { userId } = dataService.createGlobalUser({
    models,
    fixtures,
    role: Team.Role.admin,
  })

  it('returns an array', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(models)
      .execute({ userId })
    expect(Array.isArray(res)).toBeTruthy()
  })

  it('returns the correct number of active journals', async () => {
    const res = await getActiveJournalsUseCase
      .initialize(models)
      .execute({ userId })
    expect(res.length).toEqual(2)
  })

  it('only returns active Special Issues', async () => {
    const journal = fixtures.generateJournal({
      properties: {
        isActive: true,
        name: chance.company(),
        activationDate: new Date().toISOString(),
      },
      Journal,
    })
    const journalId = journal.id
    journal.specialIssues = [
      await fixtures.generateSpecialIssue({
        properties: {
          journalId,
          isActive: false,
          startDate: new Date(),
          name: chance.company(),
          callForPapers: chance.paragraph(),
          endDate: new Date(chance.date({ year: 2069, string: true })),
        },
        SpecialIssue,
      }),
      await fixtures.generateSpecialIssue({
        properties: {
          journalId,
          isActive: true,
          startDate: new Date(),
          name: chance.company(),
          callForPapers: chance.paragraph(),
          endDate: new Date(chance.date({ year: 2069, string: true })),
        },
        SpecialIssue,
      }),
    ]

    const journals = await getActiveJournalsUseCase
      .initialize(models)
      .execute({ userId })

    const journalWithSpecialIssues = journals.find(
      j => j.specialIssues.length > 0,
    )
    expect(journalWithSpecialIssues.specialIssues).toHaveLength(1)
  })
})
