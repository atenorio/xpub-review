process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { updateManuscriptFileUseCase } = require('../src/use-cases')

const chance = new Chance()
describe('Edit manuscript file', () => {
  it('edits the file type and position within the manuscript', async () => {
    const { Manuscript, File } = models
    const manuscript = fixtures.generateManuscript({ Manuscript })
    const fileName = `${chance.word()}.pdf`
    const manuscriptFile = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'manuscript',
        position: 0,
        fileName,
      },
      File,
    })

    const manuscriptFile2 = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'manuscript',
        position: 1,
      },
      File,
    })

    const supplementaryFile = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'supplementary',
        position: 0,
      },
      File,
    })

    manuscript.files.push(manuscriptFile, manuscriptFile2, supplementaryFile)

    await updateManuscriptFileUseCase
      .initialize(models)
      .execute({ fileId: manuscriptFile.id, type: 'supplementary' })

    expect(
      manuscript.files.filter(file => file.type === 'supplementary'),
    ).toHaveLength(2)

    expect(
      manuscript.files.find(file => file.type === 'manuscript').position,
    ).toEqual(0)

    expect(
      manuscript.files.find(file => file.fileName === fileName).position,
    ).toEqual(1)
  })
})
