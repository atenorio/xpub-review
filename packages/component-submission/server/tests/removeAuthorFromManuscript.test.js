process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { removeAuthorFromManuscriptUseCase } = require('../src/use-cases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = { author_removed: 'author_removed' }

describe('Remove Author from Manuscript Use Case', () => {
  it('should remove author from the team', async () => {
    const { Manuscript, Team } = models
    const manuscript = await fixtures.generateManuscript({
      properties: { version: 1, status: 'draft', publicationDates: [] },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const addedAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    await removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: addedAuthor.id,
      })

    expect(manuscript.teams[0].members.length).toBe(1)
  })
  it('should throw an error when the user is not found', async () => {
    const { Manuscript, Team } = models
    const manuscript = await fixtures.generateManuscript({
      properties: { version: 1, status: 'draft', publicationDates: [] },
      Manuscript,
    })

    const addedAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    const result = removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: `${addedAuthor.id}-invalid`,
      })

    return expect(result).rejects.toThrow(
      'The specified user is not invited as a author',
    )
  })
  it('should throw an error when the user is the submitting author', async () => {
    const { Manuscript, Team } = models
    const manuscript = await fixtures.generateManuscript({
      properties: { version: 1, status: 'draft', publicationDates: [] },
      Manuscript,
    })

    const addedAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    addedAuthor.isSubmitting = true

    const result = removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: `${addedAuthor.id}`,
      })

    return expect(result).rejects.toThrow(
      `Submitting authors can't be deleted from the team`,
    )
  })
  it('should make the submitting author corresponding if the deleted author is corresponding', async () => {
    const { Manuscript, Team } = models
    const manuscript = await fixtures.generateManuscript({
      properties: { version: 1, status: 'draft', publicationDates: [] },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    const addedAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const res = await removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: addedAuthor.id,
      })

    expect(manuscript.teams[0].members.length).toBe(1)
    expect(res[0].isCorresponding).toBe(true)
  })
  it('should not change the authors if the deleted author is not corresponding', async () => {
    const { Manuscript, Team } = models
    const manuscript = await fixtures.generateManuscript({
      properties: { version: 1, status: 'draft', publicationDates: [] },
      Manuscript,
    })

    const submittingAuthor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    const addedAuthor1 = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isCorresponding: true,
      },
      role: Team.Role.author,
    })

    const addedAuthor2 = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const result = await removeAuthorFromManuscriptUseCase
      .initialize({ models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        authorTeamMemberId: addedAuthor2.id,
      })

    const author0 = result.find(member => member.id === submittingAuthor.id)
    const author1 = result.find(member => member.id === addedAuthor1.id)

    expect(author0.isCorresponding).toBe(false)
    expect(author1.isCorresponding).toBe(true)
  })
})
