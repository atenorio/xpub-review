const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  File,
  Team,
  Journal,
  Manuscript,
  SpecialIssue,
  PeerReviewModel,
} = models
const { updateDraftManuscriptUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('Update Draft Manuscript Use Case', () => {
  it('should change the metadata info and set journal id', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const manuscript = fixtures.generateManuscript({ Manuscript })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const journalId = journal.id
    const input = {
      journalId,
      authors: [],
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
    }

    await updateDraftManuscriptUseCase.initialize(models).execute({
      manuscriptId: manuscript.id,
      autosaveInput: input,
    })

    expect(manuscript.abstract).toEqual(input.meta.abstract)
    expect(manuscript.title).toEqual(input.meta.title)
    expect(manuscript.journalId).toEqual(journalId)
  })
  it('should reorder authors if the order is changed', async () => {
    const manuscript = fixtures.generateManuscript({ Manuscript })

    const authorTeamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })
    const coAuthorTeamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: coAuthorTeamMember.alias.email,
        },
        {
          email: authorTeamMember.alias.email,
        },
      ],
    }

    const resultingManuscript = await updateDraftManuscriptUseCase
      .initialize(models)
      .execute({
        manuscriptId: manuscript.id,
        autosaveInput: input,
      })

    expect(resultingManuscript.authors[0].alias.email).toEqual(
      coAuthorTeamMember.alias.email,
    )
  })
  it('should reorder files if the order is changed', async () => {
    const manuscript = fixtures.generateManuscript({ Manuscript })
    const manuscriptFile = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'manuscript',
        position: 0,
      },
      File,
    })

    const manuscriptFile2 = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'manuscript',
        position: 1,
      },
      File,
    })

    const supplementaryFile = fixtures.generateFile({
      properties: {
        manuscriptId: manuscript.id,
        type: 'supplementary',
        position: 0,
      },
      File,
    })

    manuscript.files.push(manuscriptFile, manuscriptFile2, supplementaryFile)

    const authorTeamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const input = {
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
      authors: [
        {
          email: authorTeamMember.alias.email,
        },
      ],
      files: [
        {
          id: manuscriptFile2.id,
          type: manuscriptFile2.type,
        },
        {
          id: manuscriptFile.id,
          type: manuscriptFile.type,
        },
        {
          id: supplementaryFile.id,
          type: supplementaryFile.type,
        },
      ],
    }

    const resultingManuscript = await updateDraftManuscriptUseCase
      .initialize(models)
      .execute({
        manuscriptId: manuscript.id,
        autosaveInput: input,
      })

    expect(resultingManuscript.files[0].type).toEqual('manuscript')
    expect(resultingManuscript.files[0].id).toEqual(manuscriptFile2.id)
  })
  it('sets a special issue id if one is provided', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const manuscript = fixtures.generateManuscript({ Manuscript })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const journalId = journal.id
    const specialIssue = await fixtures.generateSpecialIssue({
      SpecialIssue,
      properties: {
        journalId,
      },
    })
    const input = {
      journalId,
      authors: [],
      specialIssueId: specialIssue.id,
      meta: {
        title: chance.word(),
        abstract: chance.paragraph(),
      },
    }

    await updateDraftManuscriptUseCase.initialize(models).execute({
      manuscriptId: manuscript.id,
      autosaveInput: input,
    })

    expect(manuscript.abstract).toEqual(input.meta.abstract)
    expect(manuscript.title).toEqual(input.meta.title)
    expect(manuscript.specialIssueId).toEqual(specialIssue.id)
  })
})
