process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')

const chance = new Chance()

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const makeMockFn = () => jest.fn(async () => {})
const logEvent = () => jest.fn(async () => {})
const eventsService = {
  publishSubmissionEvent: jest.fn(async () => {}),
}

logEvent.actions = {
  manuscript_submitted: 'manuscript_submitted',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  sendToConfirmedAuthors: makeMockFn(),
  sendToUnconfirmedAuthors: makeMockFn(),
  sendSubmittingAuthorConfirmation: makeMockFn(),
  sendEQSEmail: jest.fn(),
  notifyEditorsWhenNewManuscriptSubmitted: jest.fn(),
}
const sendPackage = makeMockFn()

const { submitManuscriptUseCase } = require('../src/use-cases')

describe('Submit Manuscript Use Case', () => {
  it('should update status and publication dates', async () => {
    const { PeerReviewModel, Journal, Team, TeamMember, Manuscript } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    const draftManuscript = await fixtures.generateManuscript({
      properties: {
        version: 1,
        status: Manuscript.Statuses.draft,
        publicationDates: [],
        journalId: journal.id,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript: draftManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    await submitManuscriptUseCase
      .initialize({
        models,
        sendPackage,
        eventsService,
        notificationService,
        logEvent,
      })
      .execute({ manuscriptId: draftManuscript.id })

    expect(draftManuscript.status).toBe('technicalChecks')
    expect(draftManuscript.technicalCheckToken).toBeDefined()
    expect(draftManuscript.publicationDates[0].type).toBe('technicalChecks')
    expect(sendPackage).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(notificationService.sendEQSEmail).toHaveBeenCalledTimes(1)
  })
  it('should auto assign a RIPE for the specific manuscripts', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      ArticleType,
    } = models

    fixtures.generateArticleTypes(ArticleType)
    const articleTypesWithRIPE = fixtures.articleTypes.filter(articleType =>
      ArticleType.TypesWithRIPE.includes(articleType.name),
    )

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.researchIntegrityPublishingEditor,
    })
    const articleType = chance.pickone(articleTypesWithRIPE)
    const draftManuscript = await fixtures.generateManuscript({
      properties: {
        version: 1,
        status: Manuscript.Statuses.draft,
        publicationDates: [],
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    draftManuscript.articleType = articleType

    await dataService.createUserOnManuscript({
      models,
      manuscript: draftManuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    await submitManuscriptUseCase
      .initialize({
        models,
        sendPackage,
        notificationService,
        logEvent,
        eventsService,
      })
      .execute({ manuscriptId: draftManuscript.id })

    const manuscript = fixtures.manuscripts.find(
      manuscript => manuscript.id === draftManuscript.id,
    )

    const ripeTeam = fixtures.teams.filter(
      team =>
        team.role === Team.Role.researchIntegrityPublishingEditor &&
        manuscript.id === draftManuscript.id,
    )

    expect(manuscript.status).toEqual(Manuscript.Statuses.makeDecision)
    expect(ripeTeam).toBeDefined()
  })
})
