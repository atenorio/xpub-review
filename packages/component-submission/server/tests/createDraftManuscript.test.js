const { models, fixtures } = require('fixture-service')

const { User, Identity, Team } = models

const { createDraftManuscriptUseCase } = require('../src/use-cases')

describe('Create Draft Manuscript Use Case', () => {
  it('creates a new draft manuscript', async () => {
    const user = fixtures.generateUser({ User, Identity })
    const initialManuscriptsLength = fixtures.manuscripts.length

    const manuscript = await createDraftManuscriptUseCase
      .initialize(models)
      .execute({ input: {}, userId: user.id })

    expect(initialManuscriptsLength).toBeLessThan(fixtures.manuscripts.length)
    expect(manuscript.journalId).toBeUndefined()
  })

  it('should add the author of the manuscript to the author team if not admin', async () => {
    const user = fixtures.generateUser({ User, Identity })

    await createDraftManuscriptUseCase
      .initialize(models)
      .execute({ input: {}, userId: user.id })

    expect(fixtures.teams[0].role).toEqual('author')
  })

  it('should create manuscript with no authors when called by an admin', async () => {
    const user = fixtures.generateUser({ User, Identity })
    const adminTeam = fixtures.generateTeam({
      properties: { role: Team.Role.admin },
      Team,
    })
    const newMember = adminTeam.addMember(user, {})
    await newMember.save()

    const manuscriptDTO = await createDraftManuscriptUseCase
      .initialize(models)
      .execute({ input: {}, userId: user.id })

    expect(manuscriptDTO).toBeDefined()
  })
})
