const config = require('config')
const { services } = require('helper-service')
const Email = require('@pubsweet/component-email-templating')
const { get } = require('lodash')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const resetPath = config.get('invite-reset-password.url')
const staffEmail = config.get('staffEmail')
const footerText = config.get('emailFooterText.registeredUsers')
const unconfirmedUsersFooterText = config.get(
  'emailFooterText.unregisteredUsers',
)

module.exports = {
  async sendToConfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'confirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'LOGIN',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async sendToUnconfirmedAuthors(
    { alias, user },
    { manuscript, submittingAuthor, editorialAssistant, journal },
  ) {
    const { name: journalName } = journal
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'unconfirmed-authors',
      manuscript,
      journalName,
      submittingAuthor,
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: alias.email,
        name: alias.surname,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        ctaLink: services.createUrl(baseUrl, resetPath, {
          userId: user.id,
          confirmationToken: user.confirmationToken,
          ...alias,
        }),
        ctaText: 'CREATE ACCOUNT',
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: user.id,
          token: user.unsubscribeToken,
        }),
        footerText: getModifiedText(
          unconfirmedUsersFooterText,
          {
            pattern: '{recipientEmail}',
            replacement: alias.email,
          },
          { pattern: '{journalName}', replacement: journalName },
        ),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendSubmittingAuthorInvitation({
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = manuscript.journal

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'submitting-author-added-by-admin',
      manuscript,
      submittingAuthor,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const email = new Email({
      type: 'user',
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: submittingAuthor.alias.surname,
      },
      content: {
        ctaText: 'LOG IN',
        signatureName: editorialAssistantName,
        subject: `Manuscript submitted`,
        ctaLink: services.createUrl(baseUrl, ''),
        paragraph,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })

    if (!submittingAuthor.isConfirmed) {
      email.content.ctaLink = services.createUrl(baseUrl, resetPath, {
        userId: submittingAuthor.user.id,
        confirmationToken: submittingAuthor.user.confirmationToken,
        ...submittingAuthor.alias,
      })
      email.content.ctaText = 'CREATE ACCOUNT'
    }

    return email.sendEmail()
  },

  async sendSubmittingAuthorConfirmation({
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const emailType = 'submitting-author-manuscript-submitted'
    const { name: journalName } = journal

    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType,
      manuscript,
      journalName,
    })
    const editorialAssistantName = editorialAssistant
      ? editorialAssistant.getName()
      : journalName
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail
    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: submittingAuthor.alias.email,
        name: `${submittingAuthor.alias.surname}`,
      },
      content: {
        subject: `Manuscript submitted to ${journalName}`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        ctaText: 'MANUSCRIPT DETAILS',
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: submittingAuthor.user.id,
          token: submittingAuthor.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: submittingAuthor.alias.email,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },

  async sendEQSEmail({ manuscript, editorialAssistant, admin, journal }) {
    const { name: journalName } = journal
    const { paragraph, ...bodyProps } = getEmailCopy({
      emailType: 'eqs-email',
      manuscript,
      journalName,
    })
    const editorialAssistantOrAdminEmail =
      get(editorialAssistant, 'alias.email') || get(admin, 'alias.email')
    const editorialAssistantOrAdminName = editorialAssistant
      ? editorialAssistant.getName()
      : admin.getName()
    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantOrAdminEmail}>`,
      toUser: {
        email: editorialAssistantOrAdminEmail,
        name: editorialAssistantOrAdminName,
      },
      content: {
        subject: `Manuscript Submitted`,
        paragraph,
        signatureJournal: journalName,
        ctaLink: services.createUrl(baseUrl, config.get('eqs-decision.url'), {
          title: manuscript.title,
          manuscriptId: manuscript.id,
          customId: manuscript.customId,
          token: manuscript.technicalCheckToken,
        }),
        ctaText: 'MAKE DECISION',
        unsubscribeLink: baseUrl,
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: editorialAssistantOrAdminEmail,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
  async notifyEditorsWhenNewManuscriptSubmitted({
    journal,
    editor,
    submittingAuthor,
    editorialAssistant,
    manuscript,
  }) {
    const { name: journalName } = journal
    const editorName = editor.getName()
    const editorEmail = get(editor, 'alias.email')
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || get(journal, 'email')

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      manuscript,
      submittingAuthor,
      emailType: 'editors-new-manuscript-submitted',
    })

    const email = new Email({
      type: 'user',
      toUser: {
        email: editorEmail,
        name: editorName,
      },
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      content: {
        subject: `${manuscript.customId}: New manuscript submitted`,
        paragraph,
        signatureName: editorialAssistantName,
        signatureJournal: journalName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: services.createUrl(
          baseUrl,
          `/details/${manuscript.submissionId}/${manuscript.id}`,
        ),
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: get(editor, 'user.id', ''),
          token: get(editor, 'user.unsubscribeToken', ''),
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: editorEmail,
        }),
      },
      bodyProps,
    })

    return email.sendEmail()
  },
}
