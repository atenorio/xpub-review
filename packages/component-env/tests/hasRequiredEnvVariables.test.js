const { hasRequiredEnvVariables } = require('../src')

describe('hasRequiredEnvVariables', () => {
  it('should return true for no values', () => {
    const res = hasRequiredEnvVariables([])
    expect(res).toBe(true)
  })
  it('should return false for just one wrong value', () => {
    const res = hasRequiredEnvVariables([10, true, undefined, 'yarn'])
    expect(res).toBe(false)
  })

  it('should return true for non-empty string values', () => {
    const res = hasRequiredEnvVariables(['fish', 'chips'])
    expect(res).toBe(true)
  })
  it('should return false for empty string value', () => {
    const res = hasRequiredEnvVariables([''])
    expect(res).toBe(false)
  })

  it('should return true for number values', () => {
    const res = hasRequiredEnvVariables([-1, 12])
    expect(res).toBe(true)
  })
  it('should return true for 0 value', () => {
    const res = hasRequiredEnvVariables([0])
    expect(res).toBe(true)
  })

  it('should return true for boolean values', () => {
    const res = hasRequiredEnvVariables([true, false])
    expect(res).toBe(true)
  })
  it('should return true for false value', () => {
    const res = hasRequiredEnvVariables([false])
    expect(res).toBe(true)
  })

  it('should return false for null values', () => {
    const res = hasRequiredEnvVariables([null])
    expect(res).toBe(false)
  })
  it('should return false undefined values', () => {
    const res = hasRequiredEnvVariables([undefined, undefined, undefined])
    expect(res).toBe(false)
  })
})
