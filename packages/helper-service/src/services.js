const logger = require('@pubsweet/logger')
const querystring = require('querystring')

const checkForUndefinedParams = (...params) => {
  if (params.includes(undefined)) {
    return false
  }

  return true
}

const validateEmailAndToken = async ({ email, token, userModel }) => {
  try {
    const user = await userModel.findOneByEmail(email)
    if (user) {
      if (token !== user.accessTokens.passwordReset) {
        logger.error(
          `invite pw reset tokens do not match: REQ ${token} vs. DB ${user.accessTokens.passwordReset}`,
        )
        return {
          success: false,
          status: 400,
          message: 'invalid request',
        }
      }
      return { success: true, user }
    }
  } catch (e) {
    if (e.name === 'NotFoundError') {
      logger.error('invite pw reset on non-existing user')
      return {
        success: false,
        status: 404,
        message: 'user not found',
      }
    } else if (e.name === 'ValidationError') {
      logger.error('invite pw reset validation error')
      return {
        success: false,
        status: 400,
        message: e.details[0].message,
      }
    }
    logger.error('internal server error')
    return {
      success: false,
      status: 500,
      message: e.details[0].message,
    }
  }
  return {
    success: false,
    status: 500,
    message: 'something went wrong',
  }
}

const handleNotFoundError = async (error, item) => {
  const response = {
    success: false,
    status: 500,
    message: 'Something went wrong. Please try again.',
  }
  if (error.name === 'NotFoundError') {
    logger.error(`invalid ${item} id`)
    response.status = 404
    response.message = `${item} not found`
    return response
  }

  logger.error(error)
  return response
}

const getBaseUrl = req => `${req.protocol}://${req.get('host')}`

const createUrl = (baseUrl, slug, queryParams = null) => {
  if (
    process.env.KEYCLOAK_SERVER_URL &&
    queryParams &&
    queryParams.confirmationToken
  ) {
    const { createRegistrationURL } = require('component-sso')
    return createRegistrationURL(
      `${baseUrl}${slug}?${querystring.encode(queryParams)}`,
    )
  }
  return !queryParams
    ? `${baseUrl}${slug}`
    : `${baseUrl}${slug}?${querystring.encode(queryParams)}`
}

const getExpectedDate = ({ timestamp = Date.now(), daysExpected = 0 }) => {
  const date = new Date(timestamp)
  let expectedDate = date.getDate() + daysExpected
  date.setDate(expectedDate)

  expectedDate = date.toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'long',
    year: 'numeric',
  })

  return expectedDate
}

const generateHash = () =>
  Array.from({ length: 4 }, () =>
    Math.random()
      .toString(36)
      .slice(4),
  ).join('')

module.exports = {
  checkForUndefinedParams,
  validateEmailAndToken,
  handleNotFoundError,
  getBaseUrl,
  createUrl,
  getExpectedDate,
  generateHash,
}
