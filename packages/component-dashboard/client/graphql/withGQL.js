import { get } from 'lodash'
import { compose } from 'recompose'
import { graphql } from 'react-apollo'

import * as queries from './queries'
import * as mutations from './mutations'

export default compose(
  graphql(queries.getManuscripts, {
    options: props => ({
      variables: {
        input: {
          priorityFilter: get(props, 'filters.values.priority', 'all'),
          dateOrder: get(props, 'filters.values.sort', 'desc'),
        },
      },
      fetchPolicy: 'cache-and-network',
    }),
  }),
  graphql(mutations.deleteManuscript, {
    name: 'deleteManuscript',
  }),
  graphql(mutations.archiveManuscript, {
    name: 'archiveManuscript',
  }),
  graphql(mutations.withdrawManuscript, {
    name: 'withdrawManuscript',
  }),
)
