A manuscript card.

```jsx
import { BrowserRouter } from 'react-router-dom'

const manuscript = {
  id: '41586006-d251-4a88-b823-63ba862ec439',
  role: 'author',
  customId: '7794066',
  status: 'draft',
  created: '2019-02-03T21:56:34.073Z',
  meta: {
    articleType: 'research',
    abstract: 'abc',
    agreeTc: true,
    title: 'iisus',
    __typename: 'ManuscriptMeta',
  },
  academicEditor: {
    id: null,
    isSubmitting: null,
    isCorresponding: null,
    alias: null,
    __typename: 'TeamMember',
  },
  authors: [
    {
      id: '7448b8dc-3773-4692-a11e-fe4b8b95e060',
      isSubmitting: true,
      isCorresponding: true,
      alias: {
        aff: 'Boko Haram',
        email: 'alexandru.munteanu+a1@thinslices.com',
        country: 'RO',
        name: {
          title: 'mr',
          surname: 'Author1',
          givenNames: 'AlexA1',
          __typename: 'Name',
        },
        __typename: 'Alias',
      },
      __typename: 'TeamMember',
    },
  ],
  __typename: 'Manuscript',
}

;<BrowserRouter>
  <ManuscriptCard manuscript={manuscript} />
</BrowserRouter>
```
