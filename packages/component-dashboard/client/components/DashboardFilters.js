import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Item, Text, Label, Menu } from '@hindawi/ui'

const DashboardFilters = ({
  values,
  options,
  changeSort,
  isConfirmed,
  changePriority,
}) => (
  <Root isConfirmed={isConfirmed}>
    <Row alignItems="flex-end" justify="flex-start" pt={isConfirmed ? 22 : 28}>
      <Text mr={2} pb={2} secondary>
        Filters
      </Text>
      <Item
        alignItems="flex-start"
        data-test-id="dashboard-filter-priority"
        flex={0}
        mr={2}
        vertical
      >
        <Label>Priority</Label>
        <Menu
          inline
          onChange={changePriority}
          options={options.priority}
          placeholder="Please select"
          value={values.priority}
          width={40}
        />
      </Item>
      <Item
        alignItems="flex-start"
        data-test-id="dashboard-filter-order"
        flex={0}
        vertical
      >
        <Label>Order</Label>
        <Menu
          inline
          onChange={changeSort}
          options={options.sort}
          placeholder="Please select"
          value={values.sort}
          width={40}
        />
      </Item>
    </Row>
  </Root>
)

export default DashboardFilters

const Root = styled.div`
  /* The filter slot's height goes to the top of
  the page because of Safari's implementation
  (weird element jumping when scrolling) */
  height: calc(
    ${({ isConfirmed }) => (isConfirmed ? 36 : 42)} * ${th('gridUnit')}
  );
  position: fixed;
  top: 0;
  left: calc(${th('gridUnit')} * 20);
  right: calc(${th('gridUnit')} * 20);
  background-color: ${th('colorBackground')};
`
