import React, { Fragment } from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { Spinner } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps, withHandlers } from 'recompose'

import withGQL from '../graphql/withGQL'
import { DashboardFilters, ManuscriptCard, withFilters } from '../components'

const Dashboard = ({
  data = {},
  filters,
  isConfirmed,
  deleteManuscript,
  withdrawManuscript,
}) =>
  get(data, 'getManuscripts', []).length === 0 && data.loading ? (
    <Spinner />
  ) : (
    <Fragment>
      <DashboardFilters {...filters} isConfirmed={isConfirmed} />
      <Root data-test-id="dashboard-list-items" isConfirmed={isConfirmed}>
        {get(data, 'getManuscripts', []).map((m, index) => (
          <ManuscriptCard
            academicEditor={m.academicEditor}
            data-test-id="row"
            deleteManuscript={deleteManuscript}
            isLast={index === data.getManuscripts.length - 1}
            key={m.id}
            manuscript={m}
            role={m.role}
            withdrawManuscript={withdrawManuscript}
          />
        ))}
      </Root>
    </Fragment>
  )

export default compose(
  withFilters,
  withGQL,
  withProps(({ currentUser }) => ({
    isConfirmed: get(currentUser, 'identities.0.isConfirmed', true),
  })),
  withHandlers({
    deleteManuscript: ({ deleteManuscript, archiveManuscript, data }) => (
      manuscript,
      modalProps,
    ) => {
      const refetchGetManuscripts = get(data, 'refetch')

      if (manuscript.status === 'draft') {
        modalProps.setFetching(true)
        deleteManuscript({
          variables: {
            manuscriptId: manuscript.id,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            refetchGetManuscripts()
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      } else if (
        manuscript.role === 'admin' ||
        manuscript.role === 'editorialAssistant'
      ) {
        modalProps.setFetching(true)
        archiveManuscript({
          variables: {
            submissionId: manuscript.submissionId,
          },
        })
          .then(() => {
            modalProps.setFetching(false)
            refetchGetManuscripts()
            modalProps.hideModal()
          })
          .catch(e => {
            modalProps.setFetching(false)
            modalProps.setError(e.message)
          })
      }
    },
    withdrawManuscript: ({ withdrawManuscript, data }) => (
      manuscript,
      modalProps,
    ) => {
      const refetchGetManuscripts = get(data, 'refetch')

      modalProps.setFetching(true)
      withdrawManuscript({
        variables: {
          submissionId: manuscript.submissionId,
        },
      })
        .then(() => {
          modalProps.setFetching(false)
          refetchGetManuscripts()
          modalProps.hideModal()
        })
        .catch(e => {
          modalProps.setFetching(false)
          modalProps.setError(e.message)
        })
    },
  }),
)(Dashboard)

// #region styles
const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 18);
`
// #endregion
