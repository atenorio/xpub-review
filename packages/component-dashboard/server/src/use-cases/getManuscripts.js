const initialize = ({ models, useCases }) => ({
  async execute({ userId, input }) {
    const { Team, User } = models
    const user = await User.find(
      userId,
      `teamMemberships.team.[
        manuscript.[
          articleType,
          teams.members,
          journal.peerReviewModel,
          specialIssue.peerReviewModel
        ],
        journal.[
          manuscripts.[
            teams.members,
            articleType,
            journal.peerReviewModel,
            specialIssue.peerReviewModel,
          ],
          teams.members
        ]
      ]`,
    )

    const userRoles = user.teamMemberships.map(tm => tm.team.role)

    let useCase = 'manuscriptRolesUseCase'

    if (userRoles.includes(Team.Role.admin)) {
      useCase = 'adminRolesUseCase'
    }
    if (userRoles.includes(Team.Role.editorialAssistant)) {
      useCase = 'journalRolesUseCase'
    }

    return useCases[useCase].initialize(models).execute({ input, user })
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
