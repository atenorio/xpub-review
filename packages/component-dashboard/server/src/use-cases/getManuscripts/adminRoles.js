const { orderBy, get } = require('lodash')
const config = require('config')

const statuses = config.get('statuses')
const FILTER_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}

const initialize = ({ Manuscript, Team, TeamMember }) => ({
  async execute({ input = {}, user }) {
    let manuscripts = []
    const { dateOrder = 'desc', priorityFilter = FILTER_VALUES.ALL } = input

    if (priorityFilter === FILTER_VALUES.ALL) {
      manuscripts = await Manuscript.findAll({
        eagerLoadRelations: [
          'articleType',
          'teams.members',
          'journal.peerReviewModel',
          'specialIssue.peerReviewModel',
        ],
        orderByField: 'updated',
        order: dateOrder,
      })
      manuscripts = manuscripts.filter(
        m => m.status !== Manuscript.Statuses.deleted,
      )
    } else {
      manuscripts = await Manuscript.findAll({
        eagerLoadRelations: [
          'articleType',
          'teams.members',
          'journal.peerReviewModel',
          'specialIssue.peerReviewModel',
        ],
      })
      manuscripts = manuscripts.filter(
        manuscript =>
          get(
            statuses,
            `${manuscript.status}.${
              user.getTeamMemberForManuscript(manuscript).team.role
            }.${priorityFilter}`,
          ) && manuscript.status !== Manuscript.Statuses.deleted,
      )
      manuscripts = orderBy(manuscripts, 'updated', dateOrder)
    }

    manuscripts = manuscripts.map(m => {
      const teamMember = user.getTeamMemberForManuscript(m)
      m.role = teamMember.team.role
      return m
    })

    manuscripts = Manuscript.filterOlderVersions(manuscripts)
    manuscripts.map(m =>
      m.setVisibleStatus({
        userId: user.id,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      }),
    )

    return manuscripts.map(m => m.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
