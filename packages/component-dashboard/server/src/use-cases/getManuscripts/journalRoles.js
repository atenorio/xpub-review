const { orderBy, get, flatMap } = require('lodash')
const config = require('config')

const statuses = config.get('statuses')
const FILTER_VALUES = {
  ALL: 'all',
  NEEDS_ATTENTION: 'needsAttention',
  IN_PROGRESS: 'inProgress',
  ARCHIVED: 'archived',
}

const initialize = ({ Manuscript, Team, TeamMember }) => ({
  async execute({ input = {}, user }) {
    const { dateOrder = 'desc', priorityFilter = FILTER_VALUES.ALL } = input

    const journals = user.teamMemberships
      .filter(member => member.team.journal)
      .map(member => {
        member.team.journal.role = member.team.role
        return member.team.journal
      })
    let manuscripts = flatMap(journals, j => {
      const manuscripts = j.manuscripts.map(m => {
        m.role = j.role
        return m
      })
      return manuscripts
    })

    manuscripts = manuscripts.filter(
      m => m.status !== Manuscript.Statuses.deleted,
    )
    if (priorityFilter !== FILTER_VALUES.ALL) {
      manuscripts = manuscripts.filter(manuscript =>
        get(
          statuses,
          `${manuscript.status}.${manuscript.role}.${priorityFilter}`,
        ),
      )
    }

    manuscripts = orderBy(manuscripts, 'updated', dateOrder)
    manuscripts = Manuscript.filterOlderVersions(manuscripts)
    manuscripts.map(m =>
      m.setVisibleStatus({
        userId: user.id,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      }),
    )
    return manuscripts.map(m => m.toDTO())
  },
})

module.exports = {
  initialize,
}
