const { orderBy, get } = require('lodash')
const config = require('config')

const statuses = config.get('statuses')
const FILTER_VALUES = {
  ALL: 'all',
  ARCHIVED: 'archived',
  IN_PROGRESS: 'inProgress',
  NEEDS_ATTENTION: 'needsAttention',
}

const initialize = ({ Manuscript, TeamMember, Team }) => ({
  async execute({ input = {}, user }) {
    let manuscripts = []
    const { dateOrder = 'desc', priorityFilter = FILTER_VALUES.ALL } = input

    manuscripts = user.teamMemberships
      .filter(
        member =>
          member.status !== TeamMember.Statuses.declined &&
          member.status !== TeamMember.Statuses.expired &&
          member.status !== TeamMember.Statuses.removed,
      )
      .filter(member => member.team.manuscript)
      .map(member => {
        member.team.manuscript.role = member.team.role

        return member.team.manuscript
      })

    manuscripts = manuscripts.filter(
      m => m.status !== Manuscript.Statuses.deleted,
    )
    manuscripts = manuscripts.filter(m => {
      if (m.role === Team.Role.author) return true
      return m.status !== Manuscript.Statuses.withdrawn
    })

    if (priorityFilter !== FILTER_VALUES.ALL) {
      manuscripts = manuscripts.filter(manuscript =>
        get(
          statuses,
          `${manuscript.status}.${manuscript.role}.${priorityFilter}`,
        ),
      )
    }

    manuscripts = orderBy(manuscripts, 'updated', dateOrder)
    manuscripts = Manuscript.filterOlderVersions(manuscripts)
    manuscripts.map(m =>
      m.setVisibleStatus({
        userId: user.id,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      }),
    )

    return manuscripts.map(m => m.toDTO())
  },
})

module.exports = {
  initialize,
}
