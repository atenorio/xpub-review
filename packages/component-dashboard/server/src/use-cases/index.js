const manuscriptRolesUseCase = require('./getManuscripts/manuscriptRoles')
const adminRolesUseCase = require('./getManuscripts/adminRoles')
const journalRolesUseCase = require('./getManuscripts/journalRoles')
const getManuscriptsUseCase = require('./getManuscripts')
const deleteManuscriptUseCase = require('./deleteManuscript')
const archiveManuscriptUseCase = require('./archiveManuscript')
const withdrawManuscriptUseCase = require('./withdrawManuscript')

module.exports = {
  manuscriptRolesUseCase,
  adminRolesUseCase,
  journalRolesUseCase,
  getManuscriptsUseCase,
  deleteManuscriptUseCase,
  archiveManuscriptUseCase,
  withdrawManuscriptUseCase,
}
