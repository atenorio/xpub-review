const { Promise } = require('bluebird')
const { last } = require('lodash')

const initialize = ({ models: { Manuscript, TeamMember }, jobsService }) => ({
  async execute(submissionId) {
    const manuscripts = await Manuscript.findBy(
      { submissionId },
      'teams.members.jobs',
    )

    await Promise.each(manuscripts, async manuscript => {
      if (
        manuscript.status === Manuscript.Statuses.draft &&
        manuscript.version === 1
      ) {
        throw new ValidationError('Draft manuscripts cannot be deleted.')
      }
      if (manuscript.status === Manuscript.Statuses.deleted) {
        throw new ValidationError('Manuscript already deleted.')
      }

      manuscript.updateStatus(Manuscript.Statuses.deleted)
      await manuscript.save()
    })
    const lastVersionOfManuscript = last(manuscripts)
    const reviewers = lastVersionOfManuscript.getReviewers()

    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.accepted,
    )
    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.pending,
    )
    jobsService.cancelReviewersJobs({ reviewers: acceptedReviewers })
    jobsService.cancelReviewersJobs({ reviewers: pendingReviewers })
  },
})

const authsomePolicies = ['isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
