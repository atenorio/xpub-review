const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyManuscriptMembers({
    authors,
    reviewers,
    manuscript,
    journalName,
    triageEditor,
    academicEditor,
    editorialAssistant,
  }) {
    const submittingAuthor = authors.find(a => a.isSubmitting)
    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`
    const editorialAssistantEmail = editorialAssistant.getEmail()

    const options = {
      titleText,
      manuscript,
      journalName,
      editorialAssistantEmail,
    }
    this.notifyAuthors({ ...options, authors })
    this.notifyReviewers({ ...options, reviewers })
    this.notifyTriageEditor({ ...options, triageEditor })
    this.notifyAcademicEditor({ ...options, academicEditor })
  },
  async notifyAuthors({
    authors,
    titleText,
    manuscript,
    journalName,
    editorialAssistantEmail,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'author-manuscript-withdrawn',
    })
    authors.forEach(async author => {
      const emailProps = getPropsService.getProps({
        paragraph,
        manuscript,
        toUser: author,
        signatureName: journalName,
        subject: `${manuscript.customId}: Manuscript Withdrawn`,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      email.sendEmail()
    })
  },
  async notifyReviewers({
    reviewers,
    titleText,
    manuscript,
    journalName,
    editorialAssistantEmail,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'reviewer-manuscript-withdrawn',
    })

    reviewers.forEach(async reviewer => {
      const emailProps = getPropsService.getProps({
        paragraph,
        manuscript,
        toUser: reviewer,
        signatureName: journalName,
        subject: `${manuscript.customId}: Manuscript Withdrawn`,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      email.sendEmail()
    })
  },
  async notifyAcademicEditor({
    titleText,
    manuscript,
    journalName,
    academicEditor,
    editorialAssistantEmail,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'academic-editor-manuscript-withdrawn',
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      toUser: academicEditor,
      signatureName: journalName,
      subject: `${manuscript.customId}: Manuscript Withdrawn`,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    email.sendEmail()
  },
  async notifyTriageEditor({
    titleText,
    manuscript,
    journalName,
    triageEditor,
    editorialAssistantEmail,
  }) {
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText,
      journalName,
      emailType: 'triage-editor-manuscript-withdrawn',
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      toUser: triageEditor,
      signatureName: journalName,
      subject: `${manuscript.customId}: Manuscript Withdrawn`,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    email.sendEmail()
  },
})

module.exports = { initialize }
