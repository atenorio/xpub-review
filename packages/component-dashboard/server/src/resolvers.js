const config = require('config')
const Email = require('@pubsweet/component-email-templating')
const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('./urlService/urlService')
const notifications = require('./notifications/notifications')

const useCases = require('./use-cases')
const JobsService = require('./jobsService/jobsService')
const getProps = require('./emailPropsService/emailPropsService')
const getEmailCopy = require('./notifications/emailCopy')
const events = require('component-events')

const resolvers = {
  Query: {
    async getManuscripts(_, { input }, ctx) {
      return useCases.getManuscriptsUseCase
        .initialize({ models, useCases })
        .execute({ input, userId: ctx.user })
    },
  },
  Mutation: {
    async deleteManuscript(_, { manuscriptId }, ctx) {
      return useCases.deleteManuscriptUseCase
        .initialize(models)
        .execute({ manuscriptId, userId: ctx.user })
    },
    async archiveManuscript(_, { submissionId }, ctx) {
      const jobsService = JobsService.initialize({ models })

      return useCases.archiveManuscriptUseCase
        .initialize({ models, jobsService })
        .execute(submissionId)
    },
    async withdrawManuscript(_, { submissionId }, ctx) {
      const jobsService = JobsService.initialize({ models })
      const eventsService = events.initialize({ models })
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = notifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.withdrawManuscriptUseCase
        .initialize({
          models,
          jobsService,
          logEvent,
          eventsService,
          notificationService,
        })
        .execute({ submissionId, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
