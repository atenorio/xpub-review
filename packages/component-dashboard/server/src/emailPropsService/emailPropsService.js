const { get } = require('lodash')

const initialize = ({
  baseUrl,
  urlService,
  footerText,
  unsubscribeSlug,
  getModifiedText,
}) => ({
  getProps({
    toUser,
    subject,
    paragraph,
    fromEmail,
    manuscript,
    journalName,
    signatureName,
  }) {
    return {
      fromEmail,
      type: 'user',
      templateType: 'notification',
      toUser: {
        email: get(toUser, 'alias.email', ''),
        name: get(toUser, 'alias.surname', ''),
      },
      content: {
        subject,
        paragraph,
        signatureName,
        signatureJournal: journalName,
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: toUser.alias.email,
        }),
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: toUser.userId,
            token: get(toUser, 'user.unsubscribeToken'),
          },
        }),
      },
    }
  },
})

module.exports = { initialize }
