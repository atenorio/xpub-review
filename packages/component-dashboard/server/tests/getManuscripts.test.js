process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const { models, fixtures } = require('fixture-service')

const { Manuscript, Team, User, Identity, TeamMember } = models
const { getManuscriptsUseCase } = require('../src/use-cases')

describe('get manuscripts use case', () => {
  let mockedUseCases = {}
  beforeEach(() => {
    mockedUseCases = {
      adminRolesUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
      manuscriptRolesUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
      journalRolesUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
    }
  })

  it('executes the admin role use case if the user is admin ', async () => {
    fixtures.generateManuscript({ Manuscript })
    fixtures.generateManuscript({ Manuscript })

    let team
    team = fixtures.getTeamByRole(Team.Role.admin)
    if (!team)
      team = fixtures.generateTeam({
        properties: { role: Team.Role.admin },
        Team,
      })

    const admin = fixtures.generateUser({ User, Identity })
    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: admin.id,
        teamId: team.id,
      },
      TeamMember,
    })
    admin.teamMemberships = admin.teamMemberships || []
    admin.teamMemberships.push(teamMember)
    teamMember.user = admin

    teamMember.team = team
    team.members.push(teamMember)

    await getManuscriptsUseCase
      .initialize({ models, useCases: mockedUseCases })
      .execute({ input: { dateOrder: 'desc' }, userId: admin.id })

    expect(mockedUseCases.adminRolesUseCase.initialize).toHaveBeenCalledTimes(1)
    expect(
      mockedUseCases.manuscriptRolesUseCase.initialize,
    ).not.toHaveBeenCalled()
    expect(mockedUseCases.journalRolesUseCase.initialize).not.toHaveBeenCalled()
  })

  it('executes the manuscript role use case if the user is author ', async () => {
    const user = fixtures.generateUser({ User, Identity })
    user.teamMemberships = user.teamMemberships || []

    await getManuscriptsUseCase
      .initialize({ models, useCases: mockedUseCases })
      .execute({ input: { dateOrder: 'desc' }, userId: user.id })

    expect(
      mockedUseCases.manuscriptRolesUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
    expect(mockedUseCases.adminRolesUseCase.initialize).not.toHaveBeenCalled()
    expect(mockedUseCases.journalRolesUseCase.initialize).not.toHaveBeenCalled()
  })
})
