process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Manuscript, Team, User, Identity } = models
const { manuscriptRolesUseCase } = require('../src/use-cases')

describe('Get manuscripts for manuscript roles (AcademicEditor, author, reviewer)', () => {
  it('returns own manuscripts if the user is an author', async () => {
    const manuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.technicalChecks,
        customId: '1112223',
      },
      Manuscript,
    })
    fixtures.generateManuscript({ Manuscript })

    const user = await fixtures.generateUser({ User, Identity })
    await dataService.addUserOnManuscript({
      user,
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
    })

    const draftManuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.draft,
      },
      Manuscript,
    })

    await dataService.addUserOnManuscript({
      user,
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: draftManuscript,
    })

    const submittedManuscript = fixtures.generateManuscript({
      properties: {
        status: Manuscript.Statuses.submitted,
        customId: '1112224',
      },
      Manuscript,
    })

    await dataService.addUserOnManuscript({
      user,
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: submittedManuscript,
    })

    const manuscripts = await manuscriptRolesUseCase
      .initialize(models)
      .execute({ input: { dateOrder: 'desc' }, user })

    expect(manuscripts).toHaveLength(user.teamMemberships.length)
    expect(manuscripts[0].role).toEqual('author')
    expect(manuscripts[0].authors).toHaveLength(1)

    expect(manuscripts[0].customId).toEqual(submittedManuscript.customId)
    expect(manuscripts[0].visibleStatus).toEqual('Submitted')

    expect(manuscripts[1].visibleStatus).toEqual('Complete Submission')

    expect(manuscripts[2].customId).toBeUndefined()
  })
})
