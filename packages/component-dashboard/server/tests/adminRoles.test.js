process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { Team, User, Identity, TeamMember, Manuscript } = models
const { adminRolesUseCase } = require('../src/use-cases')

const chance = new Chance()

describe('get manuscripts for admin roles', () => {
  it('returns latest manuscript versions if user is admin', async () => {
    let team
    team = fixtures.getTeamByRole(Team.Role.admin)
    if (!team)
      team = fixtures.generateTeam({
        properties: { role: Team.Role.admin },
        Team,
      })
    const user = fixtures.generateUser({ User, Identity })

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: user.id,
        teamId: team.id,
      },
      TeamMember,
    })

    user.teamMemberships = [teamMember]
    teamMember.user = user
    teamMember.team = team

    const timeObject = new Date()
    const submissionId = chance.guid()

    fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId,
        status: Manuscript.Statuses.olderVersion,
        updated: new Date(timeObject.getTime() + 1000 * 10),
      },
      Manuscript,
    })

    fixtures.generateManuscript({
      properties: {
        version: 2,
        submissionId,
        status: Manuscript.Statuses.makeRecommendation,
        updated: new Date(timeObject.getTime() + 2000 * 10),
      },
      Manuscript,
    })

    fixtures.generateManuscript({
      properties: {
        submissionId: chance.guid(),
        status: Manuscript.Statuses.draft,
        updated: new Date(timeObject.getTime() + 3000 * 10),
      },
      Manuscript,
    })

    fixtures.generateManuscript({
      properties: {
        submissionId: chance.guid(),
        status: Manuscript.Statuses.submitted,
        updated: new Date(timeObject.getTime() + 5000 * 10),
      },
      Manuscript,
    })

    const revisedSubmissionId = chance.guid()
    fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId: revisedSubmissionId,
        status: Manuscript.Statuses.olderVersion,
        updated: new Date(timeObject.getTime() + 6000 * 10),
      },
      Manuscript,
    })

    fixtures.generateManuscript({
      properties: {
        version: 2,
        submissionId: revisedSubmissionId,
        status: Manuscript.Statuses.underReview,
        updated: new Date(timeObject.getTime() + 7000 * 10),
      },
      Manuscript,
    })

    const manuscripts = await adminRolesUseCase
      .initialize(models)
      .execute({ input: {}, role: Team.Role.admin, user })

    expect(manuscripts).toHaveLength(4)
    expect(manuscripts[0].status).toEqual(Manuscript.Statuses.underReview)
  })
})
