const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, Manuscript } = models

const { journalRolesUseCase } = require('../src/use-cases')

describe('journal roles use case', () => {
  it('returns all manuscripts if the user is Editorial Assistant', async () => {
    const journal = fixtures.generateJournal({ Journal })

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    journal.manuscripts = [manuscript]

    const editorialAssistant = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })

    const manuscripts = await journalRolesUseCase.initialize(models).execute({
      input: { priorityFilter: 'inProgress' },
      role: Team.Role.editorialAssistant,
      user: editorialAssistant.user,
    })
    expect(manuscripts.length).toEqual(fixtures.manuscripts.length)
  })
})
