process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures } = require('fixture-service')

const { Journal, Manuscript, File, Comment, Review } = models
const {
  getFiles,
  transformFiles,
  getPackageName,
  initializeS3Operations,
} = require('../src/PackageManager')

const asyncGetObject = jest.fn()

describe('MTS Package Manager', () => {
  describe('initializeS3Operations', () => {
    it('should return promisified methods', () => {
      const s3Operations = initializeS3Operations({})
      expect(s3Operations).toHaveProperty('asyncGetObject')
      expect(s3Operations).toHaveProperty('asyncUploadS3')
      expect(s3Operations).toHaveProperty('deleteObjectsS3')
    })
  })
  describe('getPackageName', () => {
    let journal
    let manuscript
    beforeEach(() => {
      journal = fixtures.generateJournal({ Journal })
      manuscript = fixtures.generateManuscript({
        properties: {
          journalId: journal.id,
          status: Manuscript.Statuses.accepted,
        },
        Manuscript,
      })
      manuscript.journal = journal
    })
    it('sets package name for EQA', () => {
      const packageName = getPackageName({ manuscript, isEQA: true })
      expect(packageName).toEqual(
        `ACCEPTED_${journal.code}-${manuscript.customId}.${manuscript.version}`,
      )
    })
    it('sets package name for EQS', () => {
      const packageName = getPackageName({ manuscript, isEQA: false })
      expect(packageName).toEqual(`${journal.code}-${manuscript.customId}`)
    })
    it('archives REJECTED for status rejected', () => {
      manuscript.status = Manuscript.Statuses.rejected
      const packageName = getPackageName({
        manuscript,
        isEQA: true,
        isManuscriptRejected: true,
      })
      expect(packageName).toEqual(
        `REJECTED_${journal.code}-${manuscript.customId}.${manuscript.version}`,
      )
    })
    it('archives ACCEPTED for status inQA', () => {
      manuscript.status = Manuscript.Statuses.inQA
      const packageName = getPackageName({ manuscript, isEQA: true })
      expect(packageName).toEqual(
        `ACCEPTED_${journal.code}-${manuscript.customId}.${manuscript.version}`,
      )
    })
  })
  describe('getFiles', () => {
    let journal
    let manuscript
    beforeEach(() => {
      journal = fixtures.generateJournal({ Journal })
      manuscript = fixtures.generateManuscript({
        properties: {
          journalId: journal.id,
          status: Manuscript.Statuses.accepted,
        },
        Manuscript,
      })

      const manuscriptFile = fixtures.generateFile({
        properties: {
          manuscriptId: manuscript.id,
          type: File.Types.manuscript,
          fileName: 'manuscript',
        },
        File,
      })
      const supplementaryFile = fixtures.generateFile({
        properties: {
          manuscriptId: manuscript.id,
          type: File.Types.supplementary,
          fileName: 'supplementary',
        },
        File,
      })
      const figureFile = fixtures.generateFile({
        properties: {
          manuscriptId: manuscript.id,
          type: File.Types.figure,
          fileName: 'figure',
        },
        File,
      })
      manuscript.files = [manuscriptFile, supplementaryFile, figureFile]

      const reviewCommentFile = fixtures.generateFile({
        properties: {
          manuscriptId: manuscript.id,
          type: File.Types.reviewComment,
          fileName: 'review',
        },
        File,
      })
      const review = fixtures.generateReview({
        properties: {
          manuscriptId: manuscript.id,
        },
        Review,
      })
      const comment = fixtures.generateComment({
        properties: {
          type: Comment.Types.public,
          reviewId: review.id,
        },
        Comment,
      })
      comment.files = [reviewCommentFile]
      review.comments = [comment]
      manuscript.reviews = [review]
    })
    it('filters files correctly', () => {
      const files = getFiles(manuscript)

      expect(files.figureFiles).toHaveLength(1)
      expect(files.supplementalFiles).toHaveLength(1)
      expect(files.manuscriptAndCoverLetterFiles).toHaveLength(1)
      expect(files.reviewFiles).toHaveLength(1)
    })
    it('returns an empty array', () => {
      manuscript.files = []
      manuscript.reviews = []
      try {
        getFiles(manuscript)
        throw new Error('function should return error if files are empty')
      } catch (err) {
        expect(err.message).toEqual('Failed to create a package')
      }
    })
    it('transformFiles function returns an array with the same length', async () => {
      const { manuscriptAndCoverLetterFiles } = getFiles(manuscript)
      const files = await transformFiles({
        files: manuscriptAndCoverLetterFiles,
        asyncGetObject,
      })
      expect(files.length).toEqual(manuscriptAndCoverLetterFiles.length)
    })
  })
})
