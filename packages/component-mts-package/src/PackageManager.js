const fs = require('fs')
const AWS = require('aws-sdk')
const { get, isEmpty, chain } = require('lodash')
const { promisify } = require('util')
const FtpDeploy = require('ftp-deploy')
const nodeArchiver = require('archiver')
const logger = require('@pubsweet/logger')

const readFile = promisify(fs.readFile)

const initializeS3Operations = s3Config => {
  const s3 = new AWS.S3({
    secretAccessKey: s3Config.secretAccessKey,
    accessKeyId: s3Config.accessKeyId,
    region: s3Config.region,
    params: {
      Bucket: s3Config.bucket,
    },
    useAccelerateEndpoint: true,
  })
  const asyncGetObject = promisify(s3.getObject.bind(s3))
  const asyncUploadS3 = promisify(s3.upload.bind(s3))
  const deleteObjectsS3 = promisify(s3.deleteObjects.bind(s3))

  return { asyncGetObject, asyncUploadS3, deleteObjectsS3 }
}

const getPackageName = ({ manuscript, isEQA, isManuscriptRejected }) => {
  const journalCode = get(manuscript, 'journal.code', '').toUpperCase()
  const manuscriptStatus = isManuscriptRejected ? 'REJECTED' : 'ACCEPTED'
  const manuscriptVersion = get(manuscript, 'version')
  const manuscriptCustomId = get(manuscript, 'customId')
  return isEQA
    ? `${manuscriptStatus}_${journalCode}-${manuscriptCustomId}.${manuscriptVersion}`
    : `${journalCode}-${manuscriptCustomId}`
}

const getFiles = manuscript => {
  const { files = [], reviews = [] } = manuscript
  const figureFiles = files.filter(file => file.type === 'figure')
  const supplementalFiles = files.filter(file => file.type === 'supplementary')
  const manuscriptAndCoverLetterFiles = files.filter(file =>
    ['manuscript', 'coverLetter'].includes(file.type),
  )
  const reviewFiles = chain(reviews)
    .flatMap(review => review.comments)
    .filter(comm => comm.type === 'public' && comm.files.length > 0)
    .flatMap(comm => comm.files)
    .value()

  if (isEmpty(files)) throw new Error('Failed to create a package')

  return {
    reviewFiles,
    figureFiles,
    supplementalFiles,
    manuscriptAndCoverLetterFiles,
  }
}

const transformFiles = ({ files, asyncGetObject }) =>
  Promise.all(
    files.map(async file => {
      const s3File = await asyncGetObject({ Key: file.providerKey })
      return {
        ...s3File,
        fileName: file.fileName,
      }
    }),
  )

const createArchive = async ({
  xmlFile,
  mainS3Files,
  packageName,
  figureS3Files,
  supplementalS3Files,
  archiver = nodeArchiver,
}) => {
  await archiveFiles({
    archiver,
    s3Files: figureS3Files,
    archiveName: 'figure.zip',
  })
  await archiveFiles({
    archiver,
    s3Files: supplementalS3Files,
    archiveName: 'Supplementary Materials.zip',
  })

  const packageOutput = fs.createWriteStream(`${packageName}.zip`)
  const archive = archiver('zip')

  archive.pipe(packageOutput)
  archive.append(xmlFile.content, { name: xmlFile.name })

  mainS3Files.forEach(f => {
    const name = get(f, 'fileName')
    archive.append(f.Body, {
      name,
    })
  })

  if (figureS3Files.length > 0) {
    const figureReadStream = fs.createReadStream('figure.zip')
    archive.append(figureReadStream, { name: 'figure.zip' })
  }
  if (supplementalS3Files.length > 0) {
    const supplementalReadStream = fs.createReadStream(
      'Supplementary Materials.zip',
    )
    archive.append(supplementalReadStream, {
      name: 'Supplementary Materials.zip',
    })
  }

  archive.on('end', err => {
    if (err) {
      logger.debug(err)
      throw new Error('Archive failed to create')
    }
    logger.debug(`MTS archive created ${packageName} - archive stream ended`)
    deleteLocalFile('figure.zip')
    deleteLocalFile('Supplementary Materials.zip')
  })

  packageOutput.on('close', () => {
    logger.debug(
      `MTS archive created ${packageName} - ${archive.pointer()} bytes written to zip file`,
    )
  })

  await archive.finalize()
}

const archiveFiles = async ({
  archiver = nodeArchiver,
  s3Files,
  archiveName,
}) => {
  if (s3Files.length === 0) return

  const packageOutput = fs.createWriteStream(archiveName)
  const archive = archiver('zip')
  archive.pipe(packageOutput)

  s3Files.forEach(f => {
    const name = get(f, 'fileName')
    archive.append(f.Body, { name })
  })

  archive.on('end', err => {
    if (err) {
      logger.debug(err)
      throw new Error('Archive failed to create')
    }
    logger.debug('MTS figure archive created - archive stream ended')
  })
  archive.on('close', () => {
    logger.debug(
      `MTS archive created - ${archive.pointer()} bytes written to zip file`,
    )
  })
  await archive.finalize()
}

const createFilesPackage = async ({
  xmlFile,
  s3Config,
  manuscript,
  isEQA,
  isManuscriptRejected,
  archiver = nodeArchiver,
}) => {
  const { asyncGetObject } = initializeS3Operations(s3Config)
  const packageName = getPackageName({
    manuscript,
    isEQA,
    isManuscriptRejected,
  })
  const {
    reviewFiles,
    figureFiles,
    supplementalFiles,
    manuscriptAndCoverLetterFiles,
  } = getFiles(manuscript)
  const mainFiles = [...manuscriptAndCoverLetterFiles, ...reviewFiles]

  const mainS3Files = await transformFiles({ files: mainFiles, asyncGetObject })
  const supplementalS3Files = await transformFiles({
    files: supplementalFiles,
    asyncGetObject,
  })
  const figureS3Files = await transformFiles({
    files: figureFiles,
    asyncGetObject,
  })

  await createArchive({
    xmlFile,
    mainS3Files,
    packageName,
    figureS3Files,
    supplementalS3Files,
  })
}

const uploadFiles = async ({
  isEQA,
  config,
  s3Config,
  manuscript,
  isForProduction,
  productionFTPConfig,
  isManuscriptRejected,
}) => {
  const fileName = `${getPackageName({
    manuscript,
    isEQA,
    isManuscriptRejected,
  })}.zip`
  const data = await readFile(fileName)
  const { asyncUploadS3 } = initializeS3Operations(s3Config)
  const params = {
    Body: data,
    Key: `mts/${fileName}`,
  }

  return asyncUploadS3(params)
    .then(() => {
      logger.debug(`Successfully uploaded ${fileName} to S3`)
      return uploadFTP({ fileName, config })
    })
    .then(() => {
      if (isForProduction) {
        logger.debug(`Successfully uploaded ${fileName} to second FTP`)
        return uploadFTP({ fileName, config: productionFTPConfig })
      }
    })
    .then(() => {
      logger.debug(`Successfully uploaded ${fileName} to FTP`)
      deleteLocalFile(fileName)
    })
    .catch(err => {
      deleteLocalFile(fileName)
      logger.debug('Upload file failed.')
      throw err
    })
}

const uploadFTP = ({ fileName, config }) => {
  const ftpDeploy = new FtpDeploy()
  const configs = {
    ...config,
    include: [fileName],
  }
  return ftpDeploy.deploy(configs)
}

const deleteFilesS3 = async ({ fileKeys, s3Config }) => {
  const params = {
    Delete: {
      Objects: fileKeys.map(file => ({ Key: file })),
    },
  }
  const { deleteObjectsS3 } = initializeS3Operations(s3Config)
  return deleteObjectsS3(params)
}

const deleteLocalFile = fileName => {
  fs.access(fileName, fs.constants.F_OK, err => {
    if (!err) {
      fs.unlink(fileName, err => {
        logger.info(`Deleted ${fileName}`)
        if (err) throw err
      })
    }
  })
}

module.exports = {
  getFiles,
  uploadFiles,
  deleteFilesS3,
  transformFiles,
  getPackageName,
  createFilesPackage,
  initializeS3Operations,
}
