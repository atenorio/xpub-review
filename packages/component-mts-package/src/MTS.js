const { hasRequiredEnvVariables } = require('component-env')
const config = require('config')
const { get } = require('lodash')
const logger = require('@pubsweet/logger')

const PackageManager = require('./PackageManager')

const defaultS3Config = config.get('pubsweet-component-aws-s3')
const MTSConfig = config.get('mts-service')
const productionFTP = config.get('production-ftp')

const { convertToXML, composeJson, getRemoteFolder } = require('./helpers')

module.exports = {
  async sendPackage({
    manuscript = {},
    isEQA = false,
    isManuscriptRejected = false,
    s3Config = defaultS3Config,
    ftpConfig = MTSConfig.ftp,
    productionFTPConfig = productionFTP.ftp,
    options = MTSConfig.xml,
    isForProduction = false,
  }) {
    if (!hasRequiredEnvVariables(Object.values(ftpConfig))) {
      logger.warn('No env variables for MTS package. Skipping sendPackage...')
      return
    }
    if (!hasRequiredEnvVariables(Object.values(productionFTPConfig))) {
      logger.warn(
        'No env variables for production FTP package. Skipping sendPackage to second FTP...',
      )
      return
    }
    if (!manuscript.section && !get(manuscript, 'specialIssue.section')) {
      logger.info('No section found. Skipping section for MTS package...')
    }
    if (!manuscript.specialIssue) {
      logger.info(
        'No special issue found. Skipping special issue for MTS package...',
      )
    }
    const { journal, articleType, files, teams } = manuscript
    const input = {
      journal,
      articleType,
      files,
      teams,
    }
    Object.keys(input).forEach(key => {
      if (!input[key])
        throw new ValidationError(`Error: ${key} property is required.`)
    })

    const journalConfig = {
      ...MTSConfig.journal,
      articleType,
      journalIdPublisher: journal.code,
      email: journal.email,
      journalTitle: journal.name,
      issn: journal.issn,
      prefix: `${journal.code}-`,
    }
    const composedJson = composeJson({
      isEQA,
      config: journalConfig,
      options,
      manuscript,
    })

    const xmlFile = convertToXML({
      options,
      json: composedJson,
      prefix: journalConfig.prefix,
    })
    const remoteRoot = getRemoteFolder({
      isEQA,
      isManuscriptRejected,
      manuscript,
    })
    await PackageManager.createFilesPackage({
      isEQA,
      xmlFile,
      s3Config,
      manuscript,
      isManuscriptRejected,
    })
    await PackageManager.uploadFiles({
      isEQA,
      isManuscriptRejected,
      isForProduction,
      s3Config,
      manuscript,
      config: {
        ...ftpConfig,
        remoteRoot,
      },
      productionFTPConfig: {
        ...productionFTPConfig,
        remoteRoot,
      },
    })
  },
}
