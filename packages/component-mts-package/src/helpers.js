const convert = require('xml-js')
const { get, find, pick } = require('lodash')

const { getJsonTemplate } = require('./getJsonTemplate')

const {
  setFiles,
  setCounts,
  setHistory,
  setMetadata,
  setContacts,
  setReviewers,
  createFileName,
  setContributors,
  setDeclarations,
  setEmptyReviewers,
} = require('./templateSetters')

module.exports = {
  convertToXML: ({ json = {}, options, prefix }) => {
    const content = convert.json2xml(json, options)
    const customId = get(
      json,
      'article.front.article-meta.article-id[0]._text',
      createFileName({ prefix }),
    )
    const name = `${customId}.xml`
    return {
      name,
      content,
    }
  },
  composeJson: ({ config, options, isEQA = false, manuscript = {} }) => {
    const {
      teams = [],
      files = [],
      publicationDates = [],
      reviews = [],
      isPostAcceptance,
      updated,
    } = manuscript
    const journalTeams = manuscript.journal.teams

    const meta = pick(manuscript, [
      'title',
      'abstract',
      'articleType',
      'specialIssue',
      'version',
      'section',
    ])

    const submitted = get(
      find(publicationDates, { type: 'technicalChecks' }),
      'date',
      Date.now(),
    )
    const finalRevision = get(find(updated), 'date', Date.now())

    const jsonTemplate = getJsonTemplate(config)
    const fileName = createFileName({
      id: manuscript.customId,
      prefix: config.prefix,
    })

    let composedJson = {
      ...jsonTemplate,
      ...setMetadata({
        meta,
        options,
        fileName,
        jsonTemplate,
      }),
      ...setContributors(teams, jsonTemplate),
      ...setHistory(submitted, finalRevision, jsonTemplate),
      ...setFiles(files, jsonTemplate),
      ...setDeclarations(manuscript, jsonTemplate),
      ...setEmptyReviewers(jsonTemplate),
    }

    if (isEQA) {
      const mtsReviews = reviews.filter(rev => rev.submitted)
      const authorTeam = teams.find(t => t.role === 'author')

      composedJson = setReviewers({
        jsonTemplate,
        reviews: mtsReviews,
        authorsLength: authorTeam.members.length,
      })
    }
    if (isPostAcceptance) {
      composedJson = {
        ...setCounts(jsonTemplate),
        ...setContacts(journalTeams, jsonTemplate),
      }
    }
    return composedJson
  },
  getRemoteFolder: ({ isEQA, isManuscriptRejected, manuscript }) => {
    const remoteRoot = `/${manuscript.journal.code}`

    if (!isEQA && !isManuscriptRejected) return remoteRoot

    return isManuscriptRejected
      ? `${remoteRoot}/_Rejected`
      : `${remoteRoot}/_Accepted`
  },
}
