const HindawiBaseModel = require('../src/hindawiBaseModel')
const Team = require('./team')
const Identity = require('./identity')

class User extends HindawiBaseModel {
  static get tableName() {
    return 'user'
  }

  static get schema() {
    return {
      properties: {
        defaultIdentity: { type: 'string' },
        isActive: { type: 'boolean' },
        isSubscribedToEmails: { type: 'boolean', default: true },
        confirmationToken: { type: ['string', 'null'], format: 'uuid' },
        invitationToken: { type: ['string', 'null'], format: 'uuid' },
        passwordResetToken: { type: ['string', 'null'], format: 'uuid' },
        unsubscribeToken: { type: ['string', 'null'], format: 'uuid' },
        agreeTc: { type: 'boolean' },
        passwordResetTimestamp: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      identities: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Identity,
        join: {
          from: 'user.id',
          to: 'identity.userId',
        },
      },
      teamMemberships: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'user.id',
          to: 'team_member.userId',
        },
      },
    }
  }

  static async findAllWithDefaultIdentity({
    page = 0,
    pageSize = 20,
    orderBy = 'givenNames',
    order = 'asc',
  }) {
    return this.query()
      .select([
        'user.id',
        'isActive',
        'identities.surname',
        'givenNames',
        'aff',
        'isConfirmed',
        'email',
      ])
      .leftJoin(
        Identity.query()
          .where('type', 'local')
          .as('identities'),
        'User.id',
        'identities.userId',
      )
      .orderBy(orderBy, order)
      .page(page, pageSize)
  }

  getDefaultIdentity() {
    if (!this.identities) {
      throw new Error('Identities are required.')
    }

    return this.identities.find(
      identity => identity.type === this.defaultIdentity,
    )
  }

  assignIdentity(identity) {
    this.identities = this.identities || []
    this.identities.push(identity)
  }

  toDTO() {
    return {
      ...this,
      identities:
        this.identities && this.identities.map(identity => identity.toDTO()),
    }
  }

  getTeamMemberByRole(role) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }

    return this.teamMemberships.find(tm => tm.team.role === role)
  }

  getTeamMemberForManuscript(manuscript) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }

    const author = this.teamMemberships.find(
      tm =>
        tm.team.role === Team.Role.author &&
        tm.team.manuscriptId === manuscript.id,
    )

    return (
      author ||
      this.teamMemberships.find(
        member =>
          member.team.manuscriptId === manuscript.id ||
          member.team.journalId === manuscript.journalId ||
          member.team.role === Team.Role.admin ||
          member.team.role === Team.Role.editorialAssistant ||
          member.team.role === Team.Role.researchIntegrityPublishingEditor,
      )
    )
  }

  getTeamMemberForJournal(journal) {
    if (!this.teamMemberships) {
      throw new Error('User team memberships are required.')
    }
    return this.teamMemberships.find(
      member =>
        member.team.journalId === journal.id ||
        member.team.role === Team.Role.admin,
    )
  }
}

module.exports = User
