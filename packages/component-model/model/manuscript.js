const uuid = require('uuid')
const config = require('config')
const { chain, get, pick, sortBy, groupBy, last } = require('lodash')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const Team = require('./team')
const Review = require('./review')

class Manuscript extends HindawiBaseModel {
  static get tableName() {
    return 'manuscript'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        submissionId: { type: 'string', format: 'uuid' },
        status: {
          enum: Object.values(Manuscript.Statuses),
          default: Manuscript.Statuses.draft,
        },
        customId: { type: ['string', 'null'] },
        version: { type: 'number', default: 1 },
        title: { type: 'string', default: '' },
        abstract: { type: 'string', default: '' },
        publicationDates: { type: ['array', 'null'], default: [] },
        technicalCheckToken: { type: ['string', 'null'], format: 'uuid' },
        hasPassedEqa: { type: ['boolean', 'null'] },
        hasPassedEqs: { type: ['boolean', 'null'] },
        journalId: { type: ['string', null], format: 'uuid' },
        sectionId: { type: ['string', null], format: 'uuid' },
        agreeTc: { type: 'boolean', default: false },
        conflictOfInterest: { type: ['string', 'null'] },
        dataAvailability: { type: ['string', 'null'] },
        fundingStatement: { type: ['string', 'null'] },
        articleTypeId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
        isPostAcceptance: { type: 'boolean', default: false },
      },
    }
  }

  static get relationMappings() {
    return {
      files: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./file'),
        join: {
          from: 'manuscript.id',
          to: 'file.manuscriptId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'manuscript.id',
          to: 'team.manuscriptId',
        },
      },
      reviews: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./review'),
        join: {
          from: 'manuscript.id',
          to: 'review.manuscriptId',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'manuscript.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'manuscript.sectionId',
          to: 'section.id',
        },
      },
      logs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./auditLog'),
        join: {
          from: 'manuscript.id',
          to: 'audit_log.manuscriptId',
        },
      },
      reviewerSuggestions: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./reviewerSuggestion'),
        join: {
          from: 'manuscript.id',
          to: 'reviewer_suggestion.manuscriptId',
        },
      },
      articleType: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./articleType'),
        join: {
          from: 'manuscript.articleTypeId',
          to: 'article_type.id',
        },
      },
      specialIssue: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'manuscript.specialIssueId',
          to: 'special_issue.id',
        },
      },
    }
  }

  static get Statuses() {
    return {
      draft: 'draft',
      technicalChecks: 'technicalChecks',
      submitted: 'submitted',
      academicEditorInvited: 'academicEditorInvited',
      academicEditorAssigned: 'academicEditorAssigned',
      reviewersInvited: 'reviewersInvited',
      underReview: 'underReview',
      reviewCompleted: 'reviewCompleted',
      revisionRequested: 'revisionRequested',
      pendingApproval: 'pendingApproval',
      rejected: 'rejected',
      inQA: 'inQA',
      accepted: 'accepted',
      withdrawalRequested: 'withdrawalRequested',
      withdrawn: 'withdrawn',
      deleted: 'deleted',
      published: 'published',
      olderVersion: 'olderVersion',
      academicEditorAssignedEditorialType:
        'academicEditorAssignedEditorialType',
      makeDecision: 'makeDecision',
      qualityChecksRequested: 'qualityChecksRequested',
      qualityChecksSubmitted: 'qualityChecksSubmitted',
      refusedToConsider: 'refusedToConsider',
    }
  }

  static get NonRejectableStatuses() {
    const statuses = this.Statuses
    return [
      statuses.draft,
      statuses.deleted,
      statuses.rejected,
      statuses.inQA,
      statuses.accepted,
      statuses.qualityChecksRequested,
      statuses.qualityChecksSubmitted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
      statuses.refusedToConsider,
    ]
  }
  static get InProgressStatuses() {
    const statuses = this.Statuses
    return [
      statuses.submitted,
      statuses.underReview,
      statuses.makeDecision,
      statuses.reviewCompleted,
      statuses.pendingApproval,
      statuses.reviewersInvited,
      statuses.revisionRequested,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
    ]
  }

  static filterOlderVersions(manuscripts) {
    const submissions = groupBy(manuscripts, 'submissionId')
    const latestVersions = Object.values(submissions).map(versions => {
      if (versions.length === 1) {
        return versions[0]
      }

      const sortedVersions = sortBy(versions, 'version')
      const latestManuscript = last(sortedVersions)

      if (latestManuscript.status === this.Statuses.draft) {
        return sortedVersions[sortedVersions.length - 2]
      }

      return latestManuscript
    })

    return latestVersions
  }

  static async findManuscriptByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('m.*')
        .from('manuscript AS m')
        .join('team AS t', 'm.id', 't.manuscript_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findManuscriptsBySubmissionId({
    order,
    orderByField,
    submissionId,
    excludedStatus,
    eagerLoadRelations,
  }) {
    return this.query()
      .skipUndefined()
      .whereNot({ status: excludedStatus })
      .andWhere({ submissionId })
      .orderBy(orderByField, order)
      .eager(Manuscript._parseEagerRelations(eagerLoadRelations))
  }

  static async findLastManuscriptBySubmissionId({
    submissionId,
    eagerLoadRelations,
  }) {
    return this.query()
      .andWhere({ submissionId })
      .orderBy('version', 'desc')
      .first()
      .eager(Manuscript._parseEagerRelations(eagerLoadRelations))
  }

  async getEditorLabel({ SpecialIssue, Journal, role }) {
    if (this.specialIssueId) {
      const specialIssue = await SpecialIssue.find(
        this.specialIssueId,
        'peerReviewModel',
      )
      return specialIssue.peerReviewModel[`${role}Label`]
    }
    const journal = await Journal.find(this.journalId, 'peerReviewModel')
    return journal.peerReviewModel[`${role}Label`]
  }

  getSubmittingAuthor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam) {
      throw new Error('Could not find author team')
    }

    if (authorTeam.members.length === 0) {
      throw new Error('Members are required.')
    }

    return authorTeam.members.find(tm => tm.isSubmitting)
  }

  getAuthors() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')

    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return sortBy(authorTeam.members, 'position')
  }

  getReviewers() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members
  }

  getReviewersForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam || reviewerTeam.members.length === 0) {
      return []
    }

    return reviewerTeam.members.map(reviewer => {
      const reviewerData = pick(reviewer, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'responded',
      ])

      return {
        ...reviewerData,
        ...reviewer.alias,
      }
    })
  }

  getAuthorsForEventData() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const authorTeam = this.teams.find(t => t.role === 'author')
    if (!authorTeam || authorTeam.members.length === 0) {
      return []
    }

    return authorTeam.members.map(author => {
      const authorData = pick(author, [
        'id',
        'created',
        'updated',
        'userId',
        'status',
        'position',
        'isSubmitting',
        'isCorresponding',
      ])

      return {
        ...authorData,
        ...author.alias,
      }
    })
  }

  addReviewers(reviewers) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    let reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (reviewerTeam && !reviewerTeam.members) {
      throw new Error('Members are required.')
    }

    if (!reviewerTeam) {
      reviewerTeam = new Team({
        manuscriptId: this.id,
        role: 'reviewer',
      })

      this.teams.push(reviewerTeam)
    }

    reviewers.forEach(reviewer => {
      if (!reviewer.user) {
        throw new Error('User is required.')
      }
      reviewerTeam.addMember(reviewer.user, {
        status: 'pending',
        reviewerNumber: reviewer.reviewerNumber,
        alias: reviewer.alias,
      })
    })
  }

  hasAcademicEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    return !!chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()
  }

  getAcademicEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return academicEditor
  }

  getPendingAcademicEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === 'pending')
      .value()

    return academicEditor
  }

  getRIPE() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const RIPE = chain(this.teams)
      .find(t => t.role === 'researchIntegrityPublishingEditor')
      .get('members')
      .find(member => member.status === 'accepted')
      .value()

    return RIPE
  }

  _getTriageEditor() {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const triageEditorTeam = this.teams.find(
      team => team.role === 'triageEditor',
    )

    if (!triageEditorTeam) {
      return
    }

    if (!triageEditorTeam.members) {
      throw new Error('Members are required.')
    }

    const triageEditor = triageEditorTeam.members.find(
      member => member.status === 'active',
    )

    return triageEditor
  }

  getAcademicEditorByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }

    const academicEditor = chain(this.teams)
      .find(t => t.role === 'academicEditor')
      .get('members')
      .find(member => member.status === status)
      .value()

    return academicEditor
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  assignFile(file) {
    this.files = this.files || []
    this.files.push(file)
  }

  assignReview(review) {
    this.reviews = this.reviews || []
    this.reviews.push(review)
  }

  submitManuscript() {
    this.status = Manuscript.Statuses.technicalChecks
    this.technicalCheckToken = uuid.v4()
    this.publicationDates.push({
      type: Manuscript.Statuses.technicalChecks,
      date: Date.now(),
    })
  }

  updateStatus(status) {
    this.status = status
    return this
  }

  setComment() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const responseToRevisionRequest = this.reviews.find(
      review =>
        review.recommendation === Review.Recommendations.responseToRevision,
    )
    if (!responseToRevisionRequest) {
      throw new ValidationError('There has been no request to revision')
    }

    this.comment = responseToRevisionRequest.comments[0].toDTO()
  }

  getLatestEditorReview() {
    if (!this.reviews) {
      throw new Error('Reviews are required.')
    }

    const editorReviews = this.reviews.filter(review =>
      ['admin', 'triageEditor', 'academicEditor'].includes(
        review.member.team.role,
      ),
    )

    return last(sortBy(editorReviews, 'updated'))
  }

  getLatestAcademicEditorRecommendation() {
    const editorReviews = this.reviews.filter(
      review => get(review, 'member.team.role') === 'academicEditor',
    )
    return last(sortBy(editorReviews, 'updated'))
  }

  getReviewersByStatus(status) {
    if (!this.teams) {
      throw new Error('Teams are required.')
    }
    const reviewerTeam = this.teams.find(t => t.role === 'reviewer')
    if (!reviewerTeam.members.length === 0) {
      throw new Error('Members are required.')
    }
    return reviewerTeam.members.filter(m => m.status === status)
  }

  getCustomId() {
    return this.role === 'author' &&
      [Manuscript.Statuses.draft, Manuscript.Statuses.technicalChecks].includes(
        this.status,
      )
      ? undefined
      : this.customId
  }

  getSortedFiles() {
    return chain(this.files)
      .map(f => ({ ...f, filename: f.fileName }))
      .sortBy('type', 'position')
      .value()
  }

  setVisibleStatus({ userId, TeamRole, TeamMemberStatuses }) {
    let reviewerTeam
    let pendingReviewer
    let acceptedReviewer
    let academicEditorTeam
    let pendingAcademicEditor

    if (this.teams) {
      reviewerTeam = this.teams.find(t => t.role === TeamRole.reviewer)
      academicEditorTeam = this.teams.find(
        t => t.role === TeamRole.academicEditor,
      )
    }
    if (academicEditorTeam) {
      pendingAcademicEditor = academicEditorTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
    }
    if (pendingAcademicEditor) {
      this.visibleStatus = get(
        config.get('statuses'),
        'academicEditorInvited.academicEditor.label',
      )
      return
    }
    if (reviewerTeam) {
      pendingReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.pending && tm.userId === userId,
      )
      acceptedReviewer = reviewerTeam.members.find(
        tm => tm.status === TeamMemberStatuses.accepted && tm.userId === userId,
      )
    }
    if (pendingReviewer) {
      this.visibleStatus = get(
        config.get('statuses'),
        'reviewersInvited.reviewer.label',
      )
      return
    }
    if (
      acceptedReviewer &&
      this.status === Manuscript.Statuses.reviewCompleted
    ) {
      this.visibleStatus = get(
        config.get('statuses'),
        'underReview.reviewer.label',
      )
      return
    }
    if (this.status !== Manuscript.Statuses.olderVersion) {
      this.visibleStatus = get(
        config.get('statuses'),
        `${this.status}.${this.role}.label`,
      )
    } else {
      this.visibleStatus = 'Viewing An Older Version'
    }
  }

  setFilterProperties() {
    if (this.status === Manuscript.Statuses.olderVersion) {
      return true
    }
    this.needsAttention = get(
      config.get('statuses'),
      `${this.status}.${this.role}.needsAttention`,
    )
    this.inProgress = get(
      config.get('statuses'),
      `${this.status}.${this.role}.inProgress`,
    )
  }

  toDTO() {
    this.setFilterProperties()

    const manuscript = pick(this, [
      'id',
      'role',
      'status',
      'teams',
      'files',
      'created',
      'updated',
      'version',
      'reviews',
      'journal',
      'comment',
      'journalId',
      'inProgress',
      'needsAttention',
      'visibleStatus',
      'submissionId',
      'hasPassedEQS',
      'hasPassedEQA',
      'triageEditor',
      'technicalCheckToken',
      'isApprovalEditor',
    ])

    const meta = pick(this, [
      'title',
      'agreeTc',
      'abstract',
      'publicationDates',
      'dataAvailability',
      'fundingStatement',
      'conflictOfInterest',
      'articleTypeId',
    ])

    let teamsData = {
      authors: [],
      reviewers: [],
      teams: [],
    }
    let researchIntegrityPublishingEditor
    let triageEditor

    if (this.teams) {
      const academicEditor = !this.academicEditor && this.getAcademicEditor()
      const pendingAcademicEditor =
        !this.pendingAcademicEditor && this.getPendingAcademicEditor()

      teamsData = {
        authors: this.getAuthors().map(author => author.toDTO()),
        reviewers: this.getReviewers().map(reviewer => reviewer.toDTO()),
        academicEditor: academicEditor && academicEditor.toDTO(),
        pendingAcademicEditor:
          pendingAcademicEditor && pendingAcademicEditor.toDTO(),
        teams: this.teams.map(team => team.toDTO()),
      }

      researchIntegrityPublishingEditor = this.getRIPE()
      triageEditor = this._getTriageEditor()
    }

    return {
      ...manuscript,
      ...teamsData,
      meta,
      files: this.getSortedFiles(),
      customId: this.getCustomId(),
      journal: this.journal ? this.journal.toDTO() : undefined,
      section: this.section ? this.section.toDTO() : undefined,
      triageEditor: triageEditor ? triageEditor.toDTO() : undefined,
      articleType: this.articleType ? this.articleType.toDTO() : undefined,
      reviews: this.reviews ? this.reviews.map(review => review.toDTO()) : [],
      specialIssue: this.specialIssue ? this.specialIssue.toDTO() : undefined,
      researchIntegrityPublishingEditor: researchIntegrityPublishingEditor
        ? researchIntegrityPublishingEditor.toDTO()
        : undefined,
    }
  }
}

module.exports = Manuscript
