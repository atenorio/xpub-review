const HindawiBaseModel = require('../src/hindawiBaseModel')

const TeamMember = require('./teamMember')

class Team extends HindawiBaseModel {
  static get tableName() {
    return 'team'
  }

  static get schema() {
    return {
      properties: {
        role: {
          type: 'string',
          enum: Object.values(Team.Role),
          default: null,
        },
        manuscriptId: { type: ['string', 'null'] },
        journalId: { type: ['string', 'null'] },
        sectionId: { type: ['string', 'null'] },
        specialIssueId: { type: ['string', 'null'] },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'team.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'team.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'team.sectionId',
          to: 'section.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'team.specialIssueId',
          to: 'special_issue.id',
        },
      },
      members: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'team.id',
          to: 'team_member.teamId',
        },
      },
    }
  }

  static async findAllBy({ role, manuscriptId, submissionId }) {
    try {
      const results = await this.query()
        .skipUndefined()
        .select('t.*')
        .from('team AS t')
        .leftJoin('manuscript AS m', 't.manuscript_id', 'm.id')
        .where('t.role', role)
        .andWhere('m.id', manuscriptId)
        .andWhere('m.submissionId', submissionId)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByUserAndJournalId({ userId, journalId }) {
    try {
      const results = await this.query()
        .select('t.*')
        .from('team AS t')
        .join('journal AS j', 't.journal_id', 'j.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('j.id', journalId)

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findOneByManuscriptId({ userId, manuscriptId }) {
    try {
      const result = await this.query()
        .select('t.*')
        .from('team AS t')
        .join('manuscript AS m', 't.manuscript_id', 'm.id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.user_id', userId)
        .andWhere('t.manuscript_id', manuscriptId)

      return result[0]
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllByJournal({ journalId }) {
    try {
      return await this.query()
        .from('team AS t')
        .join('journal AS j', 't.journal_id', 'j.id')
        .where('j.id', journalId)
        .andWhere('t.special_issue_id', null)
        .eager('members')
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySection({ sectionId }) {
    try {
      return await this.query()
        .select('t.*', 's.name AS sectionName')
        .from('team AS t')
        .join('section AS s', 't.section_id', 's.id')
        .where('s.id', sectionId)
        .eager('members')
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllBySpecialIssue({ specialIssueId }) {
    try {
      return await this.query()
        .select('t.*', 'si.name AS specialIssueName')
        .from('team AS t')
        .join('special_issue AS si', 't.special_issue_id', 'si.id')
        .where('si.id', specialIssueId)
        .eager('members')
    } catch (e) {
      throw new Error(e)
    }
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      triageEditor: 'triageEditor',
      reviewer: 'reviewer',
      academicEditor: 'academicEditor',
      editorialAssistant: 'editorialAssistant',
      researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
    }
  }

  static get JournalRoles() {
    return [
      this.Role.triageEditor,
      this.Role.academicEditor,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  static get StaffRoles() {
    return [
      this.Role.admin,
      this.Role.editorialAssistant,
      this.Role.researchIntegrityPublishingEditor,
    ]
  }

  addMember(user, invitationOptions) {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    if (existingMember) {
      const defaultIdentity = user.getDefaultIdentity()
      if (existingMember.status === TeamMember.Statuses.removed) {
        throw new Error(
          `${this.role} invitation for ${defaultIdentity.email} was removed and can't be invited again`,
        )
      }
      if (existingMember.status === TeamMember.Statuses.expired) {
        existingMember.updateProperties({
          status: TeamMember.Statuses.pending,
        })
        return existingMember
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...invitationOptions,
      userId: user.id,
      teamId: this.id,
      position: this.members.length,
    })
    newMember.linkUser(user)

    this.members.push(newMember)

    return newMember
  }

  toDTO() {
    const object = this.manuscript || this.journal

    return {
      ...this,
      object: object ? object.toDTO() : undefined,
      objectType: this.manuscriptId ? 'manuscript' : 'journal',
      members: this.members ? this.members.map(m => m.toDTO()) : undefined,
    }
  }

  removeMember(memberId) {
    this.members = this.members || []

    const member = this.members.find(member => member.id === memberId)
    if (!member) {
      throw new NotFoundError(
        `The specified user is not invited as a ${this.role}`,
      )
    }
    if (member.isSubmitting) {
      throw new ValidationError(
        `Submitting authors can't be deleted from the team`,
      )
    }

    this.members = this.members.filter(member => member.id !== memberId)
  }
}

module.exports = Team
