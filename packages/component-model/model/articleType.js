const HindawiBaseModel = require('../src/hindawiBaseModel')

class ArticleType extends HindawiBaseModel {
  static get tableName() {
    return 'article_type'
  }

  static get schema() {
    return {
      properties: {
        name: { type: 'string' },
        hasPeerReview: { type: 'boolean' },
      },
    }
  }

  static get relationMappings() {
    return {
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalArticleType'),
        join: {
          from: 'article_type.id',
          to: 'journal_article_type.articleTypeId',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'article_type.id',
          to: 'manuscript.articleTypeId',
        },
      },
    }
  }

  static async findArticleTypeByManuscript(manuscriptId) {
    try {
      const results = await this.query()
        .select('at.*')
        .from('article_type AS at')
        .join('manuscript AS m', 'at.id', 'm.article_type_id')
        .where('m.id', manuscriptId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  static get Types() {
    return {
      editorial: 'Editorial',
      retraction: 'Retraction',
      caseSeries: 'Case Series',
      caseReport: 'Case Report',
      reviewArticle: 'Review Article',
      researchArticle: 'Research Article',
      letterToTheEditor: 'Letter to the Editor',
      expressionOfConcern: 'Expression of Concern',
      erratum: 'Erratum',
      corrigendum: 'Corrigendum',
    }
  }

  static get TypesWithRIPE() {
    return [
      this.Types.retraction,
      this.Types.expressionOfConcern,
      this.Types.erratum,
      this.Types.corrigendum,
    ]
  }

  static get EditorialTypes() {
    return [this.Types.editorial]
  }

  toDTO() {
    return {
      ...this,
    }
  }
}

module.exports = ArticleType
