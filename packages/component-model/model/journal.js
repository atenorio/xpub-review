const { chain } = require('lodash')
const HindawiBaseModel = require('../src/hindawiBaseModel')
const Team = require('./team')

class Journal extends HindawiBaseModel {
  static get tableName() {
    return 'journal'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        publisherName: { type: ['string', null] },
        issn: { type: ['string', null] },
        code: { type: 'string' },
        email: { type: 'string' },
        apc: { type: ['integer'] },
        isActive: { type: ['boolean', false] },
        activationDate: {
          type: ['string', 'null', 'object'],
          format: 'date-time',
        },
        peerReviewModelId: { type: ['string', null], format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'journal.id',
          to: 'manuscript.journalId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: Team,
        join: {
          from: 'journal.id',
          to: 'team.journalId',
        },
      },
      journalArticleTypes: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./journalArticleType'),
        join: {
          from: 'journal.id',
          to: 'journal_article_type.journalId',
        },
      },
      jobs: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./job'),
        join: {
          from: 'journal.id',
          to: 'job.journalId',
        },
      },
      sections: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./section'),
        join: {
          from: 'journal.id',
          to: 'section.journalId',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./peerReviewModel'),
        join: {
          from: 'journal.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'journal.id',
          to: 'special_issue.journalId',
        },
      },
    }
  }

  static async findJournalByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('j.*')
        .from('journal AS j')
        .join('team AS t', 'j.id', 't.journal_id')
        .join('team_member AS tm', 't.id', 'tm.team_id')
        .where('tm.id', teamMemberId)

      if (results.length > 0) return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  $formatDatabaseJson(json) {
    json = super.$formatDatabaseJson(json)
    const { email } = json

    return { ...json, email: email.toLowerCase() }
  }

  getTriageEditor() {
    if (!this.teams) {
      throw new ValidationError('Teams are required')
    }
    const triageEditorTeam = this.teams.find(
      t => t.role === Team.Role.triageEditor,
    )

    return triageEditorTeam
      ? triageEditorTeam.members.find(member => member.status === 'active')
      : undefined
  }

  getCorrespondingEditorialAssistant() {
    if (!this.teams) {
      throw new Error('Teams are required')
    }
    return chain(this.teams)
      .find(t => t.role === Team.Role.editorialAssistant)
      .get('members', [])
      .find(m => m.isCorresponding === true)
      .value()
  }

  assignTeam(team) {
    this.teams = this.teams || []
    this.teams.push(team)
  }

  toDTO() {
    const triageEditorMember = chain(this.teams)
      .find(t => t.role === Team.Role.triageEditor)
      .get('members.0')
      .value()

    return {
      ...this,
      triageEditor: triageEditorMember ? triageEditorMember.toDTO() : undefined,
      articleTypes: this.journalArticleTypes
        ? this.journalArticleTypes
            .map(jat => jat.articleType.toDTO())
            .sort((a, b) => (a.name > b.name ? -1 : 1))
        : [],
      sections: this.sections ? this.sections.map(sec => sec.toDTO()) : [],
      peerReviewModel: this.peerReviewModel && this.peerReviewModel.toDTO(),
      specialIssues: this.specialIssues
        ? this.specialIssues.map(si => si.toDTO())
        : [],
    }
  }
}

module.exports = Journal
