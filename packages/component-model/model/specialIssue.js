const { chain } = require('lodash')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const moment = require('moment-business-days')
const config = require('config')
const Team = require('./team')

class SpecialIssue extends HindawiBaseModel {
  static get tableName() {
    return 'special_issue'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        sectionId: { type: ['string', 'null'], format: 'uuid' },
        peerReviewModelId: { type: ['string', 'null'], format: 'uuid' },
        customId: { type: ['string'] },
        isActive: { type: 'boolean' },
        isCancelled: { type: 'boolean' },
        cancelReason: { type: ['string', 'null'] },
        callForPapers: { type: 'string' },
        startDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
        endDate: {
          type: ['string', 'object'],
          format: 'date-time',
        },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'special_issue.journalId',
          to: 'journal.id',
        },
      },
      section: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./section'),
        join: {
          from: 'special_issue.sectionId',
          to: 'section.id',
        },
      },
      peerReviewModel: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./peerReviewModel'),
        join: {
          from: 'special_issue.peerReviewModelId',
          to: 'peer_review_model.id',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'special_issue.id',
          to: 'team.specialIssueId',
        },
      },
    }
  }

  static async findUnique(name) {
    const objects = await this.query()
      .whereRaw('LOWER(name) LIKE ?', [name.toLowerCase()])
      .limit(1)

    if (!objects.length) {
      return
    }

    return objects[0]
  }
  static async findAllBySection({ sectionId }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('section AS s', 'si.section_id', 's.id')
        .where('s.id', sectionId)
    } catch (e) {
      throw new Error(e)
    }
  }
  static async findAllByJournal({ journalId }) {
    try {
      return await this.query()
        .from('special_issue AS si')
        .join('journal AS j', 'si.journal_id', 'j.id')
        .where('j.id', journalId)
    } catch (e) {
      throw new Error(e)
    }
  }
  toDTO() {
    let leadGuestEditorMember
    if (this.teams) {
      leadGuestEditorMember = chain(this.teams)
        .find(
          t => t.role === Team.Role.triageEditor && t.specialIssueId !== null,
        )
        .get('members.0')
        .value()
    }

    const expirationInterval = config.get('specialIssueExpirationInterval')
    this.expirationDate = moment(this.endDate)
      .add(expirationInterval.value, expirationInterval.timeUnit)
      .toISOString()

    return {
      ...this,
      leadGuestEditor: leadGuestEditorMember
        ? leadGuestEditorMember.toDTO()
        : undefined,
    }
  }
}

module.exports = SpecialIssue
