const HindawiBaseModel = require('../src/hindawiBaseModel')

const {
  jobs: { connectToJobQueue },
} = require('pubsweet-server/src')
const logger = require('@pubsweet/logger')
const config = require('config')

const newJobCheckIntervalSeconds = config.get('newJobCheckIntervalSeconds')

class Job extends HindawiBaseModel {
  static get tableName() {
    return 'job'
  }

  static get schema() {
    return {
      properties: {
        teamMemberId: { type: 'string', format: 'uuid' },
        manuscriptId: { type: ['string', 'null'], format: 'uuid' },
        journalId: { type: ['string', 'null'], format: 'uuid' },
        specialIssueId: { type: ['string', 'null'], format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'job.teamMemberId',
          to: 'team_member.id',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'job.manuscriptId',
          to: 'manuscript.id',
        },
      },
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'job.journalId',
          to: 'journal.id',
        },
      },
    }
  }

  static async schedule({
    params,
    journalId,
    jobHandler,
    teamMemberId,
    manuscriptId,
    executionDate,
    specialIssueId,
  }) {
    const jobQueue = await connectToJobQueue()

    params = JSON.parse(JSON.stringify(params))
    const queueName = 'review'

    try {
      const jobId = await jobQueue.publishAfter(
        queueName,
        params,
        {},
        executionDate,
      )

      const job = new Job({
        id: jobId,
        journalId,
        teamMemberId,
        manuscriptId,
        specialIssueId,
      })
      await job.save()

      await jobQueue.subscribe(
        queueName,
        { teamSize: 50, teamConcurrency: 50, newJobCheckIntervalSeconds },
        jobHandler,
      )
      logger.info(`Successfully scheduled job ${jobId} at: ${executionDate}`)

      return job
    } catch (e) {
      throw new Error(e)
    }
  }

  static async cancel(id) {
    const jobQueue = await connectToJobQueue()

    try {
      await jobQueue.cancel(id)
      logger.info(`Successfully cancelled job ${id}`)
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByTeamMember(teamMemberId) {
    try {
      const results = await this.query()
        .select('j.*')
        .from('job AS j')
        .where('j.team_member_id', teamMemberId)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }

  static async findAllByTeamMembers(teamMemberIds) {
    try {
      const results = await this.query()
        .select('j.*')
        .from('job AS j')
        .whereIn('j.team_member_id', teamMemberIds)

      return results
    } catch (e) {
      throw new Error(e)
    }
  }
}

module.exports = Job
