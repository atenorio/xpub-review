const { isEmpty } = require('lodash')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const Comment = require('./comment')

class Review extends HindawiBaseModel {
  static get tableName() {
    return 'review'
  }

  static get schema() {
    return {
      properties: {
        recommendation: { enum: Object.values(Review.Recommendations) },
        manuscriptId: { type: 'string', format: 'uuid' },
        teamMemberId: { type: 'string', format: 'uuid' },
        submitted: { type: ['string', 'object', 'null'], format: 'date-time' },
      },
    }
  }

  static get relationMappings() {
    return {
      comments: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./comment'),
        join: {
          from: 'review.id',
          to: 'comment.reviewId',
        },
      },
      manuscript: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'review.manuscriptId',
          to: 'manuscript.id',
        },
      },
      member: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./teamMember'),
        join: {
          from: 'review.teamMemberId',
          to: 'team_member.id',
        },
      },
    }
  }

  static get Recommendations() {
    return {
      responseToRevision: 'responseToRevision',
      minor: 'minor',
      major: 'major',
      reject: 'reject',
      publish: 'publish',
      revision: 'revision',
      returnToAcademicEditor: 'returnToAcademicEditor',
    }
  }

  static async findLatestEditorialReview({ manuscriptId, TeamRole }) {
    try {
      const results = await this.query()
        .select('r.*')
        .from('review AS r')
        .join('team_member AS tm', 'tm.id', '=', 'r.team_member_id')
        .join('team AS t', 't.id', '=', 'tm.team_id')
        .where('r.manuscript_id', '=', manuscriptId)
        .andWhere(function andWhereIn() {
          this.whereIn('t.role', [
            TeamRole.admin,
            TeamRole.editorialAssistant,
            TeamRole.triageEditor,
            TeamRole.academicEditor,
          ])
        })
        .orderBy('updated', 'desc')

      return results[0]
    } catch (e) {
      throw new Error(e)
    }
  }

  addComment({ content, type }) {
    this.comments = this.comments || []

    const commentTypeAlreadyExists = this.comments.some(c => c.type === type)
    if (commentTypeAlreadyExists)
      throw new ValidationError('Cannot add multiple comments of the same type')

    const comment = new Comment({
      content,
      type,
    })

    this.comments.push(comment)

    return comment
  }

  setSubmitted(date) {
    this.submitted = date
  }

  stripEmptyPrivateComments() {
    if (!this.comments) {
      throw new ValidationError('Comments are required.')
    }

    const privateComment = this.comments.find(comm => comm.type === 'private')
    if (privateComment && isEmpty(privateComment.content)) {
      this.comments = this.comments.filter(comm => comm.type !== 'private')
    }
  }

  assignMember(teamMember) {
    this.member = teamMember
  }

  toDTO() {
    return {
      ...this,
      comments: this.comments
        ? this.comments.map(comment => comment.toDTO())
        : [],
      member: this.member ? this.member.toDTO() : undefined,
    }
  }
}

module.exports = Review
