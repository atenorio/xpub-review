const { chain } = require('lodash')

const HindawiBaseModel = require('../src/hindawiBaseModel')
const Team = require('./team')

class Section extends HindawiBaseModel {
  static get tableName() {
    return 'section'
  }

  static get schema() {
    return {
      type: 'object',
      properties: {
        name: { type: 'string' },
        journalId: { type: 'string', format: 'uuid' },
      },
    }
  }

  static get relationMappings() {
    return {
      journal: {
        relation: HindawiBaseModel.BelongsToOneRelation,
        modelClass: require('./journal'),
        join: {
          from: 'section.journalId',
          to: 'journal.id',
        },
      },
      manuscripts: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./manuscript'),
        join: {
          from: 'section.id',
          to: 'manuscript.sectionId',
        },
      },
      teams: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./team'),
        join: {
          from: 'section.id',
          to: 'team.sectionId',
        },
      },
      specialIssues: {
        relation: HindawiBaseModel.HasManyRelation,
        modelClass: require('./specialIssue'),
        join: {
          from: 'section.id',
          to: 'special_issue.sectionId',
        },
      },
    }
  }
  static async findAllByJournal({ journalId }) {
    try {
      return await this.query()
        .from('section AS s')
        .join('journal AS j', 's.journal_id', 'j.id')
        .where('j.id', journalId)
    } catch (e) {
      throw new Error(e)
    }
  }
  toDTO() {
    const sectionEditors = chain(this.teams)
      .find(t => t.role === Team.Role.triageEditor)
      .get('members')
      .value()

    return {
      ...this,
      sectionEditors: sectionEditors
        ? sectionEditors.map(member => member.toDTO())
        : [],
      specialIssues: this.specialIssues
        ? this.specialIssues.map(specialIssue => specialIssue.toDTO())
        : [],
    }
  }
}

module.exports = Section
