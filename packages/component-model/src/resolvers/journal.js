const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async getJournal(_, { journalId }, ctx) {
      return useCases.getJournalUseCase.initialize(models).execute(journalId)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
