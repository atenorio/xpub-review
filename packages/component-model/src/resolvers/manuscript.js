const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  Query: {
    async getManuscript(_, { manuscriptId }, ctx) {
      return useCases.getManuscriptUseCase
        .initialize({ models })
        .execute({ manuscriptId, userId: ctx.user })
    },
    async getManuscriptVersions(_, { submissionId }, ctx) {
      return useCases.getManuscriptVersionsUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
    async getDraftRevision(_, { submissionId }, ctx) {
      return useCases.getDraftRevisionUseCase
        .initialize(models)
        .execute({ submissionId, userId: ctx.user })
    },
  },
  Manuscript: {
    async reviews(manuscript, query, ctx) {
      return useCases.getManuscriptReviewsUseCase
        .initialize(models)
        .execute({ manuscript, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
