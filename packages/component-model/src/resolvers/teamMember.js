const models = require('@pubsweet/models')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('../useCases')

const resolvers = {
  TeamMember: {
    async user(parent, query, ctx) {
      return useCases.getTeamMemberUserUseCase
        .initialize(models)
        .execute(parent)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
