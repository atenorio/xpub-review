const resolvers = {
  Identity: {
    __resolveType(obj, context, info) {
      if (obj.identifier) {
        return 'External'
      }
      return 'Local'
    },
  },
}

module.exports = resolvers
