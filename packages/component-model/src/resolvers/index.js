const { withAuthsomeMiddleware } = require('helper-service')

const identityResolvers = require('./identity')
const journalResolvers = require('./journal')
const manuscriptResolvers = require('./manuscript')
const teamMemberResolvers = require('./teamMember')
const userResolvers = require('./user')

const { merge } = require('lodash')
const useCases = require('../useCases')

const mergedResolvers = merge(
  userResolvers,
  identityResolvers,
  journalResolvers,
  manuscriptResolvers,
  teamMemberResolvers,
)

module.exports = withAuthsomeMiddleware(mergedResolvers, useCases)
