const { unionBy, orderBy, isEmpty } = require('lodash')

module.exports = {
  async getUserForAutoAssignmentBasedOnWorkload({
    teamRole,
    journalId,
    sectionId,
    specialIssueId,
    manuscriptStatuses,
    teamMemberStatuses,
    models: { TeamMember },
  }) {
    let teamMembers
    if (specialIssueId) {
      teamMembers = await TeamMember.findAllBySpecialIssueAndRole({
        specialIssueId,
        role: teamRole,
      })
    }

    if (isEmpty(teamMembers) && sectionId) {
      teamMembers = await TeamMember.findAllBySectionAndRole({
        sectionId,
        role: teamRole,
      })
    }

    if (isEmpty(teamMembers) && journalId) {
      teamMembers = await TeamMember.findAllByJournalAndRole({
        journalId,
        role: teamRole,
      })
    }

    if (isEmpty(teamMembers))
      throw new Error('No manuscript parent ID has been provided')

    const usersDefaultWorkload = teamMembers.map(member => ({
      userId: member.userId,
      workload: 0,
    }))
    const manuscriptTeamMembersAndWorkload = await TeamMember.findTeamMembersWorkload(
      {
        teamRole,
        journalId,
        sectionId,
        teamMemberStatuses,
        manuscriptStatuses,
      },
    )
    const users = orderBy(
      unionBy(manuscriptTeamMembersAndWorkload, usersDefaultWorkload, 'userId'),
      ['workload'],
      ['asc'],
    )

    return users[0]
  },
}
