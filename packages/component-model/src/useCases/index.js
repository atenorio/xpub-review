const getUserUseCase = require('./getUser')
const getJournalUseCase = require('./getJournal')
const currentUserUseCase = require('./currentUser')
const getManuscriptUseCase = require('./getManuscript')
const getDraftRevisionUseCase = require('./getDraftRevision')
const getTeamMemberUserUseCase = require('./getTeamMemberUser')
const getManuscriptReviewsUseCase = require('./getManuscriptReviews')
const getManuscriptVersionsUseCase = require('./getManuscriptVersions')
const getUserForAutoAssignmentBasedOnWorkload = require('./getUserForAutoAssignmentBasedOnWorkload')

module.exports = {
  getUserUseCase,
  getJournalUseCase,
  currentUserUseCase,
  getManuscriptUseCase,
  getDraftRevisionUseCase,
  getTeamMemberUserUseCase,
  getManuscriptReviewsUseCase,
  getManuscriptVersionsUseCase,
  getUserForAutoAssignmentBasedOnWorkload,
}
