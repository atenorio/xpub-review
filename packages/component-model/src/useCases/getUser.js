const initialize = ({ User, Team }) => ({
  execute: async ({ userId }) => {
    const user = await User.find(userId, '[identities, teamMemberships.team]')
    user.isAdmin = !!user.getTeamMemberByRole(Team.Role.admin)
    user.isRIPE = !!user.getTeamMemberByRole(
      Team.Role.researchIntegrityPublishingEditor,
    )

    return user.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
