const { get, chain, orderBy } = require('lodash')

const initialize = models => ({
  execute: async ({ submissionId, userId }) => {
    const { Manuscript, User, Team, TeamMember } = models

    const shouldFilterManuscripts = manuscript => {
      const excludedStatuses = [
        TeamMember.Statuses.declined,
        TeamMember.Statuses.expired,
        TeamMember.Statuses.removed,
      ]
      if (
        [
          Team.Role.admin,
          Team.Role.triageEditor,
          Team.Role.editorialAssistant,
        ].includes(manuscript.role)
      )
        return true
      return !excludedStatuses.includes(
        chain(manuscript)
          .get('teams')
          .find(manuscript.role)
          .get('members')
          .find(userId)
          .get('status')
          .value(),
      )
    }
    const shouldRemoveFiles = (requestingUserRole, requestingUserStatus) => {
      const excludedRoles = [Team.Role.academicEditor, Team.Role.reviewer]
      return (
        excludedRoles.includes(requestingUserRole) &&
        TeamMember.Statuses.pending === requestingUserStatus
      )
    }

    let manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      excludedStatus: Manuscript.Statuses.draft,
      eagerLoadRelations: [
        'files',
        'articleType',
        'journal.[peerReviewModel, teams.members]',
        'teams.[members.[team,user.[identities]]]',
        'specialIssue.peerReviewModel',
      ],
    })

    manuscripts = orderBy(manuscripts, 'version', 'desc')
    const requestingUser = chain(manuscripts)
      .flatMap(manuscript => manuscript.teams)
      .flatMap(team => team.members)
      .find(member => member.userId === userId)
      .value()

    if (
      get(requestingUser, 'team.role') === Team.Role.academicEditor &&
      get(requestingUser, 'status') === TeamMember.Statuses.removed
    ) {
      throw new AuthorizationError('Operation not permitted!')
    }

    if (
      get(requestingUser, 'team.role') === Team.Role.triageEditor &&
      get(requestingUser, 'status') === TeamMember.Statuses.removed
    ) {
      throw new AuthorizationError('Operation not permitted!')
    }

    const user = await User.find(userId, 'teamMemberships.[team]')

    manuscripts.forEach(m => {
      const teamMember = user.getTeamMemberForManuscript(m)
      if (!teamMember) return

      m.role = teamMember.team.role
      if (shouldRemoveFiles(m.role, get(requestingUser, 'status'))) {
        m.files = []
      }
    })

    const visibleManuscripts = manuscripts.filter(
      m => m.role && shouldFilterManuscripts(m),
    )

    const lastVersionOfManuscript = await Manuscript.findLastManuscriptBySubmissionId(
      { submissionId },
    )
    const isApprovalEditor = await TeamMember.isApprovalEditor({
      userId,
      models,
      manuscriptId: lastVersionOfManuscript.id,
    })

    visibleManuscripts.map(manuscript => {
      manuscript.isApprovalEditor = isApprovalEditor
      manuscript.setVisibleStatus({
        userId,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      })
      return manuscript
    })

    return chain(visibleManuscripts)
      .filter(
        m =>
          [
            Team.Role.author,
            Team.Role.editorialAssistant,
            Team.Role.admin,
          ].includes(m.role) || m.status !== Manuscript.Statuses.withdrawn,
      )
      .map(manuscript => manuscript.toDTO())
      .value()
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
