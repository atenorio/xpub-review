const { omit, isEmpty } = require('lodash')

module.exports.initialize = ({ Review, Manuscript, Comment, Team }) => {
  const filterStrategies = {
    author(reviews, { manuscriptStatus, userId }) {
      if (
        [
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.pendingApproval,
          Manuscript.Statuses.revisionRequested,
          Manuscript.Statuses.inQA,
          Manuscript.Statuses.olderVersion,
          Manuscript.Statuses.academicEditorAssigned,
          Manuscript.Statuses.reviewersInvited,
          Manuscript.Statuses.underReview,
          Manuscript.Statuses.submitted,
          Manuscript.Statuses.withdrawn,
          Manuscript.Statuses.published,
          Manuscript.Statuses.qualityChecksRequested,
          Manuscript.Statuses.refusedToConsider,
        ].includes(manuscriptStatus)
      ) {
        return reviews.map(r => {
          r.comments =
            r.comments &&
            r.comments.filter(c => c.type === Comment.Types.public)
          return r
        })
      }

      if (
        [
          Manuscript.Statuses.reviewCompleted,
          Manuscript.Statuses.academicEditorInvited,
        ].includes(manuscriptStatus)
      ) {
        return reviews.filter(r => r.member.userId === userId)
      }
      return []
    },
    reviewer(reviews, { manuscriptStatus, userId }) {
      if (
        [
          Manuscript.Statuses.accepted,
          Manuscript.Statuses.rejected,
          Manuscript.Statuses.olderVersion,
        ].includes(manuscriptStatus)
      ) {
        return reviews.map(r => {
          if (
            r.member.userId !== userId &&
            r.member.role === Team.Role.reviewer
          ) {
            r.member = omit(r.member, ['alias'])
            r.comments =
              r.comments &&
              r.comments.filter(c => c.type === Comment.Types.public)
          }
          return r
        })
      }
      if (manuscriptStatus === Manuscript.Statuses.revisionRequested) {
        return reviews.filter(
          r =>
            r.member.role === Team.Role.reviewer && r.member.userId === userId,
        )
      }
      return reviews.filter(
        r => r.member.userId === userId || r.member.role !== Team.Role.reviewer,
      )
    },
  }

  function parseByRole(object, role, filterOptions) {
    return filterStrategies[role]
      ? filterStrategies[role](object, filterOptions)
      : object
  }

  return {
    async execute({ manuscript, userId }) {
      if (
        [
          Manuscript.Statuses.draft,
          Manuscript.Statuses.technicalChecks,
        ].includes(manuscript.status)
      )
        return []

      let reviewsDTOs = manuscript.reviews

      if (isEmpty(reviewsDTOs)) {
        const reviews = await Review.findBy({ manuscriptId: manuscript.id }, [
          'comments.files',
          'member.team',
        ])

        reviewsDTOs = reviews.map(r => r.toDTO())
      }

      return parseByRole(reviewsDTOs, manuscript.role, {
        manuscriptStatus: manuscript.status,
        userId,
      })
    },
  }
}
