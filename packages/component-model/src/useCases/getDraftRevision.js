const initialize = ({ Team, Manuscript, User }) => ({
  execute: async ({ submissionId, userId }) => {
    const draftRevision = await Manuscript.findOneBy({
      queryObject: { submissionId, status: Manuscript.Statuses.draft },
      eagerLoadRelations: [
        'files',
        'teams.members.user.identities',
        'reviews.comments.files',
        'articleType',
      ],
    })

    if (!draftRevision) {
      return
    }

    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      orderByField: 'version',
      order: 'asc',
      excludedStatus: 'draft',
    })
    const latestManuscript = manuscripts[manuscripts.length - 1]
    const user = await User.find(userId, 'teamMemberships.[team]')

    const submittingAuthorMember = user.teamMemberships.find(
      member =>
        member.team.manuscriptId === draftRevision.id &&
        member.isSubmitting === true &&
        member.team.role === Team.Role.author,
    )
    if (
      submittingAuthorMember &&
      latestManuscript.status === Manuscript.Statuses.revisionRequested
    ) {
      draftRevision.setComment()
      return draftRevision.toDTO()
    }

    if (latestManuscript.status === Manuscript.Statuses.qualityChecksRequested)
      return draftRevision.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
