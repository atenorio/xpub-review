const initialize = ({ models: { Manuscript, User, Team, TeamMember } }) => ({
  execute: async ({ manuscriptId, userId }) => {
    const manuscript = await Manuscript.find(manuscriptId, [
      'files',
      'section',
      'specialIssue',
      'journal.[teams.[members]]',
      'teams.[members.[user.[identities]]]',
      'reviews.[member, comments.[files]]',
    ])

    const user = await User.find(userId, 'teamMemberships.team')

    const teamMember = user.getTeamMemberForManuscript(manuscript)
    if (!teamMember) {
      throw new AuthorizationError('Not allowed.')
    }

    manuscript.role = teamMember.team.role

    if (!manuscript.triageEditor && manuscript.journal) {
      manuscript.triageEditor = manuscript.journal.getTriageEditor()
    }

    manuscript.setVisibleStatus({
      userId,
      TeamRole: Team.Role,
      TeamMemberStatuses: TeamMember.Statuses,
    })
    return manuscript.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
