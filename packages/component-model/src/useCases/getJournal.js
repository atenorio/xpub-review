const initialize = ({ Journal }) => ({
  execute: async journalId => {
    const journal = await Journal.find(journalId, [
      'journalArticleTypes.articleType',
      'teams.[members.[user.[identities]]]',
      'peerReviewModel',
      'sections.[teams.members.user.identities, specialIssues.[section,teams.members]]',
      'specialIssues.teams.members.user.identities',
    ])

    return journal.toDTO()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
