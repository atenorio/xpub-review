const HindawiBaseModel = require('./src/hindawiBaseModel')
const typeDefs = require('./typeDefs')
const resolvers = require('./src/resolvers')

module.exports = {
  HindawiBaseModel,
  typeDefs,
  models: [
    { model: require('./model/file'), modelName: 'File' },
    { model: require('./model/articleType'), modelName: 'ArticleType' },
    { model: require('./model/comment'), modelName: 'Comment' },
    { model: require('./model/job'), modelName: 'Job' },
    {
      model: require('./model/journalArticleType'),
      modelName: 'JournalArticleType',
    },
    { model: require('./model/review'), modelName: 'Review' },
    { model: require('./model/team'), modelName: 'Team' },
    { model: require('./model/identity'), modelName: 'Identity' },
    { model: require('./model/journal'), modelName: 'Journal' },
    { model: require('./model/manuscript'), modelName: 'Manuscript' },
    { model: require('./model/teamMember'), modelName: 'TeamMember' },
    { model: require('./model/user'), modelName: 'User' },
    {
      model: require('./model/reviewerSuggestion'),
      modelName: 'ReviewerSuggestion',
    },
    { model: require('./model/auditLog'), modelName: 'AuditLog' },
    { model: require('./model/peerReviewModel'), modelName: 'PeerReviewModel' },
    { model: require('./model/section'), modelName: 'Section' },
    { model: require('./model/specialIssue'), modelName: 'SpecialIssue' },
  ],
  resolvers,
}
