process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Team, TeamMember } = models
const chance = new Chance()

const Manuscript = require('../model/manuscript')

jest.mock('component-model', () => ({
  HindawiBaseModel: jest.fn(),
}))

describe('Manuscript model', () => {
  describe('toDTO', () => {
    it('returns correct academic editor on manuscript when multiple exist', async () => {
      const currentAcademicEditorId = chance.guid()
      const manuscript = new Manuscript({
        submissionId: chance.guid(),
        id: chance.guid(),
        status: Manuscript.Statuses.reviewersInvited,
        version: 1,
      })

      manuscript.teams = [
        {
          id: chance.guid(),
          role: Team.Role.academicEditor,
          toDTO: function toDTO() {
            return this
          },
          members: [
            {
              id: chance.guid(),
              status: TeamMember.Statuses.expired,
              toDTO: function toDTO() {
                return this
              },
              position: 0,
            },
            {
              id: currentAcademicEditorId,
              status: TeamMember.Statuses.accepted,
              toDTO: function toDTO() {
                return this
              },
              position: 1,
            },
          ],
        },
        {
          id: chance.guid(),
          role: Team.Role.author,
          toDTO: function toDTO() {
            return this
          },
          members: [
            {
              id: chance.guid(),
              status: TeamMember.Statuses.pending,
              toDTO: function toDTO() {
                return this
              },
              position: 0,
            },
          ],
        },
      ]

      const { academicEditor } = manuscript.toDTO()

      expect(academicEditor).toBeDefined()
      expect(currentAcademicEditorId).toEqual(academicEditor.id)
    })
  })
  describe('setVisibleStatus', () => {
    it('returns "Submitted" visible status for author is manuscript is in status submitted', async () => {
      const manuscript = fixtures.generateManuscript({
        properties: { status: Manuscript.Statuses.submitted },
        Manuscript,
      })
      const teamMember = await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.author,
      })
      manuscript.role = Team.Role.author
      manuscript.setVisibleStatus({
        userId: teamMember.userId,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      })
      expect(manuscript.visibleStatus).toEqual('Submitted')
    })
    it('returns "Respond to Invite" visible status for invited academic editor if manuscript is in status underReview', async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          status: Manuscript.Statuses.underReview,
          id: chance.guid(),
        },
        Manuscript,
      })
      await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.author,
      })
      await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.academicEditor,
        input: {
          status: TeamMember.Statuses.accepted,
        },
      })
      const invitedAcademicEditor = await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.academicEditor,
        input: {
          status: TeamMember.Statuses.pending,
        },
      })
      manuscript.role = Team.Role.academicEditor
      manuscript.setVisibleStatus({
        userId: invitedAcademicEditor.userId,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      })
      expect(manuscript.visibleStatus).toEqual('Respond to Invite')
    })
    it('returns "Respond to Invite" visible status for invited reviewer if manuscript is in status underReview', async () => {
      const manuscript = fixtures.generateManuscript({
        properties: {
          status: Manuscript.Statuses.underReview,
          id: chance.guid(),
        },
        Manuscript,
      })
      await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.author,
      })
      await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.reviewer,
        input: {
          status: TeamMember.Statuses.accepted,
        },
      })
      const invitedReviewer = await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        role: Team.Role.reviewer,
        input: {
          status: TeamMember.Statuses.pending,
        },
      })
      manuscript.role = Team.Role.reviewer
      manuscript.setVisibleStatus({
        userId: invitedReviewer.userId,
        TeamRole: Team.Role,
        TeamMemberStatuses: TeamMember.Statuses,
      })
      expect(manuscript.visibleStatus).toEqual('Respond to Invite')
    })
  })
})
