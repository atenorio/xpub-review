process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, Manuscript } = models
const chance = new Chance()

const { getManuscriptVersionsUseCase } = require('../src/useCases')

describe('Get Manuscript Versions Use Case', () => {
  it('return manuscript order by version desc', async () => {
    const journal = fixtures.generateJournal({ Journal })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })
    const submissionId = chance.guid()

    const firstVersion = fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId,
        journal: journal.id,
        status: Manuscript.Statuses.olderVersion,
      },
      Manuscript,
    })

    const secondVersion = fixtures.generateManuscript({
      properties: {
        version: 2,
        submissionId,
        journal: journal.id,
        status: Manuscript.Statuses.olderVersion,
      },
      Manuscript,
    })

    const thirdVersion = fixtures.generateManuscript({
      properties: {
        version: 3,
        submissionId,
        status: Manuscript.Statuses.academicEditorAssigned,
        journal: journal.id,
      },
      Manuscript,
    })

    const author = await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: firstVersion,
      input: { isSubmitting: true },
    })

    const authorUser = fixtures.users.find(user => user.id === author.userId)

    await dataService.addUserOnManuscript({
      models,
      fixtures,
      user: authorUser,
      role: Team.Role.author,
      manuscript: secondVersion,
      input: { isSubmitting: true, isCorresponding: false },
    })

    await dataService.addUserOnManuscript({
      models,
      fixtures,
      user: authorUser,
      role: Team.Role.author,
      manuscript: thirdVersion,
      input: { isSubmitting: true, isCorresponding: false },
    })

    const manuscriptVersions = await getManuscriptVersionsUseCase
      .initialize(models)
      .execute({ submissionId, userId: author.userId })

    expect(manuscriptVersions[0].version).toEqual(3)
    expect(manuscriptVersions[1].version).toEqual(2)
    expect(manuscriptVersions[2].version).toEqual(1)
    expect(manuscriptVersions[2].role).toEqual(Team.Role.author)
  })
})
