import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import {
  Row,
  Item,
  Text,
  Label,
  ActionLink,
  ShadowedBox,
  IconTooltip,
  withFetching,
} from '@hindawi/ui'

const ORCIDLinkTooltip = () => (
  <Text display="inline" secondary>
    Hindawi is collecting and displaying your ORCID iD so we can ensure your
    work is recognised. When you click the “Authorize” button, we will ask you
    to share your iD using an authenticated process: either by{' '}
    <ActionLink
      display="inline"
      to="https://support.orcid.org/knowledgebase/topics/141838"
    >
      {' '}
      registering for an ORCID iD{' '}
    </ActionLink>{' '}
    or, if you already have one, to{' '}
    <ActionLink
      display="inline"
      to="https://support.orcid.org/knowledgebase/topics/141838"
    >
      sign into your ORCID account{' '}
    </ActionLink>
    , then granting us permission to get your ORCID iD. We do this to ensure
    that you are correctly identified and securely connecting your ORCID iD.
  </Text>
)

const ORCIDDisplayTooltip = () => (
  <Text display="inline" secondary>
    To acknowledge that you have used your iD and that it has been
    authenticated, we display the ORCID iD icon alongside your name on our
    website. Learn more in{' '}
    <ActionLink
      display="inline"
      to="https://orcid.org/blog/2013/02/22/how-should-orcid-id-be-displayed"
    >
      {' '}
      How should an ORCID iD be displayed
    </ActionLink>
    .
  </Text>
)

const LinkOrcID = ({ orcid, unlinkOrcid, id, isLinked, ...rest }) => (
  <ShadowedBox mt={4}>
    <Row alignItems="center">
      <Item alignItems="center">
        <Label mr={2}>ORCID iD </Label>
        <IconTooltip
          content={orcid ? ORCIDDisplayTooltip : ORCIDLinkTooltip}
          icon="tooltip"
          interactive
          primary
        />
      </Item>
      <Item data-test-id="orcid-action" justify="flex-end">
        {isLinked ? (
          <ActionLink fontWeight={700} onClick={unlinkOrcid}>
            Unlink
          </ActionLink>
        ) : (
          <Link href={`/api/users/orcid?userId=${id}`}>Link</Link>
        )}
      </Item>
    </Row>

    {isLinked && (
      <Row>
        <Item>
          <Text>
            <a
              href="https://orcid.org"
              rel="noopener noreferrer"
              target="_blank"
            >
              <Img
                alt="ORCID Logo"
                src="https://orcid.org/sites/default/files/images/orcid_16x16.png"
              />
            </a>
            <ActionLink fontWeight={700} to={`https://orcid.org/${orcid}`}>
              {`https://orcid.org/${orcid}`}
            </ActionLink>
          </Text>
        </Item>
      </Row>
    )}
  </ShadowedBox>
)

export default withFetching(LinkOrcID)

// #region styles
const Link = styled.a`
  font-family: ${th('defaultFont')};
  color: ${th('action.color')};
  cursor: pointer;
  font-weight: 700;
  text-decoration: none;

  &:hover {
    color: ${th('colorSecondary')};
    text-decoration: none;
  }

  &:active,
  &:focus {
    color: ${th('action.colorActive')};
    text-decoration: none;
  }
`

const Img = styled.img`
  vertical-align: text-bottom;
  padding-right: ${th('gridUnit')};
`
// #endregion
