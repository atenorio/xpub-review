import React from 'react'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withJournal } from 'xpub-journal'
import { withModal } from 'component-modal'
import { compose, withHandlers, withProps, withStateHandlers } from 'recompose'
import { Row, Text, Breadcrumbs, MultiAction, withFetching } from '@hindawi/ui'

import { UserProfile, LinkOrcID, EmailNotifications } from '../components'
import withUserGQL from '../graphql'

const UserProfilePage = ({
  user,
  journal,
  editUser,
  editMode,
  toggleEdit,
  unlinkOrcid,
  orcidIdentity,
  localIdentity,
  toggeleEmailSubscription,
}) => (
  <Root>
    <Breadcrumbs mb={2}>Dashboard</Breadcrumbs>

    <Row alignItems="baseline" justify="flex-start" pl={1 / 2}>
      <H2>Account Settings</H2>
      <Text ml={4} secondary>
        {get(localIdentity, 'email')}
      </Text>
    </Row>

    <UserProfile
      editMode={editMode}
      editUser={editUser}
      journal={journal}
      mt={3}
      toggleEdit={toggleEdit}
      user={localIdentity}
    />

    <EmailNotifications
      isSubscribed={get(user, 'isSubscribedToEmails', true)}
      toggeleEmailSubscription={toggeleEmailSubscription}
    />

    <LinkOrcID
      id={get(user, 'id')}
      isLinked={get(user, 'identities', '').length !== 1}
      orcid={get(orcidIdentity, 'identifier', '')}
      unlinkOrcid={unlinkOrcid}
    />
  </Root>
)

// #region compose
export default compose(
  withJournal,
  withUserGQL,
  withFetching,
  withModal({
    component: MultiAction,
    modalKey: 'subscriptionToEmails',
  }),
  withProps(({ data }) => ({
    user: get(data, 'currentUser', {}),
    orcidIdentity: get(data, 'currentUser.identities', []).find(
      i => i.identifier,
    ),
    localIdentity: get(data, 'currentUser.identities', []).find(
      i => i.__typename === 'Local',
    ),
  })),
  withStateHandlers(
    {
      editMode: false,
    },
    {
      toggleEdit: ({ editMode }) => () => ({
        editMode: !editMode,
      }),
    },
  ),
  withHandlers({
    toggeleEmailSubscription: ({
      showModal,
      subscribeToEmails,
      unsubscribeToEmails,
      user,
    }) => () =>
      showModal({
        modalKey: 'subscriptionToEmails',
        subtitle: `Are you sure you want to ${
          user.isSubscribedToEmails ? 'unsubscribe from ' : 'subscribe to'
        } emails ?`,
        title: user.isSubscribedToEmails
          ? `Unsubscribe from emails`
          : `Subscribe to emails`,
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          user.isSubscribedToEmails
            ? unsubscribeToEmails({
                variables: {
                  input: { token: user.unsubscribeToken },
                },
              })
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
            : subscribeToEmails()
                .then(() => {
                  setFetching(false)
                  hideModal()
                })
                .catch(e => {
                  setFetching(false)
                  setError(e.message)
                })
        },
      }),
    unlinkOrcid: ({ unlinkOrcid, showModal, localIdentity }) => () =>
      showModal({
        modalKey: 'subscriptionToEmails',
        subtitle: 'Are you sure you want to unlink your ORCID account?',
        title: 'Unlink ORCID',
        confirmText: 'YES',
        cancelText: 'CLOSE',
        onConfirm: ({ hideModal, setFetching, setError, clearError }) => {
          clearError()
          setFetching(true)
          unlinkOrcid()
            .then(() => {
              setFetching(false)
              hideModal()
            })
            .catch(e => {
              setFetching(false)
              setError(e.message)
            })
        },
      }),

    editUser: ({ updateUser, toggleEdit, setFetching, setError }) => ({
      givenNames,
      surname,
      aff,
      title,
      country,
    }) => {
      setFetching(true)
      updateUser({
        variables: {
          input: {
            givenNames: givenNames.trim(),
            surname: surname.trim(),
            aff,
            title,
            country,
          },
        },
      })
        .then(() => {
          setFetching(false)
          toggleEdit()
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(UserProfilePage)
// #endregion

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 calc(${th('gridUnit')} * 20);
  padding-top: calc(${th('gridUnit')} * 4);
`
// #endregion
