import React, { Fragment } from 'react'
import { Formik } from 'formik'
import { compose, withHandlers } from 'recompose'
import { Button, H2, Spinner, TextField } from '@pubsweet/ui'
import {
  Row,
  Item,
  Text,
  Label,
  validators,
  ShadowedBox,
  PasswordValidation,
  withFetching,
  ValidatedFormField,
  changePasswordValidator,
} from '@hindawi/ui'

import withUserGQL from '../graphql'

const PasswordField = input => <TextField {...input} type="password" />

const containerPadding = {
  pt: 4,
  pb: 4,
  pl: 4,
  pr: 4,
}
const ChangePassword = ({
  history,
  isFetching,
  fetchingError,
  changePassword,
}) => (
  <Formik onSubmit={changePassword} validate={changePasswordValidator}>
    {({ handleSubmit, errors, values, touched }) => (
      <ShadowedBox center mt={20} {...containerPadding}>
        <H2>Change Password</H2>
        <Row mt={6}>
          <Item vertical>
            <Label required>Current Password</Label>
            <ValidatedFormField
              component={PasswordField}
              inline
              name="currentPassword"
              validate={[validators.required]}
            />
          </Item>
        </Row>

        <PasswordValidation
          errors={errors}
          formValues={values}
          touched={touched}
        />

        {fetchingError && (
          <Row mt={2}>
            <Item>
              <Text error>{fetchingError}</Text>
            </Item>
          </Row>
        )}

        <Row mt={8}>
          {isFetching ? (
            <Spinner />
          ) : (
            <Fragment>
              <Button mr={6} onClick={history.goBack}>
                Back
              </Button>
              <Button onClick={handleSubmit} primary>
                Update password
              </Button>
            </Fragment>
          )}
        </Row>
      </ShadowedBox>
    )}
  </Formik>
)

export default compose(
  withFetching,
  withUserGQL,
  withHandlers({
    changePassword: ({ setFetching, setError, changePassword, history }) => (
      { currentPassword: oldPassword, password },
      formProps,
    ) => {
      setFetching(true)
      changePassword({
        variables: {
          input: {
            password,
            oldPassword,
          },
        },
      })
        .then(r => {
          window.localStorage.setItem('token', r.data.changePassword.token)
          setFetching(false)
          history.push('/info-page', {
            path: '/profile',
            buttonText: 'Back to account settings',
            title: 'Password Successfully Updated',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(ChangePassword)

// #region styles
// #endregion
