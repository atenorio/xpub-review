import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import * as queries from './queries'

export default compose(
  graphql(queries.currentUser),
  graphql(mutations.changePassword, {
    name: 'changePassword',
  }),
  graphql(mutations.updateUser, {
    name: 'updateUser',
    options: {
      refetchQueries: [{ query: queries.currentUser }],
    },
  }),
  graphql(mutations.subscribeToEmails, {
    name: 'subscribeToEmails',
    options: {
      refetchQueries: [{ query: queries.currentUser }],
    },
  }),
  graphql(mutations.unsubscribeToEmails, {
    name: 'unsubscribeToEmails',
    options: {
      refetchQueries: [{ query: queries.currentUser }],
    },
  }),
  graphql(mutations.unlinkOrcid, {
    name: 'unlinkOrcid',
    options: {
      refetchQueries: [{ query: queries.currentUser }],
    },
  }),
)
