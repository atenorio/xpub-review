const { hasRequiredEnvVariables } = require('component-env')
const OrcidStrategy = require('passport-orcid')
const config = require('config')
const { get, words, last, initial } = require('lodash')

const { clientID, clientSecret, callbackPath, successPath } = config.get(
  'orcid',
)
const models = require('@pubsweet/models')
const events = require('component-events')

let userId
const LinkOrcid = app => {
  if (
    !hasRequiredEnvVariables([
      clientID,
      clientSecret,
      callbackPath,
      successPath,
    ])
  ) {
    return
  }

  const { passport } = app.locals
  passport.serializeUser((user, done) => {
    done(null, user)
  })

  passport.deserializeUser((user, done) => {
    done(null, user)
  })

  passport.use(
    new OrcidStrategy(
      {
        sandbox: process.env.NODE_ENV !== 'production',
        callbackURL: `${config.get('pubsweet-client.baseUrl')}${callbackPath}`,
        clientID,
        clientSecret,
      },
      (accessToken, refreshToken, params, profile, done) => {
        profile = {
          orcid: params.orcid,
          name: params.name,
          accessToken,
          refreshToken,
          scope: params.scope,
          expiry: params.expires_in,
        }
        return done(null, profile)
      },
    ),
  )

  app.get(
    '/api/users/orcid',
    (req, res, next) => {
      userId = get(req, 'query.userId')
      next()
    },
    passport.authenticate('orcid'),
  )

  app.get(
    callbackPath,
    passport.authenticate('orcid', {
      failureRedirect: successPath,
    }),
    linkOrcidHandler(models),
  )
}

const linkOrcidHandler = models => async (req, res) => {
  const { User, Identity } = models
  const eventsService = events.initialize({ models })

  const user = await User.find(userId, 'identities')
  let orcidIdentity = user.identities.find(i => i.type === 'orcid')
  if (!orcidIdentity) {
    orcidIdentity = new Identity({
      type: 'orcid',
      userId: user.id,
      identifier: req.user.orcid,
      surname: last(words(req.user.name)),
      givenNames: initial(words(req.user.name)).toString(),
      isConfirmed: true,
    })
    await orcidIdentity.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserORCIDAdded',
    })
  }
  res.redirect(successPath)
}

module.exports = LinkOrcid
