const initialize = ({ User }) => ({
  execute: async ({ userId }) => {
    const user = await User.find(userId)
    if (user.isSubscribedToEmails) return
    user.updateProperties({ isSubscribedToEmails: true })
    await user.save()
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
