process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { unlinkOrcidUseCase } = require('../src/use-cases')

const chance = new Chance()
const eventsService = {
  publishUserEvent: jest.fn(),
}

describe('Unlink orcid account', () => {
  it('should unlink the orcid account of the user', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({ User, Identity })
    const orcidIdentity = fixtures.generateIdentity({
      properties: {
        id: chance.guid(),
        type: 'orcid',
        userId: user.id,
        identifier: chance.guid(),
        surname: chance.last(),
        givenNames: chance.first(),
        isConfirmed: true,
      },
      Identity,
    })
    user.identities.push(orcidIdentity)

    await unlinkOrcidUseCase
      .initialize({ models, eventsService })
      .execute({ userId: user.id })

    const unlinkedUser = fixtures.users.find(u => u.id === user.id)
    expect(unlinkedUser.identities.length).toEqual(1)
    expect(unlinkedUser.identities[0].type).not.toEqual('orcid')
  })
  it('should not return an error when the user Orcid account is already unlinked', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({ User, Identity })
    const result = unlinkOrcidUseCase
      .initialize({ models, eventsService })
      .execute({ userId: user.id })

    return expect(result).rejects.toThrow('There is no Orcid account linked.')
  })
})
