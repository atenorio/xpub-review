process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures } = require('fixture-service')

const { subscribeToEmailsUseCase } = require('../src/use-cases')

describe('Subscribe user to emails', () => {
  it('should subscribe the user to emails', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({
      properties: { isSubscribedToEmails: false },
      User,
      Identity,
    })
    await subscribeToEmailsUseCase
      .initialize(models)
      .execute({ userId: user.id })

    expect(user.isSubscribedToEmails).toEqual(true)
    expect(user.unsubscribeToken).toBeDefined()
  })
  it('should not return an error when the user is already subscribed to emails', async () => {
    const { User, Identity } = models
    const user = fixtures.generateUser({
      properties: { isSubscribedToEmails: true },
      User,
      Identity,
    })

    const result = subscribeToEmailsUseCase
      .initialize(models)
      .execute({ userId: user.id })

    return expect(result).resolves.not.toThrow()
  })
})
