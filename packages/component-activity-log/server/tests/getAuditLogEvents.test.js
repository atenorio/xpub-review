const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { getAuditLogEventsUseCase } = require('../src/use-cases')

describe('get all audit logs for all versions of a manuscript', () => {
  it('should return the existing audit logs in the correct form', async () => {
    const { Manuscript, Team, AuditLog, User } = models
    const manuscript = fixtures.generateManuscript({
      properties: { status: Manuscript.Statuses.submitted },
      Manuscript,
    })
    const teamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const auditLog = fixtures.generateAuditLog({
      properties: {
        userId: teamMember.userId,
        manuscriptId: manuscript.id,
        action: 'manuscript_submitted',
        objectType: 'manuscript',
        objectId: manuscript.id,
      },
      AuditLog,
    })

    manuscript.logs.push(auditLog)
    const user = await User.find(teamMember.userId)
    user.teamMemberships.push(teamMember)
    auditLog.user = user
    const result = await getAuditLogEventsUseCase
      .initialize(models)
      .execute(manuscript.id)

    expect(result.length).toEqual(1)
    expect(result[0]).toEqual(
      expect.objectContaining({
        logs: [
          expect.objectContaining({
            created: expect.anything(),
            id: expect.anything(),
            target: expect.any(Object),
            user: expect.objectContaining({
              email: user.identities[0].email,
              role: teamMember.team.role,
            }),
          }),
        ],
        version: 1,
      }),
    )
  })
  it('should return the target user data if the objectType is user', async () => {
    const { Manuscript, Team, AuditLog, User, Identity } = models
    const manuscript = fixtures.generateManuscript({
      properties: { status: Manuscript.Statuses.submitted },
      Manuscript,
    })

    const invitedUser = fixtures.generateUser({ User, Identity })
    const teamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
    })

    const auditLog = await fixtures.generateAuditLog({
      properties: {
        userId: teamMember.userId,
        manuscriptId: manuscript.id,
        action: 'reviewer_invited',
        objectType: 'user',
        objectId: invitedUser.id,
      },
      AuditLog,
    })
    manuscript.logs.push(auditLog)
    const user = await User.find(teamMember.userId)
    user.teamMemberships.push(teamMember)
    auditLog.user = user

    const result = await getAuditLogEventsUseCase
      .initialize(models)
      .execute(manuscript.id)
    expect(result.length).toEqual(1)
    expect(result[0]).toEqual(
      expect.objectContaining({
        logs: [
          expect.objectContaining({
            created: expect.anything(),
            id: expect.anything(),
            target: expect.objectContaining({
              email: invitedUser.identities[0].email,
            }),
          }),
        ],
      }),
    )
  })
  it('should correctly map logs to versions', async () => {
    const { Manuscript, Team, AuditLog, User } = models
    const manuscriptV1 = fixtures.generateManuscript({
      properties: { status: Manuscript.Statuses.submitted, version: 1 },
      Manuscript,
    })

    const manuscriptV2 = fixtures.generateManuscript({
      properties: {
        version: 2,
        status: Manuscript.Statuses.submitted,
        submissionId: manuscriptV1.submissionId,
      },
      Manuscript,
    })

    const teamMemberV1 = await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: manuscriptV1,
    })
    const teamMemberV2 = await dataService.createUserOnManuscript({
      models,
      role: Team.Role.author,
      fixtures,
      manuscript: manuscriptV2,
    })

    const auditLogV1 = fixtures.generateAuditLog({
      properties: {
        userId: teamMemberV1.userId,
        manuscriptId: manuscriptV1.id,
        action: 'manuscript_submitted',
        objectType: 'manuscript',
        objectId: manuscriptV1.id,
      },
      AuditLog,
    })

    const auditLogV2 = fixtures.generateAuditLog({
      properties: {
        userId: teamMemberV2.userId,
        manuscriptId: manuscriptV2.id,
        action: 'manuscript_submitted',
        objectType: 'manuscript',
        objectId: manuscriptV2.id,
      },
      AuditLog,
    })

    manuscriptV1.logs.push(auditLogV1)
    const userV1 = await User.find(teamMemberV1.userId)
    userV1.teamMemberships.push(teamMemberV1)
    auditLogV1.user = userV1

    manuscriptV2.logs.push(auditLogV2)
    const userV2 = await User.find(teamMemberV2.userId)
    userV2.teamMemberships.push(teamMemberV2)
    auditLogV2.user = userV2

    const result = await getAuditLogEventsUseCase
      .initialize(models)
      .execute(manuscriptV1.id)

    expect(result.length).toEqual(2)
    expect(result[0].version).toEqual(1)
    expect(result[0].logs).toBeDefined()
    expect(result[1].version).toEqual(2)
    expect(result[1].logs).toBeDefined()
  })
})
