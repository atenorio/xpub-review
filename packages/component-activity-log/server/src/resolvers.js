const models = require('@pubsweet/models')

const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')

const resolvers = {
  Query: {
    async getAuditLogEvents(_, { manuscriptId }, ctx) {
      return useCases.getAuditLogEventsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
