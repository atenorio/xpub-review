import gql from 'graphql-tag'

export const auditLogEventsFragment = gql`
  fragment auditLogEvents on AuditOutput {
    logs {
      id
      created
      user {
        email
        role
        reviewerNumber
      }
      target {
        email
        role
        reviewerNumber
      }
      action
    }
    version
  }
`
