/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { get } from 'lodash'

import * as queries from './queries'

export default compose(
  graphql(queries.getAuditLogEvents, {
    options: props => ({
      variables: {
        manuscriptId: get(props, 'match.params.manuscriptId'),
      },
    }),
  }),
)
