import { compose, withProps } from 'recompose'

import withGQL from '../graphql'
import { ActivityLog } from '../components'

export default compose(
  withGQL,
  withProps(
    ({ data: { getAuditLogEvents, loading, error }, peerReviewModel }) => ({
      events: getAuditLogEvents,
      loading,
      error,
      peerReviewModel,
    }),
  ),
)(ActivityLog)
