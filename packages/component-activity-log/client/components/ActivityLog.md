Activity log with users events.

```js
const events = [
  {
    version: 1,
    logs: [
      {
        id: '1',
        action: 'sent reviewer invite to ',
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'allie.bryan@gmail.com',
          role: '',
        },
        created: 1550629860963,
      },
      {
        id: '2',
        action: 'sent reviewer invite to',
        created: 1550629871097,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'mike-knight@educentralmanchester.co.uk',
          role: '',
        },
      },
      {
        id: '3',
        action: 'sent reviewer invite to',
        created: 1550629880332,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'charlie.bennett@gmail.com',
          role: '',
        },
      },
      {
        id: '4',
        action: 'accepted invitation to review',
        created: 1550629888928,
        user: {
          email: 'curt.weber@facultyofamazingwork.com',
          role: 'reviewer 1',
        },
        target: null,
      },
      {
        id: '5',
        action: 'sent automatic invitiation to review to',
        created: 1550629902715,
        user: null,
        target: {
          email: 'curt.weber@facultyofamazingwork.com ',
          role: 'reviewer',
        },
      },
    ],
  },
  {
    version: 2,
    logs: [
      {
        id: '6',
        action: 'submitted manuscript revision',
        created: 1550629913146,
        user: {
          email: 'pallavi.sharma@unimission.edu',
          role: 'author',
        },
        target: null,
      },
      {
        id: '7',
        action: 'requested a major revision',
        created: 1550629923667,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: null,
      },
      {
        id: '8',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '9',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'academicEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '10',
        action: 'accepted invitation to review',
        created: 1550629933543,
        user: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '11',
        action: 'submitted a report',
        created: 1550629933543,
        user: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '12',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'adolf.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '13',
        action: 'declined invitation to review',
        created: 1550629933543,
        user: {
          email: 'adolf.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },

      {
        id: '14',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '15',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '16',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '17',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '18',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '19',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '20',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '21',
        action: 'accepted invitation to review',
        created: 1550629933543,
        user: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '22',
        action: 'submitted a report',
        created: 1550629933543,
        user: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '23',
        action: 'sent reviewer invite to',
        created: 1550629933543,
        user: {
          email: 'jonathan-mike.morris@hindawieditorial.com',
          role: 'triageEditor',
        },
        target: {
          email: 'adolf.morgan@gmail.com',
          role: '',
        },
      },
      {
        id: '24',
        action: 'declined invitation to review',
        created: 1550629933543,
        user: {
          email: 'adolf.morgan@gmail.com',
          role: '',
        },
        target: {
          email: 'brent.morgan@gmail.com',
          role: '',
        },
      },
    ],
  },
]

;<ActivityLog events={events} toggle={() => {}} expanded={true} />
```

Activity log with no events displaying an error.

```js
const events = []
;<ActivityLog events={events} toggle={() => {}} expanded={true} />
```

Loading events.

```js
const events = []
;<ActivityLog
  events={events}
  loading={true}
  toggle={() => {}}
  expanded={true}
/>
```
