import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser, Icon } from '@pubsweet/ui'
import { isEmpty, filter, get, orderBy } from 'lodash'
import { compose, withProps, withHandlers, withState } from 'recompose'
import {
  Row,
  Item,
  Text,
  Label,
  Loader,
  getRoleFilter,
  ContextualBox,
  Menu,
  getShortRole,
} from '@hindawi/ui'

const ActivityLog = ({
  value,
  error,
  events,
  toggle,
  loading,
  expanded,
  errorMessage,
  peerReviewModel,
  changeFilterValue,
}) => {
  const { options: filterOptions } = getRoleFilter(peerReviewModel)
  return (
    <ContextualBox
      expanded={expanded}
      label="Activity Log"
      mt={4}
      scrollIntoView
      toggle={toggle}
    >
      <Root>
        {error ? (
          <Row mb={2} mt={6}>
            <Item alignItems="center" justify="center">
              <Icon error size={4}>
                alert-triangle
              </Icon>
              <ErrorMessage>{errorMessage}</ErrorMessage>
            </Item>
          </Row>
        ) : (
          <Fragment>
            <Item mb={2} vertical>
              <Label>Role</Label>
              <StyledMenu
                data-test-id="activity-log"
                inline
                onChange={changeFilterValue}
                options={filterOptions}
                placeholder="All"
                value={value}
              />
            </Item>
            <EnhancedActivityLogEvents
              events={events}
              loading={loading}
              options={filterOptions}
              peerReviewModel={peerReviewModel}
              value={value}
            />
          </Fragment>
        )}
      </Root>
    </ContextualBox>
  )
}

export default compose(
  withProps(({ events }) => ({
    events: orderBy(events, 'version', 'desc'),
  })),
  withProps(({ events }) => ({
    events: events.map(item => ({
      logs: orderBy(item.logs, ['created'], ['desc']),
      version: item.version,
    })),
  })),
  withState('value', 'setFilterValues', 'all'),
  withHandlers({
    changeFilterValue: ({ value, setFilterValues }) => value => {
      setFilterValues(value)
    },
  }),
)(ActivityLog)

const ActivityLogEvents = ({
  events = [],
  loading,
  getRole,
  emptyStateMessage = 'There are no activity logs',
}) => (
  <Wrapper>
    {loading ? (
      <Loader />
    ) : (
      isEmpty(events) && (
        <Item alignItems="center" justify="center">
          <ErrorMessage>{emptyStateMessage}</ErrorMessage>
        </Item>
      )
    )}
    {events.map(({ version, logs = [] }) => (
      <Events key={version}>
        <TextVersion height="auto" labelLine>
          VERSION {version}
        </TextVersion>

        {logs.map(log => (
          <Row alignItems="center" key={log.id} pl={2}>
            <Item display="inline" mb={2} mr={4} mt={2}>
              <Text fontWeight={600} mr={1}>
                {get(log.user, 'email') || 'System'}
              </Text>
              {getRole(log.user)}
              <Text medium mr={1}>
                {log.action}
              </Text>
              <Text fontWeight={600} mr={1}>
                {get(log.target, 'email')}
              </Text>
              {getRole(log.target)}
            </Item>
            <Item flex={0} justify="flex-end" pr={4}>
              <DateParser
                dateFormat="YYYY-MM-DD hh:mm [(UTC)]"
                timestamp={log.created}
              >
                {timestamp => <Date>{timestamp}</Date>}
              </DateParser>
              <DateParser humanizeThreshold={356} timestamp={log.created}>
                {timestamp => <Date>{`\u00A0${timestamp}`}</Date>}
              </DateParser>
            </Item>
          </Row>
        ))}
      </Events>
    ))}
  </Wrapper>
)

const EnhancedActivityLogEvents = compose(
  withProps(({ events, value, options, peerReviewModel }) => ({
    getRole: item => {
      const option = item.role && options.find(op => op.value === item.role)
      const role = get(option, 'value')
      return (
        role && (
          <Text mr={1 / 2}>
            (
            <Text customId fontWeight={600}>
              {getShortRole({ role, peerReviewModel })}
              {` ${get(item, 'reviewerNumber') || ''}`}
            </Text>
            )
          </Text>
        )
      )
    },
    events: events.map(item => ({
      logs: item.logs.filter(log =>
        value === 'all' ? log : (get(log.user, 'role') || '').includes(value),
      ),
      version: item.version,
    })),
  })),
  withProps(({ events, value }) => ({
    events: filter(events, item => !isEmpty(item.logs)),
  })),
)(ActivityLogEvents)

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  padding-left: calc(${th('gridUnit')} * 2);
  padding-top: calc(${th('gridUnit')} * 2);
  height: calc(${th('gridUnit')} * 130);
`
const Wrapper = styled.div`
  display: block;
  height: calc(${th('gridUnit')} * 112);
  overflow-x: auto;
`

const Date = styled(Text)`
  color: ${th('colorSecondary')};
  white-space: nowrap;
  font-size: ${th('fontSizeBaseMedium')};
`
const Events = styled.div`
  margin-top: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);
`

const ErrorMessage = styled.span`
  color: ${th('colorFurnitureHue')};
  font-weight: 600;
`

const StyledMenu = styled(Menu)`
  max-width: calc(${th('gridUnit')} * 38);
  min-width: calc(${th('gridUnit')} * 38);
`
const TextVersion = styled(Text)`
  font-size: 12px;
`
// #endregion

ActivityLog.propTypes = {
  /** Array of users events. */
  events: PropTypes.arrayOf(PropTypes.object).isRequired,
  /** The state of the contextual box. If passed from a parent then the component
   * is controlled and can be expanded/collapsed remotely.
   */
  expanded: PropTypes.bool, // eslint-disable-line
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func, // eslint-disable-line
  /** Error from graphQL where cannot retrive activity log events. */
  errorMessage: PropTypes.string,
}

ActivityLog.defaultProps = {
  errorMessage: 'Cannot retrive activity log events',
}
