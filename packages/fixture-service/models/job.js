const { assign } = require('lodash')
const Chance = require('chance')
const { Promise } = require('bluebird')

const chance = new Chance()

class Job {
  constructor(props) {
    this.id = chance.guid()
    this.journalId = props.journalId
    this.teamMemberId = props.teamMemberId
    this.manuscriptId = props.manuscriptId
    this.specialIssueId = props.specialIssueId
  }

  static async findAllByTeamMembers(teamMemberIds) {
    return []
  }

  static async findAllByTeamMember(teamMemberId) {
    return []
  }

  async save() {
    return Promise.resolve(this)
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }
}

module.exports = Job
