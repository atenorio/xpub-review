const { assign, chain } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { journals } = fixtures
const {
  findMock,
  findAllMock,
  findOneByMock,
  findOneByEmailMock,
} = require('./repositoryMocks')

const Team = require('./team')

class Journal {
  constructor(props) {
    this.id = chance.guid()
    this.name = props.name || chance.sentence({ words: 5 })
    this.code = props.code || chance.word().toUpperCase()
    this.apc = props.apc || 0
    this.email = props.email || chance.email()
    this.issn =
      props.issn ||
      `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
        min: 999,
        max: 9999,
      })}`
    this.isActive = props.isActive === undefined ? false : props.isActive
    this.teams = []
    this.jobs = props.jobs || []
    this.articleTypes = props.articleTypes || []
    this.created = props.created || Date.now()
    this.activationDate = props.activationDate || null
    this.peerReviewModelId = props.peerReviewModelId
    this.specialIssues = props.specialIssues || []
    this.sections = props.sections || []
    this.journalArticleTypes = props.journalArticleTypes || []
  }

  static findOneByEmail = value => findOneByEmailMock(value, fixtures)
  static find = id => findMock(id, 'journals', fixtures)
  static findUniqueJournal = ({ email, code, name }) => {
    const foundJournal = journals.find(
      journal =>
        journal.email.toLowerCase() === email.toLowerCase() ||
        journal.code.toLowerCase() === code.toLowerCase() ||
        journal.name.toLowerCase() === name.toLowerCase(),
    )

    return Promise.resolve(foundJournal)
  }

  static findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('journals', fixtures, orderByField, order, queryObject)
  static findOneBy = values => findOneByMock(values, 'journals', fixtures)
  static findJournalByTeamMember = teamMemberId => {
    const teamMember = fixtures.teamMembers.find(tm => tm.id === teamMemberId)
    return teamMember.team.journal
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  getCorrespondingEditorialAssistant() {
    if (!this.teams) {
      throw new Error('Teams are required')
    }
    return chain(this.teams)
      .find(t => t.role === Team.Role.editorialAssistant)
      .get('members', [])
      .find(m => m.isCorresponding === true)
      .value()
  }

  getTriageEditor() {
    if (!this.teams) {
      throw new ValidationError('Teams are required')
    }
    const triageEditorTeam = this.teams.find(
      t => t.role === Team.Role.triageEditor,
    )

    return triageEditorTeam
      ? triageEditorTeam.members.find(member => member.status === 'active')
      : undefined
  }

  async save() {
    const existingJournal = journals.find(j => j.id === this.id)

    if (existingJournal) {
      assign(existingJournal, this)
    } else {
      journals.push(this)
    }
    return Promise.resolve(this)
  }

  async saveRecursively() {
    this.save()
    if (this.journalArticleTypes.length === 0) return

    this.journalArticleTypes.map(journalArticleType =>
      journalArticleType.save(),
    )
  }

  toDTO() {
    const triageEditorMember = chain(this.teams)
      .find(t => t.role === Team.Role.triageEditor)
      .get('members.0')
      .value()

    return {
      ...this,
      triageEditor: triageEditorMember ? triageEditorMember.toDTO() : undefined,
      articleTypes: this.journalArticleTypes
        ? this.journalArticleTypes.map(jat => jat.articleType.toDTO())
        : undefined,
    }
  }
}

module.exports = Journal
