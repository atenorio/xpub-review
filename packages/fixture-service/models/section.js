const Chance = require('chance')

const chance = new Chance()
const { assign, filter } = require('lodash')

const fixtures = require('../fixtures')

const { sections } = fixtures

const {
  findMock,
  findByMock,
  findAllMock,
  findOneByMock,
} = require('./repositoryMocks')

class Section {
  constructor(props) {
    this.id = chance.guid()
    this.name = props.name || chance.word()
    this.journalId = props.journalId || null
    this.teams = props.teams || []
    this.specialIssues = props.specialIssues || []
  }

  updateProperties(properties) {
    assign(this, properties)
    return this
  }

  async save() {
    sections.push(this)
    return Promise.resolve(this)
  }

  static find = id => findMock(id, 'sections', fixtures)
  static findBy = values => findByMock(values, 'sections', fixtures)
  static findOneBy = values => findOneByMock(values, 'sections', fixtures)
  static findAll = ({ orderByField, order, queryObject }) =>
    findAllMock('sections', fixtures, orderByField, order, queryObject)

  static async findAllByJournal({ journalId }) {
    const sections = filter(fixtures.sections, { journalId })

    return sections
  }
}

module.exports = Section
