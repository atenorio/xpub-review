const { assign, remove, filter, flatMap, find } = require('lodash')
const Chance = require('chance')

const chance = new Chance()
const fixtures = require('../fixtures')

const { teams } = fixtures
const { findMock, findInMock, findOneByMock } = require('./repositoryMocks')
const TeamMember = require('./teamMember')

class Team {
  constructor(props) {
    this.id = chance.guid()
    this.role = props.role || null
    this.manuscriptId = props.manuscriptId || null
    this.journalId = props.journalId || null
    this.sectionId = props.sectionId || null
    this.members = props.members || []
    this.specialIssueId = props.specialIssueId || null
  }

  static get Role() {
    return {
      author: 'author',
      admin: 'admin',
      triageEditor: 'triageEditor',
      reviewer: 'reviewer',
      academicEditor: 'academicEditor',
      editorialAssistant: 'editorialAssistant',
      researchIntegrityPublishingEditor: 'researchIntegrityPublishingEditor',
    }
  }

  static get JournalRoles() {
    return [
      this.Role.triageEditor,
      this.Role.academicEditor,
      this.Role.editorialAssistant,
    ]
  }

  static get StaffRoles() {
    return [this.Role.admin, this.Role.researchIntegrityPublishingEditor]
  }

  static async findOneByUserAndJournalId({ userId, journalId }) {
    let teams

    if (journalId) teams = filter(fixtures.teams, { journalId })
    if (userId) teams = filter(fixtures.teams, t => find(t.members, { userId }))

    if (!teams[0]) return []
    return teams
  }

  static async findAllBy({ role, manuscriptId, submissionId }) {
    let teams
    if (manuscriptId) {
      teams = filter(fixtures.teams, { role, manuscriptId })
    }
    if (submissionId) {
      const manuscripts = filter(fixtures.manuscripts, { submissionId })
      const manuscriptTeams = flatMap(manuscripts, 'teams')
      teams = filter(manuscriptTeams, { role })
    }

    if (!teams[0]) return []

    return teams
  }

  static findOrCreate = async ({
    queryObject,
    eagerLoadRelations,
    options,
  }) => {
    let team = await this.findOneBy({
      queryObject,
      eagerLoadRelations,
    })

    if (!team) {
      team = new Team(options)
      await team.save()
    }

    return team
  }
  static findOneBy = values => findOneByMock(values, 'teams', fixtures)
  static find = id => findMock(id, 'teams', fixtures)
  static findIn = (field, options) =>
    findInMock(field, options, 'teams', fixtures)

  static async findAllByJournal({ journalId }) {
    const teams = filter(fixtures.teams, { journalId })

    return teams
  }

  static async findAllBySection({ sectionId }) {
    const teams = filter(fixtures.teams, { sectionId })

    return teams
  }

  static async findAllBySpecialIssue({ specialIssueId }) {
    const teams = filter(fixtures.teams, { specialIssueId })

    return teams
  }

  addMember(user, invitationOptions) {
    this.members = this.members || []
    const existingMember = this.members.find(
      member => member.userId === user.id,
    )
    if (existingMember) {
      const defaultIdentity = user.getDefaultIdentity()
      if (existingMember.status === TeamMember.Statuses.removed) {
        throw new Error(
          `${this.role} invitation for ${defaultIdentity.email} was removed and can't be invited again`,
        )
      }
      if (existingMember.status === TeamMember.Statuses.expired) {
        existingMember.updateProperties({
          status: TeamMember.Statuses.pending,
        })
        return existingMember
      }
      throw new ValidationError(
        `User ${defaultIdentity.email} is already invited as ${this.role}`,
      )
    }

    const newMember = new TeamMember({
      ...invitationOptions,
      userId: user.id,
      teamId: this.id,
      position: this.members.length,
    })
    newMember.linkUser(user)

    this.members.push(newMember)

    return newMember
  }

  removeMember(memberId) {
    this.members = this.members || []

    const member = this.members.find(member => member.id === memberId)
    if (!member) {
      throw new NotFoundError(
        `The specified user is not invited as a ${this.role}`,
      )
    }
    if (member.isSubmitting) {
      throw new ValidationError(
        `Submitting authors can't be deleted from the team`,
      )
    }

    this.members = this.members.filter(member => member.id !== memberId)
  }

  async save() {
    const existingTeam = teams.find(t => t.id === this.id)
    if (existingTeam) {
      assign(existingTeam, this)
    } else {
      teams.push(this)
    }
    return Promise.resolve(this)
  }
  async saveRecursively() {
    await this.save()
    if (this.members.length === 0) return

    await Promise.all(this.members.map(async member => member.save()))
  }

  async delete() {
    remove(teams, f => f.id === this.id)
  }
}

module.exports = Team
