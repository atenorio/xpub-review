const sections = []

const generateSection = ({ properties, Section }) => {
  const section = new Section(properties || {})
  sections.push(section)

  return section
}

module.exports = { sections, generateSection }
