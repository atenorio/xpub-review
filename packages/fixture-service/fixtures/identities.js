const identities = []

const generateIdentity = ({ properties, Identity }) => {
  const identity = new Identity(properties || {})
  identities.push(identity)

  return identity
}

module.exports = { identities, generateIdentity }
