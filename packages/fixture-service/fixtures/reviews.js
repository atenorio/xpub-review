const reviews = []

const generateReview = ({ properties, Review }) => {
  const review = new Review(properties || {})
  reviews.push(review)

  return review
}

module.exports = { reviews, generateReview }
