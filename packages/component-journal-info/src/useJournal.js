import { useContext } from 'react'
import { JournalContext } from '../'

function useJournal() {
  return useContext(JournalContext)
}

export default useJournal
