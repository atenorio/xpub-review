import { createContext } from 'react'

export default createContext({
  journal: null,
})
