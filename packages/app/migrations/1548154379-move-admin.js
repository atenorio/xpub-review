const models = require('@pubsweet/models')
const { v4 } = require('uuid')

module.exports = {
  up: async () => {
    if (!process.env.EMAIL_SENDER) {
      throw new Error('Please provide EMAIL_SENDER in "packages/app/.env"')
    }

    const adminUser = new models.User({
      defaultIdentity: 'local',
      isSubscribedToEmails: true,
      unsubscribeToken: v4(),
      agreeTc: true,
      isActive: true,
    })
    await adminUser.save()

    const identity = new models.Identity({
      userId: adminUser.id,
      type: 'local',
      isConfirmed: true,
      email: process.env.EMAIL_SENDER.replace('@', '+admin@'),
      givenNames: 'Admin',
      surname: 'Admin',
      title: 'dr',
      aff: 'Hindawi',
      country: 'RO',
      passwordHash:
        '$2b$12$.Ll6THdFQ1Tk26WSst75tu9/PvNjAjqu2xhkf9CZAIlomEh.Ah0pS',
    })

    await adminUser.assignIdentity(identity)
    await adminUser.saveRecursively()

    const team = new models.Team({
      role: 'admin',
    })
    await team.save()

    const teamMember = team.addMember(adminUser, {
      userId: adminUser.id,
      teamId: team.id,
    })

    teamMember.updateProperties({ status: 'accepted' })
    await teamMember.save()
  },
}
