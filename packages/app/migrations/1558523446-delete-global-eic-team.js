const { Team } = require('@pubsweet/models')

module.exports = {
  up: async () => {
    const globalEiCTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.triageEditor,
        manuscriptId: null,
        journalId: null,
      },
      eagerLoadRelations: 'members.user.identities',
    })

    if (!globalEiCTeam) return

    await globalEiCTeam.delete()
  },
}
