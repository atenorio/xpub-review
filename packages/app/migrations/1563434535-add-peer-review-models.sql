INSERT INTO peer_review_model("name",approval_editor,has_figurehead_editor,figurehead_editor_label,has_triage_editor,triage_editor_label,triage_editor_assignment_tool,handling_editor_label,handling_editor_assignment_tool,reviewer_assignement_tool) VALUES (
        'Chief Minus',
        '{"triageEditor"}',
        false,
        null,
        true,
        'Chief Editor',
        '{"manual"}',
        'Academic Editor',
        '{"manual"}',
        '{"manual","publons"}'
);

INSERT INTO peer_review_model("name",approval_editor,has_figurehead_editor,figurehead_editor_label,has_triage_editor,triage_editor_label,triage_editor_assignment_tool,handling_editor_label,handling_editor_assignment_tool,reviewer_assignement_tool) VALUES (
        'Section Editor',
        '{"triageEditor"}',
        false,
        null,
        true,
        'Section Editor',
        '{"manual"}',
        'Academic Editor',
        '{"manual"}',
        '{"manual","publons"}'
);

INSERT INTO peer_review_model("name",approval_editor,has_figurehead_editor,figurehead_editor_label,has_triage_editor,triage_editor_label,triage_editor_assignment_tool,handling_editor_label,handling_editor_assignment_tool,reviewer_assignement_tool) VALUES (
        'Associate Editor',
        '{"triageEditor"}',
        true,
        'Chief Editor',
        true,
        'Associate Editor',
        '{"manual"}',
        'Academic Editor',
        '{"manual"}',
        '{"manual","publons"}'
);