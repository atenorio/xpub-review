ALTER TABLE team
DROP CONSTRAINT team_manuscript_id_fkey;

ALTER TABLE team
ADD CONSTRAINT team_manuscript_id_fkey
FOREIGN KEY (manuscript_id) REFERENCES manuscript (id) ON DELETE CASCADE;

ALTER TABLE team_member
DROP CONSTRAINT team_member_team_id_fkey;

ALTER TABLE team_member
ADD CONSTRAINT team_member_team_id_fkey
FOREIGN KEY (team_id) REFERENCES team (id) ON DELETE CASCADE;

ALTER TABLE review
DROP CONSTRAINT review_team_member_id_fkey;

ALTER TABLE review
ADD CONSTRAINT review_team_member_id_fkey
FOREIGN KEY (team_member_id) REFERENCES team_member (id) ON DELETE CASCADE;