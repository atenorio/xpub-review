CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE "user" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "default_identity" text NOT NULL, -- local, orcid
  "is_subscribed_to_emails" boolean NOT NULL,
  "invitation_token" text,
  "confirmation_token" text,
  "password_reset_token" text,
  "unsubscribe_token" text,
  "is_active" boolean NOT NULL,
  "agree_tc" boolean NOT NULL,
  "password_reset_timestamp" timestamp
);
CREATE TABLE "identity" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "user_id" uuid NOT NULL REFERENCES "user",
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "type" text NOT NULL, -- local, orcid
  "identifier" text, -- e.g. orcid ID
  "surname" text,
  "given_names" text,
  "title" text,
  "aff" text,
  "country" text,
  "is_confirmed" bool NOT NULL,
  "password_hash" text,
  "email" text UNIQUE
);
CREATE TABLE "journal" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "title" text NOT NULL,
  "publisher_name" text
);
CREATE TABLE "manuscript" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "journal_id" uuid NOT NULL REFERENCES journal,
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "ended" timestamp, -- when a new version of the manuscript has been submitted
  "status" text, -- e.g. 'submitted', 'QA'
  "decision" text,
  "title" text,
  "article_type" text, -- research, clinical study
  "abstract" text,
  "custom_id" text, -- hindawi specific ID
  "publication_dates" jsonb, -- { type: 'submitted', date: @iso-8601-date } JATS <pub-date>
  "notes" jsonb, -- store here conflicts and declarations
  "subjects" jsonb,
  "history" jsonb,
  "form_state" text,
  "article_ids" jsonb,
  "version" integer,
  "has_passed_eqs" BOOLEAN,
  "has_passed_eqa" BOOLEAN,
  "technical_check_token" UUID,
  "submission_id" UUID, -- a unique identifier used to group manuscript versions
  "conflicts" jsonb,
  "agree_tc" BOOLEAN
);
CREATE TABLE "team" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "role" text NOT NULL, -- author, reviewer, handlingEditor, editorInChief
  "manuscript_id" uuid REFERENCES manuscript,
  "journal_id" uuid REFERENCES journal
);
CREATE TABLE "team_member" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "user_id" uuid NOT NULL REFERENCES "user",
  "team_id" uuid NOT NULL REFERENCES team,
  "position" integer,
  "is_submitting" boolean,
  "is_corresponding" boolean,
  "status" text NOT NULL, -- pending, accepted, delcined
  "alias" jsonb -- store country and affiliation data
);
CREATE TABLE "review" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "manuscript_id" uuid NOT NULL REFERENCES manuscript,
  "user_id" uuid NOT NULL REFERENCES "user",
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "submitted" timestamp NOT NULL,
  "recommendation" text, -- publish, reject, revise, response to reviewers
  "open" boolean
);
CREATE TABLE "comment" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "review_id" uuid NOT NULL REFERENCES review,
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "type" text, -- private, public
  "content" text
);
CREATE TABLE "file" (
  "id" uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  "manuscript_id" uuid REFERENCES manuscript,
  "comment_id" uuid REFERENCES "comment",
  "created" timestamp NOT NULL DEFAULT current_timestamp,
  "updated" timestamp NOT NULL DEFAULT current_timestamp,
  "type" text NOT NULL, -- JATS <fig>, <supplementary-material>, <table-wrap> e.g. 'figure', 'supplementary', 'table'
  "label" text,
  "file_name" text,
  "url" text,
  "mime_type" text,
  "size" integer,
  "signed_url" text,
  "original_name" text NOT NULL
);
