const { Review } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const reviews = await Review.query().where('recommendation', 'returnToHE')

    await Promise.each(reviews, async review => {
      review.updateProperties({
        recommendation: 'returnToAcademicEditor',
      })
      await review.save()
      logger.info(`Successfully updated recommendation for review ${review.id}`)
    })
  },
}
