INSERT INTO article_type("name") VALUES (
    'Case Report'
);

INSERT INTO article_type("name") VALUES (
    'Erratum and Corrigendum'
);

INSERT INTO article_type("name") VALUES (
    'Editorial'
);

INSERT INTO article_type("name") VALUES (
    'Retraction'
);

INSERT INTO article_type("name") VALUES (
    'Expression of Concern'
);

INSERT INTO article_type("name") VALUES (
    'Case Series'
);
