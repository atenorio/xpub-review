const { Team, TeamMember, Manuscript } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const manuscripts = await Manuscript.findAll({
      eagerLoadRelations: 'teams.members',
    })

    await Promise.each(manuscripts, async manuscript => {
      const triageEditorTeam = manuscript.teams.find(
        team => team.role === Team.Role.triageEditor,
      )

      if (!triageEditorTeam || !triageEditorTeam.length) {
        logger.error(
          `No Triage Editor team has been found for manuscript ${manuscript.id}`,
        )

        return
      }

      triageEditorTeam.members[0].updateProperties({
        status: TeamMember.Statuses.active,
      })
      await triageEditorTeam.members[0].save()

      logger.info(
        `Triage Editor ${triageEditorTeam.members[0].alias.email} had its status updated.`,
      )
    })
  },
}
