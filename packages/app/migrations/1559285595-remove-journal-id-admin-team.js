const { Team } = require('@pubsweet/models')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const adminTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.admin,
      },
    })

    if (!adminTeam) return

    try {
      adminTeam.journalId = null
      await adminTeam.save()
    } catch (e) {
      logger.error(e)
    }
  },
}
