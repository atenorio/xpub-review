UPDATE article_type
SET has_peer_review = true
WHERE name IN ('Research Article', 'Review Article', 'Case Report', 'Case Series')