const { Team, TeamMember, Manuscript } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const manuscripts = await Manuscript.query()
      .whereNotIn('status', [
        Manuscript.Statuses.draft,
        Manuscript.Statuses.technicalChecks,
      ])
      .eager('teams.members')

    await Promise.each(manuscripts, async manuscript => {
      const manuscriptTriageEditorTeam = manuscript.teams.find(
        team => team.role === Team.Role.triageEditor,
      )

      if (manuscriptTriageEditorTeam) {
        logger.info(
          `A Triage Editor team already exists for manuscript ${manuscript.id}`,
        )
      } else {
        const journalTriageEditorTeam = await Team.findOneBy({
          queryObject: {
            role: Team.Role.triageEditor,
            journalId: manuscript.journalId,
          },
          eagerLoadRelations: 'members.user.identities',
        })

        if (!journalTriageEditorTeam) {
          logger.error(
            `No Triage Editor team found for journal ${manuscript.journalId}`,
          )
          return
        }

        const newTriageEditorTeam = new Team({
          role: Team.Role.triageEditor,
          manuscriptId: manuscript.id,
        })
        await newTriageEditorTeam.save()

        const journalTriageEditorUser = journalTriageEditorTeam.members[0].user

        const teamMember = newTriageEditorTeam.addMember(
          journalTriageEditorUser,
          {
            userId: journalTriageEditorUser.id,
            teamId: newTriageEditorTeam.id,
            status: TeamMember.Statuses.pending,
          },
        )
        await teamMember.save()
        logger.info(
          `A Triage Editor team was successfully created for manuscript ${manuscript.id}`,
        )
      }
    })
  },
}
