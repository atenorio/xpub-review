ALTER TABLE comment
DROP CONSTRAINT comment_review_id_fkey;

ALTER TABLE comment
ADD CONSTRAINT comment_review_id_fkey
FOREIGN KEY (review_id) REFERENCES review (id) ON DELETE CASCADE;

ALTER TABLE file
DROP CONSTRAINT file_comment_id_fkey;

ALTER TABLE file
ADD CONSTRAINT file_comment_id_fkey
FOREIGN KEY (comment_id) REFERENCES comment (id) ON DELETE CASCADE;
