const { Journal, PeerReviewModel } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const journals = await Journal.all()
    const peerReviewModels = await PeerReviewModel.all()
    const chiefMinus = peerReviewModels.find(prm => prm.name === 'Chief Minus')

    await Promise.each(journals, async journal => {
      journal.updateProperties({
        peerReviewModelId: chiefMinus.id,
      })

      await journal.save()
      logger.info(
        `Successfully added peer review model to journal ${journal.id}`,
      )
    })
  },
}
