INSERT INTO peer_review_model (
        "name",
        approval_editor,
        has_figurehead_editor,
        figurehead_editor_label,
        has_triage_editor,
        triage_editor_label,
        triage_editor_assignment_tool,
        academic_editor_label,
        academic_editor_assignment_tool,
        reviewer_assignement_tool
        )
        VALUES (
        'Single Tier Academic Editor',
        '{"academicEditor"}',
        false,
        null,
        false,
        null,
        '{}',
        'Academic Editor',
        '{"manual", "workload"}',
        '{"manual","publons"}'
);