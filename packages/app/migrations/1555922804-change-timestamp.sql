ALTER TABLE audit_log
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE audit_log
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE comment
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE comment
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE file
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE file
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE identity
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE identity
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE journal
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE journal
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE manuscript
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE manuscript
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE review
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE review
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE reviewer_suggestion
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE reviewer_suggestion
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE team
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE team
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE team_member
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE team_member
  ALTER COLUMN updated TYPE timestamptz;

ALTER TABLE "user"
  ALTER COLUMN created TYPE timestamptz;
  
ALTER TABLE "user"
  ALTER COLUMN updated TYPE timestamptz;

