ALTER TABLE journal
  RENAME COLUMN title TO name;

ALTER TABLE journal
  ADD COLUMN code text UNIQUE;

ALTER TABLE journal
  ADD COLUMN email text UNIQUE;

ALTER TABLE journal
  ADD COLUMN issn text;

ALTER TABLE journal
  ADD COLUMN apc integer;

ALTER TABLE journal
  ADD COLUMN is_active boolean DEFAULT false;

ALTER TABLE journal
  ADD CONSTRAINT journal_name_key UNIQUE (name);

UPDATE  journal
  SET code = 'BCA', email = 'example@email', apc = 1400, is_active = true
  WHERE name='Bioinorganic Chemistry and Applications';
