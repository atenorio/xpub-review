CREATE TABLE "article_type" (
    id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
    created timestamptz NOT NULL DEFAULT now(),
    updated timestamptz NOT NULL DEFAULT now(),
    "name" text NOT NULL
)