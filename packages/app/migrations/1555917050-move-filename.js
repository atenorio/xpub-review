const { File } = require('@pubsweet/models')
const Promise = require('bluebird')

module.exports = {
  up: async () => {
    const files = await File.all()
    await Promise.each(files, async f => {
      f.providerKey = f.fileName
      f.fileName = f.originalName
      f.save()
    })
  },
}
