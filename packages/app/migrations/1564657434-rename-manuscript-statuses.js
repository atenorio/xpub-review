const { Manuscript } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const renameMap = {
      heInvited: 'academicEditorInvited',
      heAssigned: 'academicEditorAssigned',
      heAssignedEditorialType: 'academicEditorAssignedEditorialType',
    }

    const manuscripts = await Manuscript.query()
      .where('status', 'heInvited')
      .orWhere('status', 'heAssigned')
      .orWhere('status', 'heAssignedEditorialType')

    await Promise.each(manuscripts, async manuscript => {
      manuscript.updateProperties({
        status: renameMap[manuscript.status],
      })
      await manuscript.save()
      logger.info(`Successfully updated status for manuscript ${manuscript.id}`)
    })
  },
}
