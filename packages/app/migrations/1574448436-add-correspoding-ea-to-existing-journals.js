const { Team, Journal } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const journals = await Journal.query().eager('teams.members')

    await Promise.each(journals, async journal => {
      const editorialAssistantTeam = journal.teams.find(
        team => team.role === Team.Role.editorialAssistant,
      )

      if (!editorialAssistantTeam) {
        logger.error(
          `No Editorial Assistant Team has been found on journal ${journal.id}`,
        )

        return
      }

      editorialAssistantTeam.members[0].updateProperties({
        isCorresponding: true,
      })
      await editorialAssistantTeam.members[0].save()

      logger.info(
        `${editorialAssistantTeam.members[0].alias.email} is now corresponding EA on Journal ${journal.code}`,
      )
    })
  },
}
