ALTER TABLE "user"
 ALTER COLUMN unsubscribe_token SET DEFAULT uuid_generate_v4();
 
ALTER TABLE "user"
 ALTER COLUMN confirmation_token SET DEFAULT uuid_generate_v4();
