ALTER TABLE file
DROP CONSTRAINT file_manuscript_id_fkey;

ALTER TABLE file
ADD CONSTRAINT file_manuscript_id_fkey
FOREIGN KEY (manuscript_id) REFERENCES manuscript (id) ON DELETE CASCADE;