const { Manuscript, ArticleType } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const manuscripts = await Manuscript.all()
    await Promise.each(manuscripts, async manuscript => {
      const { oldArticleType } = manuscript
      if (
        !['review', 'research', 'letter-to-editor'].includes(oldArticleType)
      ) {
        logger.info(
          `Manuscript ${manuscript.id} does not have a valid article type: ${oldArticleType}`,
        )

        return
      }

      const articleTypes = {
        review: 'Review Article',
        research: 'Research Article',
        'letter-to-editor': 'Letter to the Editor',
      }

      const savedArticleType = await ArticleType.findOneBy({
        queryObject: { name: articleTypes[oldArticleType] },
      })

      manuscript.updateProperties({
        articleTypeId: savedArticleType.id,
      })

      await manuscript.save()
      logger.info(
        `Successfully added article type to manuscript ${manuscript.id}`,
      )
    })
  },
}
