INSERT INTO peer_review_model("name",approval_editors,has_figurehead_editor,figurehead_editor_label,has_triage_editor,triage_editor_label,triage_editor_assignment_tool,academic_editor_label,academic_editor_assignment_tool,reviewer_assignement_tool) VALUES (
        'Special Issue Chief Minus',
        '{"triageEditor"}',
        false,
        null,
        true,
        'Lead Guest Editor',
        '{"manual"}',
        'Guest Editor',
        '{"manual"}',
        '{"manual","publons"}'
);