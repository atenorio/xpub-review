ALTER TABLE journal
  ADD COLUMN activation_date timestamptz;

UPDATE  journal
  SET activation_date = current_timestamp
  WHERE name='Bioinorganic Chemistry and Applications';