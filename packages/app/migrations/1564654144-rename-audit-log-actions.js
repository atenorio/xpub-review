const { AuditLog } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const renameMap = {
      he_removed: 'academic_editor_removed',
      role_he_assigned: 'role_academic_editor_assigned',
      role_eic_assigned: 'role_triage_editor_assigned',
    }

    const auditLogs = await AuditLog.query()
      .where('action', 'he_removed')
      .orWhere('action', 'role_he_assigned')

    await Promise.each(auditLogs, async auditLog => {
      auditLog.updateProperties({
        action: renameMap[auditLog.action],
      })
      await auditLog.save()
      logger.info(`Successfully updated action for audit log ${auditLog.id}`)
    })
  },
}
