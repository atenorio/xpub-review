const { Manuscript } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const manuscripts = await Manuscript.all()
    await Promise.each(manuscripts, async manuscript => {
      const { conflicts } = manuscript
      if (!conflicts) {
        return
      }

      manuscript.updateProperties({
        conflictOfInterest: conflicts.message || null,
        dataAvailability: conflicts.dataAvailabilityMessage || null,
        fundingStatement: conflicts.fundingMessage || null,
      })

      await manuscript.save()
      logger.info(
        `Successfully updated questions for manuscript ${manuscript.id}`,
      )
    })
  },
}
