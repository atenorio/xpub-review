ALTER TABLE special_issue
  ADD COLUMN custom_id text DEFAULT lpad(floor(random()*999999)::varchar,6,'0');