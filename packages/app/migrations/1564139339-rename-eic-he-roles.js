const { Team } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const teams = await Team.query()
      .where('role', 'editorInChief')
      .orWhere('role', 'handlingEditor')

    await Promise.each(teams, async team => {
      const { role } = team

      team.updateProperties({
        role:
          role === 'editorInChief'
            ? Team.Role.triageEditor
            : Team.Role.academicEditor,
      })

      await team.save()

      logger.info(`Successfully updated role for team ${team.id}`)
    })
  },
}
