const { SpecialIssue, PeerReviewModel } = require('@pubsweet/models')
const { Promise } = require('bluebird')
const logger = require('@pubsweet/logger')

module.exports = {
  up: async () => {
    const specialIssues = await SpecialIssue.all()
    const specialIssueChiefMinus = await PeerReviewModel.findOneBy({
      queryObject: {
        name: 'Special Issue Chief Minus',
      },
    })

    await Promise.each(specialIssues, async specialIssue => {
      specialIssue.updateProperties({
        peerReviewModelId: specialIssueChiefMinus.id,
      })

      await specialIssue.save()
      logger.info(
        `Successfully added peer review model to specialIssue ${specialIssue.id}`,
      )
    })
  },
}
