ALTER TABLE special_issue
  ADD COLUMN is_cancelled boolean DEFAULT false;