process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const chance = new Chance()
const authsomeMode = require('../config/authsome-mode')

describe('authsome mode', () => {
  describe('admin policy', () => {
    it('should return false when the user is logged in but not admin', async () => {
      const { Team, User, Identity } = models
      let team
      team = fixtures.getTeamByRole(Team.Role.admin)
      if (!team)
        team = fixtures.generateTeam({
          properties: { role: Team.Role.admin },
          Team,
        })

      const user = fixtures.generateUser({ User, Identity })
      const context = {}
      context.models = models
      const result = await authsomeMode(
        user.id,
        { policies: [Team.Role.admin] },
        {},
        context,
      )
      expect(result).toEqual(false)
    })
    it('should return success when the user is admin', async () => {
      const { Team, User, Identity, TeamMember } = models
      let team
      team = fixtures.getTeamByRole(Team.Role.admin)
      if (!team)
        team = fixtures.generateTeam({
          properties: { role: Team.Role.admin },
          Team,
        })
      const user = fixtures.generateUser({ User, Identity })

      const teamMember = fixtures.generateTeamMember({
        properties: {
          userId: user.id,
          teamId: team.id,
        },
        TeamMember,
      })
      team.members.push(teamMember)

      const context = {}
      context.models = models
      const result = await authsomeMode(
        user.id,
        { policies: [Team.Role.admin] },
        {},
        context,
      )
      expect(result).toEqual(true)
    })
    it('should return false when the user is not logged in', async () => {
      const { Team } = models
      let team
      team = fixtures.getTeamByRole(Team.Role.admin)
      if (!team)
        team = fixtures.generateTeam({
          properties: { role: Team.Role.admin },
          Team,
        })

      const context = {}
      context.models = models
      const result = await authsomeMode(
        chance.guid(),
        { policies: [Team.Role.admin] },
        {},
        context,
      )
      expect(result).toEqual(false)
    })
  })
  describe('is editorial assistant policy', () => {
    it('should return true when the user is editorial assistant on the given journal', async () => {
      const { PeerReviewModel, Journal, Team, Manuscript, TeamMember } = models
      const peerReviewModel = fixtures.generatePeerReviewModel({
        properties: {
          approvalEditors: [Team.Role.triageEditor],
        },
        PeerReviewModel,
      })

      const journal = fixtures.generateJournal({
        properties: {
          peerReviewModelId: peerReviewModel.id,
        },
        Journal,
      })
      const role = Team.Role.editorialAssistant
      let team = fixtures.getTeamByRoleAndJournalId({
        role,
        journalId: journal.id,
      })
      if (!team)
        team = fixtures.generateTeam({
          properties: { role, journalId: journal.id },
          Team,
        })

      const editorialAssistant = await dataService.createUserOnJournal({
        models,
        role,
        journal,
        fixtures,
        input: { status: TeamMember.Statuses.pending },
      })
      team.members = [editorialAssistant]

      const manuscript = fixtures.generateManuscript({
        properties: { journalId: journal.id },
        Manuscript,
      })
      const context = {}
      context.models = models

      const result = await authsomeMode(
        editorialAssistant.userId,
        { policies: ['isEditorialAssistant'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(true)
    })
  })
  describe('is triage editor policy', () => {
    it('should return false when the user is not admin, Triage Editor or EA', async () => {
      const {
        Team,
        Journal,
        Manuscript,
        PeerReviewModel,
        User,
        Identity,
      } = models

      const peerReviewModel = fixtures.generatePeerReviewModel({
        properties: {
          approvalEditors: [Team.Role.triageEditor],
        },
        PeerReviewModel,
      })

      const journal = fixtures.generateJournal({
        properties: {
          peerReviewModelId: peerReviewModel.id,
        },
        Journal,
      })
      const role = Team.Role.triageEditor

      let team = fixtures.getTeamByRoleAndJournalId({
        role,
        journalId: journal.id,
      })
      if (!team)
        team = fixtures.generateTeam({
          properties: { role, journalId: journal.id },
          Team,
        })

      const manuscript = fixtures.generateManuscript({
        properties: { journalId: journal.id },
        Manuscript,
      })
      const user = fixtures.generateUser({ User, Identity })
      const context = {}
      context.models = models

      const result = await authsomeMode(
        user.id,
        { policies: ['isTriageEditor'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(false)
    })
    it('should return success when the user is admin, triageEditor or editorialAssistant', async () => {
      const {
        Team,
        Journal,
        Manuscript,
        PeerReviewModel,
        User,
        TeamMember,
        Identity,
      } = models

      const peerReviewModel = fixtures.generatePeerReviewModel({
        properties: {
          approvalEditors: [Team.Role.triageEditor],
        },
        PeerReviewModel,
      })

      const journal = fixtures.generateJournal({
        properties: {
          peerReviewModelId: peerReviewModel.id,
        },
        Journal,
      })
      const role = Team.Role.triageEditor

      let team = fixtures.getTeamByRoleAndJournalId({
        role,
        journalId: journal.id,
      })
      if (!team)
        team = fixtures.generateTeam({
          properties: { role, journalId: journal.id },
          Team,
        })

      const manuscript = fixtures.generateManuscript({
        properties: { journalId: journal.id },
        Manuscript,
      })

      const user = fixtures.generateUser({ User, Identity })

      const teamMember = fixtures.generateTeamMember({
        properties: {
          userId: user.id,
          teamId: team.id,
        },
        TeamMember,
      })
      team.members.push(teamMember)
      teamMember.team = team

      dataService.addUserOnManuscript({
        models,
        manuscript,
        fixtures,
        user,
        role: Team.Role.triageEditor,
        input: { status: TeamMember.Statuses.active },
      })

      const context = {}
      context.models = models
      const result = await authsomeMode(
        user.id,
        { policies: ['isTriageEditor'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(true)
    })
  })
  describe('is academic editor policy', () => {
    const { Manuscript, User, Identity, Team } = models
    it('should return false when the user is logged in but not academic editor', async () => {
      const manuscript = fixtures.generateManuscript({ Manuscript })

      const user = fixtures.generateUser({ User, Identity })
      const context = { models }

      const result = await authsomeMode(
        user.id,
        { policies: ['isAcademicEditorOnManuscript'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(false)
    })

    it('should return success when the user is academic editor', async () => {
      const manuscript = fixtures.generateManuscript({ Manuscript })
      const user = fixtures.generateUser({ User, Identity })
      dataService.addUserOnManuscript({
        models,
        manuscript,
        fixtures,
        user,
        role: Team.Role.academicEditor,
      })

      const context = { models }
      const result = await authsomeMode(
        user.id,
        { policies: ['isAcademicEditorOnManuscript'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(true)
    })
  })
  describe('has access to manuscript policy', () => {
    const { Manuscript, User, Identity, Team } = models
    it('should return false when the user does not have access to the manuscript', async () => {
      const manuscript = fixtures.generateManuscript({ Manuscript })

      const user = fixtures.generateUser({ User, Identity })
      const context = { models }

      const result = await authsomeMode(
        user.id,
        { policies: ['hasAccessToManuscript'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(false)
    })

    it('should return true when the user is has access to the manuscript', async () => {
      const manuscript = fixtures.generateManuscript({ Manuscript })
      const user = fixtures.generateUser({ User, Identity })
      dataService.addUserOnManuscript({
        models,
        manuscript,
        fixtures,
        user,
        role: Team.Role.reviewer,
      })

      const context = { models }
      const result = await authsomeMode(
        user.id,
        { policies: ['hasAccessToManuscript'] },
        { manuscriptId: manuscript.id },
        context,
      )
      expect(result).toEqual(true)
    })
  })
  describe('has access to manuscript versions policy', () => {
    const { User, Identity, Manuscript, Team } = models
    it('should return false when the user does not have access to the manuscript versions', async () => {
      const user = fixtures.generateUser({ User, Identity })
      const submissionId = chance.guid()
      fixtures.generateManuscript({
        properties: {
          version: 1,
          submissionId,
          status: Manuscript.Statuses.olderVersion,
        },
        Manuscript,
      })
      fixtures.generateManuscript({
        properties: {
          version: 2,
          submissionId,
          status: Manuscript.Statuses.accepted,
        },
        Manuscript,
      })
      const context = { models }

      const result = await authsomeMode(
        user.id,
        { policies: ['hasAccessToManuscriptVersions'] },
        { submissionId },
        context,
      )
      expect(result).toEqual(false)
    })

    it('should return true when the user is has access to the manuscript versions', async () => {
      const user = fixtures.generateUser({ User, Identity })
      const submissionId = chance.guid()
      const firstVersion = fixtures.generateManuscript({
        properties: {
          version: 1,
          submissionId,
          status: Manuscript.Statuses.olderVersion,
        },
        Manuscript,
      })
      const secondVersion = fixtures.generateManuscript({
        properties: {
          version: 2,
          submissionId,
          status: Manuscript.Statuses.accepted,
        },
        Manuscript,
      })
      dataService.addUserOnManuscript({
        models,
        manuscript: firstVersion,
        fixtures,
        user,
        role: Team.Role.reviewer,
      })
      dataService.addUserOnManuscript({
        models,
        manuscript: secondVersion,
        fixtures,
        user,
        role: Team.Role.reviewer,
      })

      const context = { models }
      const result = await authsomeMode(
        user.id,
        { policies: ['hasAccessToManuscriptVersions'] },
        { submissionId },
        context,
      )
      expect(result).toEqual(true)
    })
  })
})
