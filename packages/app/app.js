const logger = require('@pubsweet/logger')
const startServer = require('pubsweet/src/startup/start')
const tryRequireRelative = require('pubsweet-server/src/helpers/tryRequireRelative')
const { createQueueService } = require('@hindawi/queue-service')
const { forEach } = require('lodash')

const config = require('config')
require('dotenv').config()

const events = {}

if (config.has('pubsweet.components')) {
  config.get('pubsweet.components').forEach(componentName => {
    getSchemaRecursively(componentName)
  })
}

createQueueService()
  .then(messageQueue => {
    forEach(events, (handler, event) =>
      messageQueue.registerEventHandler({ event, handler }),
    )
    return messageQueue
  })
  .then(queueService => {
    queueService.start()
    global.applicationEventBus = queueService
  })
  .then(startServer)
  .catch(err => {
    logger.error('FATAL ERROR, SHUTTING DOWN:', err)
    process.exit(1)
  })

function getSchemaRecursively(componentName) {
  const component = tryRequireRelative(componentName)

  if (!component) throw new Error(`Could not find component ${componentName}`)

  if (component.eventHandlers) {
    Object.assign(events, component.eventHandlers)
  }
}
