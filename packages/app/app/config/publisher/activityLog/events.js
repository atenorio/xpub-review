module.exports = {
  manuscript_submitted: 'submitted a new manuscript',
  manuscript_returned: 'returned manuscript with comments to',
  manuscript_rejected: 'decision is to Reject manuscript',
  manuscript_refused_to_consider:
    'refused to consider manuscript after quality checks',
  manuscript_accepted: 'decision is to Publish manuscript',
  author_added: 'added as an author',
  author_removed: 'removed author',
  author_edited: 'edited author',
  withdraw_requested: 'requested to withdraw manuscript',
  eqs_approved: 'approved manuscript after technical checks',
  eqs_return_to_draft: 'returned manuscript to draft after technical checks',
  eqs_declined: 'refused to consider manuscript after technical checks',
  academic_editor_removed: 'removed from the manuscript',
  reviewer_agreed: 'accepted invitation to review',
  reviewer_declined: 'declined invitation to review',
  reviewer_invited: 'sent reviewer invitation to',
  review_submitted: 'submitted a report',
  reviewer_invitation_removed: 'removed invitation to review from',
  reminder_invitation_first:
    'sent first automatic reminder to accept invitation to',
  reminder_invitation_second:
    'sent second automatic reminder to accept invitation to',
  reminder_invitation_third:
    'sent third automatic reminder to accept invitation to',
  reminder_invitation_removed: 'removed invitation to handle manuscript from',
  reminder_invite_reviewer: 'sent automatic reminder to invite reviewers to',
  reminder_invite_reviewer_first:
    'sent first automatic reminder to invite reviewers to',
  reminder_invite_reviewer_second:
    'sent second automatic reminder to invite reviewers to',
  reminder_invite_reviewer_third:
    'sent third automatic reminder to invite reviewers to',
  reminder_not_invited_enough_reviewers:
    'send automatic reminder to inform that AE not invited enough reviewers to',
  reminder_report: 'sent automatic reminder to submit report to',
  reminder_report_first: 'sent first automatic reminder to submit report to',
  reminder_report_second: 'sent second automatic reminder to submit report to',
  reminder_report_third: 'sent third automatic reminder to submit report to',
  revision_submitted: 'submitted manuscript revision',
  revision_requested: 'requested a revision',
  revision_requested_minor: 'requested a minor revision',
  revision_requested_major: 'requested a major revision',
  recommendation_reject: 'recommended to Reject manuscript',
  recommendation_accept: 'recommended to Publish manuscript',
  invitation_sent: 'sent invitation to work on the manuscript to',
  invitation_resent: 'resent invitation to work on the manuscript to',
  invitation_revoked: 'revoked invitation sent to',
  invitation_agreed: 'accepted invitation to work on the manuscript',
  invitation_declined: 'declined invitation to work on the manuscript',
  file_added: 'added a new file',
  file_removed: 'removed a file',
  role_admin_assigned: 'assigned as admin',
  role_triage_editor_assigned: 'assigned as Chief Editor',
  role_academic_editor_assigned: 'assigned as Academic Editor',
  role_academic_editor_reassigned: 'has reassigned the manuscript to',
  reminder_submit_report_first:
    'sent first automatic reminder to submit report to',
  reminder_submit_report_second:
    'sent second automatic reminder to submit report to',
  reminder_submit_report_third:
    'sent third automatic reminder to submit report to',
  quality_checks_requested: 'requested quality check updates',
  quality_checks_submitted: 'submitted quality check updates',
  triage_editor_assigned: 'assigned on the manuscript',
  triage_editor_removed: 'removed from the manuscript',
}
