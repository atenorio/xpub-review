const events = require('./activityLog/events')
const objectTypes = require('./activityLog/objectTypes')

module.exports = {
  activityLogEvents: events,
  activityLogObjectTypes: objectTypes,
}
