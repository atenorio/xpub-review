const metadata = require('./metadata')
const manuscriptTypes = require('./manuscript-types')
const titles = require('./titles')
const statuses = require('./statuses')

module.exports = {
  statuses,
  metadata,
  manuscriptTypes,
  titles,
}
