module.exports = {
  draft: {
    color: 'statusInProgress',
    author: {
      label: 'Complete Submission',
      needsAttention: true,
      archived: false,
    },
    admin: {
      label: 'Complete Submission',
      needsAttention: false,
      inProgress: true,
    },
    editorialAssistant: {
      label: 'Complete Submission',
      needsAttention: false,
      inProgress: true,
    },
  },
  technicalChecks: {
    color: 'statusInProgress',
    author: {
      label: 'Submitted',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve QA',
      needsAttention: true,
    },
  },
  submitted: {
    color: 'statusInProgress',
    author: {
      label: 'Submitted',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'Assign AE',
      needsAttention: true,
    },
    admin: {
      label: 'Assign AE',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Assign AE',
      needsAttention: true,
    },
  },
  academicEditorInvited: {
    color: 'statusInProgress',
    author: {
      label: 'AE Invited',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    triageEditor: {
      label: 'AE Invited',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  academicEditorAssigned: {
    color: 'statusInProgress',
    author: {
      label: 'AE Assigned',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
    triageEditor: {
      label: 'AE Assigned',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Invite Reviewers',
      needsAttention: true,
    },
  },
  reviewersInvited: {
    color: 'statusInProgress',
    author: {
      label: 'Reviewers Invited',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Invitation Pending',
      needsAttention: true,
    },
    triageEditor: {
      label: 'Reviewers Invited',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    admin: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Respond to Invite',
      needsAttention: true,
    },
  },
  underReview: {
    color: 'statusInProgress',
    author: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Under Review',
      needsAttention: true,
    },
    triageEditor: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Complete Review',
      needsAttention: true,
    },
    admin: {
      label: 'Complete Review',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Complete Review',
      needsAttention: true,
    },
  },
  reviewCompleted: {
    color: 'statusInProgress',
    author: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
    triageEditor: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Recommendation',
      needsAttention: true,
    },
  },
  revisionRequested: {
    color: 'statusInProgress',
    author: {
      label: 'Submit Revision',
      needsAttention: true,
    },
    academicEditor: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'Revision Requested',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Submit Revision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Submit Revision',
      needsAttention: true,
    },
  },
  pendingApproval: {
    color: 'statusInProgress',
    author: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'Make Decision',
      needsAttention: true,
    },
    reviewer: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Decision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Decision',
      needsAttention: true,
    },
  },
  makeDecision: {
    color: 'statusInProgress',
    author: {
      label: 'Pending Approval',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Make Decision',
      needsAttention: true,
    },
    triageEditor: {
      label: 'Make Decision',
      needsAttention: true,
    },
    reviewer: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Decision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Decision',
      needsAttention: true,
    },
    researchIntegrityPublishingEditor: {
      label: 'Make Decision',
      needsAttention: true,
    },
  },
  academicEditorAssignedEditorialType: {
    color: 'statusInProgress',
    author: {
      label: 'AE Assigned',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Make Decision',
      needsAttention: true,
    },
    triageEditor: {
      label: 'Under Review',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Make Decision',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Make Decision',
      needsAttention: true,
    },
  },
  rejected: {
    color: 'statusRejected',
    author: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    academicEditor: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Rejected',
      needsAttention: false,
      archived: false,
    },
  },
  refusedToConsider: {
    color: 'statusRejected',
    author: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    academicEditor: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Refused To Consider',
      needsAttention: false,
      archived: false,
    },
  },
  inQA: {
    color: 'statusInProgress',
    author: {
      label: 'Pending approval',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    reviewer: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve QA',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve QA',
      needsAttention: true,
    },
    researchIntegrityPublishingEditor: {
      label: 'QA',
      needsAttention: false,
      inProgress: true,
    },
  },
  accepted: {
    color: 'statusApproved',
    author: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    academicEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
  },

  qualityChecksRequested: {
    color: 'statusInProgress',
    author: {
      label: 'Submit Updates',
      needsAttention: true,
      archived: false,
    },
    academicEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
  },
  qualityChecksSubmitted: {
    color: 'statusApproved',
    author: {
      label: 'Updates Submitted',
      needsAttention: false,
      archived: false,
    },
    academicEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Accepted',
      needsAttention: false,
      archived: false,
    },
  },
  published: {
    color: 'statusApproved',
    author: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    academicEditor: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    triageEditor: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    reviewer: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    admin: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    editorialAssistant: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
    researchIntegrityPublishingEditor: {
      label: 'Published',
      needsAttention: false,
      archived: false,
    },
  },

  withdrawalRequested: {
    color: 'statusApproved',
    author: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    academicEditor: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    triageEditor: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
    reviewer: {
      label: 'Withdrawal Requested',
      needsAttention: false,
      inProgress: true,
    },
    admin: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
    editorialAssistant: {
      label: 'Approve Withdrawal',
      needsAttention: true,
    },
  },
  withdrawn: {
    color: 'statusWithdrawn',
    author: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    academicEditor: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    triageEditor: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Withdrawn',
      needsAttention: false,
      archived: true,
    },
  },
  deleted: {
    color: 'statusApproved',
    author: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    academicEditor: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    triageEditor: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    reviewer: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    admin: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
    editorialAssistant: {
      label: 'Deleted',
      needsAttention: false,
      archived: true,
    },
  },
  olderVersion: {
    color: 'statusWithdrawn',
    reviewer: {
      label: 'Review Completed',
      needsAttention: false,
      archived: false,
    },
  },
}
