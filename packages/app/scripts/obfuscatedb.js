/* eslint-disable no-console */
const { Promise } = require('bluebird')
const { Chance } = require('chance')
const {
  Identity,
  TeamMember,
  Journal,
  ReviewerSuggestion,
} = require('@pubsweet/models')

const chance = new Chance()

const update = async (entities, updateFn) => {
  console.time('Done: ')
  await Promise.map(entities, updateFn, { concurrency: 10 })
  console.timeEnd('Done: ')
}

const executeIdentities = async () => {
  const identities = await Identity.all()
  const updateFn = identity => {
    identity.updateProperties({
      surname: chance.last(),
      givenNames: chance.first(),
      email: `hindawiqa+${identity.userId}@thinslices.com`,
      passwordHash:
        '$2b$12$wHEt1h6q5nwFRYHtRvVovO6b34HTIy/ykMyG/tJX2cXahLDKphQAO',
    })
    return identity.save()
  }
  await update(identities, updateFn)
}

const executeTeamMembers = async () => {
  const teamMembers = await TeamMember.findAll({
    eagerLoadRelations: 'team',
  })
  const updateFn = teamMember => {
    teamMember.updateProperties({
      alias: {
        ...teamMember.alias,
        email: `hindawiqa+${teamMember.role}-${teamMember.userId}@thinslices.com`,
        surname: chance.last(),
        givenNames: chance.first(),
      },
    })
    return teamMember.save()
  }
  await update(teamMembers, updateFn)
}

const executeJournals = async () => {
  const journals = await Journal.all()
  const updateFn = journal => {
    journal.updateProperties({
      email: `hindawiqa+${journal.code}@thinslices.com`,
    })
    return journal.save()
  }
  await update(journals, updateFn)
}

const executeReviewerSuggestions = async () => {
  const reviewerSuggestions = await ReviewerSuggestion.all()
  const updateFn = reviewerSuggestion => {
    reviewerSuggestion.updateProperties({
      surname: chance.last(),
      givenNames: chance.first(),
      email: `hindawiqa+${reviewerSuggestion.id}@thinslices.com`,
    })
    return reviewerSuggestion.save()
  }
  await update(reviewerSuggestions, updateFn)
}

const execute = async () => {
  console.time('Done all in: ')
  await executeTeamMembers()
  await executeIdentities()
  await executeReviewerSuggestions()
  await executeJournals()
  console.timeEnd('Done all in: ')
}

execute()
