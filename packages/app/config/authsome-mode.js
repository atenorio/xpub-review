const Promise = require('bluebird')
const { last } = require('lodash')

const authsomeMode = async (userId, { name, policies = [] }, object, context) =>
  Promise.reduce(
    policies,
    async (acc, policy) => {
      if (acc === false) return acc

      if (typeof policy === 'function') {
        return policy(userId, name, object, context)
      }
      if (typeof authsomePolicies[policy] === 'function') {
        return authsomePolicies[policy](userId, name, object, context)
      }
      throw new Error(
        `⛔️ Cannot find policy '${policy}' for action '${name}'.`,
      )
    },
    true,
  )

const authsomePolicies = {
  isAuthenticated(userId, name, object, context) {
    return !!userId
  },
  isUnauthenticated(userId, name, object, context) {
    return !userId
  },
  async admin(userId, name, object, context) {
    const { Team } = context.models
    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members',
    })

    if (!adminTeam) return false
    const isAdmin = adminTeam.members.find(member => member.userId === userId)

    return !!isAdmin
  },
  async isRIPE(userId, name, object, context) {
    const { Team } = context.models
    const RIPETeam = await Team.findOneBy({
      queryObject: { role: Team.Role.researchIntegrityPublishingEditor },
      eagerLoadRelations: 'members',
    })

    if (!RIPETeam) return false
    const isRIPE = RIPETeam.members.find(member => member.userId === userId)

    return !!isRIPE
  },
  async isEditorialAssistant(userId, name, object, context) {
    let manuscript
    const isAdmin = await this.admin(userId, name, object, context)

    if (isAdmin) {
      return true
    }

    const { Team, TeamMember, Manuscript } = context.models

    if (object.submissionId) {
      manuscript = await Manuscript.findOneBy({
        queryObject: { submissionId: object.submissionId },
      })
    } else if (object.manuscriptId) {
      manuscript = await Manuscript.find(object.manuscriptId)
    } else if (object.teamMemberId) {
      manuscript = await Manuscript.findManuscriptByTeamMember(
        object.teamMemberId,
      )
    }

    if (!manuscript) return false

    if (!manuscript.journalId) {
      const editorialAssistant = await TeamMember.findOneByUserAndRole({
        userId,
        role: Team.Role.editorialAssistant,
      })

      return !!editorialAssistant
    }

    const editorialAssistantTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.editorialAssistant,
        journalId: manuscript.journalId,
      },
      eagerLoadRelations: 'members',
    })

    if (!editorialAssistantTeam) {
      return false
    }

    return editorialAssistantTeam.members.some(
      member => member.userId === userId,
    )
  },
  async isAcademicEditorOnManuscript(userId, name, object, context) {
    if (
      await authsomePolicies.isEditorialAssistant(userId, name, object, context)
    ) {
      return true
    }
    const { Team } = context.models
    const manuscriptAcademicEditorTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: object.manuscriptId,
        role: Team.Role.academicEditor,
      },
      eagerLoadRelations: 'members',
    })
    if (!manuscriptAcademicEditorTeam) return false
    const isAcademicEditorOnManuscript = manuscriptAcademicEditorTeam.members.find(
      member => member.userId === userId,
    )
    return !!isAcademicEditorOnManuscript
  },
  async isRIPEOnManuscript(userId, name, object, context) {
    if (
      await authsomePolicies.isEditorialAssistant(userId, name, object, context)
    ) {
      return true
    }
    const { Team } = context.models
    const manuscriptRIPETeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: object.manuscriptId,
        role: Team.Role.researchIntegrityPublishingEditor,
      },
      eagerLoadRelations: 'members',
    })
    if (!manuscriptRIPETeam) return false

    const isRIPEOnManuscript = manuscriptRIPETeam.members.find(
      member => member.userId === userId,
    )
    return !!isRIPEOnManuscript
  },
  async isTriageEditor(userId, name, object, context) {
    let manuscript
    const isEditorialAssistant = await this.isEditorialAssistant(
      userId,
      name,
      object,
      context,
    )
    if (isEditorialAssistant) {
      return true
    }

    const { Team, Manuscript, TeamMember } = context.models
    if (object.submissionId) {
      manuscript = await Manuscript.findOneBy({
        queryObject: { submissionId: object.submissionId },
      })
    }
    if (object.manuscriptId) {
      manuscript = await Manuscript.find(object.manuscriptId)
    }
    if (object.teamMemberId) {
      const teamMember = await TeamMember.find(
        object.teamMemberId,
        'team.manuscript',
      )
      manuscript = await Manuscript.findOneBy({
        queryObject: { submissionId: teamMember.team.manuscript.submissionId },
      })
    }

    const triageEditorTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.triageEditor,
        manuscriptId: manuscript.id,
      },
      eagerLoadRelations: 'members',
    })
    if (!triageEditorTeam) {
      return false
    }
    return triageEditorTeam.members.some(
      member =>
        member.userId === userId &&
        member.status === TeamMember.Statuses.active,
    )
  },
  async hasAccessToManuscript(userId, name, object, context) {
    const isTriageEditor = await this.isTriageEditor(
      userId,
      name,
      object,
      context,
    )
    if (isTriageEditor) {
      return true
    }

    const isRIPE = await this.isRIPE(userId, name, object, context)
    if (isRIPE) {
      return true
    }

    const user = await context.models.User.find(
      userId,
      'teamMemberships.[team]',
    )

    const matchingMember = user.teamMemberships.find(
      member => member.team.manuscriptId === object.manuscriptId,
    )

    return !!matchingMember
  },
  async hasAccessToManuscriptVersions(userId, name, object, context) {
    const { Manuscript, User, TeamMember } = context.models
    const isTriageEditor = await this.isTriageEditor(
      userId,
      name,
      object,
      context,
    )
    if (isTriageEditor) {
      return true
    }
    const user = await User.find(userId, 'teamMemberships.[team]')
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: object.submissionId,
      excludedStatus: Manuscript.Statuses.draft,
    })
    const manuscriptIds = manuscripts.map(m => m.id)
    if (last(manuscripts).status === Manuscript.Statuses.deleted) {
      return false
    }

    const matchingMember = user.teamMemberships
      .filter(
        member =>
          member.status !== TeamMember.Statuses.declined &&
          member.status !== TeamMember.Statuses.expired,
      )
      .find(member => manuscriptIds.includes(member.team.manuscriptId))

    return !!matchingMember
  },
  async isApprovalEditor(userId, name, object, context) {
    const { TeamMember } = context.models
    const isEditorialAssistant = await this.isEditorialAssistant(
      userId,
      name,
      object,
      context,
    )
    if (isEditorialAssistant) {
      return true
    }
    const isRIPEOnManuscript = await this.isRIPEOnManuscript(
      userId,
      name,
      object,
      context,
    )
    if (isRIPEOnManuscript) {
      return true
    }

    const isApprovalEditor = await TeamMember.isApprovalEditor({
      userId,
      models: context.models,
      manuscriptId: object.manuscriptId,
    })
    return isApprovalEditor
  },
  async canMakeRecommendation(userId, name, object, context) {
    let canMakeRecommendation = false
    const { PeerReviewModel, Team, Manuscript } = context.models
    const manuscript = await Manuscript.find(object.manuscriptId)
    let peerReviewModel
    if (manuscript.specialIssueId) {
      peerReviewModel = await PeerReviewModel.findOneBySpecialIssue(
        manuscript.specialIssueId,
      )
    } else {
      peerReviewModel = await PeerReviewModel.findOneByJournal(
        manuscript.journalId,
      )
    }

    const team = await Team.findOneByManuscriptId({
      manuscriptId: object.manuscriptId,
      userId,
    })
    if (
      team.role === Team.Role.academicEditor &&
      !peerReviewModel.approvalEditors.includes(team.role)
    ) {
      canMakeRecommendation = true
    }
    return canMakeRecommendation
  },
  async isEditor(userId, name, object, context) {
    if (
      await authsomePolicies.isAcademicEditorOnManuscript(
        userId,
        name,
        object,
        context,
      )
    ) {
      return true
    }

    if (await authsomePolicies.isTriageEditor(userId, name, object, context)) {
      return true
    }

    return false
  },
  async isReviewerOnReview(userId, name, object, context) {
    const { Team, Review } = context.models
    const review = await Review.find(object.reviewId)

    const reviewerTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: review.manuscriptId,
        role: Team.Role.reviewer,
      },
      eagerLoadRelations: 'members',
    })
    if (!reviewerTeam) return false

    const reviewer = reviewerTeam.members.find(
      member => member.userId === userId,
    )
    return !!reviewer
  },
}

module.exports = authsomeMode
