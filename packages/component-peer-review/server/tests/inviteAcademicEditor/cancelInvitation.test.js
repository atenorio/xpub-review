const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { cancelAcademicEditorInvitationUseCase } = require('../../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'invitation_revoked',
}
logEvent.objectType = { user: 'user' }

const jobsService = {
  cancelJobs: jest.fn(),
}

describe('Cancel invitation', () => {
  let notificationService = {}
  beforeEach(() => {
    notificationService = {
      notifyAcademicEditor: jest.fn(),
    }
  })
  it('cancels an invitation when the input is correct', async () => {
    const { Team, Journal, Manuscript, TeamMember, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorInvited,
      },
      Manuscript,
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.academicEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    await cancelAcademicEditorInvitationUseCase
      .initialize({ notificationService, models, logEvent, jobsService })
      .execute({ teamMemberId: academicEditor.id })

    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
  it('cancels an invitation when the manuscript is part of a special issue', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      SpecialIssue,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    const specialIssue = fixtures.generateSpecialIssue({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      SpecialIssue,
    })
    specialIssue.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        specialIssueId: specialIssue.id,
        status: Manuscript.Statuses.academicEditorInvited,
      },
      Manuscript,
    })
    manuscript.specialIssue = specialIssue

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.academicEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })
    await cancelAcademicEditorInvitationUseCase
      .initialize({ notificationService, models, logEvent, jobsService })
      .execute({ teamMemberId: academicEditor.id })

    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
  })
})
