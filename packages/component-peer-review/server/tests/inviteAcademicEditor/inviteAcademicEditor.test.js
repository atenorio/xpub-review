const Chance = require('chance')

const chance = new Chance()
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  User,
  Identity,
  Team,
  Journal,
  Manuscript,
  TeamMember,
  PeerReviewModel,
} = models

const useCases = require('../../src/useCases/inviteAcademicEditor')

const { inviteAcademicEditorUseCase } = useCases

const invitationsService = {
  sendInvitationToAcademicEditor: jest.fn(),
}
const emailJobsService = {
  scheduleEmailsWhenAcademicEditorIsInvited: jest.fn(),
}
const removalJobsService = {
  scheduleRemovalJobForAcademicEditor: jest.fn(),
}

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_sent: 'invitation_sent',
}
logEvent.objectType = { user: 'user' }

describe('Invite Academic Editor', () => {
  it('adds a user in the Academic Editor team', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const academicEditorUser = fixtures.generateUser({ User, Identity })
    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: {
        isSubmitting: true,
      },
    })

    await inviteAcademicEditorUseCase
      .initialize({
        models,
        useCases,
        logEvent,
        emailJobsService,
        removalJobsService,
        invitationsService,
      })
      .execute({
        reqUserId: triageEditor.id,
        userId: academicEditorUser.id,
        submissionId: manuscript.submissionId,
      })

    const academicEditorTeam = fixtures.teams.find(
      t =>
        t.manuscriptId === manuscript.id && t.role === Team.Role.academicEditor,
    )

    expect(academicEditorTeam.members).toHaveLength(1)
    expect(academicEditorTeam.members[0].userId).toEqual(academicEditorUser.id)
    expect(academicEditorTeam.members[0].status).toEqual(
      TeamMember.Statuses.pending,
    )

    expect(
      invitationsService.sendInvitationToAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenAcademicEditorIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(
      removalJobsService.scheduleRemovalJobForAcademicEditor,
    ).toHaveBeenCalledTimes(1)
  })

  it('creates a team if one does not already exist', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const academicEditor = fixtures.generateUser({ User, Identity })
    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    const result = inviteAcademicEditorUseCase
      .initialize({
        invitationsService,
        models,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId: manuscript.submissionId,
        userId: academicEditor.id,
        reqUserId: triageEditor.id,
      })

    expect(
      invitationsService.sendInvitationToAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenAcademicEditorIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(
      removalJobsService.scheduleRemovalJobForAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    return expect(result).resolves.not.toThrow()
  })

  it('returns an error if the user is already invited', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
        academicEditorLabel: 'Academic Editor',
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    try {
      const result = await inviteAcademicEditorUseCase
        .initialize({
          invitationsService,
          models,
          logEvent,
          emailJobsService,
          removalJobsService,
          useCases,
        })
        .execute({
          submissionId: manuscript.submissionId,
          userId: academicEditor.userId,
          reqUserId: triageEditor.id,
        })

      expect(result).not.toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(`The Academic Editor is already invited.`)
    }
  })

  it('returns an error if the user is removed or declined on older versions', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
        academicEditorLabel: 'Academic Editor',
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      properties: { journalId: journal.id, submissionId, version: 1 },
      Manuscript,
    })

    const secondVersion = fixtures.generateManuscript({
      properties: { journalId: journal.id, submissionId, version: 2 },
      Manuscript,
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript: firstVersion,
      role: Team.Role.academicEditor,
      input: {
        status: chance.pickone([
          TeamMember.Statuses.removed,
          TeamMember.Statuses.declined,
        ]),
      },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: firstVersion,
      input: {
        isSubmitting: true,
      },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: secondVersion,
      input: {
        isSubmitting: true,
      },
    })

    try {
      const result = await inviteAcademicEditorUseCase
        .initialize({
          invitationsService,
          models,
          logEvent,
          emailJobsService,
          removalJobsService,
          useCases,
        })
        .execute({
          submissionId,
          userId: academicEditor.userId,
          reqUserId: triageEditor.id,
        })

      expect(result).not.toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(`The Academic Editor can't be invited.`)
    }
  })

  it('adds an expired user to the second version', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const submissionId = chance.guid()
    const firstVersion = fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId,
        journalId: journal.id,
        status: Manuscript.Statuses.olderVersion,
      },
      Manuscript,
    })

    const secondVersion = fixtures.generateManuscript({
      properties: {
        version: 2,
        submissionId,
        journalId: journal.id,
        status: Manuscript.Statuses.reviewCompleted,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript: firstVersion,
      fixtures,
      input: { status: TeamMember.Statuses.removed },
      role: Team.Role.academicEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: firstVersion,
      input: {
        isSubmitting: true,
      },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.author,
      manuscript: secondVersion,
      input: {
        isSubmitting: true,
      },
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript: secondVersion,
      fixtures,
      input: { status: TeamMember.Statuses.expired },
      role: Team.Role.academicEditor,
    })

    await inviteAcademicEditorUseCase
      .initialize({
        invitationsService,
        models,
        logEvent,
        emailJobsService,
        removalJobsService,
        useCases,
      })
      .execute({
        submissionId,
        userId: academicEditor.userId,
        reqUserId: triageEditor.id,
      })

    const academicEditorTeam = secondVersion.teams.find(
      team => team.role === Team.Role.academicEditor,
    )

    expect(academicEditorTeam.members).toHaveLength(1)
    expect(academicEditorTeam.members[0].status).toEqual(
      TeamMember.Statuses.pending,
    )
  })
})
