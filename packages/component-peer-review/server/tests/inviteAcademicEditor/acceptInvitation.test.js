const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const useCases = require('../../src/useCases')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_agreed: 'invitation_agreed',
}
logEvent.objectType = { manuscript: 'manuscript' }

const jobsService = {
  cancelJobs: jest.fn(),
}

describe('Accept invitation', () => {
  let notificationService = {}
  let emailJobsService = {}
  beforeEach(() => {
    notificationService = {
      notifyTriageEditor: jest.fn(),
      notifyEditorialAssistant: jest.fn(),
      notifyActiveAcademicEditor: jest.fn(),
      notifyPendingAcademicEditor: jest.fn(),
    }
    emailJobsService = {
      sendAcademicEditorRemindersToInviteReviewers: jest.fn(),
    }
  })

  it('accepts an invitation when the input is correct', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      PeerReviewModel,
      ArticleType,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    const teamMember = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.pending },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
    })

    await useCases.acceptAcademicEditorInvitationUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        jobsService,
        emailJobsService,
        notificationService,
      })
      .execute({ teamMemberId: teamMember.id, userId: teamMember.userId })

    expect(teamMember.status).toEqual(TeamMember.Statuses.accepted)
    expect(notificationService.notifyTriageEditor).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyPendingAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.sendAcademicEditorRemindersToInviteReviewers,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      Manuscript.Statuses.academicEditorAssigned,
    )
  })
  it('notifies the Editorial Assistant under the Single Tier AE peer review model', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      ArticleType,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    const teamMember = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.pending },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
    })

    await useCases.acceptAcademicEditorInvitationUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        jobsService,
        emailJobsService,
        notificationService,
      })
      .execute({ teamMemberId: teamMember.id, userId: teamMember.userId })

    expect(teamMember.status).toEqual(TeamMember.Statuses.accepted)
    expect(notificationService.notifyTriageEditor).toHaveBeenCalledTimes(0)
    expect(notificationService.notifyEditorialAssistant).toHaveBeenCalledTimes(
      1,
    )

    expect(
      notificationService.notifyPendingAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.sendAcademicEditorRemindersToInviteReviewers,
    ).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(
      Manuscript.Statuses.academicEditorAssigned,
    )
  })
  it('accepts the pending AE invitation and revokes the active AE', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      ArticleType,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await fixtures.generateArticleTypes(ArticleType)
    const articleType = fixtures.getArticleTypeByPeerReview(true)

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        articleTypeId: articleType.id,
      },
      Manuscript,
    })
    const activeAcademicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })
    const pendingAcademicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
    })

    await useCases.acceptAcademicEditorInvitationUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        jobsService,
        emailJobsService,
        notificationService,
      })
      .execute({
        teamMemberId: pendingAcademicEditor.id,
        userId: pendingAcademicEditor.userId,
      })

    expect(activeAcademicEditor.status).toEqual(TeamMember.Statuses.removed)
    expect(pendingAcademicEditor.status).toEqual(TeamMember.Statuses.accepted)
    expect(
      notificationService.notifyActiveAcademicEditor,
    ).toHaveBeenCalledTimes(1)
    expect(
      notificationService.notifyPendingAcademicEditor,
    ).toHaveBeenCalledTimes(1)
  })
})
