const { models, fixtures } = require('fixture-service')

const { getAcademicEditorsUseCase } = require('../../src/useCases')

describe('Get academic editors', () => {
  it('returns an empty array if there is no journal Academic Editor team', async () => {
    const { Team, Journal, Manuscript, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
      },
      Manuscript,
    })
    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute(manuscript.id)

    expect(academicEditorMembers).toHaveLength(0)
  })

  it('returns all academic editors except those that declined', async () => {
    const {
      Team,
      Journal,
      User,
      Identity,
      PeerReviewModel,
      TeamMember,
      Manuscript,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    let journalAcademicEditorTeam
    journalAcademicEditorTeam = fixtures.getTeamByRoleAndJournalId({
      journalId: journal.id,
      role: Team.Role.academicEditor,
    })
    if (!journalAcademicEditorTeam) {
      journalAcademicEditorTeam = fixtures.generateTeam({
        properties: { journalId: journal.id, role: Team.Role.academicEditor },
        Team,
      })
    }

    fixtures.generateUser({ User, Identity })
    fixtures.generateUser({ User, Identity })

    const academicEditor = fixtures.generateUser({ User, Identity })
    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: academicEditor.id,
        teamId: journalAcademicEditorTeam.id,
      },
      TeamMember,
    })
    teamMember.linkUser(academicEditor)
    journalAcademicEditorTeam.members.push(teamMember)

    // adding a declined Academic Editor to a manuscript
    const declinedAcademicEditor = fixtures.generateUser({ User, Identity })
    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    const manuscriptTeam = fixtures.generateTeam({
      properties: {
        role: Team.Role.academicEditor,
        manuscriptId: manuscript.id,
      },
      Team,
    })

    const declinedTeamMember = fixtures.generateTeamMember({
      properties: {
        userId: declinedAcademicEditor.id,
        teamId: manuscriptTeam.id,
        status: TeamMember.Statuses.declined,
      },
      TeamMember,
    })

    declinedTeamMember.linkUser(declinedAcademicEditor)
    manuscriptTeam.members.push(declinedTeamMember)
    manuscript.teams.push(manuscriptTeam)

    // adding the declined Academic Editor to the journal Academic Editor team
    const journalDeclinedTeamMember = fixtures.generateTeamMember({
      properties: {
        userId: declinedAcademicEditor.id,
        teamId: journalAcademicEditorTeam.id,
      },
      TeamMember,
    })

    journalDeclinedTeamMember.linkUser(declinedAcademicEditor)
    journalAcademicEditorTeam.members.push(journalDeclinedTeamMember)

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute(manuscript.id)

    expect(academicEditorMembers).toHaveLength(1)
    expect(academicEditorMembers[0].userId).toEqual(academicEditor.id)
  })

  it('returns all academic editors only from the manuscript`s journal', async () => {
    const {
      Team,
      Journal,
      User,
      Identity,
      PeerReviewModel,
      TeamMember,
      Manuscript,
    } = models
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    const otherJournal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    let journalAcademicEditorTeam
    journalAcademicEditorTeam = fixtures.getTeamByRoleAndJournalId({
      journalId: journal.id,
      role: Team.Role.academicEditor,
    })
    if (!journalAcademicEditorTeam) {
      journalAcademicEditorTeam = fixtures.generateTeam({
        properties: {
          journalId: journal.id,
          role: Team.Role.academicEditor,
        },
        Team,
      })
    }

    fixtures.generateUser({ User, Identity })
    fixtures.generateUser({ User, Identity })

    const academicEditor = fixtures.generateUser({ User, Identity })
    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: academicEditor.id,
        teamId: journalAcademicEditorTeam.id,
      },
      TeamMember,
    })
    teamMember.linkUser(academicEditor)
    journalAcademicEditorTeam.members.push(teamMember)

    // create a Academic Editor team on the second journal and add a member
    const otherJournalAcademicEditorTeam = fixtures.generateTeam({
      properties: {
        journalId: otherJournal.id,
        role: Team.Role.academicEditor,
      },
      Team,
    })
    const otherAcademicEditor = fixtures.generateUser({ User, Identity })
    const otherTeamMember = fixtures.generateTeamMember({
      properties: {
        userId: otherAcademicEditor.id,
        teamId: otherJournalAcademicEditorTeam.id,
      },
      TeamMember,
    })
    otherTeamMember.linkUser(otherAcademicEditor)
    otherJournalAcademicEditorTeam.members.push(otherTeamMember)

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })

    const academicEditorMembers = await getAcademicEditorsUseCase
      .initialize(models)
      .execute(manuscript.id)

    expect(academicEditorMembers).toHaveLength(1)
    expect(academicEditorMembers[0].userId).toEqual(academicEditor.id)
  })
})
