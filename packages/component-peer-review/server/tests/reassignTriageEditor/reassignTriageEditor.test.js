const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { reassignTriageEditorUseCase } = require('../../src/useCases')

const notificationService = {
  notifyRemovedTriageEditor: jest.fn(),
  notifyAssignedTriageEditor: jest.fn(),
}
const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  triage_editor_assigned: 'triage_editor_assigned',
}
logEvent.objectType = { user: 'user' }

describe('Reassign triage editor', () => {
  it('reassigns the triage editor on a manuscript', async () => {
    const { Team, Journal, Manuscript, TeamMember, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.admin,
    })

    const removedTriageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const assignedTriageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorAssigned,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: {
        status: TeamMember.Statuses.pending,
        isSubmitting: true,
      },
      role: Team.Role.author,
    })

    const removedTriageEditorOnManuscript = await dataService.createUserOnManuscript(
      {
        models,
        manuscript,
        fixtures,
        input: {
          userId: removedTriageEditor.userId,
          status: TeamMember.Statuses.active,
        },
        role: Team.Role.triageEditor,
      },
    )

    await reassignTriageEditorUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: assignedTriageEditor.id,
      })

    expect(
      notificationService.notifyAssignedTriageEditor,
    ).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyRemovedTriageEditor).toHaveBeenCalledTimes(
      1,
    )

    expect(removedTriageEditorOnManuscript.status).toEqual(
      TeamMember.Statuses.removed,
    )

    const assignedTriageEditorOnManuscript = fixtures.teamMembers.find(
      tm =>
        tm.status === TeamMember.Statuses.active &&
        tm.userId === assignedTriageEditor.userId,
    )

    expect(assignedTriageEditorOnManuscript).toBeDefined()
  })
})
