// const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const useCases = require('../../src/useCases')

const { requestRevisionUseCase } = useCases

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'returned manuscript with comments to',
}
logEvent.objectType = { manuscript: 'manuscript' }

const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}

const chance = new Chance()
describe('Request revision use case', () => {
  let notificationService = {}
  beforeEach(() => {
    notificationService = {
      notifyReviewers: jest.fn(),
      notifyTriageEditor: jest.fn(),
      notifyAcademicEditor: jest.fn(),
      notifySubmittingAuthor: jest.fn(),
    }
  })
  it('creates a minor review when a minor revision is requested', async () => {
    const {
      Team,
      Review,
      Journal,
      Manuscript,
      TeamMember,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const submissionId = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId,
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    manuscript.journal = journal

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.accepted },
    })

    const academicEditor = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    await requestRevisionUseCase
      .initialize({
        models,
        useCases,
        logEvent,
        jobsService,
        notificationService,
      })
      .execute({
        comment: chance.sentence(),
        manuscriptId: manuscript.id,
        userId: academicEditor.userId,
        type: Review.Recommendations.minor,
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.revisionRequested)
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    expect(manuscripts).toHaveLength(2)
    expect(
      manuscripts.find(
        m => m.version === 2 && m.status === Manuscript.Statuses.draft,
      ),
    ).toBeTruthy()
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(1)
  })
  it('creates a regular revision when a regular revision is requested', async () => {
    const { Team, Journal, Review, Manuscript, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const submissionId = chance.guid()
    const manuscript = fixtures.generateManuscript({
      properties: {
        version: 1,
        submissionId,
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    manuscript.journal = journal

    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await requestRevisionUseCase
      .initialize({
        models,
        useCases,
        logEvent,
        jobsService,
        notificationService,
      })
      .execute({
        comment: chance.sentence(),
        manuscriptId: manuscript.id,
        userId: triageEditor.userId,
        type: Review.Recommendations.revision,
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.revisionRequested)
    const manuscripts = fixtures.manuscripts.filter(
      m => m.submissionId === submissionId,
    )
    expect(manuscripts).toHaveLength(2)
    expect(
      manuscripts.find(
        m => m.version === 2 && m.status === Manuscript.Statuses.draft,
      ),
    ).toBeTruthy()
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(0)
  })
})
