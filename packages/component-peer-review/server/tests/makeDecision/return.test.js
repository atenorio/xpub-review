const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeDecisionToReturnUseCase,
} = require('../../src/useCases/makeDecision')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'returned manuscript with comments to',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyAcademicEditor: jest.fn(),
}
const chance = new Chance()
describe('Make decision to return to Academic Editor', () => {
  it('creates a new review', async () => {
    const { Team, Manuscript, Journal, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: 'accepted' },
    })
    await makeDecisionToReturnUseCase
      .initialize({ notificationService, models, logEvent })
      .execute({
        manuscriptId: manuscript.id,
        userId: triageEditor.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.reviewCompleted)
  })
})
