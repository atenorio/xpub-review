// const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeDecisionToRejectUseCase,
} = require('../../src/useCases/makeDecision')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_returned: 'returned manuscript with comments to',
}
logEvent.objectType = { manuscript: 'manuscript' }

const eventsService = {
  publishSubmissionEvent: jest.fn(() => {}),
}
const chance = new Chance()

const jobsService = { cancelJobs: jest.fn() }
const sendPackage = jest.fn()

describe('Make decision to reject', () => {
  let notificationService

  beforeEach(() => {
    notificationService = {
      notifyAcademicEditor: jest.fn(),
      notifyAuthorsNoPeerReview: jest.fn(),
      notifyTriageEditor: jest.fn(),
      notifyReviewers: jest.fn(),
      notifyAuthorsAfterPeerReview: jest.fn(),
    }
  })
  it('rejects a manuscript without peer review, approval editor is the triage editor', async () => {
    const { Team, Manuscript, TeamMember, Journal, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      TeamMember,
      role: Team.Role.triageEditor,
    })
    await makeDecisionToRejectUseCase
      .initialize({
        logEvent,
        jobsService,
        sendPackage,
        eventsService,
        notificationService,
        models,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: triageEditor.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.rejected)
    expect(notificationService.notifyAuthorsNoPeerReview).toHaveBeenCalled()
    expect(notificationService.notifyTriageEditor).toHaveBeenCalledTimes(0)
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(0)
  })
  it('rejects a manuscript with peer review, approval editor is the triage editor', async () => {
    const {
      Team,
      Journal,
      Manuscript,
      TeamMember,
      ArticleType,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })

    await fixtures.generateArticleTypes(ArticleType)
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
        articleType: fixtures.getArticleTypeByPeerReview(true),
      },
      Manuscript,
    })
    manuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.submitted },
    })

    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.academicEditor,
    })
    await makeDecisionToRejectUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        sendPackage,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: triageEditor.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.rejected)
    expect(notificationService.notifyAuthorsNoPeerReview).toHaveBeenCalledTimes(
      0,
    )
    expect(notificationService.notifyReviewers).toHaveBeenCalled()
    expect(notificationService.notifyAuthorsAfterPeerReview).toHaveBeenCalled()
  })
  it('rejects a manuscript with peer review, approval editor is the academic editor', async () => {
    const {
      Team,
      Manuscript,
      TeamMember,
      Journal,
      PeerReviewModel,
      ArticleType,
    } = models
    fixtures.generateArticleTypes(ArticleType)
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor, Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
        articleType: fixtures.getArticleTypeByPeerReview(true),
      },
      Manuscript,
    })
    manuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,

      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.submitted },
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })
    const academicEditor = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.academicEditor,
      input: { status: 'accepted' },
    })
    await makeDecisionToRejectUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        sendPackage,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: academicEditor.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.rejected)
    expect(notificationService.notifyAuthorsNoPeerReview).toHaveBeenCalledTimes(
      0,
    )
    expect(notificationService.notifyReviewers).toHaveBeenCalled()
    expect(notificationService.notifyTriageEditor).toHaveBeenCalled()
    expect(notificationService.notifyAuthorsAfterPeerReview).toHaveBeenCalled()
  })
})
