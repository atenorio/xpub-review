const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeDecisionToPublishUseCase,
} = require('../../src/useCases/makeDecision')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  manuscript_accepted: 'decision is to Publish manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const notificationService = {
  notifyEQA: jest.fn(),
}
const eventsService = {
  publishSubmissionEvent: jest.fn(() => {}),
}
const sendPackage = jest.fn()
const chance = new Chance()
const { Team, Manuscript, Journal, PeerReviewModel, Review } = models

describe('Make decision to publish', () => {
  it('updates the manuscript status', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await makeDecisionToPublishUseCase
      .initialize({
        models,
        logEvent,
        sendPackage,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: triageEditor.userId,
        content: chance.sentence(),
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.inQA)
    expect(manuscript.technicalCheckToken).toBeDefined()
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyEQA).toHaveBeenCalledTimes(1)
  })
  it('sets the correct user on the decision', async () => {
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })

    const editorialAssistant = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await makeDecisionToPublishUseCase
      .initialize({
        models,
        logEvent,
        sendPackage,
        eventsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: editorialAssistant.userId,
        content: chance.sentence(),
      })

    const publishReview = fixtures.reviews.find(
      r =>
        r.manuscriptId === manuscript.id &&
        r.recommendation === Review.Recommendations.publish,
    )

    expect(publishReview.teamMemberId).toEqual(editorialAssistant.id)
  })
})
