const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_declined: 'declined invitation to review',
}
logEvent.objectType = { manuscript: 'manuscript' }

const { declineReviewerInvitationUseCase } = require('../../src/useCases')

const notificationService = {
  notifyAcademicEditor: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}

describe('Decline invitation as a reviewer', () => {
  it('decline the invitation', async () => {
    const { PeerReviewModel, Journal, Team, TeamMember, Manuscript } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    const teamMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.reviewer,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending, isSubmitting: true },
      role: Team.Role.author,
    })

    const academicEditorMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    await declineReviewerInvitationUseCase
      .initialize({ notificationService, models, logEvent, jobsService })
      .execute({
        teamMemberId: teamMember.id,
        userId: academicEditorMember.userId,
      })

    expect(teamMember.status).toEqual(TeamMember.Statuses.declined)
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
  })
})
