const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const Chance = require('chance')
const { acceptReviewerInvitationUseCase } = require('../../src/useCases')

const chance = new Chance()

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_agreed: 'accepted invitation to review',
}
logEvent.objectType = { manuscript: 'manuscript' }

const emailJobsService = {
  scheduleEmailsWhenReviewerAcceptsInvitation: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}

const notificationService = {
  notifyReviewer: jest.fn(),
  notifyAcademicEditor: jest.fn(),
}

describe('Accept invitation as a reviewer', () => {
  const { Team, Journal, Manuscript, TeamMember, PeerReviewModel } = models
  let peerReviewModel
  let journal
  let manuscript
  let pendingReviewer
  let reviewerTeam
  beforeEach(async () => {
    peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
      input: { status: TeamMember.Statuses.pending },
    })

    manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.reviewersInvited,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true, isCorresponding: false },
    })
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })
    pendingReviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      status: TeamMember.Statuses.pending,
    })
    reviewerTeam = fixtures.generateTeam({
      properties: {
        role: Team.Role.reviewer,
      },
      Team,
    })
    reviewerTeam.members = [pendingReviewer]
    pendingReviewer.teams = [reviewerTeam]
    reviewerTeam.manuscript = [manuscript]
  })
  it('should accept the invitation', async () => {
    await acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models,
        logEvent,
        jobsService,
        emailJobsService,
        removalJobsService,
      })
      .execute({
        teamMemberId: pendingReviewer.id,
        userId: pendingReviewer.userId,
      })
    expect(pendingReviewer.status).toEqual(TeamMember.Statuses.accepted)
    expect(pendingReviewer.reviewerNumber).toEqual(1)
    expect(
      emailJobsService.scheduleEmailsWhenReviewerAcceptsInvitation,
    ).toHaveBeenCalledTimes(1)
    expect(removalJobsService.scheduleRemovalJob).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
  })
  it('should throw an error if you try to respond after recommendation', async () => {
    manuscript.status = Manuscript.Statuses.pendingApproval
    const result = acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models,
        logEvent,
        jobsService,
        emailJobsService,
        removalJobsService,
      })
      .execute({
        teamMemberId: pendingReviewer.id,
        userId: pendingReviewer.userId,
      })
    return expect(result).rejects.toThrow('Your invitation has expired')
  })
  it('should assign the old reviewer number if accepting on a new version and was accepted on an older one', async () => {
    const submissionId = chance.guid()
    manuscript.submissionId = submissionId
    manuscript.version = 1

    const secondManuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        submissionId,
        version: 2,
        status: Manuscript.Statuses.reviewersInvited,
      },
      Manuscript,
    })
    secondManuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      manuscript: secondManuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted, isSubmitting: true },
      role: Team.Role.author,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript: secondManuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    let firstReviewer
    let secondReviewer
    firstReviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted, reviewerNumber: 1 },
      role: Team.Role.reviewer,
    })
    secondReviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.submitted, reviewerNumber: 2 },
      role: Team.Role.reviewer,
    })
    firstReviewer = await dataService.addUserOnManuscript({
      models,
      fixtures,
      role: Team.Role.reviewer,
      manuscript: secondManuscript,
      input: { status: TeamMember.Statuses.pending },
      user: fixtures.users.find(u => u.id === firstReviewer.userId),
    })
    secondReviewer = await dataService.addUserOnManuscript({
      models,
      fixtures,
      manuscript: secondManuscript,
      input: { status: TeamMember.Statuses.accepted, reviewerNumber: 2 },
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === secondReviewer.userId),
    })

    await acceptReviewerInvitationUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        emailJobsService,
        removalJobsService,
        notificationService,
      })
      .execute({
        teamMemberId: firstReviewer.id,
        userId: firstReviewer.userId,
      })
    expect(firstReviewer.status).toEqual(TeamMember.Statuses.accepted)
    expect(firstReviewer.reviewerNumber).toEqual(1)
  })
  it('should assign the next reiewer number if accepting on a new version and was not accepted on an older one', async () => {
    const submissionId = chance.guid()
    manuscript.submissionId = submissionId
    manuscript.version = 1

    const secondManuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        submissionId,
        version: 2,
        status: Manuscript.Statuses.reviewersInvited,
      },
      Manuscript,
    })
    secondManuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      manuscript: secondManuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending, isSubmitting: true },
      role: Team.Role.author,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript: secondManuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    let firstReviewer
    let secondReviewer
    let thirdReviewer
    firstReviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.reviewer,
    })
    secondReviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.submitted, reviewerNumber: 1 },
      role: Team.Role.reviewer,
    })
    thirdReviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.submitted, reviewerNumber: 2 },
      role: Team.Role.reviewer,
    })
    firstReviewer = await dataService.addUserOnManuscript({
      models,
      fixtures,
      manuscript: secondManuscript,
      input: { status: TeamMember.Statuses.pendings },
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === firstReviewer.userId),
    })
    secondReviewer = await dataService.addUserOnManuscript({
      models,
      fixtures,
      manuscript: secondManuscript,
      input: { status: TeamMember.Statuses.submitted, reviewerNumber: 1 },
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === secondReviewer.userId),
    })
    thirdReviewer = await dataService.addUserOnManuscript({
      models,
      fixtures,
      manuscript: secondManuscript,
      input: { status: TeamMember.Statuses.submitted, reviewerNumber: 2 },
      role: Team.Role.reviewer,
      user: fixtures.users.find(u => u.id === thirdReviewer.userId),
    })

    await acceptReviewerInvitationUseCase
      .initialize({
        notificationService,
        models,
        logEvent,
        jobsService,
        emailJobsService,
        removalJobsService,
      })
      .execute({ teamMemberId: firstReviewer.id, userId: firstReviewer.userId })
    expect(firstReviewer.status).toEqual('accepted')
    expect(firstReviewer.reviewerNumber).toEqual(3)
  })
})
