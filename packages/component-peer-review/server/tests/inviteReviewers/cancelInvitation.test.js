const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  invitation_revoked: 'revoked invitation sent to',
}
logEvent.objectType = { user: 'user' }

const { cancelReviewerInvitationUseCase } = require('../../src/useCases')

const notificationService = {
  notifyReviewer: jest.fn(),
}
const jobsService = {
  cancelJobs: jest.fn(),
}

describe('Cancel reviewer invitation', () => {
  it('cancel the invitation to user', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      User,
      Identity,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const reviewer = fixtures.generateUser({ User, Identity })
    const identity = fixtures.generateIdentity({
      properties: { userId: reviewer.id },
      Identity,
    })
    reviewer.identities = [identity]

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    const manuscriptTeam = fixtures.generateTeam({
      properties: { role: Team.Role.reviewer, manuscriptId: manuscript.id },
      Team,
    })
    manuscript.teams.push(manuscriptTeam)
    manuscriptTeam.manuscript = manuscript

    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: reviewer.id,
        teamId: manuscriptTeam.id,
        status: TeamMember.Statuses.pending,
      },
      TeamMember,
    })
    teamMember.user = reviewer
    teamMember.team = manuscriptTeam
    teamMember.linkUser(reviewer)
    manuscriptTeam.members.push(teamMember)

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending, isSubmitting: true },
      role: Team.Role.author,
    })

    const academicEditorMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    await cancelReviewerInvitationUseCase
      .initialize({ notificationService, models, logEvent, jobsService })
      .execute({
        manuscriptId: manuscript.id,
        teamMemberId: teamMember.id,
        userId: academicEditorMember.userId,
      })

    const reviewerTeam = fixtures.teams.find(
      team => team.role === Team.Role.reviewer,
    )

    expect(notificationService.notifyReviewer).toHaveBeenCalledTimes(1)
    expect(reviewerTeam).toBeUndefined()
  })
})
