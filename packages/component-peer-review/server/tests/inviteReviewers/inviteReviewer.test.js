const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  reviewer_invited: 'sent reviewer invitation to',
}
logEvent.objectType = { user: 'user' }

const { inviteReviewerUseCase } = require('../../src/useCases')

const emailJobsService = {
  scheduleEmailsWhenReviewerIsInvited: jest.fn(),
}

const removalJobsService = {
  scheduleRemovalJob: jest.fn(),
}

const invitationsService = {
  sendInvitationToReviewer: jest.fn(),
}

describe('Invite reviewer', () => {
  it('Invite user in the reviewer team', async () => {
    const {
      PeerReviewModel,
      Journal,
      Team,
      TeamMember,
      Manuscript,
      User,
      Identity,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
        academicEditorLabel: 'Academic Editor',
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.triageEditor,
    })
    const reviewer = fixtures.generateUser({ User, Identity })
    const identity = fixtures.generateIdentity({
      properties: { userId: reviewer.id },
      Identity,
    })
    reviewer.identities = [identity]
    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })
    const manuscriptTeam = fixtures.generateTeam({
      properties: { role: Team.Role.reviewer, manuscriptId: manuscript.id },
      Team,
    })
    manuscriptTeam.members = [reviewer]
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending, isSubmitting: true },
      role: Team.Role.author,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.admin,
    })

    const academicEditorMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })

    await inviteReviewerUseCase
      .initialize({
        invitationsService,
        models,
        logEvent,
        emailJobsService,
        removalJobsService,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          email: reviewer.identities[0].email,
          givenNames: reviewer.identities[0].givenNames,
          surname: reviewer.identities[0].surname,
        },
        userId: academicEditorMember.userId,
      })

    expect(manuscriptTeam.members).toHaveLength(1)
    expect(manuscriptTeam.members.map(m => m.identities[0].userId)).toContain(
      reviewer.id,
    )
    expect(invitationsService.sendInvitationToReviewer).toHaveBeenCalledTimes(1)
    expect(
      emailJobsService.scheduleEmailsWhenReviewerIsInvited,
    ).toHaveBeenCalledTimes(1)
    expect(removalJobsService.scheduleRemovalJob).toHaveBeenCalledTimes(1)
  })
})
