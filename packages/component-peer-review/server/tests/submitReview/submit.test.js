// const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { submitReviewUseCase } = require('../../src/useCases/submitReview')

const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  review_submitted: 'submitted a report',
}
logEvent.objectType = { manuscript: 'manuscript' }

const chance = new Chance()
describe('Submit review use case', () => {
  let notificationService = {}
  let jobsService = {}
  beforeEach(() => {
    notificationService = {
      notifyReviewers: jest.fn(),
      notifyTriageEditor: jest.fn(),
      notifyAcademicEditor: jest.fn(),
      notifySubmittingAuthor: jest.fn(),
    }
    jobsService = {
      cancelJobs: jest.fn(),
      cancelStaffMemberJobs: jest.fn(),
    }
  })
  it('submits a review', async () => {
    const {
      Team,
      Review,
      Journal,
      Comment,
      Manuscript,
      TeamMember,
      PeerReviewModel,
    } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.pendingApproval,
      },
      Manuscript,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const reviewer = await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.reviewer,
      input: { status: TeamMember.Statuses.accepted },
    })

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const reviewType = chance.pickone([
      Review.Recommendations.minor,
      Review.Recommendations.major,
      Review.Recommendations.reject,
      Review.Recommendations.publish,
    ])

    const review = fixtures.generateReview({
      properties: {
        teamMemberId: reviewer.id,
        recommendation: reviewType,
        manuscriptId: manuscript.id,
      },
      Review,
    })
    await fixtures.generateComment({
      properties: {
        reviewId: review.id,
        type: Comment.Types.public,
        content: chance.sentence(),
      },
      Comment,
    })
    await fixtures.generateComment({
      properties: {
        type: Comment.Types.private,
        reviewId: review.id,
        content: chance.sentence(),
      },
      Comment,
    })

    await submitReviewUseCase
      .initialize({ notificationService, models, logEvent, jobsService })
      .execute({
        reviewId: review.id,
        userId: reviewer.userId,
      })
    expect(reviewType).toContain(review.recommendation)
    expect(notificationService.notifyAcademicEditor).toHaveBeenCalledTimes(1)
    expect(manuscript.status).toEqual(Manuscript.Statuses.reviewCompleted)
  })
})
