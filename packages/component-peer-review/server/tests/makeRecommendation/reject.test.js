const logEvent = () => jest.fn(async () => {})
logEvent.actions = {
  recommendation_reject: 'recommended to Publish manuscript',
}
logEvent.objectType = { manuscript: 'manuscript' }

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const {
  makeRecommendationToRejectUseCase,
} = require('../../src/useCases/makeRecommendation')

const notificationService = {
  notifyReviewers: jest.fn(),
  notifyTriageEditor: jest.fn(),
}

const jobsService = {
  cancelJobs: jest.fn(),
  cancelStaffMemberJobs: jest.fn(),
}

const chance = new Chance()
describe('Make recommendation to reject', () => {
  it('changes the manuscript status to pending approval', async () => {
    const { Team, Journal, Manuscript, TeamMember, PeerReviewModel } = models

    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })

    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.editorialAssistant,
      input: { status: TeamMember.Statuses.pending },
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
      input: { status: TeamMember.Statuses.pending },
    })

    const manuscript = fixtures.generateManuscript({
      properties: { journalId: journal.id },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true, isCorresponding: false },
    })

    const academicEditorMember = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const reviewer = await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.reviewer,
    })
    const reviewerTeam = fixtures.generateTeam({
      properties: {
        role: Team.Role.reviewer,
      },
      Team,
    })
    reviewerTeam.members = [reviewer]

    await makeRecommendationToRejectUseCase
      .initialize({
        models,
        logEvent,
        jobsService,
        notificationService,
      })
      .execute({
        manuscriptId: manuscript.id,
        userId: academicEditorMember.userId,
        input: {
          messageForAuthor: chance.sentence(),
          messageForTriage: chance.sentence(),
        },
      })

    expect(manuscript.status).toEqual(Manuscript.Statuses.pendingApproval)
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyTriageEditor).toHaveBeenCalledTimes(1)
  })
})
