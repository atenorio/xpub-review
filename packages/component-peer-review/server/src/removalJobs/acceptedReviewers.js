const moment = require('moment-business-days')
const logger = require('@pubsweet/logger')

const initialize = ({ Job, TeamMember, ReviewerSuggestion, logEvent }) => {
  const jobHandler = async job => {
    const { days, timeUnit, invitationId, manuscriptId } = job.data

    const teamMember = await TeamMember.find(invitationId, 'user.identities')
    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: teamMember.alias.email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = false
      await reviewerSuggestion.save()
    }

    await teamMember.delete()

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.reviewer_invitation_removed,
      objectType: logEvent.objectType.user,
      objectId: teamMember.user.id,
    })

    const message = `Job ${job.name}: the ${days} ${timeUnit} removal has been executed for invitation ${invitationId}`

    logger.info(message)
  }

  return {
    async scheduleRemovalJob({ days, timeUnit, invitationId, manuscriptId }) {
      const executionDate = moment()
        .add(days, timeUnit)
        .toISOString()

      const params = {
        days,
        timeUnit,
        invitationId,
        manuscriptId,
      }

      await Job.schedule({
        params,
        jobHandler,
        executionDate,
        teamMemberId: invitationId,
      })
    },
  }
}

module.exports = { initialize }
