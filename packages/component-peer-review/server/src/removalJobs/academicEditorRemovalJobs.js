const moment = require('moment-business-days')
const logger = require('@pubsweet/logger')
const config = require('config')

const businessDays = config.get('reminders.businessDays')

const initialize = ({ Job, Team, TeamMember, Manuscript, logEvent }) => {
  const academicEditorJobHandler = async job => {
    try {
      const { invitationId, manuscriptId, action } = job.data

      const teamMember = await TeamMember.find(invitationId)
      teamMember.updateProperties({ status: TeamMember.Statuses.expired })
      await teamMember.save()

      const manuscript = await Manuscript.find(manuscriptId)
      const activeAcademicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
        {
          role: Team.Role.academicEditor,
          status: TeamMember.Statuses.accepted,
          manuscriptId: manuscript.id,
        },
      )
      if (!activeAcademicEditor) {
        manuscript.updateProperties({ status: Manuscript.Statuses.submitted })
        await manuscript.save()
      }

      logEvent({
        userId: null,
        manuscriptId,
        action,
        objectType: logEvent.objectType.user,
        objectId: teamMember.userId,
      })

      logger.info(`Successfully expired academic editor ${invitationId}`)
    } catch (e) {
      throw new Error(e)
    }
  }

  return {
    async scheduleRemovalJobForAcademicEditor({
      days,
      timeUnit,
      invitationId,
      manuscriptId,
    }) {
      const executionDate = businessDays
        ? moment()
            .businessAdd(days)
            .toISOString()
        : moment()
            .add(days, timeUnit)
            .toISOString()

      const params = {
        invitationId,
        manuscriptId,
        action: logEvent.actions.reminder_invitation_removed,
      }

      await Job.schedule({
        params,
        executionDate,
        teamMemberId: invitationId,
        jobHandler: academicEditorJobHandler,
      })
    },
  }
}

module.exports = { initialize }
