const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifySubmittingAuthor({
    editor,
    content,
    journal,
    manuscript,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { title, customId } = manuscript

    const editorName = editor.getName()
    const authorNoteText = content ? `Reason & Details: "${content}"` : ''

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      emailType: 'submitting-author',
      titleText: `your submission "${title}" to ${journalName}`,
      comments: authorNoteText,
      journalName,
    })

    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: submittingAuthor,
      fromEmail: `${editorName} <${editorialAssistant.alias.email}>`,
      subject: `${customId}: Revision requested`,
      paragraph,
      signatureName: editorialAssistant.getName(),
      journalName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyReviewers({
    journal,
    reviewers,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal

    const titleText = `the manuscript titled "${
      manuscript.title
    }" by ${submittingAuthor.getName()}`

    const academicEditorName = academicEditor.getName()

    const editorialAssistantEmail = editorialAssistant.alias.email
    const reviewersEmailBody = getEmailCopyService.getCopy({
      emailType: 'reviewers',
      titleText,
      journalName,
      editorialAssistantEmail: editorialAssistant.alias.email,
    })

    const buildSendEmailFunction = emailBodyProps => async reviewer => {
      const { paragraph, ...bodyProps } = emailBodyProps
      const emailProps = getPropsService.getProps({
        manuscript,
        paragraph,
        journalName,
        toUser: reviewer,
        signatureName: academicEditorName,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${manuscript.customId}: Review no longer required`,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    }

    return Promise.all([
      ...reviewers.map(buildSendEmailFunction(reviewersEmailBody)),
    ])
  },
  async notifyTriageEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    academicEditorLabel,
    commentForTriageEditor,
  }) {
    const { name: journalName } = journal
    const editorName = academicEditor.getName()

    const comments = commentForTriageEditor
      ? `The ${academicEditorLabel} provided the following comments: "${commentForTriageEditor}"`
      : ''

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      comments,
      journalName,
      emailType: 'triage-editor',
      titleText: `the manuscript titled "${
        manuscript.title
      }" by ${submittingAuthor.getName()} `,
      targetUserName: academicEditor.getLastName(),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: triageEditor,
      signatureName: editorName,
      subject: `${manuscript.customId}: Revision requested`,
      fromEmail: `${editorName} <${editorialAssistant.alias.email}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
