const initialize = () => ({
  getEmailCopy({
    comments,
    emailType,
    titleText,
    staffEmail,
    targetUserName,
    academicEditorLabel,
  }) {
    let upperContent, manuscriptText, subText, lowerContent, paragraph
    let hasLink = true
    let hasIntro = true
    const hasSignature = true
    switch (emailType) {
      case 'academic-editor-accepted':
        hasIntro = false
        paragraph = `Dr. ${targetUserName} agreed to serve as the ${academicEditorLabel} on ${titleText}.
        Please click on the link below to access the manuscript.`
        break
      case 'academic-editor-declined':
        paragraph = `Dr. ${targetUserName} has declined to serve as the ${academicEditorLabel} on ${titleText}.<br/><br/>
        ${comments}<br/><br/>
        To invite another ${academicEditorLabel}, please click the link below.`
        hasIntro = false
        break
      case 'academic-editor-revoked':
        hasIntro = false
        hasLink = false
        paragraph = `${targetUserName} has removed you from the role of ${academicEditorLabel} for ${titleText}.<br/><br/>
        The manuscript will no longer appear in your dashboard. Please contact ${staffEmail} if you have any questions about this change.`
        break
      case 'academic-editor-after-accepted-invitation':
        paragraph = `Thank you for agreeing to organize the review process for this manuscript. Please check the manuscript to see if it is of reasonable quality and novelty. If so, we ask that you invite five external reviewers and make an editorial recommendation based on their reports. If not, you may reject the manuscript right away without sending it out for review. <div>&nbsp;</div>
          You can view the submitted manuscript and take any of the above actions using the link below.<div>&nbsp;</div>
          Once you have assigned reviewers, they will be automatically contacted by the Manuscript Tracking System, and you will be notified once these reviewers agree or decline to review the manuscript. As we aim to provide a fast review process, please invite reviewers for this manuscript within one week.<div>&nbsp;</div>
          If a potential conflict of interest exists between yourself and either the authors or the subject of the manuscript, please decline to handle the manuscript. If a conflict becomes apparent during the review process, please let us know at the earliest possible opportunity. For more information about our conflicts of interest policies, please see:
          <a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.<div>&nbsp;</div>
          Thank you very much for your contribution to the journal.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return {
      hasLink,
      subText,
      hasIntro,
      paragraph,
      upperContent,
      lowerContent,
      hasSignature,
      manuscriptText,
    }
  },
})

module.exports = {
  initialize,
}
