const config = require('config')

const staffEmail = config.get('staffEmail')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAcademicEditor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      staffEmail,
      academicEditorLabel,
      emailType: 'academic-editor-revoked',
      targetUserName: triageEditor.getName(),
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })
    const emailProps = getPropsService.getProps({
      toUser: academicEditor,
      subject: `${customId}: Editor invitation cancelled`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
