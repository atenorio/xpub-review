const initialize = () => ({
  getCopy({ titleText, emailType, targetUserName }) {
    let paragraph
    const hasLink = true
    const hasIntro = true
    const hasSignature = true
    switch (emailType) {
      case 'academic-editor':
        paragraph = `We are pleased to inform you that Dr. ${targetUserName} has submitted a review for ${titleText}.<br/><br/>
      To see the full report, please visit the manuscript details page.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return { paragraph, hasLink, hasIntro, hasSignature }
  },
})

module.exports = {
  initialize,
}
