const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyRemovedTriageEditor({
    journal,
    manuscript,
    triageEditor,
    triageEditorLabel,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail = editorialAssistant.getEmail()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      titleText: title,
      triageEditorLabel,
      editorialAssistantEmail,
      emailType: 'triage-editor-removed',
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: triageEditor,
      signatureName: editorialAssistantName,
      subject: `${customId}: You have been removed from ${title}`,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyAssignedTriageEditor({
    journal,
    manuscript,
    triageEditor,
    triageEditorLabel,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail = editorialAssistant.getEmail()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      titleText: title,
      triageEditorLabel,
      editorialAssistantEmail,
      emailType: 'triage-editor-assigned',
    })
    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: triageEditor,
      signatureName: editorialAssistantName,
      subject: `${customId}: You have been assigned on ${title}`,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
