const initialize = () => ({
  getEmailCopy({
    emailType,
    titleText,
    journalName,
    triageEditorLabel,
    academicEditorLabel,
    editorialAssistantEmail,
  }) {
    let upperContent, manuscriptText, subText, lowerContent, paragraph
    let hasLink = true
    let hasIntro = true
    let hasSignature = true
    switch (emailType) {
      case 'author-academic-editor-removed':
        hasIntro = true
        hasLink = false
        hasSignature = true
        paragraph = `We had to replace the ${academicEditorLabel} of your manuscript ${titleText}. We apologise for any inconvenience, but it was necessary in order to move your manuscript forward.<br/><br/>
        If you have questions please email them to ${editorialAssistantEmail}.<br/><br/>
        Thank you for your submission to ${journalName}.`
        break
      case 'academic-editor-removed':
        hasIntro = true
        hasLink = false
        hasSignature = true
        paragraph = `The ${triageEditorLabel} removed you from the manuscript "${titleText}".<br/><br/>
        If you have any questions regarding this action, please let us know at ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
        break
      case 'reviewer-academic-editor-removed':
        hasIntro = true
        hasLink = false
        hasSignature = true
        paragraph = `We had to replace the ${academicEditorLabel} of the manuscript "${titleText}". We apologise for any inconvenience this may cause.<br/><br/>
        If you have started the review process please email the content to ${editorialAssistantEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
        break
      default:
        throw new Error(`The ${emailType} email type is not defined.`)
    }
    return {
      hasLink,
      subText,
      hasIntro,
      paragraph,
      upperContent,
      lowerContent,
      hasSignature,
      manuscriptText,
    }
  },
})

module.exports = {
  initialize,
}
