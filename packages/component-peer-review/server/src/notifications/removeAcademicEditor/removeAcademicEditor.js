const Promise = require('bluebird')

const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAuthor({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    triageEditorLabel,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const triageEditorName = triageEditor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      titleText: title,
      triageEditorLabel,
      academicEditorLabel,
      editorialAssistantEmail,
      targetUserName: academicEditor,
      emailType: 'author-academic-editor-removed',
    })
    const emailProps = getPropsService.getProps({
      toUser: submittingAuthor,
      subject: `${customId}: Your manuscript's editor was changed`,
      paragraph,
      fromEmail: `${triageEditorName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: triageEditorName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyAcademicEditor({
    journal,
    manuscript,
    academicEditor,
    triageEditorLabel,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantName = editorialAssistant.getName()
    const editorialAssistantEmail = editorialAssistant.getEmail()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      titleText: title,
      targetUserName: academicEditor,
      emailType: 'academic-editor-removed',
      editorialAssistantEmail,
    })
    const emailProps = getPropsService.getProps({
      toUser: academicEditor,
      subject: `${customId}: The ${triageEditorLabel} removed you from ${title}`,
      paragraph,
      fromEmail: `${editorialAssistantName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistantName,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyReviewers({
    journal,
    reviewers,
    manuscript,
    triageEditor,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const triageEditorName = triageEditor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      titleText: title,
      emailType: 'reviewer-academic-editor-removed',
      editorialAssistantEmail,
    })

    const filteredReviewers = reviewers.filter(r => r.status !== 'declined')
    await Promise.each(filteredReviewers, async reviewer => {
      const emailProps = getPropsService.getProps({
        toUser: reviewer,
        subject: `${customId}: The ${academicEditorLabel} of a manuscript that you were reviewing was changed`,
        paragraph,
        fromEmail: `${triageEditorName} <${editorialAssistantEmail}>`,
        manuscript,
        journalName,
        signatureName: triageEditorName,
      })
      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },
})

module.exports = {
  initialize,
}
