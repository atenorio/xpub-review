const initialize = ({ Email, getPropsService, getEmailCopyService }) => ({
  async notifyAuthorsNoPeerReview({
    journal,
    authors,
    manuscript,
    rejectEditor,
    editorialAssistant,
    comment: { content },
  }) {
    const { name: journalName } = journal
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      emailType: 'authors-manuscript-rejected-no-review',
      comments: content,
      journalName,
    })
    const rejectEditorName = rejectEditor.getName()

    authors.forEach(async author => {
      const emailProps = getPropsService.getProps({
        paragraph,
        manuscript,
        toUser: author,
        signatureName: rejectEditorName,
        subject: `${manuscript.customId}: Manuscript rejected`,
        fromEmail: `${rejectEditorName} <${editorialAssistant.alias.email}>`,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },
  async notifyReviewers({
    journal,
    reviewers,
    manuscript,
    rejectEditor,
    submittingAuthor,
    editorialAssistant,
    academicEditorLabel,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()

    const rejectEditorName = rejectEditor.getName()
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      journalName,
      academicEditorLabel,
      emailType: 'reviewers-manuscript-rejected',
      editorialAssistantEmail: editorialAssistant.email,
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })

    reviewers.forEach(async reviewer => {
      const emailProps = getPropsService.getProps({
        manuscript,
        toUser: reviewer,
        fromEmail: `${rejectEditorName} <${editorialAssistant.alias.email}>`,
        subject: `${customId}: A manuscript you reviewed has been rejected`,
        paragraph,
        signatureName: rejectEditorName,
      })

      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },
  async notifyAcademicEditor({
    journal,
    manuscript,
    rejectEditor,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      targetUserName: rejectEditor.getName(),
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
      emailType: 'academic-editor-manuscript-rejected',
      journalName,
    })
    const rejectEditorName = rejectEditor.getName()
    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: academicEditor,
      fromEmail: `${rejectEditorName} <${editorialAssistant.alias.email}>`,
      subject: `${customId}: Editorial decision confirmed`,
      paragraph,
      signatureName: rejectEditorName,
    })
    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
  async notifyAuthorsAfterPeerReview({
    journal,
    authors,
    manuscript,
    rejectEditor,
    editorialAssistant,
    comment: { content },
  }) {
    const { name: journalName } = journal
    const { customId, title } = manuscript
    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      titleText: title,
      emailType: 'authors-manuscript-rejected-after-review',
      comments: content,
      journalName,
    })

    const rejectEditorName = rejectEditor.getName()
    authors.forEach(async author => {
      const emailProps = getPropsService.getProps({
        manuscript,
        toUser: author,
        fromEmail: `${rejectEditorName} <${editorialAssistant.alias.email}>`,
        subject: `${customId}: Manuscript rejected`,
        paragraph,
        signatureName: rejectEditorName,
      })
      emailProps.bodyProps = bodyProps
      const email = new Email(emailProps)
      await email.sendEmail()
    })
  },
  async notifyTriageEditor({
    journal,
    manuscript,
    triageEditor,
    rejectEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { name: journalName } = journal
    const editorName = rejectEditor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getCopy({
      journalName,
      emailType: 'triage-editor-manuscript-rejected',
      titleText: `the manuscript titled "${
        manuscript.title
      }" by ${submittingAuthor.getName()} `,
      targetUserName: rejectEditor.getLastName(),
    })

    const emailProps = getPropsService.getProps({
      paragraph,
      manuscript,
      journalName,
      toUser: triageEditor,
      signatureName: editorName,
      subject: `${manuscript.customId}: Manuscript rejected`,
      fromEmail: `${editorName} <${editorialAssistant.alias.email}>`,
    })

    emailProps.bodyProps = bodyProps
    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = { initialize }
