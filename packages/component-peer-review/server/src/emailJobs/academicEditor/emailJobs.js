const config = require('config')
const { get } = require('lodash')

const daysListAcceptInvitation = config.get(
  'reminders.academicEditor.acceptInvitation.days',
)
const removalDayAcceptInvitation = config.get(
  'reminders.academicEditor.acceptInvitation.remove',
)
const dayListInviteReviewers = config.get(
  'reminders.academicEditor.inviteReviewers.days',
)
const timeUnitInviteReviewers = config.get(
  'reminders.academicEditor.inviteReviewers.timeUnit',
)
const staffEmail = config.get('staffEmail')

const { getExpectedDate } = require('../../dateService/dateService')
const {
  getInvitationEmailProps,
} = require('../../invitations/academicEditor/getInvitationEmailProps')

const reminders = require('./reminders')

const initialize = ({ logEvent, Email, Job, getPropsService }) => ({
  async scheduleEmailsWhenAcademicEditorIsInvited({
    journal,
    manuscript,
    triageEditor,
    academicEditor,
    submittingAuthor,
    authorTeamMembers,
    triageEditorLabel,
    editorialAssistant,
  }) {
    const { title, customId, id: manuscriptId } = manuscript
    const { created, id: invitationId } = academicEditor
    const { name: journalName } = journal
    const submittingAuthorName = submittingAuthor.getName()
    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    const expectedDate = getExpectedDate({
      timestamp: new Date(created).getTime(),
      daysExpected: 0,
    })
    const titleText = `the manuscript titled "${title}" by ${submittingAuthorName}`
    const emailProps = getInvitationEmailProps({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      editorialAssistant,
      subject: `${customId}: Invitation to edit a manuscript reminder`,
      authorTeamMembers,
    })

    const {
      scheduleFirstAcademicEditorReminder,
      scheduleSecondAcademicEditorReminder,
      scheduleAcademicEditorRemovalNotificationForAcademicEditor,
      scheduleAcademicEditorRemovalNotificationForTriageEditor,
      scheduleAcademicEditorRemovalNotificationForEditorialAssistant,
    } = reminders.initialize({ logEvent, Email, Job })

    await scheduleFirstAcademicEditorReminder({
      invitationId,
      titleText,
      emailProps,
      manuscriptId,
      expectedDate,
      userId: academicEditor.userId,
      firstDate: daysListAcceptInvitation.first,
      journalName,
    })
    await scheduleSecondAcademicEditorReminder({
      titleText,
      emailProps,
      journalName,
      invitationId,
      manuscriptId,
      expectedDate,
      secondDate: daysListAcceptInvitation.second,
      userId: academicEditor.userId,
    })

    const notificationTitleText = `the manuscript titled <strong>"${title}"</strong> by <strong>${submittingAuthorName}</strong> et al.`

    await scheduleAcademicEditorRemovalNotificationForAcademicEditor({
      journalName,
      manuscriptId,
      invitationId,
      expectedDate,
      triageEditorLabel,
      titleText: notificationTitleText,
      days: removalDayAcceptInvitation,
      emailProps: getPropsService.getProps({
        manuscript,
        toUser: academicEditor,
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
        subject: `${customId}: The ${triageEditorLabel} removed you from ${title}`,
        signatureName: journalName,
      }),
    })

    if (triageEditor) {
      await scheduleAcademicEditorRemovalNotificationForTriageEditor({
        titleText: notificationTitleText,
        targetUserName: academicEditor.getName(),
        emailProps: getPropsService.getProps({
          manuscript,
          toUser: triageEditor,
          subject: `${customId}: Editor Declined`,
          fromEmail: `${journalName} <${editorialAssistantEmail}>`,
          signatureName: journalName,
        }),
        manuscriptId,
        invitationId,
        expectedDate,
        days: removalDayAcceptInvitation,
      })
    } else {
      await scheduleAcademicEditorRemovalNotificationForEditorialAssistant({
        titleText: notificationTitleText,
        targetUserName: academicEditor.getName(),
        emailProps: getPropsService.getProps({
          manuscript,
          toUser: editorialAssistant,
          subject: `${customId}: Editor Declined`,
          fromEmail: `${journalName} <${editorialAssistantEmail}>`,
          signatureName: journalName,
        }),
        manuscriptId,
        invitationId,
        expectedDate,
        days: removalDayAcceptInvitation,
      })
    }
  },
  async sendAcademicEditorRemindersToInviteReviewers({
    user,
    manuscript,
    journalName,
    triageEditor,
    editorialAssistant,
    submittingAuthorName,
    triageEditorLabel,
  }) {
    const { title, id: manuscriptId, customId, articleType } = manuscript
    const titleText = `${articleType.name} titled "${title}" by ${submittingAuthorName}`

    let fromName = editorialAssistant.getName()
    if (triageEditor) {
      fromName = triageEditor.getName()
    }

    const editorialAssistantEmail = editorialAssistant.getEmail()
    const emailProps = getPropsService.getProps({
      manuscript,
      toUser: user,
      subject: `${customId}: Inviting Reviewers Reminder`,
      signatureName: editorialAssistant.getName(),
      fromEmail: `${fromName} <${editorialAssistantEmail}>`,
    })

    const {
      scheduleFirstAcademicEditorInviteReviewerReminder,
      scheduleSecondAcademicEditorInviteReviewerReminder,
      scheduleEAInviteReviewerReminder,
    } = reminders.initialize({
      logEvent,
      Email,
      Job,
    })

    await scheduleFirstAcademicEditorInviteReviewerReminder({
      userId: user.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      emailProps,
      manuscriptId,
      firstDate: dayListInviteReviewers.first,
      invitationId: user.id,
    })

    await scheduleSecondAcademicEditorInviteReviewerReminder({
      userId: user.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      emailProps,
      manuscriptId,
      secondDate: dayListInviteReviewers.second,
      invitationId: user.id,
    })

    await scheduleEAInviteReviewerReminder({
      triageEditorLabel,
      userId: editorialAssistant.userId,
      timeUnit: timeUnitInviteReviewers,
      titleText,
      manuscriptId,
      invitationId: editorialAssistant.id,
      thirdDate: dayListInviteReviewers.third,
      emailProps: getPropsService.getProps({
        manuscript,
        toUser: editorialAssistant,
        subject: `${customId}: Reviewers needed`,
        signatureName: editorialAssistant.getName(),
        fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      }),
    })
  },
})

module.exports = {
  initialize,
}
