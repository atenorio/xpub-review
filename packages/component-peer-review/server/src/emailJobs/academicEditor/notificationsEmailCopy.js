const config = require('config')

const staffEmail = config.get('staffEmail')

const getNotificationsEmailCopy = ({
  emailType,
  titleText,
  journalName,
  targetUserName,
  triageEditorLabel,
}) => {
  let paragraph
  let hasLink = true
  let hasIntro = true
  let hasSignature = true
  switch (emailType) {
    case 'academic-editor-removed':
      hasIntro = true
      hasLink = false
      hasSignature = true
      paragraph = `The ${triageEditorLabel} removed you from ${titleText}.<br/><br/>
        If you have any questions regarding this action, please let us know at ${staffEmail}.<br/><br/>
        Thank you for reviewing ${journalName}.`
      break
    case 'triage-editor-academic-editor-removed':
      paragraph = `Dr. ${targetUserName} has declined to handle ${titleText}. Please log in to the system and assign the manuscript to another editor.<br/><br/>`
      break
    case 'editorial-assistant-academic-editor-removed':
      paragraph = `Dr. ${targetUserName} has declined to handle ${titleText}. Please log in to the system and assign the manuscript to another editor.<br/><br/>`
      break
    case 'academic-editor-reviewer-invitation-first-reminder':
      paragraph = `This is a follow up regarding ${titleText}, which you are assigned to handle. We would appreciate it if you could invite at least 2 or 3 reviewers as soon as possible so that we may proceed with the review process. If you have any difficulty inviting reviewers please let us know so that we can assist you.<br/><br/>
You can view the submitted manuscript and invite reviewers using the following direct link: <br/><br/>`
      break
    case 'academic-editor-reviewer-invitation-second-reminder':
      hasLink = false
      paragraph = `This is a follow up regarding ${titleText}. In order to avoid delaying the authors, we would appreciate it if you could invite reviewers within two days. If you are having difficulties inviting reviewers, please follow the instructions below: <br/><br/>
<ul><li>From the Manuscript Details area, you should open the ‘Reviewer Details & Reports’ area
<li>You are then able to invite a reviewer from the suggestions provided, or you may enter the details of a specific reviewer if you have someone in mind
</ul> <br/>
Please let me know if there is anything that I can help you with.<br/><br/>`
      break
    case 'ea-academic-editor-not-invited-enough-reviewers':
      hasIntro = false
      paragraph = `The Editor has not invited enough reviewers for manuscript ${titleText}. <br/><br/>
Please can you check the reviewer invitations status and contact the Editor directly if required, or contact the ${triageEditorLabel} if you feel the Editor is now unresponsive. <br/><br/>`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    hasIntro,
    paragraph,
    hasSignature,
  }
}

module.exports = {
  getNotificationsEmailCopy,
}
