const getEmailCopy = ({
  baseUrl,
  emailType,
  authorName,
  targetUserName,
  manuscriptTitle,
  academicEditorLabel,
}) => {
  let upperContent, subText, lowerContent
  const hasLink = true
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'academic-editor-assigned':
      upperContent = `<strong>${targetUserName}</strong> has invited you to
        serve as the ${academicEditorLabel} for the manuscript titled <strong>"${manuscriptTitle}"</strong>
        by <strong>${authorName}</strong> et al.`
      subText = `<div><br/><strong>To see all active manuscripts go to your <a style="color:#007e92; text-decoration: none;" href=${baseUrl}>dashboard</a></strong></div><br/> The manuscript’s abstract and author information is below to help you decide. Once you have agreed to review, you will be able to download the full article PDF.`
      lowerContent = `If a potential conflict of interest exists between yourself and either the authors or
        the subject of the manuscript, please decline to handle the manuscript. If a conflict
        becomes apparent during the review process, please let me know at the earliest possible
        opportunity. For more information about our conflicts of interest policies, please
        see:
        <a style="color:#007e92; text-decoration: none;" href="https://www.hindawi.com/ethics/#coi">https://www.hindawi.com/ethics/#coi</a>.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }

  return {
    hasLink,
    subText,
    hasIntro,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
