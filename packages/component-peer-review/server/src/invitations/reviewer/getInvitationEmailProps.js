const config = require('config')
const { get, concat } = require('lodash')
const { getModifiedText } = require('component-transform-text')

const urlService = require('../../urlService/urlService')

const acceptReviewPath = config.get('accept-review.url')
const acceptReviewNewUserPath = config.get('accept-review-new-user.url')
const declineReviewPath = config.get('decline-review.url')
const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const staffEmail = config.get('staffEmail')
const confirmedUserFooterText = config.get('emailFooterText.registeredUsers')
const unconfirmedUserFooterText = config.get(
  'emailFooterText.unregisteredUsers',
)

module.exports = {
  getInvitationEmailProps({
    manuscript,
    reviewer,
    academicEditor,
    authors,
    editorialAssistant,
    journalName,
  }) {
    const footerText = reviewer.user.identities[0].isConfirmed
      ? confirmedUserFooterText
      : unconfirmedUserFooterText

    const {
      title,
      abstract,
      submissionId,
      id: manuscriptId,
      customId,
    } = manuscript

    const subjectText = `${customId}: Review invitation`

    const declineLink = urlService.createUrl({
      baseUrl,
      slug: declineReviewPath,
      queryParams: {
        invitationId: reviewer.id,
        manuscriptId,
      },
    })

    let agreeLink = urlService.createUrl({
      baseUrl,
      slug: acceptReviewPath,
      queryParams: {
        invitationId: reviewer.id,
        submissionId,
        manuscriptId,
      },
    })
    const reviewerIdentity = reviewer.user.getDefaultIdentity()
    if (!reviewerIdentity.isConfirmed) {
      agreeLink = urlService.createUrl({
        baseUrl,
        slug: acceptReviewNewUserPath,
        queryParams: {
          invitationId: reviewer.id,
          submissionId: manuscript.submissionId,
          manuscriptId: manuscript.id,
          email: get(reviewer, 'alias.email', ''),
          givenNames: get(reviewer, 'alias.givenNames', ''),
          surname: get(reviewer, 'alias.surname', ''),
          aff: get(reviewer, 'alias.aff', ''),
          country: get(reviewer, 'alias.country', ''),
          token: reviewer.user.passwordResetToken,
          confirmationToken: reviewer.user.passwordResetToken,
        },
      })
    }

    const authorsNameList = authors
      .map((author, index) => `${author.getName()}<sup>${index + 1}</sup>`)
      .join(', ')

    const authorAffiliationsList = authors
      .map(
        (author, index) =>
          `<div><sup style="color:#242424">${index +
            1}</sup><span style="color: #586971;">${get(
            author,
            'alias.aff',
          )}.</span></div>`,
      )
      .join(' ')

    const authorsList = concat(authorsNameList, authorAffiliationsList)
    const academicEditorName = academicEditor.getName()

    const editorialAssistantEmail =
      get(editorialAssistant, 'alias.email') || staffEmail

    return {
      type: 'user',
      templateType: 'invitation',
      fromEmail: `${academicEditorName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(reviewer, 'alias.email'),
        name: get(reviewer, 'alias.surname'),
      },
      content: {
        title,
        abstract,
        agreeLink,
        declineLink,
        signatureJournal: journalName,
        signatureName: academicEditorName,
        authorsList,
        subject: subjectText,
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: reviewer.user.id,
            token: reviewer.user.unsubscribeToken,
          },
        }),
        footerText: getModifiedText(
          footerText,
          {
            pattern: '{recipientEmail}',
            replacement: reviewer.alias.email,
          },
          { pattern: '{journalName}', replacement: journalName },
        ),
      },
    }
  },
}
