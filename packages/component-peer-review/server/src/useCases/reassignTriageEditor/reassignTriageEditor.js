const Promise = require('bluebird')

const initialize = ({
  models: { User, Team, Journal, TeamMember, Manuscript, SpecialIssue },
  logEvent,
  notificationService,
}) => ({
  async execute({ teamMemberId, manuscriptId, userId }) {
    const journalTriageEditor = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const removedTriageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })

    const queryObject = {
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
    }
    const triageEditorTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })

    const submissionTriageEditorMembers = await TeamMember.findAllBySubmissionAndRole(
      {
        role: Team.Role.triageEditor,
        submissionId: manuscript.submissionId,
      },
    )
    await Promise.each(submissionTriageEditorMembers, member => {
      member.updateProperties({ status: TeamMember.Statuses.removed })
      return member.save()
    })

    const triageEditorUser = await User.find(
      journalTriageEditor.userId,
      'identities',
    )

    const assignedTriageEditor = triageEditorTeam.addMember(triageEditorUser, {
      status: TeamMember.Statuses.active,
    })
    await assignedTriageEditor.save()
    await triageEditorTeam.save()

    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })

    let specialIssue
    if (manuscript.specialIssueId) {
      specialIssue = await SpecialIssue.find(
        manuscript.specialIssueId,
        'peerReviewModel',
      )
    }
    const triageEditorLabel = specialIssue
      ? specialIssue.peerReviewModel.triageEditorLabel
      : journal.peerReviewModel.triageEditorLabel

    notificationService.notifyRemovedTriageEditor({
      journal,
      manuscript,
      triageEditorLabel,
      editorialAssistant,
      triageEditor: removedTriageEditor,
    })

    notificationService.notifyAssignedTriageEditor({
      journal,
      manuscript,
      triageEditorLabel,
      editorialAssistant,
      triageEditor: assignedTriageEditor,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      objectType: logEvent.objectType.user,
      objectId: removedTriageEditor.userId,
      action: logEvent.actions.triage_editor_removed,
    })

    logEvent({
      userId,
      manuscriptId: manuscript.id,
      objectType: logEvent.objectType.user,
      objectId: assignedTriageEditor.userId,
      action: logEvent.actions.triage_editor_assigned,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isTriageEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
