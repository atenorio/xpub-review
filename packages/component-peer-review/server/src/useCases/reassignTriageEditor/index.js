const reassignTriageEditorUseCase = require('./reassignTriageEditor')
const getTriageEditorsUseCase = require('./getTriageEditors')

module.exports = {
  reassignTriageEditorUseCase,
  getTriageEditorsUseCase,
}
