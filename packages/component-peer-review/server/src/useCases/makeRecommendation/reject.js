const Promise = require('bluebird')

const initialize = ({
  notificationService,
  models: {
    Job,
    Team,
    User,
    Review,
    Comment,
    Journal,
    TeamMember,
    Manuscript,
    SpecialIssue,
  },
  jobsService,
  logEvent,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndUser({
      userId,
      manuscriptId,
      role: Team.Role.academicEditor,
    })

    const review = new Review({
      recommendation: Review.Recommendations.reject,
      submitted: new Date().toISOString(),
      manuscriptId,
      teamMemberId: academicEditor.id,
    })
    await review.save()

    const authorComment = new Comment({
      content: input.messageForAuthor,
      reviewId: review.id,
      type: 'public',
    })
    await authorComment.save()

    if (input.messageForTriage) {
      const triageComment = new Comment({
        content: input.messageForTriage,
        reviewId: review.id,
        type: 'private',
      })
      await triageComment.save()
    }

    manuscript.updateStatus(Manuscript.Statuses.pendingApproval)
    await manuscript.save()

    let staffMember = await TeamMember.findOneByJournalAndRole({
      role: Team.Role.editorialAssistant,
      journalId: manuscript.journalId,
    })

    if (!staffMember) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const academicEditorJobs = await Job.findAllByTeamMembers([
      academicEditor.id,
    ])
    const staffMemberJobs = await Job.findAllByTeamMembers([staffMember.id])

    jobsService.cancelJobs(academicEditorJobs)
    jobsService.cancelStaffMemberJobs({
      staffMemberJobs,
      manuscriptId: manuscript.id,
    })

    const acceptedReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.accepted,
      },
    )
    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: manuscript.id,
        status: TeamMember.Statuses.pending,
      },
    )
    await Promise.each(
      [...acceptedReviewers, ...pendingReviewers],
      async reviewer => (reviewer.user = await User.find(reviewer.userId)),
    )

    const reviewersJobs = await Job.findAllByTeamMembers([
      ...acceptedReviewers.map(ar => ar.id),
      ...pendingReviewers.map(pr => pr.id),
    ])
    jobsService.cancelJobs(reviewersJobs)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscriptId)
    const journal = await Journal.find(manuscript.journalId)

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      manuscriptId: manuscript.id,
      journalId: manuscript.journalId,
      sectionId: manuscript.sectionId,
    })
    triageEditor.user = await User.find(triageEditor.userId)

    notificationService.notifyReviewers({
      journal,
      manuscript,
      submittingAuthor,
      academicEditor,
      pendingReviewers,
      acceptedReviewers,
    })

    const academicEditorLabel = await manuscript.getEditorLabel({
      SpecialIssue,
      Journal,
      role: Team.Role.academicEditor,
    })
    notificationService.notifyTriageEditor({
      journal,
      manuscript,
      staffMember,
      triageEditor,
      academicEditor,
      submittingAuthor,
      academicEditorLabel,
      commentForTriage: input.messageForTriage,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.recommendation_reject,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'canMakeRecommendation']

module.exports = {
  initialize,
  authsomePolicies,
}
