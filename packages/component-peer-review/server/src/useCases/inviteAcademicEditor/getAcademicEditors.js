const { pullAllBy, isEmpty } = require('lodash')

const initialize = ({ TeamMember, Team, Manuscript }) => ({
  async execute(manuscriptId) {
    const manuscript = await Manuscript.find(manuscriptId)
    let members
    members = await TeamMember.findAllBySpecialIssueAndRole({
      specialIssueId: manuscript.specialIssueId,
      role: Team.Role.academicEditor,
    })
    if (isEmpty(members)) {
      members = await TeamMember.findAllByJournalAndRole({
        role: Team.Role.academicEditor,
        journalId: manuscript.journalId,
      })
    }

    const declinedManuscriptAcademicEditorMembers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.declined,
      },
    )

    const removedSubmissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndStatus(
      {
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.removed,
        submissionId: manuscript.submissionId,
      },
    )

    return pullAllBy(
      members,
      [
        ...declinedManuscriptAcademicEditorMembers,
        ...removedSubmissionAcademicEditorMembers,
      ],
      'userId',
    ).map(tm => tm.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated', 'isTriageEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
