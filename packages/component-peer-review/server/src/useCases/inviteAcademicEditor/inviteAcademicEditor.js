const { find, last } = require('lodash')
const config = require('config')
const { Promise } = require('bluebird')

const timeUnit = config.get(
  'reminders.academicEditor.acceptInvitation.timeUnit',
)
const removalDays = config.get(
  'reminders.academicEditor.acceptInvitation.remove',
)

const initialize = ({
  logEvent,
  emailJobsService,
  removalJobsService,
  invitationsService,
  models: { Journal, Manuscript, User, Team, TeamMember, SpecialIssue },
}) => ({
  async execute({ submissionId, userId, reqUserId, hasWorkloadAssignment }) {
    const manuscripts = await Manuscript.findManuscriptsBySubmissionId({
      submissionId,
      order: 'ASC',
      orderByField: 'version',
    })
    const manuscript = last(manuscripts)
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }
    // Throw early
    const manuscriptAcademicEditorMembers = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      },
    )
    const invitedAcademicEditor = find(manuscriptAcademicEditorMembers, [
      'userId',
      userId,
    ])

    const academicEditorLabel = await manuscript.getEditorLabel({
      SpecialIssue,
      Journal,
      role: Team.Role.academicEditor,
    })
    if (
      invitedAcademicEditor &&
      invitedAcademicEditor.status !== TeamMember.Statuses.expired
    ) {
      throw Error(`The ${academicEditorLabel} is already invited.`)
    }

    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRole(
      {
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )

    const invalidAcademicEditorMembers = submissionAcademicEditorMembers.filter(
      tm =>
        tm.status === TeamMember.Statuses.declined ||
        tm.status === TeamMember.Statuses.removed,
    )
    if (find(invalidAcademicEditorMembers, ['userId', userId])) {
      throw Error(`The ${academicEditorLabel} can't be invited.`)
    }

    // Academic Editor
    const academicEditorUser = await User.find(userId, 'identities')
    await Promise.each(manuscripts, async manuscript => {
      const queryObject = {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
      }
      const academicEditorTeam = await Team.findOrCreate({
        queryObject,
        options: queryObject,
      })
      const academicEditor = academicEditorTeam.addMember(academicEditorUser, {
        status: TeamMember.Statuses.pending,
      })
      await academicEditor.save()
      await academicEditorTeam.save()
    })

    // Manuscript
    if (manuscript.status === Manuscript.Statuses.submitted) {
      manuscript.updateProperties({
        status: Manuscript.Statuses.academicEditorInvited,
      })
      await manuscript.save()
    }

    // Notifications
    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })
    const triageEditor = await TeamMember.findOneByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.triageEditor,
    })
    const triageEditorLabel = await manuscript.getEditorLabel({
      SpecialIssue,
      Journal,
      role: Team.Role.triageEditor,
    })

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.pending,
      },
    )
    academicEditor.user = academicEditorUser

    const authorTeamMembers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: manuscript.id,
    })

    invitationsService.sendInvitationToAcademicEditor({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      submittingAuthor,
      authorTeamMembers,
      editorialAssistant,
      academicEditorLabel,
    })

    emailJobsService.scheduleEmailsWhenAcademicEditorIsInvited({
      journal,
      manuscript,
      triageEditor,
      academicEditor,
      submittingAuthor,
      authorTeamMembers,
      triageEditorLabel,
      editorialAssistant,
    })

    removalJobsService.scheduleRemovalJobForAcademicEditor({
      timeUnit,
      days: removalDays,
      manuscriptId: manuscript.id,
      invitationId: academicEditor.id,
    })

    // Activity Log
    logEvent({
      userId: hasWorkloadAssignment ? null : reqUserId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_sent,
      objectType: logEvent.objectType.user,
      objectId: userId,
    })
  },
})

const authsomePolicies = [
  'isAuthenticated',
  'isTriageEditor',
  'hasAccessToManuscriptVersions',
]

module.exports = {
  initialize,
  authsomePolicies,
}
