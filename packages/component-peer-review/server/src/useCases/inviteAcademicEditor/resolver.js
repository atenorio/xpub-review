const config = require('config')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')

const invitations = require('../../invitations/invitations')

const jobs = require('../../jobsService/jobsService')
const emailJobs = require('../../emailJobs/academicEditor/emailJobs')
const removalJobs = require('../../removalJobs/academicEditorRemovalJobs')

const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/inviteAcademicEditor/getEmailCopy')
const acceptInvitationNotifications = require('../../notifications/inviteAcademicEditor/acceptInvitation')
const cancelInvitationNotifications = require('../../notifications/inviteAcademicEditor/cancelInvitation')
const declineInvitationNotifications = require('../../notifications/inviteAcademicEditor/declineInvitation')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const resolver = {
  Query: {
    async getAcademicEditors(_, { manuscriptId }, ctx) {
      return useCases.getAcademicEditorsUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteAcademicEditor(_, { submissionId, userId }, ctx) {
      const { Manuscript, TeamMember, Job, Team } = models
      const invitationsService = invitations.initialize({ Email })
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })

      const emailJobsService = emailJobs.initialize({
        Job,
        Email,
        logEvent,
        getPropsService,
      })
      const removalJobsService = removalJobs.initialize({
        Job,
        Team,
        logEvent,
        TeamMember,
        Manuscript,
      })
      const jobsService = jobs.initialize({ models })

      return useCases.inviteAcademicEditorUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          invitationsService,
          emailJobsService,
          removalJobsService,
        })
        .execute({ submissionId, userId, reqUserId: ctx.user })
    },
    async acceptAcademicEditorInvitation(_, { teamMemberId }, ctx) {
      const { Job } = models
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const jobsService = jobs.initialize({ models })
      const emailJobsService = emailJobs.initialize({
        Job,
        Email,
        logEvent,
        getPropsService,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = acceptInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.acceptAcademicEditorInvitationUseCase
        .initialize({
          models,
          useCases,
          logEvent,
          jobsService,
          emailJobsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async cancelAcademicEditorInvitation(_, { teamMemberId }, ctx) {
      const jobsService = jobs.initialize({ models })

      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = cancelInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.cancelAcademicEditorInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineAcademicEditorInvitation(_, input, ctx) {
      const jobsService = jobs.initialize({ models })

      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = declineInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      return useCases.declineAcademicEditorInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          notificationService,
        })
        .execute({ input, userId: ctx.user })
    },
  },
}

module.exports = resolver
