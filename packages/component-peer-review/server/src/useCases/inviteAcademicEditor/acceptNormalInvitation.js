const { Promise } = require('bluebird')

const initialize = ({
  logEvent,
  jobsService,
  emailJobsService,
  notificationService,
  models: {
    Job,
    Team,
    User,
    Journal,
    Manuscript,
    TeamMember,
    ArticleType,
    SpecialIssue,
  },
}) => ({
  async execute({ teamMemberId, userId }) {
    const academicEditorMember = await TeamMember.find(teamMemberId)
    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    // Academic Editor
    const submissionAcademicEditorMembers = await TeamMember.findAllBySubmissionAndRoleAndUser(
      {
        userId,
        role: Team.Role.academicEditor,
        submissionId: manuscript.submissionId,
      },
    )

    await Promise.each(submissionAcademicEditorMembers, async tm => {
      tm.updateProperties({ status: TeamMember.Statuses.accepted })
      await tm.save()
    })

    // Manuscript
    const articleType = await ArticleType.findArticleTypeByManuscript(
      manuscript.id,
    )
    const newStatus = articleType.hasPeerReview
      ? Manuscript.Statuses.academicEditorAssigned
      : Manuscript.Statuses.academicEditorAssignedEditorialType
    manuscript.updateStatus(newStatus)
    await manuscript.save()

    // Jobs
    const academicEditorMemberJobs = await Job.findAllByTeamMember(
      academicEditorMember.id,
    )
    await jobsService.cancelJobs(academicEditorMemberJobs)

    // Notifications
    const journal = await Journal.find(manuscript.journalId, 'peerReviewModel')
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      role: Team.Role.editorialAssistant,
      journalId: journal.id,
    })
    academicEditorMember.user = await User.find(academicEditorMember.userId)

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    manuscript.articleType = await ArticleType.find(manuscript.articleTypeId)
    manuscript.journal = journal

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    if (triageEditor) {
      triageEditor.user = await User.find(triageEditor.userId)
    }

    const academicEditorLabel = await manuscript.getEditorLabel({
      SpecialIssue,
      Journal,
      role: Team.Role.academicEditor,
    })

    if (journal.peerReviewModel.hasTriageEditor) {
      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        submittingAuthor,
        editorialAssistant,
        academicEditorLabel,
        academicEditor: academicEditorMember,
      })
    } else {
      notificationService.notifyEditorialAssistant({
        journal,
        manuscript,
        submittingAuthor,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })
    }

    if (articleType.hasPeerReview) {
      notificationService.notifyPendingAcademicEditor({
        journal,
        manuscript,
        editorialAssistant,
        academicEditor: academicEditorMember,
      })

      const triageEditorLabel = await manuscript.getEditorLabel({
        role: Team.Role.triageEditor,
        SpecialIssue,
        Journal,
      })
      emailJobsService.sendAcademicEditorRemindersToInviteReviewers({
        manuscript,
        triageEditor,
        editorialAssistant,
        triageEditorLabel: triageEditorLabel || 'EA',
        journalName: journal.name,
        user: academicEditorMember,
        submittingAuthorName: submittingAuthor.getName(),
      })
    }

    // Activity Log
    logEvent({
      userId,
      manuscriptId: manuscript.id,
      action: logEvent.actions.invitation_agreed,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = { initialize }
