const getAcademicEditorsUseCase = require('./getAcademicEditors')
const inviteAcademicEditorUseCase = require('./inviteAcademicEditor')
const acceptAcademicEditorInvitationUseCase = require('./acceptInvitation')
const cancelAcademicEditorInvitationUseCase = require('./cancelInvitation')
const declineAcademicEditorInvitationUseCase = require('./declineInvitation')
const acceptNormalAcademicEditorInvitationUseCase = require('./acceptNormalInvitation')
const acceptReassignmentAcademicEditorInvitationUseCase = require('./acceptReassignmentInvitation')

module.exports = {
  getAcademicEditorsUseCase,
  inviteAcademicEditorUseCase,
  acceptAcademicEditorInvitationUseCase,
  cancelAcademicEditorInvitationUseCase,
  declineAcademicEditorInvitationUseCase,
  acceptNormalAcademicEditorInvitationUseCase,
  acceptReassignmentAcademicEditorInvitationUseCase,
}
