const Promise = require('bluebird')

const initialize = ({
  models,
  useCases,
  jobsService,
  notificationService,
}) => ({
  async execute({
    journal,
    manuscript,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { Team, TeamMember, User, Journal, SpecialIssue } = models

    await useCases.cancelAllJobsUseCase
      .initialize({ models, jobsService })
      .execute({
        manuscript,
        academicEditor,
        editorialAssistant,
      })

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: manuscript.id,
    })
    const acceptedReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.accepted,
    )
    const pendingReviewers = reviewers.filter(
      reviewer => reviewer.status === TeamMember.Statuses.pending,
    )

    const triageEditor = await TeamMember.findTriageEditor({
      TeamRole: Team.Role,
      journalId: journal.id,
      manuscriptId: manuscript.id,
      sectionId: manuscript.sectionId,
    })
    triageEditor.user = await User.find(triageEditor.userId)

    await Promise.each(
      [...acceptedReviewers, ...pendingReviewers],
      async reviewer => (reviewer.user = await User.find(reviewer.userId)),
    )

    if (triageEditor) {
      const academicEditorLabel = await manuscript.getEditorLabel({
        SpecialIssue,
        Journal,
        role: Team.Role.academicEditor,
      })
      notificationService.notifyTriageEditor({
        journal,
        manuscript,
        triageEditor,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
        academicEditorLabel,
      })
    }

    notificationService.notifyReviewers({
      journal,
      manuscript,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
      reviewers: [...acceptedReviewers, ...pendingReviewers],
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isAcademicEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
