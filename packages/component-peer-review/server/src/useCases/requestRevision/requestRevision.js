const { createVersion } = require('./createVersion')

const initialize = ({
  models,
  useCases,
  logEvent,
  jobsService,
  notificationService,
}) => ({
  async execute({ manuscriptId, content, type, userId }) {
    const { Team, User, Review, Comment, Manuscript, TeamMember } = models
    const manuscript = await Manuscript.find(
      manuscriptId,
      'journal.peerReviewModel',
    )

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )
    submittingAuthor.user = await User.find(submittingAuthor.userId)

    await createVersion({
      models,
      manuscript,
    })

    manuscript.updateStatus(Manuscript.Statuses.revisionRequested)

    await manuscript.save()

    let editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
      userId,
      manuscriptId,
      role: Team.Role.academicEditor,
    })
    if (!editorialMember) {
      editorialMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
        userId,
        manuscriptId,
        role: Team.Role.triageEditor,
      })
    }

    if (!editorialMember) {
      editorialMember = await TeamMember.findOneByJournalAndRole({
        journalId: manuscript.journalId,
        role: Team.Role.editorialAssistant,
      })
    }

    const review = new Review({
      manuscriptId,
      teamMemberId: editorialMember.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations[type],
    })
    await review.save()

    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    const { journal } = manuscript
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })

    notificationService.notifySubmittingAuthor({
      content,
      journal,
      manuscript,
      editor: editorialMember,
      submittingAuthor,
      editorialAssistant,
    })

    const isRegularRevision = type === Review.Recommendations.revision
    let eventAction
    if (isRegularRevision) {
      eventAction = 'revision_requested'
    } else {
      eventAction =
        type === Review.Recommendations.major
          ? 'revision_requested_major'
          : 'revision_requested_minor'
    }

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      action: logEvent.actions[eventAction],
      objectType: logEvent.objectType.manuscript,
    })

    if (!isRegularRevision) {
      return useCases.requestMinorOrMajorUseCase
        .initialize({ models, jobsService, notificationService, useCases })
        .execute({
          journal,
          manuscript,
          submittingAuthor,
          editorialAssistant,
          academicEditor: editorialMember,
        })
    }
    if (
      journal.peerReviewModel.approvalEditors.includes(Team.Role.academicEditor)
    )
      return useCases.cancelAllJobsUseCase
        .initialize({ models, jobsService })
        .execute({
          manuscript,
          editorialAssistant,
          academicEditor: editorialMember,
        })
  },
})

const authsomePolicies = ['isAuthenticated', 'isEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
