const uuid = require('uuid')
const { s3service } = require('component-files/server')
const { Promise } = require('bluebird')

module.exports = {
  async createVersion({
    models: { Manuscript, Review, Comment, File, Team, TeamMember },
    manuscript,
  }) {
    const newManuscript = new Manuscript({
      ...manuscript,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
      version: manuscript.version + 1,
      status: Manuscript.Statuses.draft,
    })
    delete newManuscript.id
    await newManuscript.save()

    const files = await File.findBy({ manuscriptId: manuscript.id })
    await uploadNewFiles({
      File,
      files,
      newManuscriptId: newManuscript.id,
    })

    const newAuthorTeam = new Team({
      role: Team.Role.author,
      manuscriptId: newManuscript.id,
    })
    await newAuthorTeam.save()
    const newAcademicEditorTeam = new Team({
      role: Team.Role.academicEditor,
      manuscriptId: newManuscript.id,
    })
    await newAcademicEditorTeam.save()

    const manuscriptId = manuscript.id
    const existingAuthors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId,
      role: Team.Role.author,
    })
    const existingAcademicEditors = await TeamMember.findAllByManuscriptAndRole(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
      },
    )

    const existingTriageEditor = await TeamMember.findOneByManuscriptAndRole({
      role: Team.Role.triageEditor,
      manuscriptId,
    })
    if (existingTriageEditor) {
      const newTriageEditorTeam = new Team({
        role: Team.Role.triageEditor,
        manuscriptId: newManuscript.id,
      })
      await newTriageEditorTeam.save()
      const newTriageEditor = new TeamMember({
        ...existingTriageEditor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newTriageEditorTeam.id,
      })
      delete newTriageEditor.id
      await newTriageEditor.save()
    }

    await Promise.each(existingAuthors, existingAuthor => {
      const newAuthor = new TeamMember({
        ...existingAuthor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newAuthorTeam.id,
      })
      delete newAuthor.id
      return newAuthor.save()
    })

    const newSubmittingAuthor = await TeamMember.findSubmittingAuthor(
      newManuscript.id,
    )
    const review = new Review({
      manuscriptId: newManuscript.id,
      recommendation: Review.Recommendations.responseToRevision,
      teamMemberId: newSubmittingAuthor.id,
    })
    await review.save()

    const comment = new Comment({
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    await Promise.each(existingAcademicEditors, existingAcademicEditor => {
      const newAcademicEditor = new TeamMember({
        ...existingAcademicEditor,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: newAcademicEditorTeam.id,
      })
      delete newAcademicEditor.id
      return newAcademicEditor.save()
    })
  },
}

const uploadNewFiles = async ({ files, File, newManuscriptId }) => {
  files.map(async file => {
    const newKey = `${newManuscriptId}/${uuid.v4()}`
    await s3service.copyObject({ prevKey: file.providerKey, newKey })

    const newFile = new File({
      ...file,
      providerKey: newKey,
      manuscriptId: newManuscriptId,
      created: new Date().toISOString(),
      updated: new Date().toISOString(),
    })
    delete newFile.id
    return newFile.save()
  })
}
