const config = require('config')
const useCases = require('./index')
const { sendPackage } = require('component-mts-package')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')
const requestRevisionNotifications = require('../../notifications/requestRevision/requestRevision')

const JobsService = require('../../jobsService/jobsService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/requestRevision/getEmailCopy')

const resolver = {
  Query: {},
  Mutation: {
    async requestRevision(_, { manuscriptId, type, content }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = requestRevisionNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })
      const jobsService = JobsService.initialize({ models })

      return useCases.requestRevisionUseCase
        .initialize({
          models,
          logEvent,
          useCases,
          sendPackage,
          jobsService,
          notificationService,
        })
        .execute({ manuscriptId, userId: ctx.user, type, content })
    },
  },
}

module.exports = resolver
