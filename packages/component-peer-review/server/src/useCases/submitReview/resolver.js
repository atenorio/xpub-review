const config = require('config')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const useCases = require('./index')
const urlService = require('../../urlService/urlService')
const JobsService = require('../../jobsService/jobsService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/submitReview/getEmailCopy')
const submitReviewNotifications = require('../../notifications/submitReview/submit')

const resolver = {
  Query: {},
  Mutation: {
    async updateDraftReview(_, { reviewId, input }, ctx) {
      return useCases.updateDraftReviewUseCase
        .initialize({ models })
        .execute({ reviewId, input, userId: ctx.user })
    },
    async submitReview(_, { reviewId }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = submitReviewNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
      })

      const jobsService = JobsService.initialize({ models })
      return useCases.submitReviewUseCase
        .initialize({
          notificationService,
          models,
          logEvent,
          jobsService,
        })
        .execute({ reviewId, userId: ctx.user })
    },
  },
}

module.exports = resolver
