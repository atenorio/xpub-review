const updateDraftReviewUseCase = require('./updateDraft')
const submitReviewUseCase = require('./submit')

module.exports = { updateDraftReviewUseCase, submitReviewUseCase }
