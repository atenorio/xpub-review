const { Promise } = require('bluebird')

const initialize = ({ models: { Review, TeamMember, Comment, Team } }) => ({
  async execute({ reviewId, input }) {
    const review = await Review.find(reviewId)

    review.updateProperties({ recommendation: input.recommendation })
    await review.save()

    const existingComments = await Comment.findBy({ reviewId: review.id })

    await Promise.each(input.comments, async inputComm => {
      const comment = existingComments.find(com => com.id === inputComm.id)
      comment.updateProperties({ content: inputComm.content })
      await comment.save()
    })

    review.comments = existingComments
    review.member = await TeamMember.find(review.teamMemberId)
    const memberTeam = await Team.find(review.member.teamId)
    review.member.role = memberTeam.role

    return review
  },
})

const authsomePolicies = ['isAuthenticated', 'isReviewerOnReview']

module.exports = {
  initialize,
  authsomePolicies,
}
