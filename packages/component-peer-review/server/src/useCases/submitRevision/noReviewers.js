const initialize = ({ notificationService }) => ({
  async execute({
    draft,
    journal,
    academicEditor,
    submittingAuthor,
    ManuscriptStatuses,
    editorialAssistant,
  }) {
    draft.updateStatus(ManuscriptStatuses.academicEditorAssigned)
    await draft.save()

    notificationService.notifyAcademicEditor({
      draft,
      journal,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })
  },
})

module.exports = { initialize }
