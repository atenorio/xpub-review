const { Promise } = require('bluebird')

const initialize = ({ notificationService, models }) => ({
  async execute({
    draft,
    journal,
    ManuscriptStatuses,
    beforeDraft,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { TeamMember, Team } = models
    const pendingReviewers = await TeamMember.findAllByManuscriptAndRoleAndStatus(
      {
        role: Team.Role.reviewer,
        manuscriptId: beforeDraft.id,
        status: TeamMember.Statuses.pending,
      },
    )
    await Promise.each(pendingReviewers, pR => pR.delete())

    draft.updateStatus(ManuscriptStatuses.reviewCompleted)
    await draft.save()

    notificationService.notifyAcademicEditor({
      draft,
      journal,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })
  },
})

module.exports = { initialize }
