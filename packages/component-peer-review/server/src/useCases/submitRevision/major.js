const { Promise } = require('bluebird')

const initialize = ({
  models,
  emailJobsService,
  removalJobsService,
  invitationsService,
  notificationService,
}) => ({
  execute: async ({
    draft,
    journal,
    logEvent,
    timeUnit,
    beforeDraft,
    removalDays,
    academicEditor,
    submittingAuthor,
    editorialAssistant,
  }) => {
    const { Manuscript, TeamMember, Team, User, Identity } = models
    draft.updateStatus(Manuscript.Statuses.underReview)
    await draft.save()

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: beforeDraft.id,
    })

    const pendingReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.pending,
    )
    await Promise.each(pendingReviewers, pR => pR.delete())

    const submittedReviewers = reviewers.filter(
      r => r.status === TeamMember.Statuses.submitted,
    )

    const reviewerTeam = await Team.findOrCreate({
      queryObject: { role: Team.Role.reviewer, manuscriptId: draft.id },
      options: {
        role: Team.Role.reviewer,
        manuscriptId: draft.id,
      },
    })

    const newVersionReviewers = []
    await Promise.each(submittedReviewers, async reviewer => {
      const newReviewer = new TeamMember({
        reviewerNumber: reviewer.reviewerNumber,
        position: reviewer.position,
        alias: reviewer.alias,
        userId: reviewer.userId,
        created: new Date().toISOString(),
        updated: new Date().toISOString(),
        teamId: reviewerTeam.id,
        status: TeamMember.Statuses.pending,
      })
      newVersionReviewers.push(newReviewer)
      await newReviewer.save()
      await reviewerTeam.save()
    })

    await Promise.each(newVersionReviewers, async reviewer => {
      reviewer.user = await User.find(reviewer.userId)
      const userIdentity = await Identity.findOneBy({
        queryObject: {
          type: 'local',
          userId: reviewer.userId,
        },
      })
      reviewer.user.identities = [userIdentity]
    })

    const authors = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.author,
      manuscriptId: draft.id,
    })

    invitationsService.sendInvitationToReviewerAfterMajorRevision({
      journal,
      authors,
      academicEditor,
      submittingAuthor,
      manuscript: draft,
      editorialAssistant,
      newVersionReviewers,
    })

    notificationService.notifyAcademicEditor({
      draft,
      journal,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })

    await Promise.each(newVersionReviewers, reviewer =>
      logEvent({
        userId: null,
        manuscriptId: draft.id,
        action: logEvent.actions.reviewer_invited,
        objectType: logEvent.objectType.user,
        objectId: reviewer.userId,
      }),
    )

    await Promise.each(newVersionReviewers, async reviewer => {
      await emailJobsService.scheduleEmailsWhenReviewerIsInvitedAfterMajorRevision(
        {
          manuscript: draft,
          reviewer,
          authors,
          academicEditor,
          editorialAssistant,
          submittingAuthorName: submittingAuthor.getName(),
        },
      )
      await removalJobsService.scheduleRemovalJob({
        timeUnit,
        days: removalDays,
        invitationId: reviewer.id,
        manuscriptId: draft.id,
      })
    })
  },
})

module.exports = { initialize }
