const { last } = require('lodash')
const config = require('config')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')

const initialize = ({
  models,
  logEvent,
  useCases,
  emailJobsService,
  invitationsService,
  removalJobsService,
  notificationService,
}) => ({
  async execute({ submissionId }) {
    const { Team, User, Review, Journal, TeamMember, Manuscript } = models
    const manuscripts = await Manuscript.findAll({
      order: 'asc',
      orderByField: 'version',
      queryObject: { submissionId },
    })
    const latestManuscriptVersion = manuscripts.find(
      manuscript => manuscript.status !== Manuscript.Statuses.olderVersion,
    )
    const manuscript = manuscripts[0]
    const journal = await Journal.find(manuscript.journalId, 'teams.members')

    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })

    const draft = last(manuscripts)
    const beforeDraft = manuscripts[manuscripts.length - 2]

    beforeDraft.updateStatus(Manuscript.Statuses.olderVersion)
    await beforeDraft.save()

    const latestRecommendation = await Review.findLatestEditorialReview({
      manuscriptId: beforeDraft.id,
      TeamRole: Team.Role,
    })
    if (!latestRecommendation) {
      throw new ConflictError('No recommendation found')
    }

    const submittingAuthor = await TeamMember.findSubmittingAuthor(draft.id)
    const responseToRevision = await Review.findOneBy({
      queryObject: {
        recommendation: Review.Recommendations.responseToRevision,
        manuscriptId: draft.id,
      },
    })

    if (responseToRevision) {
      responseToRevision.setSubmitted(new Date().toISOString())
      await responseToRevision.save()
    }

    let useCase = latestRecommendation.recommendation

    const reviewers = await TeamMember.findAllByManuscriptAndRole({
      role: Team.Role.reviewer,
      manuscriptId: beforeDraft.id,
    })
    if (
      reviewers.length === 0 &&
      latestRecommendation.recommendation !== Review.Recommendations.revision
    ) {
      useCase = 'noReviewers'
    }

    logEvent({
      objectId: latestManuscriptVersion.id,
      userId: submittingAuthor.userId,
      manuscriptId: latestManuscriptVersion.id,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.revision_submitted,
    })

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: beforeDraft.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    if (academicEditor) {
      academicEditor.user = await User.find(academicEditor.userId)
    }

    return useCases[`${useCase}RevisionUseCase`]
      .initialize({
        models,
        emailJobsService,
        invitationsService,
        removalJobsService,
        notificationService,
      })
      .execute({
        draft,
        journal,
        logEvent,
        timeUnit,
        removalDays,
        beforeDraft,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
        ManuscriptStatuses: Manuscript.Statuses,
      })
  },
})

const authsomePolicies = ['isAuthenticated', 'hasAccessToManuscriptVersions']

module.exports = {
  initialize,
  authsomePolicies,
}
