const config = require('config')
const useCases = require('./index')
const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const Email = require('@pubsweet/component-email-templating')
const { getModifiedText } = require('component-transform-text')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const urlService = require('../../urlService/urlService')

const acceptInvitationNotifications = require('../../notifications/inviteReviewers/acceptInvitation')
const cancelInvitationNotifications = require('../../notifications/inviteReviewers/cancelInvitation')
const declineInvitationNotifications = require('../../notifications/inviteReviewers/declineInvitation')

const JobsService = require('../../jobsService/jobsService')
const invitations = require('../../invitations/invitations')
const emailJobs = require('../../emailJobs/reviewer/emailJobs')
const { getExpectedDate } = require('../../dateService/dateService')
const getProps = require('../../emailPropsService/emailPropsService')
const getEmailCopy = require('../../notifications/inviteReviewers/getEmailCopy')
const pendingReviewersRemovalJobs = require('../../removalJobs/pendingReviewers')
const acceptedReviewersRemovalJobs = require('../../removalJobs/acceptedReviewers')

const resolver = {
  Query: {
    async getReviewersInvited(_, { manuscriptId }, ctx) {
      return useCases.getReviewersInvitedUseCase
        .initialize(models)
        .execute(manuscriptId)
    },
  },
  Mutation: {
    async inviteReviewer(_, { manuscriptId, input }, ctx) {
      const { TeamMember, ReviewerSuggestion, Job } = models
      const jobsService = JobsService.initialize({ models })

      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const emailJobsService = emailJobs.initialize({
        Job,
        Email,
        logEvent,
        getPropsService,
      })
      const invitationsService = invitations.initialize({ Email })
      const removalJobsService = pendingReviewersRemovalJobs.initialize({
        logEvent,
        Job,
        TeamMember,
        ReviewerSuggestion,
      })

      const sso = process.env.KEYCLOAK_SERVER_URL
        ? require('component-sso')
        : null

      return useCases.inviteReviewerUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          emailJobsService,
          removalJobsService,
          invitationsService,
          sso,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async acceptReviewerInvitation(_, { teamMemberId }, ctx) {
      const { TeamMember, Job, ReviewerSuggestion } = models
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = acceptInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      const emailJobsService = emailJobs.initialize({
        logEvent,
        Email,
        Job,
        getPropsService,
      })
      const removalJobsService = acceptedReviewersRemovalJobs.initialize({
        logEvent,
        Job,
        TeamMember,
        ReviewerSuggestion,
      })
      return useCases.acceptReviewerInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          emailJobsService,
          removalJobsService,
          notificationService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async declineReviewerInvitation(_, { teamMemberId }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = declineInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      return useCases.declineReviewerInvitationUseCase
        .initialize({
          models,
          logEvent,
          notificationService,
          jobsService,
        })
        .execute({ teamMemberId, userId: ctx.user })
    },
    async cancelReviewerInvitation(_, { manuscriptId, teamMemberId }, ctx) {
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const getEmailCopyService = getEmailCopy.initialize()
      const notificationService = cancelInvitationNotifications.initialize({
        Email,
        getPropsService,
        getEmailCopyService,
        getExpectedDate,
      })
      const jobsService = JobsService.initialize({ models })
      return useCases.cancelReviewerInvitationUseCase
        .initialize({
          models,
          logEvent,
          jobsService,
          notificationService,
        })
        .execute({ manuscriptId, teamMemberId, userId: ctx.user })
    },
  },
}

module.exports = resolver
