const { get } = require('lodash')

const initialize = ({ Team }) => ({
  async execute(manuscriptId) {
    const reviewersTeam = await Team.findOneBy({
      queryObject: {
        role: Team.Role.reviewer,
        manuscriptId,
      },
      eagerLoadRelations: 'members.[user.[identities]]',
    })
    const teamMembers = get(reviewersTeam, 'members', [])
    return teamMembers.map(m => m.toDTO())
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
