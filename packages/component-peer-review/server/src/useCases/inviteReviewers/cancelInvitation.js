const { isEmpty } = require('lodash')

const initialize = ({
  notificationService,
  models: {
    Job,
    Team,
    User,
    Journal,
    Section,
    TeamMember,
    Manuscript,
    ReviewerSuggestion,
  },
  logEvent,
  jobsService,
}) => ({
  async execute({ manuscriptId, teamMemberId, userId }) {
    const teamMember = await TeamMember.find(teamMemberId)

    const teamMemberJobs = await Job.findAllByTeamMember(teamMemberId)
    jobsService.cancelJobs(teamMemberJobs)

    const manuscript = await Manuscript.findManuscriptByTeamMember(teamMemberId)

    if (!manuscript) {
      throw new NotFoundError('Manuscript does not exist')
    }
    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    await teamMember.delete()

    const reviewers = await TeamMember.findAllByStatuses({
      role: Team.Role.reviewer,
      statuses: [
        TeamMember.Statuses.pending,
        TeamMember.Statuses.accepted,
        TeamMember.Statuses.submitted,
      ],
      manuscriptId,
    })
    const team = await Team.find(teamMember.teamId)

    if (isEmpty(reviewers)) {
      await manuscript.updateStatus(Manuscript.Statuses.academicEditorAssigned)
      await manuscript.save()
      await team.delete()
    }
    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: teamMember.alias.email,
        manuscriptId: manuscript.id,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = false
      await reviewerSuggestion.save()
    }

    const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscriptId)

    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })
    teamMember.user = await User.find(teamMember.userId)

    notificationService.notifyReviewer({
      journal,
      manuscript,
      user: teamMember,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.invitation_revoked,
      objectType: logEvent.objectType.user,
      objectId: teamMember.userId,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isAcademicEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
