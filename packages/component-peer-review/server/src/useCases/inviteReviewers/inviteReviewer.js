const uuid = require('uuid')
const config = require('config')

const removalDays = config.get('reminders.reviewer.acceptInvitation.remove')
const timeUnit = config.get('reminders.reviewer.acceptInvitation.timeUnit')

const initialize = ({
  invitationsService,
  models: {
    Job,
    Team,
    User,
    Journal,
    Identity,
    TeamMember,
    Manuscript,
    SpecialIssue,
    ReviewerSuggestion,
  },
  logEvent,
  jobsService,
  emailJobsService,
  removalJobsService,
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    const userIdentity = await Identity.findOneByEmail(input.email)
    let user = userIdentity ? await User.find(userIdentity.userId) : undefined
    let identity = userIdentity

    if (!user) {
      user = new User({
        defaultIdentity: 'local',
        isActive: true,
        isSubscribedToEmails: true,
        confirmationToken: uuid.v4(),
        unsubscribeToken: uuid.v4(),
        passwordResetToken: uuid.v4(),
        agreeTc: true,
      })
      await user.save()

      identity = new Identity({
        type: 'local',
        isConfirmed: false,
        passwordHash: null,
        givenNames: input.givenNames,
        surname: input.surname,
        email: input.email,
        aff: input.aff || '',
        userId: user.id,
        country: input.country || '',
      })
      await identity.save()
      user.assignIdentity(identity)
    }

    let manuscriptReviewersTeam = await Team.findOneBy({
      queryObject: {
        manuscriptId: manuscript.id,
        role: Team.Role.reviewer,
      },
    })

    let teamMember
    if (!manuscriptReviewersTeam) {
      manuscriptReviewersTeam = new Team({
        manuscriptId,
        role: Team.Role.reviewer,
      })
      manuscript.updateStatus(Manuscript.Statuses.reviewersInvited)
      await manuscriptReviewersTeam.save()
      await manuscript.save()
    } else {
      teamMember = await TeamMember.findOneByManuscriptAndRoleAndUser({
        manuscriptId,
        userId: user.id,
        role: Team.Role.reviewer,
      })
    }
    if (teamMember && teamMember.status !== TeamMember.Statuses.expired) {
      throw new ValidationError(
        `User ${identity.email} is already invited as ${Team.Role.reviewer}`,
      )
    }
    let reviewerMembers = []

    if (teamMember && teamMember.status === TeamMember.Statuses.expired) {
      teamMember.updateProperties({ status: TeamMember.Statuses.pending })
      await teamMember.save()
    } else {
      reviewerMembers = await TeamMember.findAllByManuscriptAndRole({
        role: Team.Role.reviewer,
        manuscriptId,
      })
      teamMember = new TeamMember({
        alias: input,
        userId: user.id,
        teamId: manuscriptReviewersTeam.id,
        position: reviewerMembers.length + 1,
      })
      teamMember.linkUser(user)
      await teamMember.save()
    }

    const reviewerSuggestion = await ReviewerSuggestion.findOneBy({
      queryObject: {
        email: input.email,
        manuscriptId,
      },
    })
    if (reviewerSuggestion) {
      reviewerSuggestion.isInvited = true
      await reviewerSuggestion.save()
    }

    let staffMember = await TeamMember.findOneByJournalAndRole({
      role: Team.Role.editorialAssistant,
      journalId: manuscript.journalId,
    })
    if (!staffMember) {
      staffMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    const academicEditorJobs = await Job.findAllByTeamMember(academicEditor.id)
    const staffMemberJobs = await Job.findAllByTeamMember(staffMember.id)
    teamMember.user = user
    user.identities = userIdentity ? [userIdentity] : [identity]

    if (reviewerMembers.length + 1 >= config.mininumNumberOfInvitedReviewers) {
      jobsService.cancelJobs(academicEditorJobs)
      jobsService.cancelStaffMemberJobs({
        staffMemberJobs,
        manuscriptId,
      })
    }

    const journal = await Journal.find(manuscript.journalId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(manuscriptId)
    const authors = await TeamMember.findAllByManuscriptAndRole({
      manuscriptId: manuscript.id,
      role: Team.Role.author,
    })

    const academicEditorLabel = await manuscript.getEditorLabel({
      SpecialIssue,
      Journal,
      role: Team.Role.academicEditor,
    })

    invitationsService.sendInvitationToReviewer({
      manuscript,
      journal,
      editorialAssistant: staffMember,
      reviewer: teamMember,
      academicEditorLabel,
      submittingAuthorName: submittingAuthor.getName(),
      authors,
      academicEditor,
    })

    await emailJobsService.scheduleEmailsWhenReviewerIsInvited({
      journal,
      manuscript,
      reviewer: teamMember,
      submittingAuthorName: submittingAuthor.getName(),
      authors,
      academicEditor,
    })

    await removalJobsService.scheduleRemovalJob({
      timeUnit,
      days: removalDays,
      invitationId: teamMember.id,
      manuscriptId: manuscript.id,
    })

    logEvent({
      userId,
      manuscriptId,
      action: logEvent.actions.reviewer_invited,
      objectType: logEvent.objectType.user,
      objectId: teamMember.user.id,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isAcademicEditorOnManuscript']

module.exports = {
  initialize,
  authsomePolicies,
}
