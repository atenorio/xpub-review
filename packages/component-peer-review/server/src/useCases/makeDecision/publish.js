const uuid = require('uuid')
const { uploadToMTS } = require('./uploadToMTS')

const initialize = ({
  models,
  logEvent,
  sendPackage,
  eventsService,
  notificationService,
}) => ({
  async execute({ manuscriptId, userId }) {
    const { User, Team, Review, Journal, TeamMember, Manuscript } = models

    const manuscript = await Manuscript.find(manuscriptId)

    if (
      [Manuscript.Statuses.deleted, Manuscript.Statuses.withdrawn].includes(
        manuscript.status,
      )
    ) {
      throw new AuthorizationError('Unauthorized')
    }

    let publishingMember = await TeamMember.findOneByManuscriptAndUser({
      userId,
      manuscriptId,
    })

    if (!publishingMember) {
      publishingMember = await TeamMember.findOneByJournalAndUser({
        userId,
        journalId: manuscript.journalId,
      })
    }
    if (!publishingMember) {
      publishingMember = await TeamMember.findOneByRole({
        role: Team.Role.admin,
      })
    }

    const review = new Review({
      manuscriptId,
      teamMemberId: publishingMember.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.publish,
    })

    await review.save()
    manuscript.assignReview(review)
    if (manuscript.hasPassedEqa) {
      manuscript.updateStatus(Manuscript.Statuses.accepted)
    } else {
      manuscript.updateProperties({
        status: Manuscript.Statuses.inQA,
        technicalCheckToken: uuid.v4(),
      })
    }

    await manuscript.save()

    await uploadToMTS({ manuscriptId, models, sendPackage })

    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })
    editorialAssistant.user = await User.find(editorialAssistant.userId)
    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    eventsService.publishSubmissionEvent({
      submissionId: manuscript.submissionId,
      eventName: 'SubmissionAccepted',
    })

    notificationService.notifyEQA({
      manuscript,
      publishingMember,
      submittingAuthor,
      editorialAssistant,
      journalName: journal.name,
    })

    logEvent({
      userId,
      manuscriptId,
      objectId: manuscriptId,
      objectType: logEvent.objectType.manuscript,
      action: logEvent.actions.manuscript_accepted,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isApprovalEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
