const initialize = ({
  notificationService,
  models: { Team, User, Review, Journal, Comment, TeamMember, Manuscript },
  logEvent,
}) => ({
  async execute({ manuscriptId, content, userId }) {
    const manuscript = await Manuscript.find(manuscriptId)
    if (manuscript.status === Manuscript.Statuses.deleted) {
      throw new AuthorizationError('Unauthorized')
    }

    const allowedStatuses = [Manuscript.Statuses.pendingApproval]

    if (!allowedStatuses.includes(manuscript.status)) {
      throw new ValidationError(
        `Cannot return a manuscript in the current status.`,
      )
    }

    let approvalEditor = await TeamMember.findApprovalEditor({
      manuscriptId,
      TeamRole: Team.Role,
    })

    if (!approvalEditor) {
      approvalEditor = await TeamMember.findOneByJournalAndRole({
        journalId: manuscript.journalId,
        role: Team.Role.editorialAssistant,
      })
    }

    const review = new Review({
      manuscriptId,
      teamMemberId: approvalEditor.id,
      submitted: new Date().toISOString(),
      recommendation: Review.Recommendations.returnToAcademicEditor,
    })
    await review.save()

    const comment = new Comment({
      content,
      reviewId: review.id,
      type: Comment.Types.public,
    })
    await comment.save()

    manuscript.assignReview(review)
    manuscript.updateStatus(Manuscript.Statuses.reviewCompleted)
    await manuscript.save()

    const journal = await Journal.find(manuscript.journalId)
    const editorialAssistant = await TeamMember.findOneByJournalAndRole({
      journalId: journal.id,
      role: Team.Role.editorialAssistant,
    })

    const submittingAuthor = await TeamMember.findSubmittingAuthor(
      manuscript.id,
    )

    const academicEditor = await TeamMember.findOneByManuscriptAndRoleAndStatus(
      {
        manuscriptId: manuscript.id,
        role: Team.Role.academicEditor,
        status: TeamMember.Statuses.accepted,
      },
    )
    academicEditor.user = await User.find(academicEditor.userId)

    notificationService.notifyAcademicEditor({
      journal,
      comment,
      manuscript,
      approvalEditor,
      academicEditor,
      submittingAuthor,
      editorialAssistant,
    })

    logEvent({
      userId,
      manuscriptId,
      objectId: academicEditor.userId,
      objectType: logEvent.objectType.user,
      action: logEvent.actions.manuscript_returned,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isApprovalEditor']

module.exports = {
  initialize,
  authsomePolicies,
}
