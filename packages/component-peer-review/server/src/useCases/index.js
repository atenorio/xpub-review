const makeDecisionUseCases = require('./makeDecision')
const submitRevisionUseCases = require('./submitRevision')
const submitReviewUseCases = require('./submitReview')
const requestRevisionUseCases = require('./requestRevision')
const makeRecommendationUseCases = require('./makeRecommendation')
const inviteReviewersUseCases = require('./inviteReviewers')
const inviteAcademicEditorUseCases = require('./inviteAcademicEditor')
const removeAcademicEditorUseCases = require('./removeAcademicEditor')
const reassignTriageEditorUseCase = require('./reassignTriageEditor')

module.exports = {
  ...submitReviewUseCases,
  ...makeDecisionUseCases,
  ...submitRevisionUseCases,
  ...requestRevisionUseCases,
  ...inviteReviewersUseCases,
  ...makeRecommendationUseCases,
  ...reassignTriageEditorUseCase,
  ...inviteAcademicEditorUseCases,
  ...removeAcademicEditorUseCases,
}
