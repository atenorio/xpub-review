module.exports = {
  getExpectedDate({ timestamp = Date.now(), daysExpected = 0 }) {
    const date = new Date(timestamp)
    let expectedDate = date.getDate() + daysExpected
    date.setDate(expectedDate)

    expectedDate = date.toLocaleDateString('en-US', {
      day: 'numeric',
      month: 'long',
      year: 'numeric',
    })

    return expectedDate
  },
}
