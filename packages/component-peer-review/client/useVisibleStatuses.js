import { get, chain, isEmpty } from 'lodash'
import { useJournal } from 'component-journal-info'

const useVisibleStatuses = ({
  currentUser,
  manuscript = {},
  revisionDraft = false,
  editorialReviews = [],
  isLatestVersion,
  reviewerReports = [],
}) => {
  const { parsedStatuses: statuses } = useJournal()
  const articleType = get(manuscript, 'articleType.name', '')
  const status = get(manuscript, 'status')
  const role = get(manuscript, 'role')
  const reviewers = get(manuscript, 'reviewers') || []
  // const reviews = get(manuscript, 'reviews') || []
  const academicEditor = get(manuscript, 'academicEditor')
  const currentUserId = get(currentUser, 'id')
  // const version = get(manuscript, 'version')
  const authorResponse = get(manuscript, 'reviews', []).find(
    r => get(r, 'recommendation') === 'responseToRevision',
  )
  const currentReviewer = chain(reviewers)
    .find(reviewer => reviewer.user.id === currentUserId)
    .value()

  const AuthorReviews = {
    // author: {
    //   isVisible:
    //     [
    //       statuses.revisionRequested,
    //       statuses.accepted,
    //       statuses.rejected,
    //     ].includes(status) || !isLatestVersion,
    // },
  }
  const CommentWithFile = {
    //  TODO: disabled added rules, remove later
    // author: {
    //   isVisible: !isEmpty(authorResponse),
    //   startExpanded:
    //     !isEmpty(authorResponse) && status !== statuses.revisionRequested,
    // },
    // admin: {
    //   isVisible:
    //     !isEmpty(authorResponse) && status !== statuses.revisionRequested,
    //   startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    // },
    // editorialAssistant: {
    //   isVisible:
    //     !isEmpty(authorResponse) && status !== statuses.revisionRequested,
    //   startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    // },
    // triageEditor: {
    //   isVisible:
    //     !isEmpty(authorResponse) && status !== statuses.revisionRequested,
    //   startExpanded: !isEmpty(authorResponse) && status === statuses.submitted,
    // },
    // academicEditor: {
    //   isVisible:
    //     !isEmpty(authorResponse) &&
    //     version > 1 &&
    //     get(academicEditor, 'status') === 'accepted',
    //   startExpanded:
    //     [
    //       statuses.academicEditorAssigned,
    //       statuses.reviewersInvited,
    //       statuses.underReview,
    //     ].includes(status) && !isEmpty(authorResponse),
    // },
    // reviewer: {
    //   isVisible:
    //     authorResponse &&
    //     ['accepted', 'submitted'].includes(get(currentReviewer, 'status')),
    //   startExpanded:
    //     status === statuses.underReview && !isEmpty(authorResponse),
    // },
  }
  const EditorialComments = {
    author: {
      isVisible:
        !!editorialReviews.length &&
        ![statuses.pendingApproval, statuses.inQA].includes(status),
      startExpanded: true,
    },
    admin: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    editorialAssistant: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    triageEditor: {
      isVisible: !!editorialReviews.length,
      startExpanded: true,
    },
    academicEditor: {
      isVisible:
        !!editorialReviews.length &&
        get(academicEditor, 'status') === 'accepted',
      startExpanded: true,
    },
    reviewer: {
      isVisible:
        !!editorialReviews.length &&
        ['accepted', 'submitted'].includes(get(currentReviewer, 'status')),
      startExpanded: true,
    },
  }

  const TriageEditorAssignAcademicEditor = {
    // admin: {
    //   isVisible: status === statuses.submitted,
    // },
    // editorialAssistant: {
    //   isVisible: status === statuses.submitted,
    // },
    // triageEditor: {
    //   isVisible: status === statuses.submitted,
    // },
  }

  const TriageEditorDecision = {
    admin: {
      isVisible: ![
        statuses.technicalChecks,
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),

      isHighlighted: [statuses.pendingApproval].includes(status),
    },
    editorialAssistant: {
      isVisible: ![
        statuses.technicalChecks,
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isHighlighted: [statuses.pendingApproval].includes(status),
    },
    triageEditor: {
      isVisible: ![
        statuses.rejected,
        statuses.inQA,
        statuses.accepted,
        statuses.olderVersion,
      ].includes(status),
      isEditorialVisible:
        ![
          statuses.academicEditorAssigned,
          statuses.inQA,
          statuses.accepted,
          statuses.rejected,
          statuses.academicEditorAssignedEditorialType,
          statuses.academicEditorInvited,
        ].includes(status) && articleType === 'Editorial',
      isHighlighted: [statuses.pendingApproval].includes(status),
    },
    academicEditor: {
      isEditorialVisible:
        ![
          statuses.inQA,
          statuses.accepted,
          statuses.rejected,
          statuses.academicEditorInvited,
        ].includes(status) && articleType === 'Editorial',
      isHighlighted: [statuses.academicEditorAssignedEditorialType].includes(
        status,
      ),
    },
  }

  const AcademicEditorRecommendation = {
    academicEditor: {
      isVisible: [
        statuses.academicEditorAssigned,
        statuses.reviewCompleted,
        statuses.academicEditorAssignedEditorialType,
      ].includes(status),
      highlight: [statuses.reviewCompleted].includes(status),
    },
  }

  const RespondToEditorialInvitation = {
    academicEditor: {
      isVisible: status === statuses.academicEditorInvited,
      isHighlighted: true,
      startExpanded: true,
    },
  }

  const RespondToReviewerInvitation = {
    reviewer: {
      isVisible: get(currentReviewer, 'status') === 'pending',
      isHighlighted: true,
      startExpanded: true,
    },
  }

  const ReviewerDetailsTriageEditor = {
    admin: {
      isVisible: get(academicEditor, 'status') === 'accepted',
      startExpanded: false,
    },
    editorialAssistant: {
      isVisible: get(academicEditor, 'status') === 'accepted',
      startExpanded: false,
    },
    triageEditor: {
      isVisible:
        get(academicEditor, 'status') === 'accepted' &&
        articleType !== 'Editorial',
      startExpanded: false,
    },
  }
  const academicEditorAdmin = {
    isVisible:
      ![statuses.academicEditorInvited].includes(status) &&
      get(academicEditor, 'status') === 'accepted' &&
      articleType !== 'Editorial',

    startExpanded:
      ([
        statuses.academicEditorAssigned,
        statuses.reviewersInvited,
        statuses.underReview,
      ].includes(status) &&
        isEmpty(authorResponse)) ||
      status === statuses.reviewCompleted,

    isHighlighted: [
      statuses.academicEditorAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
    ].includes(status),

    canInviteReviewers: [
      statuses.academicEditorAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
    ].includes(status),

    canInvitePublons: [
      statuses.academicEditorAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
    ].includes(status),
  }
  const ReviewerDetailsAcademicEditor = {
    academicEditor: academicEditorAdmin,
    admin: academicEditorAdmin,
    editorialAssistant: academicEditorAdmin,
  }

  // const reviewerCanSeeHisReport = !!chain(reviews)
  //   .find(review => get(review, 'member.user.id') === currentUserId)
  //   .get('submitted')
  //   .value()
  // const reviewerCanSubmitReport =
  //   get(currentReviewer, 'status') === 'accepted' &&
  //   ![statuses.revisionRequested, statuses.olderVersion].includes(status)
  const ReviewerReportBox = {
    // reviewer: {
    //   isVisible: reviewerCanSeeHisReport,
    //   startExpanded: true,
    // },
  }

  const SubmitReview = {
    // reviewer: {
    //   isVisible: reviewerCanSubmitReport && !reviewerCanSeeHisReport,
    //   isHighlighted: true,
    // },
  }

  const SubmitRevision = {
    // author: {
    //   isVisible: revisionDraft && status === statuses.revisionRequested,
    //   isHighlighted: true,
    // },
  }

  return {
    AuthorReviewsBox: AuthorReviews[role],
    CommentWithFileBox: CommentWithFile[role],
    EditorialCommentsBox: EditorialComments[role],
    TriageEditorAssignAcademicEditorBox: TriageEditorAssignAcademicEditor[role],
    TriageEditorDecisionBox: TriageEditorDecision[role],
    AcademicEditorRecommendationBox: AcademicEditorRecommendation[role],
    RespondToEditorialInvitationBox: RespondToEditorialInvitation[role],
    RespondToReviewerInvitationBox: RespondToReviewerInvitation[role],
    ReviewerDetailsTriageEditorBox: ReviewerDetailsTriageEditor[role],
    ReviewerDetailsAcademicEditorBox: ReviewerDetailsAcademicEditor[role],
    ReviewerReportContextualBox: ReviewerReportBox[role],
    SubmitReviewBox: SubmitReview[role],
    SubmitRevisionBox: SubmitRevision[role],
  }
}

export default useVisibleStatuses
