import React from 'react'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { ReviewerDetails } from '../../'
import { render } from '../testUtils'

describe('Reviewer Details', () => {
  afterEach(cleanup)

  it('should show the correct tab', () => {
    const { getByText, queryByText } = render(
      <ReviewerDetails
        isVisible
        startExpanded
        tabButtons={['Button1', 'Button2', 'Button3']}
      >
        <div>tab content one</div>
        <div>tab content two</div>
        <div>tab content three</div>
      </ReviewerDetails>,
    )

    expect(getByText('tab content one')).toBeInTheDocument()
    expect(queryByText('tab content two')).toBeNull()
    expect(queryByText('tab content three')).toBeNull()
  })

  it('should switch tabs', () => {
    const { getByText, queryByText } = render(
      <ReviewerDetails
        isVisible
        startExpanded
        tabButtons={['Button1', 'Button2', 'Button3']}
      >
        <div>tab content one</div>
        <div>tab content two</div>
        <div>tab content three</div>
      </ReviewerDetails>,
    )

    expect(getByText('tab content one')).toBeInTheDocument()
    expect(queryByText('tab content two')).toBeNull()
    expect(queryByText('tab content three')).toBeNull()

    fireEvent.click(getByText('Button3'))
    expect(queryByText('tab content one')).toBeNull()
    expect(queryByText('tab content two')).toBeNull()
    expect(getByText('tab content three')).toBeInTheDocument()
  })

  it('should render custom tabs', () => {
    const TabButton1 = () => <button>React stateless component</button>
    const TabButton2 = <h3>Plain html</h3>
    const TabButton3 = 'a plain string'

    const { getByText } = render(
      <ReviewerDetails
        isVisible
        startExpanded
        tabButtons={[TabButton1, TabButton2, TabButton3]}
      >
        <div>tab content one</div>
        <div>tab content two</div>
        <div>tab content three</div>
      </ReviewerDetails>,
    )

    expect(getByText('React stateless component')).toBeInTheDocument()
    expect(getByText('Plain html')).toBeInTheDocument()
    expect(getByText('a plain string')).toBeInTheDocument()
  })
})
