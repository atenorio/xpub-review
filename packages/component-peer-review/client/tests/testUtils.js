import React from 'react'
import { theme } from '@hindawi/ui'
import { ModalProvider } from 'component-modal'
import { ThemeProvider } from 'styled-components'
import { fireEvent, render as rtlRender, wait } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import { MockedProvider } from '@apollo/react-testing'

export const render = ui => {
  const Component = () => (
    <MockedProvider>
      <ModalProvider>
        <div id="ps-modal-root" />
        <ThemeProvider theme={theme}>{ui}</ThemeProvider>
      </ModalProvider>
    </MockedProvider>
  )

  const utils = rtlRender(<Component />)
  return {
    ...utils,
    selectOption: value => {
      fireEvent.click(utils.container.querySelector(`button[type=button]`))
      wait(() => utils.getByText(value))
      fireEvent.click(utils.getByText(value))
    },
  }
}
