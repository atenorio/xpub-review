import { useMutation } from 'react-apollo'
import { mutations } from '../graphql/'
import {
  refetchGetAuditLogEvents,
  refetchGetManuscriptVersions,
} from '../graphql/refetchQueries'

const useAssignAcademicEditor = ({ role, match, manuscript }) => {
  const [inviteAcademicEditorMutation] = useMutation(
    mutations.inviteAcademicEditor,
    {
      refetchQueries:
        role === 'admin'
          ? [
              refetchGetManuscriptVersions(match),
              refetchGetAuditLogEvents(match),
            ]
          : [refetchGetManuscriptVersions(match)],
    },
  )

  const inviteAcademicEditor = (academicEditor, modalProps) => {
    modalProps.setFetching(true)
    inviteAcademicEditorMutation({
      variables: {
        submissionId: manuscript.submissionId,
        userId: academicEditor.user.id,
      },
    })
      .then(() => {
        modalProps.setFetching(true)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return {
    inviteAcademicEditor,
  }
}

export default useAssignAcademicEditor
