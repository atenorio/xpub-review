import { useMutation } from 'react-apollo'
import { mutations } from '../graphql'
import {
  refetchGetManuscripts,
  refetchGetManuscriptVersions,
} from '../graphql/refetchQueries'

const useEditorialRecommendation = ({ manuscriptId, match }) => {
  const [requestRevision] = useMutation(mutations.requestRevision, {
    refetchQueries: [
      refetchGetManuscripts(),
      refetchGetManuscriptVersions(match),
    ],
  })
  const [makeRecommendationToReject] = useMutation(
    mutations.makeRecommendationToReject,
    {
      refetchQueries: [
        refetchGetManuscripts(),
        refetchGetManuscriptVersions(match),
      ],
    },
  )
  const [makeRecommendationToPublish] = useMutation(
    mutations.makeRecommendationToPublish,
    {
      refetchQueries: [
        refetchGetManuscripts(),
        refetchGetManuscriptVersions(match),
      ],
    },
  )

  const handleEditorialRecommendation = (values, modalProps) => {
    modalProps.setFetching(true)
    let handleRecommendationFn
    switch (values.recommendation) {
      case 'reject':
        handleRecommendationFn = makeRecommendationToReject({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
            },
          },
        })
        break
      case 'publish':
        handleRecommendationFn = makeRecommendationToPublish({
          variables: {
            manuscriptId,
            input: {
              messageForAuthor: values.public,
              messageForTriage: values.private,
            },
          },
        })
        break
      case 'minor':
      case 'major':
        handleRecommendationFn = requestRevision({
          variables: {
            manuscriptId,
            content: values.public,
            type: values.recommendation,
          },
        })
        break
      default:
        return null
    }
    handleRecommendationFn
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return { handleEditorialRecommendation }
}

export default useEditorialRecommendation
