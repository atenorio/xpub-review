import React, { useEffect } from 'react'
import { Loader, parseSearchParams } from '@hindawi/ui'

const AcademicEditorInvitationRedirect = ({ location, history, action }) => {
  useEffect(() => {
    const { manuscriptId, submissionId, teamMemberId } = parseSearchParams(
      location.search,
    )

    if (action === 'accept-he') {
      history.replace({
        pathname: `/emails/accept-academic-editor`,
        search: `teamMemberId=${teamMemberId}&manuscriptId=${manuscriptId}&submissionId=${submissionId}`,
      })
    } else if (action === 'decline-he') {
      history.replace({
        pathname: `/emails/decline-academic-editor`,
        search: `teamMemberId=${teamMemberId}`,
      })
    }
  }, [history, location.search, action])

  return <Loader />
}

export default AcademicEditorInvitationRedirect
