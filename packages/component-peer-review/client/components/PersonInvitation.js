import React, { Fragment } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { compose, withHandlers, withProps } from 'recompose'
import { Label, Icon, MultiAction, Text } from '@hindawi/ui'
import { Button } from '@pubsweet/ui'

const PersonInvitation = ({
  role,
  label,
  invitation,
  academicEditors,
  toggleAssignAcademicEditor,
  withUnassigned,
  invitationName,
  invitationStatus,
  revokeInvitation,
  manuscriptStatus,
  withName = true,
}) => (
  <Root>
    {label && <Label>{label}</Label>}
    {withUnassigned && !get(invitation, 'id') && <Text>Unassigned</Text>}
    {withName && <Text ml={label ? 1 : 0}>{invitationName}</Text>}
    {invitationStatus === 'pending' && manuscriptStatus !== 'rejected' ? (
      <Fragment>
        <Modal
          cancelText="BACK"
          component={MultiAction}
          confirmText="REVOKE"
          modalKey={`${invitation.id}-revoke`}
          onConfirm={revokeInvitation}
          subtitle="Are you sure you want to revoke the invitation?"
          title={invitationName}
        >
          {showModal => (
            <Icon
              data-test-id="revoke-icon"
              fontSize="14px"
              icon="remove"
              ml={2}
              onClick={showModal}
            />
          )}
        </Modal>
      </Fragment>
    ) : (
      ['admin', 'editorialAssistant'].includes(role) &&
      invitationStatus === 'accepted' &&
      academicEditors.length > 1 && (
        <Fragment>
          <Button
            data-test-id="reassign-academic-editor"
            ml={2}
            onClick={toggleAssignAcademicEditor}
            primary
            xs
          >
            <Icon icon="reassign" mr={1} />
            Reassign
          </Button>
        </Fragment>
      )
    )}
  </Root>
)

export default compose(
  withProps(({ invitation }) => ({
    invitationName: `${get(invitation, 'alias.name.givenNames', '')} ${get(
      invitation,
      'alias.name.surname',
      '',
    )}`,
    invitationStatus: get(invitation, 'status'),
  })),
  withHandlers({
    revokeInvitation: ({ onRevoke, invitation }) => modalProps => {
      if (typeof onRevoke === 'function') {
        onRevoke(invitation, modalProps)
      }
    },
  }),
)(PersonInvitation)

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${space};
`
// #endregion
