import React from 'react'
import { get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { Row, Text, Loader, ActionLink, ShadowedBox } from '@hindawi/ui'

const EmailResponseLayout = ({
  history,
  journal,
  isFetching,
  fetchingError,
}) => (
  <ShadowedBox center mt={4} width={120}>
    {!isFetching && fetchingError && <H2>Error</H2>}
    {!isFetching && !fetchingError && <H2>Thank you for letting us know</H2>}

    {isFetching && <H2>Loading...</H2>}

    {!isFetching && (
      <Row justify="center" mt={4}>
        {fetchingError ? (
          <Text error>{fetchingError}</Text>
        ) : (
          <Text align="center">
            We hope you will review for Hindawi in the future. If you want any
            more information, or would like to submit a review for this article,
            then please contact us at
            <ActionLink
              display="inline"
              ml={1}
              to={`mailto:${get(journal, 'metadata.supportEmail')}`}
            >
              {get(journal, 'metadata.supportEmail')}
            </ActionLink>
            .
          </Text>
        )}
      </Row>
    )}

    <Row justify="center" mt={4}>
      {isFetching && <Loader />}
    </Row>
  </ShadowedBox>
)

export default EmailResponseLayout
