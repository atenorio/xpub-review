import React, { Fragment } from 'react'
import { Formik } from 'formik'
import styled from 'styled-components'
import { get, chain, set } from 'lodash'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { Button, FilePicker, Spinner } from '@pubsweet/ui'
import { File, withGQL as withFilesGQL } from 'component-files/client'
import { compose, withHandlers, withProps, withState } from 'recompose'
import {
  Row,
  Item,
  Icon,
  Menu,
  Label,
  Textarea,
  ActionLink,
  MultiAction,
  withFetching,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

import { parseReviewValues } from '../../client/utils'

const validate = values => {
  const errors = {}
  if (!values.public.content.trim() && values.files.length === 0) {
    set(errors, 'public.content', 'A file or text is required.')
  }
  if (!values.recommendation) {
    set(errors, 'recommendation', 'Required')
  }

  return errors
}

const allowedFileExtensions = ['pdf', 'doc', 'docx', 'txt', 'rdf', 'odt']

const ReportForm = ({
  values = {},
  hasNote,
  onUpload,
  onDelete,
  isFetching,
  toggleNote,
  setFieldValue,
}) => (
  <Fragment>
    <Row mt={2}>
      <Item alignItems="center" justify="space-between">
        <Item alignItems="center" height={6}>
          <Label mr={2} required>
            Your report
          </Label>
          {isFetching ? (
            <Spinner />
          ) : (
            <FilePicker
              allowedFileExtensions={allowedFileExtensions}
              disabled={values.files.length > 0}
              name="files"
              onUpload={file => onUpload({ file, setFieldValue })}
            >
              <ActionLink
                data-test-id="form-report-upload-file"
                disabled={values.files.length > 0}
                fontSize="12px"
                fontWeight="bold"
              >
                <Icon bold fontSize="10px" icon="expand" mr={1} />
                UPLOAD FILE
              </ActionLink>
            </FilePicker>
          )}
        </Item>
        <Item flex={0}>
          <ActionLink to="https://about.hindawi.com/authors/peer-review-at-hindawi/">
            Hindawi Reviewer Guidelines
          </ActionLink>
          <Icon icon="link" ml={1} />
        </Item>
      </Item>
    </Row>
    <Item vertical>
      <ValidatedFormField
        component={Textarea}
        data-test-id="form-report-textarea"
        minHeight={18}
        name="public.content"
      />
    </Item>
    {values.files.length > 0 && (
      <Row justify="flex-start" mb={4}>
        <File
          data-test-id="form-report-file-item-actions"
          hasDelete
          item={values.files[0]}
          onDelete={file => onDelete({ file, setFieldValue })}
        />
      </Row>
    )}
    <Row alignItems="center">
      {hasNote ? (
        <Fragment>
          <Row mt={4}>
            <Item>
              <Label>Confidential note for the Editorial Team</Label>
            </Item>
            <Item justify="flex-end">
              <ActionLink
                alignItems="flex-start"
                data-test-id="form-report-remove-note"
                onClick={toggleNote}
              >
                <Icon bold icon="remove1" mr={1 / 2} />
                Remove
              </ActionLink>
            </Item>
          </Row>
        </Fragment>
      ) : (
        <Item mt={4}>
          <ActionLink data-test-id="form-report-add-note" onClick={toggleNote}>
            Add Confidential note for the Editorial Team
          </ActionLink>
        </Item>
      )}
    </Row>
    {hasNote && (
      <Row mt={1}>
        <ValidatedFormField
          component={Textarea}
          data-test-id="textarea-form-report-add-note"
          minHeight={18}
          name="private.content"
        />
      </Row>
    )}
  </Fragment>
)

const SubmitButton = ({ handleSubmit }) => (
  <Row justify="flex-end" mb={4} mt={4}>
    <Button
      data-test-id="submit-reviewer-report"
      onClick={handleSubmit}
      primary
      width={48}
    >
      Submit report
    </Button>
  </Row>
)

const SubmitReview = ({
  options,
  hasNote,
  onSubmit,
  onUpload,
  onDelete,
  isFetching,
  toggleNote,
  initialValues,
  reviewerReport,
  autosaveReviewForm,
  ...rest
}) => (
  <Formik initialValues={initialValues} onSubmit={onSubmit} validate={validate}>
    {({ handleSubmit, values, setFieldValue }) => (
      <ContextualBox label="Your Report" mt={4} {...rest}>
        {autosaveReviewForm(values)}
        <Root>
          <Row width={62}>
            <Item vertical>
              <Label mb={1} required>
                Recommendation
              </Label>
              <ValidatedFormField
                component={Menu}
                data-test-id="reviewer-report-menu"
                name="recommendation"
                options={options}
                placeholder="Choose in the list"
              />
            </Item>
          </Row>
          <Fragment>
            <ReportForm
              hasNote={hasNote}
              isFetching={isFetching}
              onDelete={onDelete}
              onUpload={onUpload}
              setFieldValue={setFieldValue}
              toggleNote={toggleNote({ setFieldValue, values })}
              values={values}
            />
            <SubmitButton handleSubmit={handleSubmit} values={values} />
          </Fragment>
        </Root>
      </ContextualBox>
    )}
  </Formik>
)

export default compose(
  withFetching,
  withFilesGQL(),
  withProps(({ reviewerReport }) => ({
    initialValues: {
      public: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'public')
        .mergeWith({ content: '' }, objValue => {
          if (objValue === null) {
            return objValue || ''
          }
          return objValue
        })
        .value(),
      private: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'private')
        .mergeWith({ content: '' }, objValue => {
          if (objValue === null) {
            return objValue || ''
          }
          return objValue
        })
        .value(),
      recommendation: get(reviewerReport, 'recommendation')
        ? get(reviewerReport, 'recommendation')
        : undefined,
      reviewId: get(reviewerReport, 'id'),
      files: chain(reviewerReport)
        .get('comments', [])
        .find(c => c.type === 'public')
        .get('files', [])
        .value(),
    },
  })),
  withState('hasNote', 'setNote', false),
  withModal({
    component: MultiAction,
    modalKey: 'reviewerReport',
  }),
  withHandlers({
    toggleNote: ({ setNote, updateDraftReview }) => ({
      setFieldValue,
      values,
    }) => () => {
      setFieldValue('private.content', '')
      setNote(v => !v)
      values.private.content = ''
      const parsedValues = parseReviewValues(values)
      updateDraftReview({ variables: { ...parsedValues } })
    },
    onSubmit: ({ onSubmit, showModal }) => values => {
      showModal({
        cancelText: 'Not yet',
        confirmText: 'Submit report',
        subtitle: "Once submited, the report can't be modified",
        onConfirm: modalProps => onSubmit(values, modalProps),
        title: 'Ready to submit your report?',
      })
    },
    onUpload: ({
      uploadFile,
      initialValues: {
        public: { id },
      },
      setFetching,
      setError,
    }) => ({ file, setFieldValue }) => {
      const fileInput = {
        type: 'reviewComment',
        size: file.size,
      }
      setFetching(true)
      uploadFile({ entityId: id, fileInput, file })
        .then(file => {
          setFieldValue('files', [file])
          setFetching(false)
        })
        .catch(e => {
          setError(e.message)
          setFetching(false)
        })
    },
    onDelete: ({ deleteFile, setFetching, setError }) => ({
      file,
      setFieldValue,
    }) => {
      setFetching(true)
      deleteFile(file.id)
        .then(() => {
          setFetching(false)
          setFieldValue('files', [])
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(SubmitReview)

const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};

  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: 0;
`
