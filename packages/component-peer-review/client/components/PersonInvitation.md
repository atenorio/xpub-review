Shows the invited person and the possibility to cancel the invite.

```js
const invitation = {
  id: '6dfd6286-3366-41fb-83e4-aed80955e579',
  isSubmitting: null,
  isCorresponding: null,
  status: 'pending',
  alias: {
    aff: 'Boko Haram',
    email: 'alexandru.munteanu+ae1@thinslices.com',
    country: 'RO',
    name: {
      surname: 'MuntAE1',
      givenNames: 'AlexAE1',
    },
  },
}

;<PersonInvitation
  label="Academic Editor"
  invitation={invitation}
  onRevoke={id => console.log('revoke invitation with id', id)}
/>
```
