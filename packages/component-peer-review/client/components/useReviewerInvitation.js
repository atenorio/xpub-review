import { useMutation } from 'react-apollo'
import { parseError } from '../utils'
import { mutations } from '../graphql'
import {
  refetchGetManuscriptVersions,
  refetchLoadReviewerSuggestions,
} from '../graphql/refetchQueries'

const useReviewerInvitation = ({ match, manuscript }) => {
  const [inviteReviewerMutation] = useMutation(mutations.inviteReviewer, {
    refetchQueries: [
      refetchGetManuscriptVersions(match),
      refetchLoadReviewerSuggestions(match),
    ],
  })
  const [cancelReviewerInvitationMutation] = useMutation(
    mutations.cancelReviewerInvitation,
    {
      refetchQueries: [refetchGetManuscriptVersions(match)],
    },
  )

  const inviteReviewer = (input, modalProps) => {
    modalProps.setFetching(true)
    inviteReviewerMutation({
      variables: {
        manuscriptId: manuscript.id,
        input,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
        if (modalProps.formProps) {
          modalProps.formProps.resetForm({})
        }
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(e))
      })
  }

  const cancelReviewerInvitation = (reviewer, modalProps) => {
    modalProps.setFetching(true)
    cancelReviewerInvitationMutation({
      variables: {
        manuscriptId: manuscript.id,
        teamMemberId: reviewer.id,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(e))
      })
  }

  return { inviteReviewer, cancelReviewerInvitation }
}

export default useReviewerInvitation
