import React, { Fragment } from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { DateParser } from '@pubsweet/ui'
import { File } from 'component-files/client'
import { withProps, compose } from 'recompose'
import { Row, Text, Item, Label } from '@hindawi/ui'

const ReviewerReportAuthor = ({
  reviewFile,
  publicReport,
  reviewerNumber,
  report,
}) => (
  <Root>
    <Row alignItems="flex-start" justify="space-between" mb={1}>
      <Item>{publicReport && <Label>Report</Label>}</Item>
      <Item alignItems="baseline" justify="flex-end">
        <Text customId ml={2} mr={2}>{`Reviewer ${reviewerNumber}`}</Text>
        <DateParser timestamp={report.submitted}>
          {date => <Text secondary>{date}</Text>}
        </DateParser>
      </Item>
    </Row>
    {publicReport && (
      <Row justify="flex-start" mb={reviewFile ? 4 : 0}>
        <Text whiteSpace="pre-wrap">{publicReport}</Text>
      </Row>
    )}

    {reviewFile && (
      <Fragment>
        <Label mb={1}>File</Label>
        <Row justify="flex-start" mb={1}>
          <Item flex={0} mr={2}>
            <File item={reviewFile} />
          </Item>
        </Row>
      </Fragment>
    )}
  </Root>
)

export default compose(
  withProps(({ report, recommendations = [] }) => ({
    recommendation: get(
      recommendations.find(r => r.value === report.recommendation),
      'label',
    ),
    reviewFile: get(report, 'comments.0.files.0'),
    publicReport: get(report, 'comments.0.content'),
    reviewerName: `${get(report, 'reviewer.firstName', '')} ${get(
      report,
      'reviewer.lastName',
      '',
    )}`,
    reviewerNumber: get(report, 'member.reviewerNumber', ''),
  })),
)(ReviewerReportAuthor)

ReviewerReportAuthor.propTypes = {
  /** Pass object with informations about the report. */
  report: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Unique id for user. */
    userId: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    createdOn: PropTypes.number,
    /** When the comment was updated. */
    updatedOn: PropTypes.number,
    /** When the comment was submited. */
    submittedOn: PropTypes.number,
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** Type of recommendation. */
    recommendationType: PropTypes.string,
  }),
}

ReviewerReportAuthor.defaultProps = {
  report: {},
}

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue')};
  border: ${th('borderWidth')} ${th('borderStyle')} ${th('colorBackgroundHue3')};
  border-radius: ${th('borderRadius')};
  padding: calc(${th('gridUnit')} * 4);
  margin: calc(${th('gridUnit')} * 2);
`
// #endregion
