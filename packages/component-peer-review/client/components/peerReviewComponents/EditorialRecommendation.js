import React from 'react'
import { get } from 'lodash'
import { Formik } from 'formik'
import PropTypes from 'prop-types'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { withModal } from 'component-modal'
import { compose, withProps, withHandlers } from 'recompose'

import {
  Row,
  Text,
  Menu,
  Item,
  Label,
  Textarea,
  validators,
  MultiAction,
  ContextualBox,
  ValidatedFormField,
} from '@hindawi/ui'

const RecommendationForm = ({
  triageEditorLabel,
  options,
  onVisibility,
  formValues,
  handleSubmit,
  fieldForAuthor,
  fieldAuthorOptional,
  fieldForTriageEditor,
}) => (
  <Root>
    <Row width={62}>
      <Item vertical>
        <Label required>Recommendation</Label>
        <ValidatedFormField
          component={Menu}
          name="recommendation"
          options={options}
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <ResponsiveRow mt={2}>
      {fieldForAuthor && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-author"
          mr={2}
          vertical
        >
          <Label required>Message for Author </Label>
          <ValidatedFormField
            component={Textarea}
            name="public"
            validate={[validators.required]}
          />
        </ResponsiveItem>
      )}

      {fieldAuthorOptional && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-author"
          ml={2}
          vertical
        >
          <Label>
            Message for Author
            <Text ml={2} secondary>
              Optional
            </Text>
          </Label>
          <ValidatedFormField component={Textarea} name="public" />
        </ResponsiveItem>
      )}

      {fieldForTriageEditor && (
        <ResponsiveItem
          data-test-id="editorial-recommendation-message-for-triage-editor"
          ml={2}
          vertical
        >
          <Label>
            Message for {triageEditorLabel}
            <Text ml={2} secondary>
              Optional
            </Text>
          </Label>
          <ValidatedFormField component={Textarea} name="private" />
        </ResponsiveItem>
      )}
    </ResponsiveRow>

    {onVisibility && (
      <Row justify="flex-end" mt={2}>
        <Button
          data-test-id="button-editorial-recommendation-submit"
          mb={4}
          onClick={handleSubmit}
          primary
          width={48}
        >
          {
            options.find(o => o.value === get(formValues, 'recommendation', ''))
              .button
          }
        </Button>
      </Row>
    )}
  </Root>
)

const EnhancedRecommendationForm = compose(
  withProps(({ formValues, options, manuscriptStatus, triageEditorLabel }) => ({
    fieldAuthorOptional: formValues.recommendation === 'publish',
    fieldForTriageEditor: ['publish', 'reject'].includes(
      formValues.recommendation,
    ),
    fieldForAuthor: ['reject', 'major', 'minor'].includes(
      formValues.recommendation,
    ),
    onVisibility: ['reject', 'minor', 'publish', 'major'].includes(
      formValues.recommendation,
    ),
    options: ['academicEditorAssigned'].includes(manuscriptStatus)
      ? options.filter(o => !['publish'].includes(o.value))
      : options.filter(o => o.value),
  })),
)(RecommendationForm)

const EditorialRecommendation = ({
  options,
  manuscriptStatus,
  toggle,
  triageEditorLabel,
  onSubmit,
  hasPeerReview,
  initialValues,
  expanded,
  highlight,
  ...rest
}) => (
  <Formik initialValues={initialValues} onSubmit={onSubmit}>
    {({ handleSubmit, values: formValues }) => (
      <ContextualBox
        expanded={expanded}
        highlight={highlight}
        label="Your Editorial Recommendation"
        mt={4}
        toggle={toggle}
        {...rest}
        data-test-id="contextual-box-editorial-recommendation"
      >
        <EnhancedRecommendationForm
          formValues={formValues}
          handleSubmit={handleSubmit}
          hasPeerReview={hasPeerReview}
          manuscriptStatus={manuscriptStatus}
          options={options}
          triageEditorLabel={triageEditorLabel}
        />
      </ContextualBox>
    )}
  </Formik>
)

export default compose(
  withProps(),
  withModal({
    component: MultiAction,
    modalKey: 'editorialRecommendation',
  }),
  withHandlers({
    onSubmit: ({ onSubmit, showModal, options }) => values => {
      const modalTitle = options.find(
        o => o.value === get(values, 'recommendation', ''),
      ).message

      const confirmMessage = options.find(
        o => o.value === get(values, 'recommendation', ''),
      ).button

      showModal({
        title: `${modalTitle}?`,
        content:
          "This will automatically remove reviewers who haven't submitted a review",
        confirmText:
          confirmMessage === 'Submit Recommendation'
            ? 'Submit'
            : confirmMessage,
        cancelText: 'CLOSE',
        onConfirm: modalProps => onSubmit(values, modalProps),
      })
    },
  }),
)(EditorialRecommendation)

EditorialRecommendation.propTypes = {
  /** Specifies AcademicEditor recommendation options */
  options: PropTypes.arrayOf(PropTypes.object),
  /** Handles the submission of the recommendation */
  highlight: PropTypes.bool,
}

EditorialRecommendation.defaultProps = {
  options: [],
  highlight: false,
}

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  display: flex;
  flex-direction: column;
  padding: calc(${th('gridUnit')} * 4);
  padding-bottom: 0;
`
const ResponsiveRow = styled(Row)`
  @media (max-width: 800px) {
    flex-direction: column;
  }
`
const ResponsiveItem = styled(Item)`
  @media (max-width: 800px) {
    margin-right: 0;
    margin-left: 0;
    width: 100%;
  }
`
// #endregion
