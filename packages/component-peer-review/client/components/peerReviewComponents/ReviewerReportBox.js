import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps } from 'recompose'
import { ContextualBox, Row, Text } from '@hindawi/ui'

import ReviewerReport from './ReviewerReport'

const SubmittedReports = ({ reports }) => (
  <Row fitContent justify="flex-end">
    <Text customId mr={2}>
      {reports}
    </Text>
    <Text mr={2} pr={2} secondary>
      submitted
    </Text>
  </Row>
)

const ReviewerReportBox = ({
  options,
  startExpanded = false,
  reports,
  isLatestVersion,
}) => (
  <ContextualBox
    label={
      isLatestVersion && reports.length === 1
        ? 'Your Report'
        : 'Reviewer Reports'
    }
    mt={4}
    reports={reports.length}
    rightChildren={SubmittedReports}
    startExpanded={startExpanded}
  >
    <Wrapper>
      {reports.map(report => (
        <ReviewerReport
          key={report.id}
          options={options}
          reviewerReport={report}
        />
      ))}
    </Wrapper>
  </ContextualBox>
)

export default compose(
  withProps(({ reviewerReports }) => ({
    reports: reviewerReports.filter(review => review.submitted),
  })),
)(ReviewerReportBox)

ReviewerReportBox.propTypes = {
  reviewerReport: PropTypes.shape({
    /** Unique id for report. */
    id: PropTypes.string,
    /** Comments by reviewers. */
    comments: PropTypes.arrayOf(PropTypes.object),
    /** When the comment was created. */
    created: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** When the comment was submited. */
    submitted: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    /** The recommendation given by reviewer. */
    recommendation: PropTypes.string,
    /** The number of reviewer. */
    reviewerNumber: PropTypes.number,
    /** Details about reviewer. */
    member: PropTypes.object,
  }),
}

ReviewerReportBox.defaultProps = {
  reviewerReport: {},
}

const Wrapper = styled.div`
  background-color: ${th('colorBackgroundHue2')};
`
