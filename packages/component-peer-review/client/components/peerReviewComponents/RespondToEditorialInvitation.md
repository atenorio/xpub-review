Respond to an editorial invitation as a Academic Editor.

```js
<RespondToEditorialInvitation
  startExpanded
  isVisible
  highlight
  isVisible
  onSubmit={(values, modalProps) => {
    console.log('the values', values)
    modalProps.hideModal()
  }}
/>
```
