import React, { Fragment, useState } from 'react'
import { get } from 'lodash'
import { space } from 'styled-system'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { Button, TextField } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import {
  Row,
  Item,
  Text,
  Icon,
  Label,
  MultiAction,
  ContextualBox,
} from '@hindawi/ui'
import { useReassignTriageEditor } from '..'

const ReassignTriageEditor = ({
  role,
  match,
  toggle,
  expanded,
  manuscript,
  triageEditors,
  triageEditorLabel,
}) => {
  const [searchValue, setSearchValue] = useState('')
  const clearSearch = () => setSearchValue('')

  const { reassignTriageEditor } = useReassignTriageEditor({
    role,
    match,
    manuscript,
  })

  const onReassign = ({
    name,
    email,
    searchIndex,
    ...triageEditor
  }) => modalProps => reassignTriageEditor(triageEditor, modalProps)

  const filteredTriageEditors = triageEditors
    .map(triageEditor => {
      const givenNames = get(triageEditor, 'alias.name.givenNames', '')
      const surname = get(triageEditor, 'alias.name.surname', '')
      const email = get(triageEditor, 'alias.email', '')
      return {
        ...triageEditor,
        email,
        name: `${givenNames} ${surname}`,
        searchIndex: `${givenNames} ${surname} ${email}`,
      }
    })
    .filter(triageEditor =>
      triageEditor.searchIndex.toLowerCase().includes(searchValue),
    )
  const assignedEditorId = get(manuscript, 'triageEditor.user.id', '')

  return (
    <ContextualBox
      expanded={expanded}
      height={8}
      highlight={expanded}
      label={`Reassign ${triageEditorLabel}`}
      mt={4}
      toggle={toggle}
    >
      <Root>
        <TextContainer>
          <TextField
            data-test-id="manuscript-reassign-triage-editor-filter"
            inline
            onChange={e => {
              setSearchValue(e.target.value.toLowerCase())
            }}
            placeholder="Filter by name or email"
            value={searchValue}
          />
          {searchValue !== '' && (
            <Icon
              data-test-id="clear-filter"
              icon="remove"
              onClick={clearSearch}
              right={10}
              top={10}
            />
          )}
        </TextContainer>

        <Fragment>
          <Row height={6} pl={4}>
            <Item alignItems="center" flex={1}>
              <Label>Full name</Label>
            </Item>
            <Item alignItems="center" flex={2}>
              <Label>Email</Label>
            </Item>
            <Item alignItems="center" flex={1} />
          </Row>

          {filteredTriageEditors.map((triageEditor, index) => (
            <CustomRow
              data-test-id={`manuscript-reassign-triage-editor-invite-${triageEditor.id}`}
              height={8}
              isFirst={index === 0}
              isLast={index === filteredTriageEditors.length - 1}
              key={triageEditor.id}
              pl={4}
            >
              {assignedEditorId === triageEditor.user.id ? (
                <Fragment>
                  <WithEllipsis>
                    <Text ellipsis secondary title={triageEditor.name}>
                      {triageEditor.name}
                    </Text>
                  </WithEllipsis>
                  <Item flex={2}>
                    <Text secondary>{triageEditor.email}</Text>
                  </Item>
                  <Item flex={1} justify="flex-end">
                    <CustomAssignedButton disabled mr={6} xs>
                      Assigned
                    </CustomAssignedButton>
                  </Item>
                </Fragment>
              ) : (
                <Fragment>
                  <WithEllipsis>
                    <Text ellipsis labelline title={triageEditor.name}>
                      {triageEditor.name}
                    </Text>
                  </WithEllipsis>
                  <Item flex={2}>
                    <Text labelline>{triageEditor.email}</Text>
                  </Item>

                  <Item flex={1} justify="flex-end">
                    <Modal
                      cancelText="CANCEL"
                      component={MultiAction}
                      confirmText="Assign"
                      modalKey={`${triageEditor.id}-reassignTriageEditor`}
                      onConfirm={onReassign(triageEditor)}
                      subtitle={`Are you sure you want to assign as ${triageEditorLabel}?`}
                      title={triageEditor.name}
                    >
                      {showModal => (
                        <CustomButton mr={6} onClick={showModal} xs>
                          Assign
                        </CustomButton>
                      )}
                    </Modal>
                  </Item>
                </Fragment>
              )}
            </CustomRow>
          ))}
        </Fragment>
      </Root>
    </ContextualBox>
  )
}

export default ReassignTriageEditor

// #region styles
const Root = styled.div`
  background-color: ${th('colorBackgroundHue2')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;

  ${space};
`
const WithEllipsis = styled(Item)`
  display: inline-grid;
`
const TextContainer = styled.div`
  margin: calc(${th('gridUnit')} * 2);
  margin-bottom: calc(${th('gridUnit')} * 4);
  position: relative;
  width: calc(${th('gridUnit')} * 80);
`

const CustomAssignedButton = styled(Button)`
  pointer-events: none;
  :disabled {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomButton = styled(Button)`
  height: calc(${th('gridUnit')} * 6);
  opacity: 0;
  line-height: calc(${th('gridUnit')} * 4);
  background-color: #000;
  border-color: #000;
  color: ${th('white')};

  &:hover {
    background-color: ${th('actionSecondaryColor')};
    border-color: ${th('actionSecondaryColor')};
    color: ${th('backgroundColor')};
  }
`

const CustomRow = styled(Row)`
  border-top: ${props => props.isFirst && th('box.border')};
  border-bottom: ${props => !props.isLast && th('box.border')};

  &:hover {
    background-color: #eee;
    border-bottom-left-radius: ${props => props.isLast && '3px'};
    border-bottom-right-radius: ${props => props.isLast && '3px'};

    ${CustomButton} {
      opacity: 1;
    }
  }
`
// #endregion
