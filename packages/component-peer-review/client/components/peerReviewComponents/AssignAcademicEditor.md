A list of academic editors.

```js
const academicEditors = [
  {
    id: '992eb580-63d7-4e10-bccf-55255f8fa450',
    isSubmitting: true,
    isCorresponding: true,
    alias: {
      aff: 'Boko Haram',
      email: 'alexandru.munteanu+a1@thinslices.com',
      country: 'RO',
      name: {
        givenNames: 'Alex',
        surname: 'Author',
        title: null,
      },
    },
  },
  {
    id: '992eb580-63d7-4e10-bccf-55255f8fa451',
    isSubmitting: true,
    isCorresponding: true,
    alias: {
      aff: 'PSG',
      email: 'nigerian.prince@thinslices.com',
      country: 'RO',
      name: {
        givenNames: 'Fernand Raul',
        surname: 'Gonzles',
        title: null,
      },
    },
  },
  {
    id: '992eb580-63d7-4e10-bccf-55255f8fa452',
    isSubmitting: true,
    isCorresponding: true,
    alias: {
      aff: 'Manchester United',
      email: 'paul.pogba@thinslices.com',
      country: 'RO',
      name: {
        givenNames: 'Paul',
        surname: 'Pogba',
        title: null,
      },
    },
  },
];

<AssignAcademicEditor
  academicEditors={academicEditors}
  onInvite={(academicEditor, modalProps) => {
    console.log('the args', academicEditor, modalProps)
  }}
/>
```
