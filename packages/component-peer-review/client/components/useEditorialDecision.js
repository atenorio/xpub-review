import { useMutation } from 'react-apollo'
import { mutations } from '../graphql'
import {
  refetchGetAuditLogEvents,
  refetchGetManuscriptVersions,
} from '../graphql/refetchQueries'

const useEditorialDecision = ({ manuscriptId, role, match }) => {
  const [makeDecisionToPublish] = useMutation(mutations.makeDecisionToPublish, {
    refetchQueries:
      role === 'admin'
        ? [refetchGetManuscriptVersions(match), refetchGetAuditLogEvents(match)]
        : [refetchGetManuscriptVersions(match)],
  })
  const [makeDecisionToReject] = useMutation(mutations.makeDecisionToReject, {
    refetchQueries:
      role === 'admin'
        ? [refetchGetManuscriptVersions(match), refetchGetAuditLogEvents(match)]
        : [refetchGetManuscriptVersions(match)],
  })
  const [makeDecisionToReturn] = useMutation(mutations.makeDecisionToReturn, {
    refetchQueries:
      role === 'admin'
        ? [refetchGetManuscriptVersions(match), refetchGetAuditLogEvents(match)]
        : [refetchGetManuscriptVersions(match)],
  })
  const [requestRevision] = useMutation(mutations.requestRevision, {
    refetchQueries:
      role === 'admin'
        ? [refetchGetManuscriptVersions(match), refetchGetAuditLogEvents(match)]
        : [refetchGetManuscriptVersions(match)],
  })

  const handlePublish = modalProps => {
    modalProps.setFetching(true)

    return makeDecisionToPublish({
      variables: {
        manuscriptId,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const handleReject = (content, modalProps) => {
    modalProps.setFetching(true)

    return makeDecisionToReject({
      variables: {
        manuscriptId,
        content,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const handleReturn = (content, modalProps) => {
    modalProps.setFetching(true)

    return makeDecisionToReturn({
      variables: {
        manuscriptId,
        content,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const handleRevision = (content, modalProps, formikBag) => {
    modalProps.setFetching(true)

    return requestRevision({
      variables: {
        manuscriptId,
        content,
        type: 'revision', // TODO: this shouldn't be hardcoded here
      },
    })
      .then(() => {
        formikBag.resetForm({ decision: '', message: '' })
        modalProps.setFetching(false)
        modalProps.hideModal()
        // collapseEicDecision()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const handleEditorialDecision = (
    { decision, message },
    modalProps,
    formikBag,
  ) => {
    switch (decision) {
      case 'publish':
        return handlePublish(modalProps)
      case 'reject':
        return handleReject(message, modalProps)
      case 'return-to-academic-editor':
        return handleReturn(message, modalProps)
      case 'revision':
        return handleRevision(message, modalProps, formikBag)
      default:
        break
    }
  }

  return { handleEditorialDecision }
}

export default useEditorialDecision
