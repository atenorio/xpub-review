import { get, chain } from 'lodash'
import moment from 'moment'
import { useQuery } from 'react-apollo'
import { getManuscriptVersions } from '../graphql/queries'

const useManuscriptVersionData = ({ match }) => {
  const { data, loading, error } = useQuery(getManuscriptVersions, {
    variables: {
      submissionId: get(match, 'params.submissionId', ''),
    },
  })

  const versions = chain(data)
    .get('getManuscriptVersions', [])
    .map(({ version, id }) => ({ version, id }))
    .value()

  const manuscript = chain(data)
    .get('getManuscriptVersions', [])
    .find(m => m.id === get(match, 'params.manuscriptId'))
    .value()

  const pendingAcademicEditor = get(manuscript, 'pendingAcademicEditor')

  const academicEditor =
    get(manuscript, 'academicEditor') || pendingAcademicEditor

  const role = get(manuscript, 'role')

  const reviews = get(manuscript, 'reviews') || []
  const reviewers = chain(manuscript)
    .get('reviewers')
    .toArray()
    .filter(reviewer => get(reviewer, 'status') !== 'expired')
    .map(reviewer => ({
      ...reviewer,
      review: reviews.find(
        review => review.member.user.id === reviewer.user.id,
      ),
    }))
    .value()
  const isLatestVersion =
    chain(data)
      .get('getManuscriptVersions', [])
      .maxBy('version')
      .get('id')
      .value() === get(match, 'params.manuscriptId')
  const submittingAuthor = get(manuscript, 'authors', []).find(a =>
    get(a, 'isSubmitting'),
  )

  const authorResponse = get(manuscript, 'reviews', []).find(
    r => get(r, 'recommendation') === 'responseToRevision',
  )
  const editorialReviews = reviews
    .filter(r =>
      [
        'admin',
        'triageEditor',
        'academicEditor',
        'editorialAssistant',
        'researchIntegrityPublishingEditor',
      ].includes(get(r, 'member.role')),
    )
    .map(r => ({
      ...r,
      submittedTimestamp: moment(get(r, 'submitted', '')).valueOf(),
    }))
    .sort((a, b) => b.submittedTimestamp - a.submittedTimestamp)

  const reviewerReports = reviews.filter(
    review => review.member.role === 'reviewer',
  )

  const revisionDraft = get(data, 'getDraftRevision')

  const peerReviewModel = get(manuscript, 'specialIssue')
    ? get(manuscript, 'specialIssue.peerReviewModel')
    : get(manuscript, 'journal.peerReviewModel')
  return {
    role,
    error,
    reviews,
    loading,
    versions,
    reviewers,
    manuscript,
    revisionDraft,
    academicEditor,
    authorResponse,
    peerReviewModel,
    reviewerReports,
    isLatestVersion,
    submittingAuthor,
    editorialReviews,
    pendingAcademicEditor,
  }
}

export default useManuscriptVersionData
