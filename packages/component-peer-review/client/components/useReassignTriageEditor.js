import { useMutation } from 'react-apollo'
import { mutations } from '../graphql/'
import {
  refetchGetAuditLogEvents,
  refetchGetManuscriptVersions,
  refetchGetTriageEditors,
} from '../graphql/refetchQueries'

const useReassignTriageEditor = ({ role, match, manuscript }) => {
  const [reassignTriageEditorMutation] = useMutation(
    mutations.reassignTriageEditor,
    {
      refetchQueries:
        role === 'admin'
          ? [
              refetchGetManuscriptVersions(match),
              refetchGetAuditLogEvents(match),
              refetchGetTriageEditors(match),
            ]
          : [
              refetchGetManuscriptVersions(match),
              refetchGetTriageEditors(match),
            ],
    },
  )

  const reassignTriageEditor = (triageEditor, modalProps) => {
    modalProps.setFetching(true)
    reassignTriageEditorMutation({
      variables: {
        manuscriptId: manuscript.id,
        teamMemberId: triageEditor.id,
      },
    })
      .then(() => {
        modalProps.setFetching(true)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  return {
    reassignTriageEditor,
  }
}

export default useReassignTriageEditor
