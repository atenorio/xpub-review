import { useMutation } from 'react-apollo'
import { chain, get, isEmpty } from 'lodash'
import { mutations } from '../graphql'
import { parseError, autosaveReviewForm as autosave } from '../utils'
import { refetchGetManuscriptVersions } from '../graphql/refetchQueries'

const useSubmitReview = ({ reviewerReports, userId, match }) => {
  const [updateDraftReview] = useMutation(mutations.updateDraftReview)
  const [updateAutosaveReview] = useMutation(mutations.updateAutosaveReview)
  const [submitReviewMutation] = useMutation(mutations.submitReview, {
    refetchQueries: [refetchGetManuscriptVersions(match)],
  })

  const autosaveReviewForm = values => {
    autosave({
      values,
      updateDraftReview,
      updateAutosaveReview,
    })
  }

  const submitReview = (values, modalProps) => {
    modalProps.setFetching(true)
    submitReviewMutation({
      variables: {
        reviewId: values.reviewId,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(e.message))
      })
  }

  const currentReviewerReport = chain(reviewerReports)
    .filter(review => get(review, 'member.user.id') === userId)
    .map(review => !isEmpty(review) && { ...review, currentUserReview: true })
    .first()
    .value()

  return {
    submitReview,
    updateDraftReview,
    autosaveReviewForm,
    currentReviewerReport,
  }
}

export default useSubmitReview
