import React from 'react'
import { Button } from '@pubsweet/ui'
import { Row } from '@hindawi/ui'

import PersonInvitation from './PersonInvitation'

const AcademicEditorInvitation = ({
  toggle,
  academicEditor,
  cancelInvitation,
}) => (
  <Row justify="flex-start">
    <PersonInvitation
      invitation={academicEditor}
      label="Academic Editor"
      onRevoke={cancelInvitation}
    />
    {!academicEditor.id && (
      <Button ml={2} onClick={toggle} primary size="small">
        INVITE
      </Button>
    )}
  </Row>
)

export default AcademicEditorInvitation
