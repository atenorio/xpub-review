Manuscript version component

```js
const history = {
  push: v => console.log('go to version -> ', v),
}

const fragment = {}
const collection = {
  fragments: ['1', '2'],
}
const versions = [
  { label: 'Version 1', value: '10d28459-6f8e-4f6c-a57e-65979e5f8d2' },
  { label: 'Version 2', value: '10d28459-6f8e-4f6c-a57e-65979e5f854' },
]
;<ManuscriptVersion
  collection={collection}
  history={history}
  fragment={fragment}
  versions={versions}
/>
```
