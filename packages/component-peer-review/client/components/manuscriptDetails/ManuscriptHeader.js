import React, { Fragment } from 'react'
import { get } from 'lodash'
import { useJournal } from 'component-journal-info'
import { H2, H4, Button, DateParser } from '@pubsweet/ui'
import { Row, Text, Label, StatusTag, AuthorTagList, Icon } from '@hindawi/ui'
import { useMutation } from 'react-apollo'
import { mutations } from '../../graphql'
import {
  refetchGetManuscripts,
  refetchGetManuscriptVersions,
} from '../../graphql/refetchQueries'
import PersonInvitation from '../PersonInvitation'

const AcademicEditorAssignator = ({
  role,
  academicEditorLabel,
  hasPeerReview,
  canSeeInvite,
  canSeeRevoke,
  assignedAcademicEditor,
  academicEditors,
  isLatestVersion,
  isApprovalEditor,
  manuscriptStatus,
  academicEditor,
  removeAcademicEditor,
  toggleAssignAcademicEditor,
  onCancelAcademicEditorInvitation,
  researchIntegrityPublishingEditor,
}) => {
  const articleTypeCheck =
    !hasPeerReview && ['inQA', 'accepted'].includes(manuscriptStatus)
  return (
    (canSeeInvite || canSeeRevoke) &&
    (!researchIntegrityPublishingEditor && (
      <Fragment>
        <PersonInvitation
          academicEditors={academicEditors}
          hasPeerReview={hasPeerReview}
          invitation={assignedAcademicEditor}
          isApprovalEditor={isApprovalEditor}
          label={academicEditorLabel}
          manuscriptStatus={manuscriptStatus}
          onRevoke={onCancelAcademicEditorInvitation}
          role={role}
          toggleAssignAcademicEditor={toggleAssignAcademicEditor}
        />
        {!get(assignedAcademicEditor, 'id', null) &&
          isLatestVersion &&
          (!articleTypeCheck ? (
            <Button
              ml={2}
              onClick={toggleAssignAcademicEditor}
              primary
              width={12}
              xs
            >
              INVITE
            </Button>
          ) : (
            <Text>Unassigned</Text>
          ))}
      </Fragment>
    ))
  )
}
const TriageEditorAssignator = ({ toggleReassignTriageEditor, role }) =>
  ['admin', 'editorialAssistant'].includes(role) && (
    <Fragment>
      <Button mr={4} onClick={toggleReassignTriageEditor} primary width={12} xs>
        <Icon icon="reassign" mr={1} />
        Reassign
      </Button>
    </Fragment>
  )

const AcademicEditorLabel = ({
  academicEditorLabel,
  academicEditorVisibleStatus,
  canSeeLabelAuthorReviewer,
  canSeeLabelOldVersionsAdminOrTriageEditor,
  canSeeLabelUnassigned,
}) =>
  (canSeeLabelAuthorReviewer ||
    canSeeLabelOldVersionsAdminOrTriageEditor ||
    canSeeLabelUnassigned) && (
    <Fragment>
      <Label mr={2}>{academicEditorLabel}</Label>
      <Text>{academicEditorVisibleStatus}</Text>
    </Fragment>
  )

const RIPELabel = ({ researchIntegrityPublishingEditorName }) => (
  <Fragment>
    <Label mr={2}>Research Integrity Publishing Editor</Label>
    <Text>{researchIntegrityPublishingEditorName}</Text>
  </Fragment>
)

const ManuscriptHeader = ({
  match,
  manuscript,
  academicEditors,
  triageEditors,
  isLatestVersion,
  isApprovalEditor,
  triageEditorLabel,
  academicEditorLabel,
  toggleReassignTriageEditor,
  toggleAssignAcademicEditor,
}) => {
  const { statuses } = useJournal()

  const [cancelAcademicEditorInvitation] = useMutation(
    mutations.cancelAcademicEditorInvitation,
    {
      refetchQueries: [
        refetchGetManuscriptVersions(match),
        refetchGetManuscripts(),
      ],
    },
  )

  const handleCancelAcademicEditorInvitation = (invitation, modalProps) => {
    modalProps.setFetching(true)
    cancelAcademicEditorInvitation({
      variables: {
        teamMemberId: invitation.id,
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(e => {
        modalProps.setFetching(false)
        modalProps.setError(e.message)
      })
  }

  const statusColor = get(statuses, `${manuscript.status}.color`)
  const articleType = get(manuscript, 'articleType.name', '')
  const role = get(manuscript, 'role')
  const journalTitle = get(manuscript.journal, 'name', '')
  const manuscriptStatus = get(manuscript, 'status', '')
  const visibleStatus = get(manuscript, 'visibleStatus', '')
  const hasPeerReview = get(manuscript, 'articleType.hasPeerReview', '')
  const assignedAcademicEditor = get(manuscript, 'academicEditor', {})
  const academicEditorStatus = get(manuscript, 'academicEditor.status', '')
  const triageEditor = get(manuscript, 'triageEditor')

  const triageEditorName = `${get(
    triageEditor,
    'alias.name.givenNames',
    '',
  )} ${get(triageEditor, 'alias.name.surname', 'Unassigned')}`
  const researchIntegrityPublishingEditor = get(
    manuscript,
    'researchIntegrityPublishingEditor',
    {},
  )
  const researchIntegrityPublishingEditorName = `${get(
    manuscript,
    'researchIntegrityPublishingEditor.alias.name.givenNames',
  )} ${get(manuscript, 'researchIntegrityPublishingEditor.alias.name.surname')}`

  const {
    canSeeLabelAuthorReviewer,
    canSeeLabelOldVersionsAdminOrTriageEditor,
    canSeeLabelUnassigned,
    canSeeInvite,
    canSeeRevoke,
  } = getLabelVisibility({
    role,
    academicEditorStatus,
    isLatestVersion,
    manuscriptStatus,
  })

  const { academicEditorVisibleStatus } = getAcademicEditorStatus({
    role,
    assignedAcademicEditor,
  })

  return (
    <Fragment>
      <Row
        alignItems="center"
        data-test-id="manuscript-title"
        height={10}
        justify="space-between"
      >
        <H2>{manuscript.meta.title || 'No title'}</H2>
        <StatusTag
          hasPeerReview={hasPeerReview}
          old={!isLatestVersion}
          statusColor={statusColor}
          version={manuscript.version}
        >
          {isLatestVersion || manuscript.status === 'withdrawn'
            ? `${visibleStatus}`
            : 'Viewing An Older Version'}
        </StatusTag>
      </Row>

      {manuscript.authors.length > 0 && (
        <Row
          alignItems="center"
          data-test-id="authors-row"
          justify="flex-start"
          mt={2}
        >
          <AuthorTagList
            authors={manuscript.authors}
            withAffiliations
            withTooltip
          />
        </Row>
      )}
      <Row alignItems="center" justify="flex-start" mt={2}>
        {manuscript.customId && (
          <Text
            customId
            data-test-id="manuscript-id"
            mr={2}
          >{`ID ${manuscript.customId}`}</Text>
        )}
        {manuscript.created && (
          <DateParser durationThreshold={0} timestamp={manuscript.created}>
            {timestamp => <Text mr={6}>Submitted on {timestamp}</Text>}
          </DateParser>
        )}
        <Text>{articleType}</Text>
        <Text journal ml={2}>
          {journalTitle}
        </Text>
      </Row>

      <Row alignItems="center" justify="flex-start" mt={2}>
        {triageEditor && (
          <>
            <H4>{triageEditorLabel}</H4>
            <Text ml={2} mr={2}>
              {triageEditorName}
            </Text>
            {isLatestVersion && triageEditors.length > 1 && (
              <TriageEditorAssignator
                role={role}
                toggleReassignTriageEditor={toggleReassignTriageEditor}
              />
            )}
          </>
        )}
        {researchIntegrityPublishingEditor ? (
          <RIPELabel
            researchIntegrityPublishingEditorName={
              researchIntegrityPublishingEditorName
            }
          />
        ) : (
          <AcademicEditorLabel
            academicEditorLabel={academicEditorLabel}
            academicEditorVisibleStatus={academicEditorVisibleStatus}
            canSeeLabelAuthorReviewer={canSeeLabelAuthorReviewer}
            canSeeLabelOldVersionsAdminOrTriageEditor={
              canSeeLabelOldVersionsAdminOrTriageEditor
            }
            canSeeLabelUnassigned={canSeeLabelUnassigned}
          />
        )}

        <AcademicEditorAssignator
          academicEditorLabel={academicEditorLabel}
          academicEditors={academicEditors}
          articleType={articleType}
          assignedAcademicEditor={assignedAcademicEditor}
          canSeeInvite={canSeeInvite}
          canSeeRevoke={canSeeRevoke}
          hasPeerReview={hasPeerReview}
          isApprovalEditor={isApprovalEditor}
          isLatestVersion={isLatestVersion}
          manuscriptStatus={manuscriptStatus}
          onCancelAcademicEditorInvitation={
            handleCancelAcademicEditorInvitation
          }
          researchIntegrityPublishingEditor={researchIntegrityPublishingEditor}
          role={role}
          toggleAssignAcademicEditor={toggleAssignAcademicEditor}
          visibleStatus={visibleStatus}
        />
      </Row>
    </Fragment>
  )
}

export default ManuscriptHeader

const getAcademicEditorStatus = ({ role, assignedAcademicEditor }) => {
  const academicEditorStatus = get(assignedAcademicEditor, 'status', '')
  let academicEditorVisibleStatus = 'Unassigned'
  if (academicEditorStatus === 'accepted') {
    academicEditorVisibleStatus = `${get(
      assignedAcademicEditor,
      'alias.name.surname',
      '',
    )} ${get(assignedAcademicEditor, 'alias.name.givenNames', '')}`
  } else if (role === 'author' && academicEditorStatus === 'pending') {
    academicEditorVisibleStatus = 'Assigned'
  } else if (academicEditorStatus === 'pending') {
    academicEditorVisibleStatus = 'Invited'
  }
  return {
    academicEditorVisibleStatus,
  }
}

const getLabelVisibility = ({
  role,
  academicEditorStatus,
  isLatestVersion,
  manuscriptStatus,
}) => ({
  canSeeLabelAuthorReviewer:
    ['pending', 'accepted', ''].includes(academicEditorStatus) &&
    !['admin', 'triageEditor', 'editorialAssistant'].includes(role),

  canSeeLabelOldVersionsAdminOrTriageEditor:
    ['accepted', '', 'pending'].includes(academicEditorStatus) &&
    !isLatestVersion &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role),

  canSeeLabelUnassigned:
    academicEditorStatus === '' &&
    ['admin', 'triageEditor'].includes(role) &&
    ['technicalChecks', 'revisionRequested'].includes(manuscriptStatus),

  canSeeInvite:
    isLatestVersion &&
    academicEditorStatus === '' &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role) &&
    ![
      'technicalChecks',
      'revisionRequested',
      'rejected',
      'makeDecision',
      'withdrawn',
      'refusedToConsider',
    ].includes(manuscriptStatus),

  canSeeRevoke:
    isLatestVersion &&
    academicEditorStatus === 'accepted' &&
    ['admin', 'triageEditor', 'editorialAssistant'].includes(role) &&
    !['technicalChecks', 'rejected', 'accepted'].includes(manuscriptStatus),
})
