import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { File } from 'component-files/client'
import { Item, Row, Text } from '@hindawi/ui'

const ManuscriptFileSection = ({ list, label }) => (
  <Fragment>
    {!!list.length && (
      <Fragment>
        {label && (
          <Text fontWeight="bold" labelLine mb={2} mt={2} smallSize>
            {label}
          </Text>
        )}
        <Row flexWrap="wrap" justify="flex-start" mb={2}>
          {list.map(file => (
            <Item
              alignItems="flex-start"
              flex={0}
              key={file.id}
              mr={2}
              vertical
            >
              <File item={file} mb={2} />
            </Item>
          ))}
        </Row>
      </Fragment>
    )}
  </Fragment>
)

ManuscriptFileSection.propTypes = {
  /** List of uploaded files */
  list: PropTypes.arrayOf(PropTypes.object),
  /** Category name of uploaded files. */
  label: PropTypes.string,
}

ManuscriptFileSection.defaultProps = {
  list: [],
  label: '',
}

export default ManuscriptFileSection
