import React, { Fragment } from 'react'
import { compose, withProps } from 'recompose'

import { ManuscriptFileSection } from '../'

const ManuscriptFileList = ({
  files = [],
  figure,
  coverLetter,
  manuscripts,
  supplementary,
}) => (
  <Fragment>
    <ManuscriptFileSection label="MAIN MANUSCRIPT" list={manuscripts} />

    <ManuscriptFileSection label="COVER LETTER" list={coverLetter} />

    <ManuscriptFileSection label="SUPPLEMENTAL FILES" list={supplementary} />

    <ManuscriptFileSection label="FIGURE FILES" list={figure} />
  </Fragment>
)

export default compose(
  withProps(({ files }) => ({
    figure: files.filter(f => f.type === 'figure'),
    manuscripts: files.filter(f => f.type === 'manuscript'),
    coverLetter: files.filter(f => f.type === 'coverLetter'),
    supplementary: files.filter(f => f.type === 'supplementary'),
  })),
)(ManuscriptFileList)
