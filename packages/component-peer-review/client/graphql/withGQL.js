import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'

export default compose(
  graphql(mutations.getFileSignedUrl, {
    name: 'getFileSignedUrl',
  }),
)

export { default as withAuthorGQL } from './withAuthorGQL'
export { default as withReviewerGQL } from './withReviewerGQL'
export { default as withAcademicEditorGQL } from './withAcademicEditorGQL'
