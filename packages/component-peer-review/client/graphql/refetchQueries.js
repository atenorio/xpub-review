import { queries as activityLogQueries } from 'component-activity-log/client'
import { queries as dashboardQueries } from 'component-dashboard/client'
import * as queries from './queries'

export const refetchGetManuscriptVersions = ({
  params: { submissionId = '' },
}) => ({
  query: queries.getManuscriptVersions,
  variables: { submissionId },
})

export const refetchGetAuditLogEvents = ({
  params: { manuscriptId = '' },
}) => ({
  query: activityLogQueries.getAuditLogEvents,
  variables: { manuscriptId },
})

export const refetchLoadReviewerSuggestions = ({
  params: { manuscriptId = '' },
}) => ({
  query: queries.loadReviewerSuggestions,
  variables: { manuscriptId },
})
export const refetchGetTriageEditors = ({ params: { manuscriptId = '' } }) => ({
  query: queries.getTriageEditors,
  variables: { manuscriptId },
})

export const refetchGetManuscripts = () => ({
  query: dashboardQueries.getManuscripts,
  variables: {},
})
