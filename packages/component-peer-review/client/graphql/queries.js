import gql from 'graphql-tag'

import {
  manuscriptFragment,
  userFragment,
  fileFragment,
  teamMember,
} from './fragments'

export const getManuscript = gql`
  query getManuscript($manuscriptId: String!) {
    getManuscript(manuscriptId: $manuscriptId) {
      ...manuscriptDetails
    }
  }
  ${manuscriptFragment}
`

export const getManuscriptVersions = gql`
  query getManuscriptVersions($submissionId: String!) {
    getManuscriptVersions(submissionId: $submissionId) {
      ...manuscriptDetails
    }
    getDraftRevision(submissionId: $submissionId) {
      ...manuscriptDetails
      comment {
        id
        type
        content
        files {
          ...manuscriptDetailsFile
        }
      }
    }
  }
  ${manuscriptFragment}
  ${fileFragment}
`

export const getCurrentUser = gql`
  query {
    getCurrentUser {
      ...manuscriptDetailsUser
    }
  }
  ${userFragment}
`

export const getAcademicEditors = gql`
  query getAcademicEditors($manuscriptId: String!) {
    getAcademicEditors(manuscriptId: $manuscriptId) {
      ...teamMember
    }
  }
  ${teamMember}
`
export const getTriageEditors = gql`
  query getTriageEditors($manuscriptId: String!) {
    getTriageEditors(manuscriptId: $manuscriptId) {
      ...teamMember
    }
  }
  ${teamMember}
`
export const loadReviewerSuggestions = gql`
  query loadReviewerSuggestions($manuscriptId: String!) {
    loadReviewerSuggestions(manuscriptId: $manuscriptId) {
      id
      email
      givenNames
      surname
      aff
      profileUrl
      numberOfReviews
      isInvited
    }
  }
`
