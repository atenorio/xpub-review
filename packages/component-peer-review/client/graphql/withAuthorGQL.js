import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import { mutations as submissionMutations } from 'component-submission/client'
import * as mutations from './mutations'
import {
  refetchGetManuscripts,
  refetchGetManuscriptVersions,
} from './refetchQueries'

export default compose(
  graphql(mutations.updateAutosave, {
    name: 'updateAutosave',
  }),
  graphql(submissionMutations.removeAuthorFromManuscript, {
    name: 'removeAuthor',
  }),
  graphql(submissionMutations.editAuthorFromManuscript, {
    name: 'editAuthor',
  }),
  graphql(submissionMutations.addAuthorToManuscript, {
    name: 'addAuthorToManuscript',
  }),
  graphql(submissionMutations.updateManuscriptFile, {
    name: 'updateManuscriptFile',
  }),
  graphql(mutations.updateDraftRevision, {
    name: 'updateDraftRevision',
  }),
  graphql(mutations.submitRevision, {
    name: 'submitRevision',
    options: ({ match }) => ({
      refetchQueries: [
        refetchGetManuscripts(),
        refetchGetManuscriptVersions(match),
      ],
    }),
  }),
)
