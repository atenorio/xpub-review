import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import {
  refetchGetManuscripts,
  refetchGetManuscriptVersions,
} from '../graphql/refetchQueries'

export default compose(
  graphql(mutations.acceptReviewerInvitation, {
    name: 'acceptReviewerInvitation',
    options: ({ match }) => ({
      refetchQueries: [refetchGetManuscriptVersions(match)],
    }),
  }),
  graphql(mutations.declineReviewerInvitation, {
    name: 'declineReviewerInvitation',
    options: ({ match, currentUser, data }) => {
      if (!currentUser) return {}
      return data.getManuscriptVersions && data.getManuscriptVersions.length > 1
        ? [refetchGetManuscripts(), refetchGetManuscriptVersions(match)]
        : [refetchGetManuscripts()]
    },
  }),
  graphql(mutations.updateDraftReview, {
    name: 'updateDraftReview',
  }),
  graphql(mutations.updateAutosaveReview, {
    name: 'updateAutosaveReview',
  }),
  graphql(mutations.submitReview, {
    name: 'submitReview',
    options: {
      refetchQueries: [refetchGetManuscripts()],
    },
  }),
)
