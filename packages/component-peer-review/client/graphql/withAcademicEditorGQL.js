import { graphql } from 'react-apollo'
import { compose } from 'recompose'

import * as mutations from './mutations'
import {
  refetchGetManuscripts,
  refetchGetManuscriptVersions,
  refetchLoadReviewerSuggestions,
} from './refetchQueries'

const withAcademicEditorGQL = compose(
  graphql(mutations.acceptAcademicEditorInvitation, {
    name: 'acceptAcademicEditorInvitation',
    options: ({ match }) => ({
      refetchQueries: match.params.submissionId
        ? [refetchGetManuscriptVersions(match)]
        : [],
    }),
  }),
  graphql(mutations.declineAcademicEditorInvitation, {
    name: 'declineAcademicEditorInvitation',
    options: {
      refetchQueries: [refetchGetManuscripts()],
    },
  }),
  graphql(mutations.inviteReviewer, {
    name: 'inviteReviewer',
    options: ({ match }) => ({
      refetchQueries: [
        refetchGetManuscriptVersions(match),
        refetchLoadReviewerSuggestions(match),
      ],
    }),
  }),
  graphql(mutations.cancelReviewerInvitation, {
    name: 'cancelReviewerInvitation',
    options: ({ match }) => ({
      refetchQueries: [refetchGetManuscriptVersions(match)],
    }),
  }),
)

export default withAcademicEditorGQL
