import { chain, get } from 'lodash'

const rules = {
  author: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({ manuscriptStatus }) =>
        manuscriptStatus &&
        !['pendingApproval', 'inQA'].includes(manuscriptStatus),
      'manuscript:viewAuthorReply': () => true,
      'manuscript:viewReviewerReports': ({
        isLatestVersion,
        manuscriptStatus,
      }) =>
        [
          'revisionRequested',
          'accepted',
          'rejected',
          'withdrawn',
          'published',
          'qualityChecksRequested',
          'refusedToConsider',
        ].includes(manuscriptStatus) || !isLatestVersion,
      'manuscript:submitRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'revisionRequested',
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
    },
  },
  reviewer: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return (
          currentReviewer &&
          ['accepted', 'submitted'].includes(currentReviewer.status)
        )
      },
      'manuscript:viewAuthorReply': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return (
          currentReviewer &&
          ['accepted', 'submitted'].includes(currentReviewer.status)
        )
      },
      'manuscript:respondToReviewerInvitation': ({ userId, reviewers }) => {
        if (!reviewers || !userId) return false
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        return currentReviewer && currentReviewer.status === 'pending'
      },
      'manuscript:submitReview': ({
        userId,
        reviews,
        reviewers,
        manuscriptStatus,
      }) => {
        const currentReviewer = reviewers.find(
          reviewer => reviewer.user.id === userId,
        )

        const reviewerCanSeeHisReport = !!chain(reviews)
          .find(review => get(review, 'member.user.id') === userId)
          .get('submitted')
          .value()
        const reviewerCanSubmitReport =
          get(currentReviewer, 'status') === 'accepted' &&
          !['revisionRequested', 'olderVersion'].includes(manuscriptStatus)

        return reviewerCanSubmitReport && !reviewerCanSeeHisReport
      },
      'manuscript:viewReviewerReports': ({ userId, reviews }) => {
        const reviewerCanSeeHisReport = !!chain(reviews)
          .find(review => get(review, 'member.user.id') === userId)
          .get('submitted')
          .value()

        return reviewerCanSeeHisReport
      },
    },
  },
  academicEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': ({
        academicEditorStatus,
        academicEditorUserId,
        userId,
      }) =>
        academicEditorStatus === 'accepted' && academicEditorUserId === userId,
      'manuscript:viewAuthorReply': ({
        academicEditorStatus,
        academicEditorUserId,
        userId,
      }) =>
        academicEditorStatus === 'accepted' && academicEditorUserId === userId,
      'manuscript:respondToEditorialInvitation': ({
        userId,
        pendingAcademicEditorStatus,
        pendingAcademicEditorUserId,
        isLatestVersion,
      }) =>
        isLatestVersion &&
        pendingAcademicEditorUserId === userId &&
        pendingAcademicEditorStatus === 'pending',
      'manuscript:inviteReviewers': ({
        userId,
        hasPeerReview,
        manuscriptStatus,
        academicEditorStatus,
        academicEditorUserId,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        academicEditorUserId === userId &&
        hasPeerReview,
      'manuscript:makeRecommendation': ({
        userId,
        manuscriptStatus,
        hasPeerReview,
        isApprovalEditor,
        academicEditorStatus,
        academicEditorUserId,
      }) =>
        academicEditorUserId === userId &&
        !isApprovalEditor &&
        [
          'academicEditorAssigned',
          'reviewCompleted',
          'academicEditorAssignedEditorialType',
        ].includes(manuscriptStatus) &&
        hasPeerReview,
      'manuscript:makeDecision': ({
        manuscriptStatus,
        isApprovalEditor,
        isLatestVersion,
      }) =>
        isApprovalEditor &&
        isLatestVersion &&
        ![
          'inQA',
          'accepted',
          'rejected',
          'academicEditorInvited',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'refusedToConsider',
        ].includes(manuscriptStatus),
    },
  },
  triageEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:makeDecision': ({ manuscriptStatus, isApprovalEditor }) =>
        isApprovalEditor
          ? ![
              'inQA',
              'rejected',
              'published',
              'accepted',
              'olderVersion',
              'refusedToConsider',
              'qualityChecksRequested',
              'qualityChecksSubmitted',
            ].includes(manuscriptStatus)
          : ![
              'academicEditorAssigned',
              'inQA',
              'accepted',
              'rejected',
              'academicEditorAssignedEditorialType',
              'academicEditorAssigned',
              'reviewersInvited',
              'underReview',
              'olderVersion',
              'makeDecision',
              'refusedToConsider',
              'qualityChecksRequested',
              'qualityChecksSubmitted',
              'published',
            ].includes(manuscriptStatus),
      'manuscript:viewReviewers': ({ academicEditorStatus, hasPeerReview }) =>
        academicEditorStatus === 'accepted' && hasPeerReview,
      'manuscript:assignAcademicEditor': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
    },
  },
  editorialAssistant: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:inviteReviewers': ({
        manuscriptStatus,
        academicEditorStatus,
        hasPeerReview,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        hasPeerReview,
      'manuscript:makeDecision': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:viewActivityLog': () => true,
      'manuscript:assignAcademicEditor': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:reassignTriageEditor': ({
        manuscriptStatus,
        triageEditors,
      }) =>
        triageEditors.length > 1 &&
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
    },
  },
  researchIntegrityPublishingEditor: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:makeDecision': ({ reviews }) => reviews.length === 0,
    },
  },
  admin: {
    static: [],
    dynamic: {
      'manuscript:viewEditorialComments': () => true,
      'manuscript:viewAuthorReply': ({ manuscriptStatus }) =>
        manuscriptStatus !== 'revisionRequested',
      'manuscript:inviteReviewers': ({
        manuscriptStatus,
        academicEditorStatus,
        hasPeerReview,
      }) =>
        !['academicEditorInvited', 'qualityChecksSubmitted'].includes(
          manuscriptStatus,
        ) &&
        academicEditorStatus === 'accepted' &&
        hasPeerReview,
      'manuscript:makeDecision': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:viewActivityLog': () => true,
      'manuscript:assignAcademicEditor': ({ manuscriptStatus }) =>
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
      'manuscript:submitQualityChecksRevision': ({ manuscriptStatus }) =>
        manuscriptStatus === 'qualityChecksRequested',
      'manuscript:reassignTriageEditor': ({
        manuscriptStatus,
        triageEditors,
      }) =>
        triageEditors.length > 1 &&
        ![
          'technicalChecks',
          'rejected',
          'inQA',
          'accepted',
          'olderVersion',
          'withdrawn',
          'qualityChecksRequested',
          'qualityChecksSubmitted',
          'published',
          'refusedToConsider',
        ].includes(manuscriptStatus),
    },
  },
}

export default rules
