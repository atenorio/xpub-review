import React from 'react'
import { Formik } from 'formik'
import { graphql } from 'react-apollo'
import { withRouter } from 'react-router'
import { Button, H2 } from '@pubsweet/ui'
import { compose, withHandlers, withProps } from 'recompose'
import { Row, Text, Loader, ShadowedBox, PasswordValidation } from '@hindawi/ui'
import { mutations } from 'component-authentication/client'

import { withReviewerGQL } from '../graphql/withGQL'

const NewReviewerEmail = ({
  fetchingError,
  isFetching,
  initialValues,
  onSubmit,
}) => (
  <Formik initialValues={initialValues} onSubmit={onSubmit}>
    {({ handleSubmit, errors, touched, values }) => (
      <ShadowedBox center mt={4}>
        <H2>Invitation to review</H2>
        <Text align="center" secondary>
          {initialValues.email}
        </Text>
        <PasswordValidation
          errors={errors}
          formValues={values}
          pl={2}
          pr={2}
          touched={touched}
        />
        <Row height={10} mt={6} pl={8} pr={8}>
          {isFetching ? (
            <Loader />
          ) : (
            <Button fullWidth onClick={handleSubmit} primary type="submit">
              SET PASSWORD & AGREE TO REVIEW
            </Button>
          )}
        </Row>

        {fetchingError && (
          <Row justify="flex-start" mt={4} pl={8} pr={8}>
            <Text error>{fetchingError}</Text>
          </Row>
        )}
      </ShadowedBox>
    )}
  </Formik>
)

export default compose(
  withRouter,
  withReviewerGQL,
  graphql(mutations.loginUser, {
    name: 'login',
  }),
  graphql(mutations.resetPassword, {
    name: 'resetPassword',
  }),
  withProps(
    ({ params: { token, invitationId, manuscriptId, ...initialValues } }) => ({
      initialValues,
    }),
  ),
  withHandlers({
    onSubmit: ({
      login,
      history,
      setError,
      setFetching,
      resetPassword,
      acceptReviewerInvitation,
      params: { manuscriptId, invitationId, token, submissionId },
    }) => ({ email, password }) => {
      setFetching(true)
      resetPassword({
        variables: {
          input: {
            email,
            password,
            token,
          },
        },
      })
        .then(() =>
          login({
            variables: {
              input: {
                email,
                password,
              },
            },
          }).then(r => {
            window.localStorage.setItem('token', r.data.localLogin.token)
            return acceptReviewerInvitation({
              variables: {
                teamMemberId: invitationId,
              },
            })
          }),
        )
        .then(() => {
          setFetching(false)
          history.replace(`/details/${submissionId}/${manuscriptId}`)
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
          history.replace('/')
        })
    },
  }),
)(NewReviewerEmail)
