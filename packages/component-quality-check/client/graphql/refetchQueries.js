import { queries } from 'component-peer-review/client/graphql'

export const refetchGetManuscriptVersions = ({
  params: { submissionId = '' },
}) => ({
  query: queries.getManuscriptVersions,
  variables: { submissionId },
})
