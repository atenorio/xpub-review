import moize from 'moize'
import { chain, cloneDeep, get, omit, isEqual, debounce, set } from 'lodash'

export const parseAuthor = a =>
  omit({ ...a, ...get(a, 'alias'), ...get(a, 'alias.name') }, [
    '__typename',
    'id',
    'name',
    'alias',
    'user',
    'status',
    'invited',
    'reviewerNumber',
    'responded',
  ])

export const parseQualityChecksFormValues = values => {
  const manuscriptId = get(values, 'id', '')
  const meta = omit(get(values, 'meta', {}), [
    '__typename',
    'hasConflictOfInterest',
  ])

  const authors = chain(values)
    .get('authors', [])
    .filter(a => a.id !== 'unsaved-author')
    .map(parseAuthor)
    .value()

  const files = chain(values)
    .get('files', {})
    .flatMap()
    .map(
      ({
        mimeType,
        originalName,
        filename,
        providerKey,
        __typename,
        ...f
      }) => ({
        ...f,
        name: originalName,
      }),
    )
    .value()

  return {
    manuscriptId,
    autosaveInput: {
      authors,
      files,
      meta,
    },
  }
}

export const autosaveRequest = ({
  values,
  updateDraftRevision,
  updateAutosave,
}) => {
  const variables = parseQualityChecksFormValues(values)

  updateAutosave({
    variables: {
      params: {
        error: null,
        inProgress: true,
        updatedAt: null,
      },
    },
  })

  updateDraftRevision({
    variables,
  }).then(r => {
    updateAutosave({
      variables: {
        params: {
          error: null,
          inProgress: false,
          updatedAt: Date.now(),
        },
      },
    })
  })
}

const memoizedAutosaveRequest = moize(autosaveRequest, {
  maxSize: 1,
  equals: ({ values: valuesOne }, { values: valuesTwo }) =>
    isEqual(valuesOne, valuesTwo),
})

export const autosaveForm = debounce(memoizedAutosaveRequest, 1000)

export const validateQualityChecksRevision = values => {
  const errors = {}
  if (values.isEditing) {
    errors.isEditing = 'An author is being edited.'
  }

  if (values.authors.length === 0) {
    errors.authorError = 'At least one author is required.'
  }

  if (get(values, 'files.manuscript').length === 0) {
    set(errors, 'fileError', 'At least one manuscript is required.')
  }

  return errors
}

export const parseManuscriptFiles = (files = []) =>
  files.reduce(
    (acc, file) => ({
      ...acc,
      [file.type]: [...acc[file.type], file],
    }),
    {
      figure: [],
      manuscript: [],
      coverLetter: [],
      supplementary: [],
      responseToReviewers: [],
    },
  )

export const removeTypename = (inputObject = {}) => {
  const o = cloneDeep(inputObject)
  Object.keys(inputObject).forEach(key => {
    if (key === '__typename') delete o[key]
    if (o[key] !== null && typeof o[key] === 'object') {
      o[key] = removeTypename(o[key])
    }
  })
  return o
}

export const setQualityChecksInitialValues = values => {
  const id = get(values, 'id')
  const meta = omit(get(values, 'meta', {}), '__typename')
  const articleTypeId = get(values, 'articleType.id')
  const authors = chain(values)
    .get('authors', [])
    .map(removeTypename)
    .value()

  const files = chain(values)
    .get('files', [])
    .map(removeTypename)
    .value()

  return {
    id,
    authors,
    files: parseManuscriptFiles(files),
    meta: {
      ...meta,
      hasConflictOfInterest: meta.conflictOfInterest ? 'yes' : 'no',
      articleTypeId,
    },
  }
}

export const parseError = e =>
  get(e, 'message', e).replace('GraphQL error: ', '')
