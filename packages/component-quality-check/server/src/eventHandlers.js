const models = require('@pubsweet/models')
const { logEvent } = require('component-activity-log/server')
const { sendPackage } = require('component-mts-package')
const events = require('component-events')

const useCases = require('./useCases')
const notificationService = require('./notifications/notification')

module.exports = {
  async SubmissionQualityCheckFilesRequested(data) {
    return useCases.requestQualityChecksUseCase
      .initialize({ models, logEvent })
      .execute(data)
  },
  async SubmissionQualityCheckPassed(data) {
    const eventsService = events.initialize({ models })

    return useCases.createPackageForProductionUseCase
      .initialize({ models, sendPackage, eventsService })
      .execute({ data })
  },
  async SubmissionQualityCheckRTCd(data) {
    const eventsService = events.initialize({ models })

    return useCases.rejectQualityCheckUseCase
      .initialize({ models, eventsService, logEvent, notificationService })
      .execute({ data })
  },
}
