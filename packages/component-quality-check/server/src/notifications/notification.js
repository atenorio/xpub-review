const config = require('config')
const { services } = require('helper-service')
const { get } = require('lodash')
const Email = require('@pubsweet/component-email-templating')

const { getEmailCopy } = require('./emailCopy')
const { getModifiedText } = require('component-transform-text')

const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')
const baseUrl = config.get('pubsweet-client.baseUrl')

module.exports = {
  async notifyAuthor({ journalName, editorialAssistant, author, manuscript }) {
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const editorialAssistantName = editorialAssistant.getName()

    const { paragraph, ...bodyProps } = getEmailCopy({
      journalName,
      emailType: 'author-manuscript-rejected-eqa',
    })

    const email = new Email({
      type: 'user',
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      toUser: {
        email: get(author, 'alias.email', ''),
        name: get(author, 'alias.surname', ''),
      },
      content: {
        subject: `${manuscript.customId}: Manuscript rejected`,
        paragraph,
        signatureName: editorialAssistantName,
        unsubscribeLink: services.createUrl(baseUrl, unsubscribeSlug, {
          id: author.user.id,
          token: author.user.unsubscribeToken,
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: author.alias.email,
        }),
      },
      bodyProps,
    })
    return email.sendEmail()
  },
}
