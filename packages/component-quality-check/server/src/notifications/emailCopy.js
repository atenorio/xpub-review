const getEmailCopy = ({ emailType, journalName }) => {
  let upperContent, subText, lowerContent, paragraph
  const hasIntro = true
  const hasSignature = true
  switch (emailType) {
    case 'author-manuscript-rejected-eqa':
      paragraph = `I regret to inform you that your manuscript has been rejected for publication in ${journalName}.
        Thank you for your submission, and please do consider submitting again in the future.`
      break
    default:
      throw new Error(`The ${emailType} email type is not defined.`)
  }
  return {
    subText,
    hasIntro,
    paragraph,
    upperContent,
    lowerContent,
    hasSignature,
  }
}

module.exports = {
  getEmailCopy,
}
