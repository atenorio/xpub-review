const initialize = ({
  models: { Manuscript },
  sendPackage,
  eventsService,
}) => ({
  async execute({ data }) {
    const { submissionId } = data

    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
      eagerLoadRelations: `[
           articleType,
           journal.[teams.members.team,peerReviewModel],
           files,
           section,
           teams.members.user.identities, 
           reviews.[
             comments.files,
             member.team
            ], 
           specialIssue.section
          ]`,
    })
    lastManuscript.updateProperties({
      status: Manuscript.Statuses.published,
    })
    await lastManuscript.save()

    await sendPackage({
      manuscript: lastManuscript,
      isForProduction: true,
    })

    eventsService.publishSubmissionEvent({
      submissionId: lastManuscript.submissionId,
      eventName: 'SubmissionPackageCreated',
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
