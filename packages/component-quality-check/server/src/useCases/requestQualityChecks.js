const { createVersion } = require('../services/createVersion')

const initialize = ({ models, models: { Manuscript }, logEvent }) => ({
  async execute({ submissionId }) {
    const lastManuscript = await Manuscript.findLastManuscriptBySubmissionId({
      submissionId,
    })

    lastManuscript.updateStatus(Manuscript.Statuses.qualityChecksRequested)
    await lastManuscript.save()

    await createVersion({
      models,
      submissionId,
    })

    logEvent({
      userId: null,
      manuscriptId: lastManuscript.id,
      action: logEvent.actions.quality_checks_requested,
      objectType: logEvent.objectType.manuscript,
      objectId: lastManuscript.id,
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
