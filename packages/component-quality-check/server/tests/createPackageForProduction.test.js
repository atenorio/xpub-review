const { models, fixtures } = require('fixture-service')

const { createPackageForProductionUseCase } = require('../src/useCases')

const sendPackage = jest.fn()
const eventsService = {
  publishSubmissionEvent: jest.fn(() => {}),
}

describe('Manuscript ready for production use case', () => {
  let peerReviewModel
  let journal
  beforeAll(async () => {
    const { PeerReviewModel, Journal, Team } = models
    peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
  })
  it('Should update last manuscript status to qualityChecksRequested', async () => {
    const { Manuscript } = models
    const manuscriptVersion1 = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.inQA,
        version: 1,
      },
      Manuscript,
    })

    const manuscriptVersion2 = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.inQA,
        version: 2,
        submissionId: manuscriptVersion1.submissionId,
      },
      Manuscript,
    })

    await createPackageForProductionUseCase
      .initialize({ models, sendPackage, eventsService })
      .execute({ data: { submissionId: manuscriptVersion1.submissionId } })

    expect(manuscriptVersion2.status).toEqual(Manuscript.Statuses.published)
    expect(sendPackage).toHaveBeenCalledTimes(1)
    expect(eventsService.publishSubmissionEvent).toHaveBeenCalledTimes(1)
  })
})
