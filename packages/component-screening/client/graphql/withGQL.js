/* eslint-disable */
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { queries } from 'component-activity-log/client'
import { queries as manuscriptDetailsQueries } from 'component-peer-review/client'
import { queries as dashboardQueries } from 'component-dashboard/client'

import * as mutations from './mutations'
import { parseSearchParams } from '../utils'

const mutationOptions = ({ location }) => {
  const { manuscriptId, submissionId } = parseSearchParams(location.search)
  return {
    refetchQueries: [
      {
        query: queries.getAuditLogEvents,
        variables: { manuscriptId },
      },
      {
        query: manuscriptDetailsQueries.getManuscriptVersions,
        variables: { submissionId },
      },
      {
        query: dashboardQueries.getManuscripts,
      },
    ],
  }
}

export default compose(
  graphql(mutations.declineEQS, {
    name: 'declineEQS',
    options: mutationOptions,
  }),
  graphql(mutations.approveEQS, {
    name: 'approveEQS',
    options: mutationOptions,
  }),
  graphql(mutations.approveEQA, {
    name: 'approveEQA',
    options: mutationOptions,
  }),
  graphql(mutations.returnToApprovalEditor, {
    name: 'returnToApprovalEditor',
    options: mutationOptions,
  }),
)
