process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Promise = require('bluebird')
const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { approveEQAUseCase } = require('../src/use-cases')

const notificationService = {
  notifyAuthors: jest.fn(),
  notifyReviewers: jest.fn(),
}
const chance = new Chance()

describe('Approve EQA use case', () => {
  const {
    Team,
    Journal,
    Manuscript,
    TeamMember,
    PeerReviewModel,
    SpecialIssue,
  } = models
  let peerReviewModel
  let journal
  let manuscript
  let token
  let admin

  beforeEach(async () => {
    peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    token = chance.guid()
    admin = await dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    manuscript = fixtures.generateManuscript({
      properties: {
        journal,
        technicalCheckToken: token,
        status: Manuscript.Statuses.inQA,
        hasPassedEqa: null,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { isSubmitting: true, isCorresponding: false },
      role: Team.Role.author,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.accepted },
      role: Team.Role.academicEditor,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      input: { status: TeamMember.Statuses.submitted },
      role: Team.Role.reviewer,
    })
  })
  afterEach(() => {
    jest.clearAllMocks()
  })

  it('changes manucript fields', async () => {
    await approveEQAUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })

    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.hasPassedEqa).toBe(true)
    expect(manuscript.status).toBe(Manuscript.Statuses.accepted)
  })
  it('should delete the special issue id if special issue was cancelled', async () => {
    const specialIssue = await fixtures.generateSpecialIssue({
      properties: {
        isCancelled: false,
      },
      SpecialIssue,
    })
    manuscript.specialIssueId = specialIssue.id

    specialIssue.isCancelled = true

    await approveEQAUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })

    expect(manuscript.specialIssueId).toEqual(null)
  })
  it('should send email to authors and reviewers', async () => {
    await approveEQAUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })

    expect(notificationService.notifyAuthors).toHaveBeenCalledTimes(1)
    expect(notificationService.notifyReviewers).toHaveBeenCalledTimes(1)
  })
  it('should throw error if the manuscript already passed EQA', async () => {
    manuscript.hasPassedEqa = true
    const result = approveEQAUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })
    return expect(result).rejects.toThrow('Manuscript already handled.')
  })
  it('should throw error if the token is invalid', async () => {
    token = chance.guid()
    const result = approveEQAUseCase
      .initialize({ notificationService, models })
      .execute({
        manuscriptId: manuscript.id,
        input: { token },
        userId: admin.userId,
      })
    return expect(result).rejects.toThrow('Invalid token.')
  })
  it('should throw error if the manuscript is in a wrong status', async () => {
    const statuses = Manuscript.Statuses
    const wrongStatuses = [
      statuses.draft,
      statuses.technicalChecks,
      statuses.submitted,
      statuses.academicEditorInvited,
      statuses.academicEditorAssigned,
      statuses.reviewersInvited,
      statuses.underReview,
      statuses.reviewCompleted,
      statuses.revisionRequested,
      statuses.pendingApproval,
      statuses.accepted,
      statuses.rejected,
      statuses.deleted,
      statuses.published,
      statuses.withdrawn,
      statuses.withdrawalRequested,
      statuses.olderVersion,
    ]
    await Promise.each(wrongStatuses, async wrongStatus => {
      const manuscript = await fixtures.generateManuscript({
        properties: {
          status: wrongStatus,
          journal,
          technicalCheckToken: token,
          hasPassedEqa: null,
        },
        Manuscript,
      })
      dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        input: { isSubmitting: true, isCorresponding: false },
        role: Team.Role.author,
      })
      await dataService.createUserOnManuscript({
        models,
        manuscript,
        fixtures,
        input: { status: TeamMember.Statuses.submitted },
        role: Team.Role.reviewer,
      })
      try {
        await approveEQAUseCase
          .initialize({ notificationService, models })
          .execute({
            manuscriptId: manuscript.id,
            input: { token },
            userId: admin.userId,
          })
        throw new Error(`Use-case should fail for status ${wrongStatus}`)
      } catch (err) {
        expect(err.message).toEqual(`Cannot approve EQA in the current status.`)
      }
    })
  })
})
