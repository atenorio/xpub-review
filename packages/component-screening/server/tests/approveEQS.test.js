const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const { approveEQSUseCase } = require('../src/use-cases')

const {
  inviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor')

// #region inviteAcademicEditor
const invitationsService = {
  sendInvitationToAcademicEditor: jest.fn(),
}
const emailJobsService = {
  scheduleEmailsWhenAcademicEditorIsInvited: jest.fn(),
}
const removalJobsService = {
  scheduleRemovalJobForAcademicEditor: jest.fn(),
}

const logEventAcademicEditor = () => jest.fn(async () => {})
logEventAcademicEditor.actions = {
  invitation_sent: 'invitation_sent',
}
logEventAcademicEditor.objectType = { user: 'user' }

// #endregion

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eqs_approved: 'eqs_approved' }
logEvent.objectType = { manuscript: 'manuscript' }

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
  isActive: false,
})

const {
  Team,
  Section,
  Journal,
  Manuscript,
  TeamMember,
  ArticleType,
  PeerReviewModel,
} = models

describe('Approve EQS use case', () => {
  let mockedUseCases = {}
  let input
  let notificationService = {}

  beforeEach(() => {
    input = generateInput()
    mockedUseCases = {
      assignTriageEditorOnManuscriptUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
    }
    notificationService = {
      notifyApprovalEditor: jest.fn(),
    }
  })
  it('sets  hasEQS to true', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: ['academicEditor'],
        academicEditorAssignmentTool: ['manual'],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    PeerReviewModel.findPeerReviewModelByManuscript = jest.fn(
      () => peerReviewModel,
    )
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
        hasPassedEqs: null,
        journalId: journal.id,
      },
      Manuscript,
    })
    manuscript.journal = journal
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: {},
    })
    await approveEQSUseCase
      .initialize({ models, logEvent, notificationService })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('submitted')
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)
  })

  it('returns an error when the token is invalid', async () => {
    const journal = await fixtures.generateJournal({
      properties: input,
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: chance.guid(),
        hasPassedEqs: null,
        journalId: journal.id,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: {},
    })
    const result = approveEQSUseCase
      .initialize({ models, logEvent, notificationService })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token: chance.guid(),
        },
      })

    return expect(result).rejects.toThrow('Invalid token.')
  })

  it('returns an error if the manuscript has already passed/failed EQS', async () => {
    const token = chance.guid()
    const journal = await fixtures.generateJournal({
      properties: input,
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: null,
        hasPassedEqs: chance.pickone([false, true]),
        journalId: journal.id,
      },
      Manuscript,
    })
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: {},
    })
    const result = approveEQSUseCase
      .initialize({ models, logEvent, notificationService })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    return expect(result).rejects.toThrow('Manuscript already handled.')
  })

  it('auto assigns an academic editor based on workload', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
        academicEditorAssignmentTool: ['workload', 'manual'],
      },
      PeerReviewModel,
    })
    const journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    const firstAcademicEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.academicEditor,
    })
    const secondAcademicEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.academicEditor,
    })
    const thirdAcademicEditor = await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.academicEditor,
    })

    const firstManuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorAssigned,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript: firstManuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await dataService.addUserOnManuscript({
      user: fixtures.users.find(u => u.id === firstAcademicEditor.userId),
      models,
      fixtures,
      manuscript: firstManuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.accepted },
    })

    const secondManuscript = fixtures.generateManuscript({
      properties: {
        journalId: journal.id,
        status: Manuscript.Statuses.academicEditorAssigned,
      },
      Manuscript,
    })

    await dataService.createUserOnManuscript({
      models,
      manuscript: secondManuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await dataService.addUserOnManuscript({
      user: fixtures.users.find(u => u.id === secondAcademicEditor.userId),
      models,
      fixtures,
      manuscript: secondManuscript,
      role: Team.Role.academicEditor,
      input: { status: TeamMember.Statuses.pending },
    })
    fixtures.generateArticleTypes(ArticleType)

    const reviewArticleType = fixtures.articleTypes.find(
      at => at.name === ArticleType.Types.reviewArticle,
    )

    const thirdManuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: token,
        hasPassedEqs: null,
        journalId: journal.id,
        articleTypeId: reviewArticleType.id,
      },
      Manuscript,
    })
    thirdManuscript.journal = journal
    thirdManuscript.articleType = reviewArticleType

    await dataService.createUserOnManuscript({
      models,
      manuscript: thirdManuscript,
      fixtures,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    const inviteAcademicEditorService = inviteAcademicEditorUseCase.initialize({
      models,
      emailJobsService,
      removalJobsService,
      invitationsService,
      logEvent: logEventAcademicEditor,
    })

    await approveEQSUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        inviteAcademicEditorService,
      })
      .execute({
        manuscriptId: thirdManuscript.id,
        input: {
          token,
        },
      })
    const academicEditorTeam = fixtures.teams.find(
      t =>
        t.role === Team.Role.academicEditor &&
        t.manuscriptId === thirdManuscript.id,
    )
    const academicEditorTeamMember = fixtures.teamMembers.find(
      tm =>
        tm.userId === thirdAcademicEditor.userId &&
        tm.teamId === academicEditorTeam.id,
    )

    expect(academicEditorTeam).toBeDefined()
    expect(academicEditorTeamMember).toBeDefined()
  })
  it('automatically assigns an Associate Editor if the peer review model is Associate Editor', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Associate Editor',
        triageEditorLabel: 'Associate Editor',
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    // create multiple Associate Editors on the Journal
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        journalId: journal.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
      },
    })
    manuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('submitted')
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
  it('automatically assigns a Section Editor if the peer review model is Section Editor', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Section Editor',
        triageEditorLabel: 'Section Editor',
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    const section = await fixtures.generateSection({
      properties: { ...input, journalId: journal.id },
      Section,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    // create multiple Section Editors on the Section
    await dataService.createUserOnSection({
      models,
      section,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnSection({
      models,
      section,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        sectionId: section.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
        journalId: journal.id,
      },
    })
    manuscript.section = section
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('submitted')
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
  it('automatically assigns a Chief Editor if the peer review model is Chief Minus', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Chief Minus',
        triageEditorLabel: 'Chief Editor',
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        journalId: journal.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
      },
    })
    manuscript.journal = journal
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        manuscriptId: manuscript.id,
        input: {
          token,
        },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual('submitted')
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
})
