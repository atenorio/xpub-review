const Chance = require('chance')

const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')
const { approveEQSByEventUseCase } = require('../src/use-cases')

const logEventAcademicEditor = () => jest.fn(async () => {})
logEventAcademicEditor.actions = {
  invitation_sent: 'invitation_sent',
}
logEventAcademicEditor.objectType = { user: 'user' }

// #endregion

const chance = new Chance()
const logEvent = () => jest.fn(async () => {})
logEvent.actions = { eqs_approved: 'eqs_approved' }
logEvent.objectType = { manuscript: 'manuscript' }

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
  isActive: false,
})

const {
  Team,
  Section,
  Journal,
  Manuscript,
  TeamMember,
  PeerReviewModel,
} = models

describe('Approve EQS use case', () => {
  let mockedUseCases = {}
  let input
  let notificationService = {}

  beforeEach(() => {
    input = generateInput()
    mockedUseCases = {
      assignTriageEditorOnManuscriptUseCase: {
        initialize: jest.fn(() => ({ execute: jest.fn() })),
      },
    }
    notificationService = {
      notifyApprovalEditor: jest.fn(),
    }
  })
  it('sets  hasEQS to true and technicalCheckToken to null', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: ['academicEditor'],
        academicEditorAssignmentTool: ['manual'],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    PeerReviewModel.findPeerReviewModelByManuscript = jest.fn(
      () => peerReviewModel,
    )
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    const manuscript = fixtures.generateManuscript({
      properties: {
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
        hasPassedEqs: null,
        journalId: journal.id,
      },
      Manuscript,
    })
    manuscript.journal = journal
    await dataService.createUserOnManuscript({
      models,
      manuscript,
      fixtures,
      role: Team.Role.author,
      input: {},
    })

    await approveEQSByEventUseCase
      .initialize({
        logEvent,
        notificationService,
        models,
      })
      .execute({
        data: { submissionId: manuscript.submissionId },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)
  })
  it('automatically assigns an Associate Editor if the peer review model is Associate Editor', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Associate Editor',
        triageEditorLabel: 'Associate Editor',
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    // create multiple Associate Editors on the Journal
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        journalId: journal.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
      },
    })
    manuscript.journal = journal

    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        data: { submissionId: manuscript.submissionId },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
  it('automatically assigns a Section Editor if the peer review model is Section Editor', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Section Editor',
        triageEditorLabel: 'Section Editor',
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    const section = await fixtures.generateSection({
      properties: { ...input, journalId: journal.id },
      Section,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    // create multiple Section Editors on the Section
    await dataService.createUserOnSection({
      models,
      section,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })
    await dataService.createUserOnSection({
      models,
      section,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        sectionId: section.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
        journalId: journal.id,
      },
    })
    manuscript.section = section
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        data: { submissionId: manuscript.submissionId },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
  it('automatically assigns a Chief Editor if the peer review model is Chief Minus', async () => {
    const token = chance.guid()
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        hasTriageEditor: true,
        name: 'Chief Minus',
        triageEditorLabel: 'Chief Editor',
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
    const journal = await fixtures.generateJournal({
      properties: { ...input, peerReviewModelId: peerReviewModel.id },
      Journal,
    })
    journal.peerReviewModel = peerReviewModel

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.editorialAssistant,
    })

    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      input: { status: TeamMember.Statuses.pending },
      role: Team.Role.triageEditor,
    })

    const manuscript = fixtures.generateManuscript({
      Manuscript,
      properties: {
        hasPassedEqs: null,
        journalId: journal.id,
        technicalCheckToken: token,
        status: Manuscript.Statuses.technicalChecks,
      },
    })
    manuscript.journal = journal
    await dataService.createUserOnManuscript({
      models,
      fixtures,
      manuscript,
      role: Team.Role.author,
      input: { isSubmitting: true },
    })

    await approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        notificationService,
        useCases: mockedUseCases,
      })
      .execute({
        data: { submissionId: manuscript.submissionId },
      })

    expect(manuscript.hasPassedEqs).toBeTruthy()
    expect(manuscript.technicalCheckToken).toBeNull()
    expect(manuscript.status).toEqual(Manuscript.Statuses.submitted)
    expect(notificationService.notifyApprovalEditor).toHaveBeenCalledTimes(1)

    expect(
      mockedUseCases.assignTriageEditorOnManuscriptUseCase.initialize,
    ).toHaveBeenCalledTimes(1)
  })
})
