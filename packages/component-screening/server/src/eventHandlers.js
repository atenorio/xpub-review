const useCases = require('./use-cases')
const models = require('@pubsweet/models')

const config = require('config')
const Email = require('@pubsweet/component-email-templating')
const { sendPackage } = require('component-mts-package')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const urlService = require('./urlService/urlService')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const approveEQSNotifications = require('./notifications/approveEQS')
const getEmailCopy = require('./notifications/getEmailCopy')
const getProps = require('./notifications/getProps')

const {
  inviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor')

const inviteAcademicEditorDependencies = {
  invitations: require('component-peer-review/server/src/invitations/invitations'),
  emailJobs: require('component-peer-review/server/src/emailJobs/academicEditor/emailJobs'),
  removalJobs: require('component-peer-review/server/src/removalJobs/academicEditorRemovalJobs'),
  getProps: require('component-peer-review/server/src/emailPropsService/emailPropsService'),
}
module.exports = {
  async SubmissionScreeningPassed(data) {
    const getEmailCopyService = getEmailCopy.initialize()
    const getPropsService = getProps.initialize({
      baseUrl,
      urlService,
      footerText,
      unsubscribeSlug,
      getModifiedText,
    })
    const notificationService = approveEQSNotifications.initialize({
      Email,
      getEmailCopyService,
      getPropsService,
    })

    // #region inviteAcademicEditor
    const { Manuscript, TeamMember, Job } = models
    const invitationsService = inviteAcademicEditorDependencies.invitations.initialize(
      { Email },
    )
    const getPropsServiceInviteAcademicEditor = inviteAcademicEditorDependencies.getProps.initialize(
      {
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      },
    )
    const emailJobsService = inviteAcademicEditorDependencies.emailJobs.initialize(
      {
        Job,
        Email,
        logEvent,
        getPropsService: getPropsServiceInviteAcademicEditor,
      },
    )
    const removalJobsService = inviteAcademicEditorDependencies.removalJobs.initialize(
      {
        Job,
        logEvent,
        TeamMember,
        Manuscript,
      },
    )
    const inviteAcademicEditorService = inviteAcademicEditorUseCase.initialize({
      models,
      logEvent,
      invitationsService,
      emailJobsService,
      removalJobsService,
    })
    // #endregion
    return useCases.approveEQSByEventUseCase
      .initialize({
        models,
        logEvent,
        useCases,
        notificationService,
        inviteAcademicEditorService,
      })
      .execute({ data })
  },
  async SubmissionScreeningRTCd(data) {
    return useCases.declineEQSByEventUseCase
      .initialize({ models, logEvent, sendPackage })
      .execute({ data })
  },
  async SubmissionScreeningReturnedToDraft(data) {
    return useCases.returnToDraftUseCase
      .initialize({ models, logEvent })
      .execute({ data })
  },
}
