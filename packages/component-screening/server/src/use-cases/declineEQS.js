const initialize = ({ models: { Manuscript }, logEvent, sendPackage }) => ({
  async execute({ manuscriptId, input }) {
    const { token } = input
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[journal, articleType, files, teams.members, section, specialIssue.section]',
    )

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    manuscript.updateProperties({
      hasPassedEqs: false,
      technicalCheckToken: null,
      status: Manuscript.Statuses.refusedToConsider,
    })
    await manuscript.save()

    await sendPackage({ manuscript, isManuscriptRejected: true })

    logEvent({
      userId: null,
      manuscriptId,
      action: logEvent.actions.eqs_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscriptId,
    })
  },
})

const authsomePolicies = ['isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
