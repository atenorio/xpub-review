const initialize = ({
  manuscriptStatuses,
  models: { TeamMember, Team, User },
  getUserForAutoAssignmentBasedOnWorkload,
}) => ({
  async execute({ manuscriptId, journalId, sectionId, specialIssueId }) {
    const triageEditor = await getUserForAutoAssignmentBasedOnWorkload({
      journalId,
      sectionId,
      specialIssueId,
      manuscriptStatuses,
      models: { TeamMember },
      teamRole: Team.Role.triageEditor,
      teamMemberStatuses: [TeamMember.Statuses.pending],
    })

    const queryObject = {
      manuscriptId,
      role: Team.Role.triageEditor,
    }
    const triageEditorTeam = await Team.findOrCreate({
      queryObject,
      options: queryObject,
    })
    const triageEditorUser = await User.find(triageEditor.userId, 'identities')

    const manuscriptTriageEditor = triageEditorTeam.addMember(
      triageEditorUser,
      {
        status: TeamMember.Statuses.active,
      },
    )
    await manuscriptTriageEditor.save()
    await triageEditorTeam.save()
  },
})

module.exports = {
  initialize,
}
