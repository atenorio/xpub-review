const initialize = ({
  notificationService,
  models: { Manuscript, TeamMember, Team },
}) => ({
  async execute({ manuscriptId, input }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, journal.[teams.members, peerReviewModel], articleType]',
    )
    const approvalEditorRole = await TeamMember.getApprovalEditorRole({
      manuscript,
      TeamRole: Team.Role,
    })
    const { token } = input
    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new ValidationError(
        `Cannot return manuscript in the current status.`,
      )
    }

    if (manuscript.hasPassedEqa !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    const newStatus =
      approvalEditorRole === Team.Role.academicEditor
        ? Manuscript.Statuses.makeDecision
        : Manuscript.Statuses.pendingApproval
    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: newStatus,
    })
    await manuscript.save()

    const approvalEditor = await TeamMember.findApprovalEditor({
      manuscriptId,
      TeamRole: Team.Role,
    })
    const { journal } = manuscript
    const editorialAssistant = journal.getCorrespondingEditorialAssistant()
    const submittingAuthor = manuscript.getSubmittingAuthor()

    notificationService.notifyApprovalEditor({
      manuscript,
      approvalEditor,
      submittingAuthor,
      editorialAssistant,
      comments: input.reason,
      journalName: journal.name,
    })
  },
})

const authsomePolicies = ['isAuthenticated', 'isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
