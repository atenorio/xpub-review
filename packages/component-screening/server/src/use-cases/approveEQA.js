const initialize = ({
  notificationService,
  models: { Manuscript, Team, TeamMember, SpecialIssue },
}) => ({
  async execute({ manuscriptId, input, userId }) {
    const manuscript = await Manuscript.find(
      manuscriptId,
      '[teams.members.user, journal.teams.members]',
    )

    let specialIssue
    if (manuscript.specialIssueId) {
      specialIssue = await SpecialIssue.find(manuscript.specialIssueId)
    }

    if (manuscript.status !== Manuscript.Statuses.inQA) {
      throw new ValidationError(`Cannot approve EQA in the current status.`)
    }

    const { token } = input
    if (manuscript.hasPassedEqa !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    if (token !== manuscript.technicalCheckToken) {
      throw new Error('Invalid token.')
    }

    if (specialIssue && specialIssue.isCancelled) {
      manuscript.updateProperties({ specialIssueId: null })
    }
    manuscript.updateProperties({
      technicalCheckToken: null,
      hasPassedEqa: true,
      status: Manuscript.Statuses.accepted,
    })
    await manuscript.save()

    const approvalEditor = await TeamMember.findApprovalEditor({
      manuscriptId,
      TeamRole: Team.Role,
    })

    const { journal } = manuscript
    const editorialAssistant = journal.getCorrespondingEditorialAssistant()
    const academicEditor = manuscript.getAcademicEditor()
    const submittingAuthor = manuscript.getSubmittingAuthor()
    const authors = manuscript.getAuthors()
    const reviewerTeam = manuscript.teams.find(
      t => t.role === Team.Role.reviewer,
    )

    notificationService.notifyAuthors({
      authors,
      manuscript,
      journalName: journal.name,
      approvalEditor,
      academicEditor,
      editorialAssistant,
    })

    if (reviewerTeam) {
      const reviewers = manuscript.getReviewersByStatus(
        TeamMember.Statuses.submitted,
      )
      notificationService.notifyReviewers({
        reviewers,
        manuscript,
        journalName: journal.name,
        approvalEditor,
        academicEditor,
        submittingAuthor,
        editorialAssistant,
      })
    }
  },
})

const authsomePolicies = ['isAuthenticated', 'isEditorialAssistant']

module.exports = {
  initialize,
  authsomePolicies,
}
