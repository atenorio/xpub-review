const initialize = ({ logEvent, models: { Manuscript } }) => ({
  async execute({ data }) {
    const { submissionId } = data

    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
    })

    manuscript.updateProperties({
      status: Manuscript.Statuses.draft,
    })
    await manuscript.save()

    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_return_to_draft,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

const authsomePolicies = []

module.exports = {
  initialize,
  authsomePolicies,
}
