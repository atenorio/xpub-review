const initialize = ({ models: { Manuscript }, logEvent, sendPackage }) => ({
  async execute({ data }) {
    const { submissionId } = data
    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
      eagerLoadRelations:
        '[journal, articleType, files, teams.members, section, specialIssue.section]',
    })

    if (!manuscript) {
      throw new Error('Manuscript not found.')
    }

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }

    manuscript.updateProperties({
      hasPassedEqs: false,
      technicalCheckToken: null,
      status: Manuscript.Statuses.refusedToConsider,
    })
    await manuscript.save()

    await sendPackage({ manuscript, isManuscriptRejected: true })

    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_declined,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
