const {
  getUserForAutoAssignmentBasedOnWorkload,
} = require('component-model/src/useCases/getUserForAutoAssignmentBasedOnWorkload')

const initialize = ({
  logEvent,
  useCases,
  notificationService,
  models,
  inviteAcademicEditorService,
}) => ({
  async execute({ data }) {
    const { submissionId } = data
    const {
      Manuscript,
      Journal,
      Team,
      TeamMember,
      PeerReviewModel,
      User,
    } = models

    const manuscript = await Manuscript.findOneBy({
      queryObject: { submissionId },
      eagerLoadRelations: '[teams.members, articleType]',
    })
    const journal = await Journal.find(
      manuscript.journalId,
      '[teams.members, peerReviewModel]',
    )
    const editorialAssistant = journal.getCorrespondingEditorialAssistant()
    manuscript.journal = journal
    const approvalEditorRole = await TeamMember.getApprovalEditorRole({
      manuscript,
      TeamRole: Team.Role,
    })
    const submittingAuthor = manuscript.getSubmittingAuthor()

    if (manuscript.hasPassedEqs !== null) {
      throw new ValidationError('Manuscript already handled.')
    }
    manuscript.updateProperties({
      hasPassedEqs: true,
      technicalCheckToken: null,
      status: Manuscript.Statuses.submitted,
    })
    await manuscript.save()

    const peerReviewModel = await PeerReviewModel.findOneByManuscript({
      manuscriptId: manuscript.id,
    })

    logEvent({
      userId: null,
      manuscriptId: manuscript.id,
      action: logEvent.actions.eqs_approved,
      objectType: logEvent.objectType.manuscript,
      objectId: manuscript.id,
    })

    if (peerReviewModel.hasTriageEditor || manuscript.specialIssueId) {
      await useCases.assignTriageEditorOnManuscriptUseCase
        .initialize({
          models: { TeamMember, Team, User },
          getUserForAutoAssignmentBasedOnWorkload,
          manuscriptStatuses: Manuscript.InProgressStatuses,
        })
        .execute({
          manuscriptId: manuscript.id,
          journalId: journal.id,
          sectionId: manuscript.sectionId,
          specialIssueId: manuscript.specialIssueId,
        })
    }

    const hasWorkloadAssignment = peerReviewModel.academicEditorAssignmentTool.includes(
      'workload',
    )

    if (hasWorkloadAssignment) {
      const academicEditor = await getUserForAutoAssignmentBasedOnWorkload({
        models,
        journalId: journal.id,
        teamRole: Team.Role.academicEditor,
        teamMemberStatuses: [
          TeamMember.Statuses.pending,
          TeamMember.Statuses.accepted,
        ],
        manuscriptStatuses: Manuscript.InProgressStatuses,
      })

      if (academicEditor) {
        await inviteAcademicEditorService.execute({
          submissionId: manuscript.submissionId,
          userId: academicEditor.userId,
          hasWorkloadAssignment: true,
        })
      }
    }

    if (approvalEditorRole !== Team.Role.academicEditor) {
      const triageEditor = await TeamMember.findTriageEditor({
        TeamRole: Team.Role,
        journalId: journal.id,
        manuscriptId: manuscript.id,
        sectionId: manuscript.sectionId,
      })
      triageEditor.user = await User.find(triageEditor.userId)
      notificationService.notifyApprovalEditor({
        manuscript,
        submittingAuthor,
        editorialAssistant,
        journalName: journal.name,
        approvalEditor: triageEditor,
      })
    }
  },
})

module.exports = {
  initialize,
  authsomePolicies: [],
}
