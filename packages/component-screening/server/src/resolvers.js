const config = require('config')
const models = require('@pubsweet/models')
const Email = require('@pubsweet/component-email-templating')
const { sendPackage } = require('component-mts-package')
const { logEvent } = require('component-activity-log/server')
const { getModifiedText } = require('component-transform-text')
const { withAuthsomeMiddleware } = require('helper-service')
const urlService = require('./urlService/urlService')
const useCases = require('./use-cases')

const baseUrl = config.get('pubsweet-client.baseUrl')
const unsubscribeSlug = config.get('unsubscribe.url')
const footerText = config.get('emailFooterText.registeredUsers')

const approveEQSNotifications = require('./notifications/approveEQS')
const returnToApprovalEditorNotifications = require('./notifications/returnToApprovalEditor')
const approveEQANotifications = require('./notifications/approveEQA')
const getEmailCopy = require('./notifications/getEmailCopy')
const getProps = require('./notifications/getProps')

const {
  inviteAcademicEditorUseCase,
} = require('component-peer-review/server/src/useCases/inviteAcademicEditor')

const inviteAcademicEditorDependencies = {
  invitations: require('component-peer-review/server/src/invitations/invitations'),
  emailJobs: require('component-peer-review/server/src/emailJobs/academicEditor/emailJobs'),
  removalJobs: require('component-peer-review/server/src/removalJobs/academicEditorRemovalJobs'),
  getProps: require('component-peer-review/server/src/emailPropsService/emailPropsService'),
}

const resolvers = {
  Query: {},
  Mutation: {
    async approveEQS(_, { manuscriptId, input }, ctx) {
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = approveEQSNotifications.initialize({
        Email,
        getEmailCopyService,
        getPropsService,
      })

      // #region inviteAcademicEditor
      const { Manuscript, TeamMember, Job } = models
      const invitationsService = inviteAcademicEditorDependencies.invitations.initialize(
        { Email },
      )
      const getPropsServiceInviteAcademicEditor = inviteAcademicEditorDependencies.getProps.initialize(
        {
          baseUrl,
          urlService,
          footerText,
          unsubscribeSlug,
          getModifiedText,
        },
      )
      const emailJobsService = inviteAcademicEditorDependencies.emailJobs.initialize(
        {
          Job,
          Email,
          logEvent,
          getPropsService: getPropsServiceInviteAcademicEditor,
        },
      )
      const removalJobsService = inviteAcademicEditorDependencies.removalJobs.initialize(
        {
          Job,
          logEvent,
          TeamMember,
          Manuscript,
        },
      )
      const inviteAcademicEditorService = inviteAcademicEditorUseCase.initialize(
        {
          models,
          logEvent,
          invitationsService,
          emailJobsService,
          removalJobsService,
        },
      )
      // #endregion

      return useCases.approveEQSUseCase
        .initialize({
          models,
          logEvent,
          useCases,
          notificationService,
          inviteAcademicEditorService,
        })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async declineEQS(_, { manuscriptId, input }, ctx) {
      return useCases.declineEQSUseCase
        .initialize({ models, logEvent, sendPackage })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async approveEQA(_, { manuscriptId, input }, ctx) {
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = approveEQANotifications.initialize({
        Email,
        getEmailCopyService,
        getPropsService,
      })

      return useCases.approveEQAUseCase
        .initialize({ models, notificationService })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
    async returnToApprovalEditor(_, { manuscriptId, input }, ctx) {
      const getEmailCopyService = getEmailCopy.initialize()
      const getPropsService = getProps.initialize({
        baseUrl,
        urlService,
        footerText,
        unsubscribeSlug,
        getModifiedText,
      })
      const notificationService = returnToApprovalEditorNotifications.initialize(
        {
          Email,
          getEmailCopyService,
          getPropsService,
        },
      )

      return useCases.returnToApprovalEditorUseCase
        .initialize({ models, notificationService })
        .execute({ manuscriptId, input, userId: ctx.user })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
