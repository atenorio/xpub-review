const querystring = require('querystring')

module.exports = {
  getBaseUrl(req) {
    return `${req.protocol}://${req.get('host')}`
  },
  createUrl({ baseUrl, slug, queryParams = null }) {
    if (
      process.env.KEYCLOAK_SERVER_URL &&
      queryParams &&
      queryParams.confirmationToken
    ) {
      const { createRegistrationURL } = require('component-sso')
      return createRegistrationURL(
        `${baseUrl}${slug}?${querystring.encode(queryParams)}`,
      )
    }
    return !queryParams
      ? `${baseUrl}${slug}`
      : `${baseUrl}${slug}?${querystring.encode(queryParams)}`
  },
}
