const initialize = ({ Email, getEmailCopyService, getPropsService }) => ({
  async notifyApprovalEditor({
    manuscript,
    journalName,
    approvalEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const submittingAuthorName = submittingAuthor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      emailType: 'approval-editor-manuscript-approved-after-eqs',
      titleText: `The manuscript titled "${title}" by ${submittingAuthorName}`,
      targetUserName: '',
      journalName,
    })
    const emailProps = getPropsService.getProps({
      type: 'user',
      toUser: approvalEditor,
      subject: `${customId}: New manuscript submitted`,
      paragraph,
      fromEmail: `${journalName} <${editorialAssistantEmail}>`,
      manuscript,
      journalName,
      signatureName: editorialAssistant.getName(),
    })
    emailProps.bodyProps = bodyProps

    const email = new Email(emailProps)
    await email.sendEmail()
  },
})

module.exports = {
  initialize,
}
