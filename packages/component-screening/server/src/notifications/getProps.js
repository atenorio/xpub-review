const { get } = require('lodash')

const initialize = ({
  urlService,
  baseUrl,
  unsubscribeSlug,
  footerText,
  getModifiedText,
}) => ({
  getProps({
    type,
    toUser,
    subject,
    paragraph,
    fromEmail,
    manuscript,
    journalName,
    signatureName,
  }) {
    return {
      type,
      toUser: {
        email: get(toUser, 'alias.email', ''),
        name: get(toUser, 'alias.surname', ''),
      },
      fromEmail,
      content: {
        subject,
        paragraph,
        signatureName,
        signatureJournal: journalName,
        ctaText: 'MANUSCRIPT DETAILS',
        ctaLink: urlService.createUrl({
          baseUrl,
          slug: `/details/${manuscript.submissionId}/${manuscript.id}`,
        }),
        unsubscribeLink: urlService.createUrl({
          baseUrl,
          slug: unsubscribeSlug,
          queryParams: {
            id: toUser.userId,
            token: get(toUser, 'user.unsubscribeToken'),
          },
        }),
        footerText: getModifiedText(footerText, {
          pattern: '{recipientEmail}',
          replacement: get(toUser, 'alias.email', ''),
        }),
      },
    }
  },
})

module.exports = { initialize }
