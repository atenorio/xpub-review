const initialize = ({ Email, getEmailCopyService, getPropsService }) => ({
  async notifyAuthors({
    authors,
    manuscript,
    journalName,
    approvalEditor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const approvalEditorName = approvalEditor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      targetUserName: approvalEditorName,
      emailType: 'author-manuscript-approved-by-eqa',
      titleText: title,
    })

    authors.forEach(async author => {
      const emailProps = getPropsService.getProps({
        type: 'user',
        toUser: author,
        subject: `${customId}: Manuscript accepted`,
        paragraph,
        fromEmail: `${approvalEditorName} <${editorialAssistantEmail}>`,
        manuscript,
        journalName,
        signatureName: approvalEditorName,
      })
      emailProps.bodyProps = bodyProps

      const email = new Email(emailProps)
      return email.sendEmail()
    })
  },
  async notifyReviewers({
    reviewers,
    manuscript,
    journalName,
    approvalEditor,
    submittingAuthor,
    editorialAssistant,
  }) {
    const { customId, title } = manuscript
    const editorialAssistantEmail = editorialAssistant.getEmail()
    const submittingAuthorName = submittingAuthor.getName()
    const approvalEditorName = approvalEditor.getName()

    const { paragraph, ...bodyProps } = getEmailCopyService.getEmailCopy({
      journalName,
      emailType: 'reviewer-manuscript-approved-by-eqa',
      titleText: `the manuscript titled "${title}" by ${submittingAuthorName}`,
    })

    reviewers.forEach(async reviewer => {
      const emailProps = getPropsService.getProps({
        type: 'user',
        toUser: reviewer,
        subject: `${customId}: A manuscript you reviewed has been accepted`,
        paragraph,
        fromEmail: `${approvalEditorName} <${editorialAssistantEmail}>`,
        manuscript,
        journalName,
        signatureName: approvalEditorName,
      })
      emailProps.bodyProps = bodyProps

      const email = new Email(emailProps)
      return email.sendEmail()
    })
  },
})

module.exports = {
  initialize,
}
