import React from 'react'

import KeycloakContext from './KeycloakContext'

function KeycloakProvider({ children, keycloak }) {
  return (
    <KeycloakContext.Provider value={keycloak}>
      {children}
    </KeycloakContext.Provider>
  )
}

export default KeycloakProvider
