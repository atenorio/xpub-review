import Keycloak from 'keycloak-js'

const init = (config, onSuccess, onError) => {
  const { authServerURL, realm, clientID } = config

  const keycloak = Keycloak({
    url: authServerURL,
    clientId: clientID,
    realm,
  })

  keycloak
    .init({ onLoad: 'check-sso' })
    .success(() => {
      onSuccess(keycloak)
    })
    .error(error => {
      onError(error)
    })
}

export default {
  init,
}
