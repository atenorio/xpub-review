const config = require('config')
const jwt = require('jsonwebtoken')
const jwksClient = require('jwks-rsa')
const BearerStrategy = require('passport-http-bearer').Strategy

const { checkUserIdentity } = require('./keycloakService')

const client = jwksClient({
  cache: true,
  jwksUri: config.get('keycloak.certs'),
})

const getKey = (header, callback) => {
  client.getSigningKey(header.kid, (err, key) => {
    if (err) {
      callback(err)
    }
    const signingKey = key.publicKey || key.rsaPublicKey
    callback(null, signingKey)
  })
}

const verifyToken = (token, done) =>
  jwt.verify(
    token,
    getKey,
    {
      algorithms: ['RS256'],
    },
    async (err, decoded) => {
      if (err || !decoded) return done(null)
      const { sub, username } = decoded

      const user = await checkUserIdentity(decoded)

      return done(null, user.userId || sub, {
        username,
        id: user.userId || sub,
        token,
      })
    },
  )

const useKeycloakBearer = () => app => {
  if (!process.env.KEYCLOAK_SERVER_URL) {
    return
  }
  const { passport } = app.locals
  passport.use('keycloakBearer', new BearerStrategy(verifyToken))

  app.use(
    '/graphql',
    passport.authenticate(['bearer', 'keycloakBearer', 'anonymous'], {
      session: false,
    }),
  )
}

module.exports = useKeycloakBearer
