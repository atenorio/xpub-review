const config = require('config')
const logger = require('@pubsweet/logger')
const { User, Identity } = require('@pubsweet/models')
const KeycloakAdminClient = require('keycloak-admin').default

const {
  public: { authServerURL, realm, clientID },
  admin: { username, password },
} = config.get('keycloak')

const createSSOUser = async profile => {
  const kcAdminClient = new KeycloakAdminClient({
    baseUrl: authServerURL,
    realmName: 'master',
  })
  await kcAdminClient.auth({
    username,
    password,
    grantType: 'password',
    clientId: 'admin-cli',
  })
  kcAdminClient.setConfig({
    realmName: realm,
  })
  const { user, identity } = profile

  logger.info('Creating keycloak user for: ', identity.email)
  return kcAdminClient.users
    .create({
      username: identity.email,
      email: identity.email,
      enabled: user.isActive,
      emailVerified: false,
      firstName: identity.givenNames,
      lastName: identity.surname,
      requiredActions: ['UPDATE_PASSWORD'],
      attributes: {
        country: identity.country,
        affiliation: identity.aff,
        title: identity.title,
      },
    })
    .then(kcUser => {
      logger.info('Sending keycloak verify email to: ', identity.email)
      kcAdminClient.users
        .sendVerifyEmail({
          id: kcUser.id,
          clientId: clientID,
          redirectUri: process.env.HOSTNAME,
        })
        .catch(err => logger.error('Keycloak email: ', err, err.message))
    })
    .catch(err => logger.error('Keycloak: ', err, err.message))
}

const createDBUser = async profile => {
  logger.info('Creating user from keycloak for', profile.email)

  const user = new User({
    id: profile.sub,
    defaultIdentity: 'local',
    isActive: true,
    agreeTc: true,
  })

  const identity = new Identity({
    type: 'local',
    userId: profile.sub,
    isConfirmed: profile.email_verified,
    email: profile.email,
    aff: profile.affiliation,
    country: profile.country,
    surname: profile.family_name,
    givenNames: profile.given_name,
    title: profile.title,
  })

  await user.assignIdentity(identity)

  return user.saveRecursively()
}

const confirmIdentity = async identity => {
  logger.info('Confirming local identity for', identity.email)

  identity.isConfirmed = true
  await identity.save()

  return identity
}

const createRegistrationURL = (redirect_uri = process.env.HOSTNAME) =>
  `${authServerURL}/realms/${realm}/protocol/openid-connect/registrations?client_id=${clientID}&redirect_uri=${encodeURIComponent(
    redirect_uri,
  )}&response_mode=fragment&response_type=code&scope=openid`

const checkUserIdentity = async profile => {
  const identities = await Identity.findOneByEmail(profile.email)
  if (identities) {
    if (profile.email_verified && !identities.isConfirmed) {
      await confirmIdentity(identities)
    }
    return identities
  }

  await createDBUser(profile)
  return profile
}

module.exports = {
  createSSOUser,
  createDBUser,
  createRegistrationURL,
  checkUserIdentity,
}
