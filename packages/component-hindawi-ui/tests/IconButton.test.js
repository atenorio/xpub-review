import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import IconButton from '../src/IconButton'

describe('IconButton', () => {
  afterEach(cleanup)

  it('should be defined', () => {
    expect(IconButton).toBeDefined()
  })

  describe('onClick', () => {
    it('should trigger onClick', () => {
      const doSomething = jest.fn()
      const { getByTestId } = render(
        <IconButton
          data-test-id="my-button"
          icon="eye"
          onClick={doSomething}
        />,
      )
      fireEvent.click(getByTestId('my-button'))

      expect(doSomething).toHaveBeenCalledTimes(1)
    })

    it('should not trigger onClick if button is disabled', () => {
      const doSomething = jest.fn()
      const { getByTestId } = render(
        <IconButton
          data-test-id="my-button"
          disabled
          icon="eye"
          onClick={doSomething}
        />,
      )
      fireEvent.click(getByTestId('my-button'))

      expect(doSomething).not.toHaveBeenCalled()
    })
  })
})
