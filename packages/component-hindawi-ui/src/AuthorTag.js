import React from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

import { Tag, Text, heightHelper } from '../'

const AuthorTag = ({
  author: {
    id,
    alias: {
      aff,
      email,
      country,
      name: { surname, givenNames },
    },
    affiliationNumber,
    isSubmitting,
    isCorresponding,
  },
}) => (
  <Root data-test-id={`author-tag-${id}`}>
    <Text>{`${givenNames} ${surname}`}</Text>
    {isSubmitting && <Tag ml={1}>SA</Tag>}
    {isCorresponding && (
      <Tag ml={1} mr={1}>
        CA
      </Tag>
    )}
    {affiliationNumber && <Superscript>{affiliationNumber}</Superscript>}
  </Root>
)

export default AuthorTag

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;

  ${heightHelper};
`
const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`
// #endregion
