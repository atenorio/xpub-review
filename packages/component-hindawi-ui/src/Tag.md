A corresponding author.

```js
<Tag>CA</Tag>
```

A Triage Editor.

```js
<Tag>TriageEditor</Tag>
```

A tag used for a manuscript status.

```js
<Tag status>Pending Approval</Tag>
```
