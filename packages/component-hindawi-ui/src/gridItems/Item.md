An item. By default the content is displayed in a row.

```js
<Item>
  <span>I am on the left side</span>
  <span>I am on the right side</span>
</Item>
```

All items are wrapped into a flex container

```js
<Item flex={1}>
  <div>i m the only flex container</div>
</Item>
```

Displayed in a column.

```js
<Item vertical>
  <span>I am on top</span>
  <span>I am at the bottom</span>
</Item>
```

Items are aligned in their containing block

```js
<Item alignItems="center" vertical>
  <div>I m the first item</div>
  <div>I m the second item</div>
  <div>I m the third item</div>
</Item>
```

Long items wrap on the next row

```js
<Item flexWrap="wrap">
  <div>wrap us together please, i m first, and i will be the longest to prove my point</div>
  <div>wrap us together please, i m second</div>
  <div>wrap us together please, i m last</div>
</Item>
```

Adds spaces between items

```js
<Item justify="space-between">
  <div>group us from the left, i m first</div>
  <div>group us from the left, i m second</div>
  <div>group us from the left, i m last</div>
</Item>
```
