import { useState } from 'react'
import { withStateHandlers } from 'recompose'

export const withSteps = withStateHandlers(
  {
    step: 0,
  },
  {
    nextStep: ({ step }) => () => ({
      step: step + 1,
    }),
    prevStep: ({ step }) => () => ({
      step: Math.max(0, step - 1),
    }),
  },
)

export const useSteps = (initialValue = 0) => {
  const [step, setStep] = useState(initialValue)

  const nextStep = () => {
    setStep(step => step + 1)
  }

  const prevStep = () => {
    setStep(step => Math.max(0, step - 1))
  }

  return {
    step,
    setStep,
    nextStep,
    prevStep,
  }
}
