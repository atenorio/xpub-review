import { withProps } from 'recompose'
import countries from 'country-list'

const countryMapper = (c = 'GB') => {
  switch (c) {
    case 'GB':
      return 'UK'
    case 'RS':
      return 'SRB'
    case 'ME':
      return 'MNT'
    default:
      return c
  }
}

const codeMapper = (c = '') => {
  switch (c) {
    case 'UK':
      return 'GB'
    case 'SRB':
      return 'RS'
    case 'MNT':
      return 'ME'
    default:
      return c
  }
}

const countryList = countries
  .getNames()
  .map(c => ({ value: countryMapper(countries.getCode(c)), label: c }))

export default withProps(() => ({
  countries: countryList,
  countryLabel: code => countries.getName(codeMapper(code)),
}))
