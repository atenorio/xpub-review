import { get } from 'lodash'
import { withJournal } from 'xpub-journal'
import { compose, withProps } from 'recompose'

const roles = {
  user: 'User',
  admin: 'Admin',
  triageEditor: 'Chief Editor',
  academicEditor: 'Academic Editor',
  editorialAssistant: 'Editorial Assistant',
}
export default compose(
  withJournal,
  withProps(({ journal }) => ({
    titles: get(journal, 'titles', []),
    roles: Object.entries(roles).reduce(
      (acc, el) => [
        ...acc,
        {
          value: el[0],
          label: el[1],
        },
      ],
      [],
    ),
  })),
)
