/* eslint-disable react/require-default-props */
/* eslint-disable react/prefer-stateless-function */

import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { Icon } from '@pubsweet/ui'
import { space } from 'styled-system'
import styled from 'styled-components'
import { th, override } from '@pubsweet/ui-toolkit'

const HeaderComponent = ({ icon, label, toggle, expanded, ...props }) => (
  <Header expanded={expanded} onClick={toggle} {...props}>
    <HeaderIcon expanded={expanded}>
      <Icon primary size={3}>
        {icon}
      </Icon>
    </HeaderIcon>
    <HeaderLabel>{label}</HeaderLabel>
  </Header>
)

class Accordion extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      expanded: get(props, 'startExpanded', false),
    }
  }

  componentDidUpdate(prevProps) {
    const shouldScroll = !prevProps.expanded && this.props.expanded

    if (this.props.scrollIntoView && shouldScroll) {
      this._accordion.scrollIntoView && this._accordion.scrollIntoView()
    }
  }
  _accordion = null

  toggle = () => {
    this.setState(p => ({ expanded: !p.expanded }))
  }

  render() {
    const {
      children,
      icon = 'chevron_up',
      header: Header = HeaderComponent,
      ...rest
    } = this.props
    const { expanded } = this.state
    return (
      <Root expanded={expanded} ref={r => (this._accordion = r)} {...rest}>
        <Header
          expanded={expanded}
          icon={icon}
          toggle={this.toggle}
          {...rest}
        />
        {expanded && children}
      </Root>
    )
  }
}

Accordion.propTypes = {
  /** Header icon, from the [Feather](https://feathericons.com/) icon set. */
  icon: PropTypes.string,
  /** Initial state of the accordion. */
  startExpanded: PropTypes.bool,
  /** Function called when toggling the accordion. The new state is passed as a paremeter. */
  onToggle: PropTypes.func,
}

Accordion.defaultProps = {
  onToggle: null,
  startExpanded: false,
}

export default Accordion

// #region styles
const Root = styled.div`
  display: flex;
  flex-direction: column;
  transition: all ${th('transitionDuration')};

  ${space};
  ${override('ui.Accordion')};
`

const Header = styled.div`
  align-items: center;
  cursor: pointer;
  display: flex;
  justify-content: flex-start;

  ${override('ui.Accordion.Header')};
`

const HeaderLabel = styled.span`
  color: ${th('colorPrimary')};
  font-family: ${th('fontHeading')};
  font-size: ${th('fontSizeBase')};

  ${override('ui.Accordion.Header.Label')};
`

const HeaderIcon = styled.div`
  align-items: center;
  display: flex;
  justify-content: center;

  transform: ${({ expanded }) => `rotateZ(${expanded ? 0 : 180}deg)`};
  transition: transform ${th('transitionDuration')};

  ${override('ui.Accordion.Header.Icon')};
`
// #endregion
