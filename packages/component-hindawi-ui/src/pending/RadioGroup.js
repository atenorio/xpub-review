import React from 'react'
import { Radio } from '@pubsweet/ui'

import { Row } from '../../'

const RadioGroup = ({ options, ...rest }) => (
  <Row justify="flex-start">
    {options.map(o => (
      <Radio
        checked={o.checked}
        inline
        key={o.value}
        label={o.label}
        mr={5}
        {...rest}
        value={o.value}
      />
    ))}
  </Row>
)

export default RadioGroup
