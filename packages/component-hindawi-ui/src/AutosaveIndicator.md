Display the status of the form (Saving in progress, Saved or Error while saving)

Saving in progress. The indicator will be hidden after a certain delay.

```js
const autosave = {
  isFetching: true
};
 
<AutosaveIndicator delay={4000} autosave={autosave} />
```

Changes saved.

```js
const autosave = {
  isFetching: false,
  lastUpdate: new Date(),
};

<AutosaveIndicator autosave={autosave} />
```

Error saving changes.

```js
const autosave = {
  isFetching: false,
  error: 'Something went wrong. Please try again.',
};

<AutosaveIndicator autosave={autosave} />
```