A clickable text button.

```js
<ActionLink onClick={() => console.log('I am clicked.')}>
  Default action
</ActionLink>
```

A disabled text buton.

```js
<ActionLink disabled onClick={() => console.log('i am not called')}>
  Disabled
</ActionLink>
```

A navigation link.

```js
<ActionLink disabled to="https://www.google.com">
  Icon on the right
</ActionLink>
```
