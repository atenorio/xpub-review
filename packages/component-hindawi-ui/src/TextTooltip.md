Wrap component with simple tooltip

```js
const title = 'β-Carboline Silver Compound Binding Studies with Human Serum Albumin: A Comprehensive Multispectroscopic Analysis and Molecular Modeling Study'

;<TextTooltip title={title}>
  <div>{title}</div>
</TextTooltip>
```
