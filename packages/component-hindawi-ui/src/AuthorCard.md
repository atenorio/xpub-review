A component that shows details about an author. It has two modes: a presentation mode and an edit mode. This component can be a part of a submission wizard as well as in a sortable list.

An author card.

```js
const author = {
  id: '456',
  alias: {
    aff: 'Mormon Church',
    email: 'that.nigerian.prince@niger.com',
    country: 'RO',
    name: {
      givenNames: 'Nairobi',
      surname: 'Mogadishu',
    },
  },
}
;<div>
  <AuthorCard
    onDeleteAuthor={(author, props) => {
      console.log({ author, props })
    }}
    isSubmitting
    index={0}
    item={author}
    onEdit={e => console.log('click on edit', e)}
    onSaveAuthor={(values, props) => {
      console.log({ values, props })
    }}
    setCorresponding={(author, props) => console.log({ author, props })}
  />
  <AuthorCard
    onDeleteAuthor={(author, props) => {
      console.log({ author, props })
    }}
    index={1}
    item={author}
    onEdit={() => console.log('click on edit')}
    onSaveAuthor={(values, props) => console.log(values, props)}
  />
  <AuthorCard
    onDeleteAuthor={(author, props) => {
      console.log({ author, props })
    }}
    index={2}
    item={author}
    isSubmitting
    onEdit={() => console.log('click on edit')}
    onSaveAuthor={(values, props) => console.log({ values, props })}
  />
</div>
```

Author card with drag handle (used for sortable lists).

```jsx
import DragHandle from './DragHandle'

const author = {
  id: '456',
  alias: {
    aff: 'Mormon Church',
    email: 'that.nigerian.prince@niger.com',
    country: 'RO',
    name: {
      givenNames: 'Nairobi',
      surname: 'Mogadishu',
    },
  },
}

;<AuthorCard
  item={author}
  dragHandle={<DragHandle />}
  onDeleteAuthor={(author, props) => () => {
    console.log({ author, props })
  }}
/>
```
