A multi action modal.

```js
<MultiAction
  title="Ready to Submit your Report?"
  subtitle="Once submited, the report can’t be modified."
  confirmText="SUBMIT REPORT"
  cancelText="NOT YET"
  onCancel={() => console.log('i am canceling')}
  onConfirm={() => console.log('i am confirming')}
/>
```

A multi action modal with content.

```js
const content = `<h3>What is Lorem Ipsum?</h3>
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.`;

<MultiAction
  title="Ready to Rumble?"
  subtitle="Once rumbling, you can never stop."
  confirmText="SUBMIT REPORT"
  cancelText="NOT YET"
  content={content}
  onCancel={() => console.log('i am canceling')}
  onConfirm={() => console.log('i am confirming')}
/>
```

A multi action modal with pending API request.

```js
<MultiAction
  title="Ready to Rumble?"
  subtitle="Once submited, the report can’t be modified."
  confirmText="SUBMIT REPORT"
  cancelText="NOT YET"
  isFetching
  onCancel={() => console.log('i am canceling')}
  onConfirm={() => console.log('i am confirming')}
/>
```
