A success modal.

```js
<SingleActionModal
  title="Report submitted"
  onConfirm={() => console.log('confirma')}
/>
```

An error modal.

```js
<SingleActionModal
  error
  title="Error submitting report"
  subtitle="Please try again later."
  onConfirm={() => console.log('confirma')}
/>
```
