An author tag.

```js
const authors = [
  {
    id: 1,
    alias: {
      email: 'john.doe@gmail.com',
    name: {
      surname: 'Doe',
      givenNames: 'John'
      },
    },
    isSubmitting: true,
  },
  {
    id: 2,
    alias: {
      email: 'michael.felps@gmail.com',
    name: {
      surname: 'Felps',
      givenNames: 'Michael',
     },
    },
    isSubmitting: true,
    isCorresponding: true,
    },
  {
    id: 3,
    alias: {
      aff: 'US Presidency',
      email: 'barrack.obama@gmail.com',
    name: {
      surname: 'Obama',
      givenNames: 'Barrack',
      },
    }
  },
];

<div style={{display: 'flex'}}>{authors.map(a => <AuthorTag key={a.email} author={a} />)}</div>
```
