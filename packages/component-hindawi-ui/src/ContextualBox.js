import React from 'react'
import { H3 } from '@pubsweet/ui'
import PropTypes from 'prop-types'
import { isUndefined } from 'lodash'
import styled from 'styled-components'
import { override } from '@pubsweet/ui-toolkit'

import { Icon } from '../'
import Accordion from './pending/Accordion'
import ControlledAccordion from './pending/ControlledAccordion'

const CustomHeader = ({
  label,
  height,
  toggle,
  expanded,
  transparent,
  rightChildren = () => <div />,
  ...props
}) => (
  <Header
    expanded={expanded}
    hasChildren={rightChildren}
    height={height}
    onClick={toggle}
    transparent={transparent}
  >
    <HeaderContent>
      <Icon
        color="colorSecondary"
        fontSize="12px"
        icon={expanded ? 'collapse' : 'expand'}
        ml={2}
        mr={2}
      />
      <H3>{label}</H3>
    </HeaderContent>
    {typeof rightChildren === 'function'
      ? rightChildren({ ...props, expanded, transparent })
      : React.createElement(rightChildren, props)}
  </Header>
)

const ContextualBox = ({
  label,
  children,
  rightChildren,
  toggle,
  expanded,
  ...rest
}) =>
  !isUndefined(expanded) ? (
    <ControlledAccordion
      expanded={expanded}
      header={CustomHeader}
      label={label}
      rightChildren={rightChildren}
      toggle={toggle}
      {...rest}
    >
      {children}
    </ControlledAccordion>
  ) : (
    <Accordion
      header={CustomHeader}
      label={label}
      rightChildren={rightChildren}
      {...rest}
    >
      {children}
    </Accordion>
  )

export default ContextualBox

ContextualBox.propTypes = {
  /** Label of the contextual box. */
  label: PropTypes.string,
  /** Component or html to be rendered on the right side. */
  // rightChildren: PropTypes.any,
  rightChildren: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.func,
    PropTypes.string,
  ]),
  /** The state of the contextual box. If passed from a parent then the component
   * is controlled and can be expanded/collapsed remotely.
   */
  expanded: PropTypes.bool, // eslint-disable-line
  /** Callback function used to control the state of the component.
   * To be used together with the `expanded` prop.
   */
  toggle: PropTypes.func, // eslint-disable-line
}

ContextualBox.defaultProps = {
  label: '',
  rightChildren: undefined,
}

// #region styles
const HeaderContent = styled.div`
  align-items: center;
  display: flex;
  height: inherit;
`

const Header = styled.div`
  align-items: center;
  /* background: ${props => (props.transparent ? 'transparent' : '#fff')}; */
  cursor: pointer;
  display: flex;
  justify-content: ${props =>
    props.hasChildren ? 'space-between' : 'flex-start'};

  ${override('ui.Accordion.Header')}; 
`
// #endregion
