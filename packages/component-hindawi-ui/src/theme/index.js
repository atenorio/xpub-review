import './fonts/index.css'
import './fonts/icons.css'

import {
  Icon,
  Menu,
  Steps,
  Radio,
  Button,
  Action,
  Heading,
  Checkbox,
  TextField,
  Accordion,
  ValidatedTextField,
} from './elements'

import oldTheme from './oldTheme'

function generateSpaces(gridUnit = 4) {
  return Array.from({ length: 200 }, (v, i) => i * gridUnit)
}

// all values are in gridUnits
// fonts and lineheights are the only exceptions
const colors = {
  actionPrimaryColor: '#81BA40',
  actionSecondaryColor: '#828282',
  backgroundColor: '#F5F5F5',
  contrastGrayColor: '#4F4F4F',
  furnitureColor: '#E0E0E0',
  infoColor: '#FCB74B',
  labelColor: '#F5F5F5',
  labelLineColor: '#828282',
  colorBorder: '#DBDBDB',
  mainTextColor: '#333',
  textPrimaryColor: '#333333',
  textSecondaryColor: '#4F4F4F',
  transparent: 'transparent',
  warningColor: '#FC6A4B',
  white: '#FFFFFF',
  statusPending: '#FF5547',
  statusInvite: '#FCB624',
  statusApproved: '#1BD2AD',
  statusRejected: '#FC6A4B',
  statusInProgress: '#FCB74B',
  statusWithdrawn: '#828282',
  highlightColor: '#F5F5F5',
  disabledColor: '#C9C9C9',
}

const sizes = {
  borderRadius: '4px',
  h1Size: '30px',
  h1LineHeight: '37px',
  h2Size: '24px',
  h2LineHeight: '29px',
  h3Size: '16px',
  h3LineHeight: '18px',
  mainTextSize: '14px',
  mainTextLineHeight: '18px',
  secondaryTextSize: '14px',
  secondaryTextLineHeight: '17px',
}

const buttons = {
  borderSize: '2px',
  small: {
    height: 6,
    fontSize: '12px',
    fontWeight: 700,
    lineHeight: '14px',
    minWidth: 20,
  },
  medium: {
    height: 8,
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '15px',
    minWidth: 24,
  },
  default: {
    height: 10,
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '18px',
    minWidth: 30,
  },
  xLarge: {
    height: 10,
    fontSize: '14px',
    fontWeight: 700,
    lineHeight: '18px',
    minWidth: 48,
  },
  primary: {
    backgroundColor: colors.actionPrimaryColor,
    color: colors.white,
    disabledBgColor: colors.furnitureColor,
    fontSize: sizes.h4Size,
    lineHeight: sizes.h4LineHeight,
  },
  secondary: {
    backgroundColor: colors.transparent,
    borderColor: colors.labelColor,
    color: colors.labelColor,
    disabledBorderColor: colors.furnitureColor,
    disabledColor: colors.furnitureColor,
    fontSize: sizes.h4Size,
    lineHeight: sizes.h4LineHeight,
  },
}

const shadows = {
  shadowMedium: '3px 4px 10px 1px rgba(51, 51, 51, 0.18)',
}

const text = {
  textSecondaryColor: colors.textSecondaryColor,
  textPrimaryColor: colors.textPrimaryColor,
  warningColor: colors.warningColor,
  furnitureColor: colors.furnitureColor,

  fontSizeBase: '14px',
  fontSizeBaseMedium: '13px',
  fontSizeBaseSmall: '11px',
}

const zIndex = {
  appBar: '200',
  modal: '300',
  select: '400',
  datePicker: '300',
}

export default {
  ...oldTheme,
  buttons,
  text,
  zIndex,
  shadows,
  gridUnit: '4px',
  lightenPercent: 20,
  defaultFont: 'Nunito',
  ...colors,
  ...sizes,
  // properties used by styled-system
  space: generateSpaces(),
  // override pubsweet components
  cssOverrides: {
    ui: Object.assign(
      {
        Icon,
        Menu,
        MenuCountry: Menu,
        Radio,
        Steps,
        Action,
        Button,
        Checkbox,
        Accordion,
        TextField,
        ValidatedTextField,
      },
      Heading,
    ),
  },
}
