import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const checkIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16">
  <path fill="#63A945" fill-rule="evenodd" d="M8 4C5.792 4 4 5.792 4 8s1.792 4 4 4 4-1.792 4-4-1.792-4-4-4zm0-4C3.584 0 0 3.584 0 8s3.584 8 8 8 8-3.584 8-8-3.584-8-8-8zm0 14.4A6.398 6.398 0 0 1 1.6 8c0-3.536 2.864-6.4 6.4-6.4 3.536 0 6.4 2.864 6.4 6.4 0 3.536-2.864 6.4-6.4 6.4z"/>
</svg>`

export default {
  Root: css`
    font-family: ${th('defaultFont')};
    min-height: unset;

    &::before {
      display: none;
    }
    
    input:checked + span::before {
      border-color: ${th('actionPrimaryColor')};
      background: url('data:image/svg+xml;utf8,${encodeURIComponent(
        checkIcon,
      )}') center no-repeat;
    }
  `,
  Input: css`
    opacity: 0;
    margin-left: calc(${th('gridUnit')} * -4);
  `,
  Label: css`
    color: ${th('textPrimaryColor')};
    font-family: ${th('defaultFont')};
    margin-right: ${th('gridUnit')};

    &::before {
      content: ' ';
      align-items: center;
      background: transparent;
      border: 1px solid #939393;
      border-radius: 50%;
      display: inline-flex;
      margin-right: ${th('gridUnit')};
      vertical-align: text-bottom;

      height: calc(${th('gridUnit')} * 4);
      width: calc(${th('gridUnit')} * 4);
    }
  `,
}
