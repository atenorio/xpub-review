/* eslint-disable no-nested-ternary */

import { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const checkIcon = `<svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 24 24">
  <path fill="#fff" d="M20.285 2l-11.285 11.567-5.286-5.011-3.714 3.716 9 8.728 15-15.285z"/>
</svg>`

export default {
  Root: css`
    margin: 0 auto;
    margin-top: calc(${th('gridUnit')} * 4);
    width: calc(${th('gridUnit')} * 160);
  `,
  Step: css`
    height: 20px;
    width: 20px;
    box-sizing: border-box;
    border: ${props =>
      props.isPast
        ? 'none'
        : props.isCurrent
        ? '6px solid #dbdbdb'
        : '2px solid #dbdbdb'};
    background-color: ${props =>
      !props.isCurrent && !props.isPast
        ? th('colorBackground')
        : 'transparent'};
    z-index: 10;
  `,
  Separator: css`
    background-color: ${props =>
      props.isPast ? th('actionPrimaryColor') : th('colorBorder')};
    margin: 0 -2px;
    z-index: 1;
  `,
  Bullet: css`
    background-color: ${th('actionPrimaryColor')};
    width: calc(${th('gridUnit')} * 2);
    height: calc(${th('gridUnit')} * 2);
  `,
  Success: css`
    background: url('data:image/svg+xml;utf8,${encodeURIComponent(
      checkIcon,
    )}') center no-repeat, ${th('actionPrimaryColor')};
  `,
  StepTitle: css`
    top: ${props => (props.isCurrent ? '20px' : '25px')};
    color: ${props =>
      props.isCurrent ? th('steps.currentStepColor') : th('colorText')};
    font-weight: ${props =>
      props.isCurrent ? th('steps.activeFontWeight') : 'normal'};
    font-family: ${th('defaultFont')};
    font-size: ${th('fontSizeBase')};
    line-height: ${th('lineHeightBase')};
    width: calc(${th('gridUnit')} * 50);
  `,
}
