import { get } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const tagCSS = props => {
  if (get(props, 'oldStatus')) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 6)
      font-weight: ${th('tag.fontWeight')};
      padding: ${th('gridUnit')} ${th('gridUnit')} 0px
        ${th('gridUnit')};
    `
  }

  if (get(props, `status`)) {
    return css`
      background-color: ${th('tag.statusBackgroundColor')};
      padding: calc(${th('gridUnit')} / 2) ${th('gridUnit')};
      height: calc(${th('gridUnit')} * 6);
      font-family: ${th('defaultFont')};
      display: flex;
      align-items: center;
    `
  }
  if (get(props, `pending`)) {
    return css`
      background-color: ${th('colorFurnitureHue')};
      height: calc(${th('gridUnit')} * 6);
      width: calc(${th('gridUnit')} * 20);
      display: flex;
      justify-content: center;
      align-items: center;
      font-family: ${th('defaultFont')}
      font-size: ${th('button.smallSize')};
      border-radius: ${th('borderRadius')};
    `
  }

  return css`
    background-color: ${th('tag.backgroundColor')};
    height: calc(${th('gridUnit')} * 4);
    font-family: ${th('defaultFont')};
  `
}

/** @component */
const Tag = styled.div`
  border-radius: ${th('tag.borderRadius')
    ? th('tag.borderRadius')
    : th('borderRadius')};
  color: ${th('tag.color')};
  display: initial;
  font-weight: ${th('tag.fontWeight')};
  font-size: ${th('tag.fontSize')};
  padding: 0 ${th('gridUnit')};
  text-align: center;
  white-space: nowrap;
  width: fit-content;

  ${tagCSS};
  ${space};
`

Tag.propTypes = {
  /** Old status of the corresponding user. */
  oldStatus: PropTypes.bool,
  /** New status of the corresponding user. */
  status: PropTypes.bool,
}

Tag.defaultProps = {
  oldStatus: false,
  status: false,
}

/** @component */
export default Tag
