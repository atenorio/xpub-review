import React, { Fragment } from 'react'
import 'react-tippy/dist/tippy.css'
import { Tooltip } from 'react-tippy'
import { ThemeProvider, withTheme } from 'styled-components'

import { AuthorTag, Text, Row } from '../'

const AuthorTooltip = ({ author = {}, key, theme = {} }) => (
  <ThemeProvider theme={theme}>
    <Fragment>
      <Row mt={1}>
        <AuthorTag author={author} key={key} />
      </Row>
      <Row>
        <Text>{author.alias.email}</Text>
      </Row>
      <Row>
        <Text>{author.alias.aff}</Text>
      </Row>
    </Fragment>
  </ThemeProvider>
)

const AuthorWithTooltip = ({ theme = {}, ...rest }) => (
  <Tooltip
    arrow
    html={<AuthorTooltip theme={theme} {...rest} />}
    position="bottom"
    theme="light"
  >
    <AuthorTag {...rest} />
  </Tooltip>
)

export default withTheme(AuthorWithTooltip)
