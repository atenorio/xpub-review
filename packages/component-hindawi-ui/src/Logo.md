A clickable logo. Used for App headers.

```js
<Logo
  onClick={() => console.log('Hindawi best publish!')}
  title="Hindawi"
  src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ca/Hindawi.svg/1200px-Hindawi.svg.png"
/>
```
