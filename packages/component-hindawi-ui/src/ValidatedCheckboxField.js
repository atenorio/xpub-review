import React from 'react'
import { Field } from 'formik'
import { Checkbox } from '@pubsweet/ui'

const ValidatedCheckboxField = ({ name, label }) => (
  <Field name={name}>
    {({ field, form }) => (
      <Checkbox
        checked={field.value}
        label={label}
        onChange={() => {
          form.setFieldValue(field.name, !field.value)
        }}
      />
    )}
  </Field>
)

export default ValidatedCheckboxField
