A tag used for a manuscript old status.

```js
<StatusTag old version={1}>
  Old Version
</StatusTag>
```

A tag used for a manuscript in invite status.

```js
<StatusTag version={1} statusColor="statusInvite">
  Invite AcademicEditor
</StatusTag>
```

A tag used for a manuscript in pending status.

```js
<StatusTag version={1} statusColor="statusPending">
  Pending Approval
</StatusTag>
```

A tag used for a manuscript in approved status.

```js
<StatusTag version={1} statusColor="statusApproved">
  Accepted
</StatusTag>
```

A tag used for a manuscript in withdrawn status.

```js
<StatusTag version={1} statusColor="statusWithdrawn">
  Withdrawn
</StatusTag>
```
