import React, { Fragment } from 'react'
import { get, has } from 'lodash'
import PropTypes from 'prop-types'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import { withProps, compose } from 'recompose'
import styled, { css } from 'styled-components'

import {
  Row,
  Item,
  Icon,
  Text,
  Label,
  PasswordField,
  ValidatedFormField,
  validators,
} from '../'

import {
  minLength,
  atLeastOneDigit,
  atLeastOneUppercase,
  atLeastOneLowerrcase,
  atLeastOnePunctuation,
} from './Utils.js'

const PasswordValidation = ({
  pl,
  pr,
  isValid,
  hasError,
  minLength,
  atLeastOneDigit,
  atLeastOneUppercase,
  atLeastOneLowerrcase,
  atLeastOnePunctuation,
}) => (
  <Fragment>
    <Row height={12} mt={6} pl={pl} pr={pr}>
      <Item vertical>
        <Label required>Password</Label>
        <ValidatedFormField
          component={PasswordField}
          data-test-id="password"
          inline
          name="password"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row height={12} mt={6} pl={pl} pr={pr}>
      <Item vertical>
        <Label required>Retype password</Label>
        <ValidatedFormField
          component={PasswordField}
          data-test-id="confirm-password"
          inline
          name="confirmPassword"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row alignItems="baseline" justify="flex-start" mt={4} pl={pl} pr={pr}>
      <Icon
        bold
        color={isValid ? 'actionPrimaryColor' : 'actionSecondaryColor'}
        fontSize="14px"
        icon="info"
        mr={2}
      />
      <Text secondary>The password must contain:</Text>
    </Row>

    <Rules pl={pl} pr={pr}>
      <Row height={4} justify="flex-start">
        <Rule error={hasError && !minLength} fulfilled={minLength}>
          at least 6 characters
        </Rule>
      </Row>

      <Row height={4} justify="flex-start">
        <Rule
          error={hasError && !atLeastOneUppercase}
          fulfilled={atLeastOneUppercase}
        >
          at least 1 uppercase character (A-Z)
        </Rule>
      </Row>

      <Row height={4} justify="flex-start">
        <Rule
          error={hasError && !atLeastOneLowerrcase}
          fulfilled={atLeastOneLowerrcase}
        >
          at least 1 lowercase character (a-z)
        </Rule>
      </Row>

      <Row height={4} justify="flex-start">
        <Rule error={hasError && !atLeastOneDigit} fulfilled={atLeastOneDigit}>
          at least 1 digit (0-9)
        </Rule>
      </Row>

      <Row height={4} justify="flex-start">
        <Rule
          error={hasError && !atLeastOnePunctuation}
          fulfilled={atLeastOnePunctuation}
        >
          at least 1 special character (punctuation)
        </Rule>
      </Row>
    </Rules>
  </Fragment>
)

PasswordValidation.propTypes = {
  /** If the password requirements are not fulfilled it will return true. */
  hasError: PropTypes.bool,
  /** If the password's length is greater or equal than the minimum value, it will return true. */
  minLength: PropTypes.bool,
  /** If the password has at least one digit it will return true. */
  atLeastOneDigit: PropTypes.bool,
  /** If the password has at least one uppercase it will return true. */
  atLeastOneUppercase: PropTypes.bool,
  /** If the password has at least one lowercase it will return true. */
  atLeastOneLowerrcase: PropTypes.bool,
  /** If the password has at least one punctuation it will return true. */
  atLeastOnePunctuation: PropTypes.bool,
}

PasswordValidation.defaultProps = {
  hasError: false,
  minLength: false,
  atLeastOneDigit: false,
  atLeastOneUppercase: false,
  atLeastOneLowerrcase: false,
  atLeastOnePunctuation: false,
}

export default compose(
  withProps(({ formValues, touched, errors }) => ({
    hasError: get(touched, 'password', false) && has(errors, 'password'),
    minLength: minLength(get(formValues, 'password', ''), 6),
    atLeastOneUppercase: atLeastOneUppercase(get(formValues, 'password', '')),
    atLeastOneLowerrcase: atLeastOneLowerrcase(get(formValues, 'password', '')),
    atLeastOneDigit: atLeastOneDigit(get(formValues, 'password', '')),
    atLeastOnePunctuation: atLeastOnePunctuation(
      get(formValues, 'password', ''),
    ),
  })),
  withProps(
    ({
      hasError,
      minLength,
      atLeastOneUppercase,
      atLeastOneLowerrcase,
      atLeastOneDigit,
      atLeastOnePunctuation,
    }) => ({
      isValid:
        !hasError &&
        minLength &&
        atLeastOneUppercase &&
        atLeastOneLowerrcase &&
        atLeastOneDigit &&
        atLeastOnePunctuation,
    }),
  ),
)(PasswordValidation)

// #region styles
const Rules = styled.div`
  color: ${th('colorText')};
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBaseMedium')};
  line-height: ${th('fontSizeBaseMedium')};

  ${space};
`
const ruleHelper = ({ error, fulfilled, theme }) => {
  const textDecoration = fulfilled ? 'line-through' : 'none'

  let textColor = 'inherit'
  if (error) {
    textColor = get(theme, 'warningColor', 'inherit')
  } else if (fulfilled) {
    textColor = get(theme, 'actionPrimaryColor', 'inherit')
  }

  return css`
    color: ${textColor};
    text-decoration: ${textDecoration};
  `
}

const Rule = styled.p`
  margin: 0;
  ${ruleHelper};
`
// #endregion
