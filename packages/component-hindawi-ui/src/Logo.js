/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */

import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'

const Logo = ({ goTo, src, onClick, title, height, marginLeft, ...rest }) => (
  <Root>
    <img
      alt={title}
      data-test-id={get(rest, 'data-test-id', 'journal-logo')}
      height={height}
      onClick={goTo}
      src={src}
      title={title}
    />
  </Root>
)

Logo.propTypes = {
  /** Height of the logo. */
  height: PropTypes.number,
}

Logo.defaultProps = {
  height: 36,
}
export default Logo

const Root = styled.div`
  margin-left: calc(${th('gridUnit')} * 6);
`
