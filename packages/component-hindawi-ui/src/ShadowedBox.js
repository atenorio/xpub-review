import { isNumber, get } from 'lodash'
import { H2 } from '@pubsweet/ui'
import { space } from 'styled-system'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'

const width = props => css`
  width: calc(${th('gridUnit')} * ${get(props, 'width', 100)});
`

const centered = props => {
  if (props.center) {
    return css`
      margin: 0 auto;
    `
  }
}

export default styled.div.attrs(props => ({
  pt: get(props, 'pt', 4),
  pr: get(props, 'pr', 4),
  pb: get(props, 'pb', 4),
  pl: get(props, 'pl', 4),
}))`
  background-color: ${th('colorBackgroundHue')};
  border-radius: ${th('borderRadius')};
  box-shadow: ${th('boxShadow')};
  display: flex;
  flex: ${({ flex }) => (isNumber(flex) ? flex : 1)};
  flex-direction: column;

  position: ${props => get(props, 'position', 'initial')};

  ${centered};
  ${width};
  ${space};

  ${H2} {
    text-align: center;
  }
`
