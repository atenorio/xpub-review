import React from 'react'
import { get } from 'lodash'
import PropTypes from 'prop-types'
import { th } from '@pubsweet/ui-toolkit'
import styled, { css } from 'styled-components'
import { space, fontWeight } from 'styled-system'

const textHelper = props => {
  if (get(props, 'selected')) {
    return css`
      color: ${th('white')};
    `
  }
  if (get(props, 'secondary')) {
    return css`
      color: ${th('actionSecondaryColor')};
    `
  }
  if (get(props, 'error')) {
    return css`
      color: ${th('text.warningColor')};
    `
  }

  if (get(props, 'alert')) {
    return css`
      color: ${th('warningColor')};
    `
  }

  if (get(props, 'emptyState')) {
    return css`
      color: ${th('text.textSecondaryColor')};
      font-size: ${th('fontSizeBaseMedium')};
      line-height: ${th('lineHeightHeading3')};
    `
  }

  if (get(props, 'customId')) {
    return css`
      color: ${th('actionPrimaryColor')};
      font-size: ${th('fontSizeBaseMedium')};
      font-weight: 700;
    `
  }

  if (get(props, 'labelLine')) {
    return css`
      color: ${th('labelLineColor')};
      display: flex;
      &:after {
        display: block;
        content: ' ';
        border-bottom: 1px solid ${th('colorBorder')};
        flex: 1 1 auto;
        margin-left: calc(${th('gridUnit')} * 4);
      }
    `
  }

  if (get(props, 'journal')) {
    return css`
      color: ${th('text.textSecondaryColor')};
      &:before {
        content: '•';
        padding-right: ${th('gridUnit')};
      }
    `
  }

  if (get(props, 'invited')) {
    return css`
      color: ${th('statusInvite')};
    `
  }

  return css`
    color: ${th('mainTextColor')};
    line-height: ${get(props, 'lineHeight', 1)};
  `
}

const fontSize = props => {
  if (get(props, 'small')) {
    return css`
      font-size: ${th('fontSizeBaseSmall')};
    `
  }
  if (get(props, 'medium')) {
    return css`
      font-size: ${th('fontSizeBaseMedium')};
    `
  }
  return css`
    font-size: ${th('mainTextSize')};
    line-height: ${th('mainTextLineHeight')};
  `
}

const ellipsis = props => {
  if (props.ellipsis) {
    return css`
      text-overflow: ellipsis;
      overflow: hidden;
      white-space: nowrap;
    `
  }

  return css`
    white-space: ${props => get(props, 'whiteSpace', 'initial')};
  `
}

const StyledText = styled.span`
  align-items: center;
  display: ${props => get(props, 'display', 'inline-block')};
  font-family: ${th('defaultFont')};
  font-style: ${props => get(props, 'fontStyle', 'normal')};
  height: ${props => get(props, 'height', 'initial')};
  text-align: ${props => get(props, 'align', 'start')};
  opacity: ${props => (props.opacity ? props.opacity : '1')};

  ${fontSize};
  ${ellipsis};
  ${textHelper};
  ${space}
  ${fontWeight};
`

const Text = ({ children, ...rest }) => (
  <StyledText {...rest}>{children}</StyledText>
)

Text.propTypes = {
  /** Default color for non-primary actions. */
  secondary: PropTypes.bool,
  /** Default color for error actions. */
  error: PropTypes.bool,
  /** Default style for the customId text. */
  customId: PropTypes.bool,
  /** Default style for text used as a label Line. */
  labelLine: PropTypes.bool,
  /** Default style used for journal text. */
  journal: PropTypes.bool,
  /** Default style used for small text. */
  small: PropTypes.bool,
}

Text.defaultProps = {
  secondary: false,
  error: false,
  customId: false,
  labelLine: false,
  journal: false,
  small: false,
}

export default Text
