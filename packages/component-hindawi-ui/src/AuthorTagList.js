import React from 'react'
import { get } from 'lodash'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withProps, withStateHandlers } from 'recompose'

import {
  Row,
  Icon,
  Item,
  Text,
  AuthorTag,
  ActionLink,
  AuthorWithTooltip,
} from '../'

const parseAffiliations = (authors = []) =>
  authors.reduce(
    (acc, curr) => {
      if (acc.affiliations.includes(curr.alias.aff)) {
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.alias.aff) + 1,
          },
        ]
      } else {
        acc.affiliations = [...acc.affiliations, curr.alias.aff]
        acc.authors = [
          ...acc.authors,
          {
            ...curr,
            affiliationNumber:
              acc.affiliations.findIndex(e => e === curr.alias.aff) + 1,
          },
        ]
      }
      return acc
    },
    { affiliations: [], authors: [] },
  )

const AuthorTagList = ({
  authors,
  affiliationList,
  separator,
  authorKey = 'id',
  withTooltip,
  withAffiliations,
  showAffiliation,
  toggleAffiliation,
}) => (
  <Root>
    {authors
      .map(a =>
        withTooltip ? (
          <AuthorWithTooltip author={a} key={get(a, authorKey, '')} />
        ) : (
          <AuthorTag author={a} key={get(a, authorKey, '')} />
        ),
      )
      .reduce(
        (prev, curr, index) =>
          index === 0 ? [prev, curr] : [prev, separator, curr],
        [],
      )}
    {withAffiliations && (
      <ActionLink
        data-test-id="author-affiliations"
        ml={1}
        onClick={toggleAffiliation}
      >
        <Icon
          bold
          color="colorSecondary"
          fontSize="10px"
          icon={showAffiliation ? 'collapse' : 'expand'}
          mr={1}
        />
        <Text fontWeight={600} secondary>
          {showAffiliation ? 'Hide Affiliations' : 'Show Affiliations'}
        </Text>
      </ActionLink>
    )}

    {withAffiliations && showAffiliation && (
      <AffiliationColumn mt={1}>
        {affiliationList.map((aff, i) => (
          <Item data-test-id={`affiliation-${i + 1}`} flex={1} key={aff}>
            <Text>{aff}</Text>
            <Superscript>{i + 1}</Superscript>
          </Item>
        ))}
      </AffiliationColumn>
    )}
  </Root>
)
export default compose(
  withStateHandlers(
    { showAffiliation: false },
    {
      toggleAffiliation: ({ showAffiliation }) => () => ({
        showAffiliation: !showAffiliation,
      }),
    },
  ),
  withProps(({ authors = [] }) => ({
    parsedAffiliations: parseAffiliations(authors),
  })),
  withProps(({ authors = [], withAffiliations, parsedAffiliations }) => ({
    authors: withAffiliations
      ? get(parsedAffiliations, 'authors', [])
      : authors,
    affiliationList:
      withAffiliations && get(parsedAffiliations, 'affiliations', []),
  })),
)(AuthorTagList)

AuthorTagList.defaultProps = {
  authorKey: 'id',
  authors: [],
  separator: `,\u2004`,
  withTooltip: false,
  withAffiliations: false,
}

// #region styles
const Root = styled.div`
  align-items: center;
  display: flex;
  flex-flow: row wrap;

  font-family: ${th('defaultFont')};
`
const AffiliationColumn = styled(Row)`
  align-items: flex-start;
  flex-direction: column;
`
const Superscript = styled.span`
  position: relative;
  top: -0.5em;
  font-size: 80%;
  font-family: ${th('defaultFont')};
`
// #endregion
