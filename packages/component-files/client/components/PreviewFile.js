import React from 'react'
import { last } from 'lodash'
import { Icon } from '@hindawi/ui'
import { withProps, withHandlers, compose } from 'recompose'

import { withSignedUrl } from '../graphql'

const hasPreview = (originalName = '') => {
  const extension = last(originalName.split('.')).toLocaleLowerCase()
  return ['pdf', 'png', 'jpg'].includes(extension)
}

const PreviewFile = ({ onPreview, hasPreview }) =>
  hasPreview ? (
    <Icon
      color="colorSecondary"
      fontSize="16px"
      icon="preview"
      ml={1}
      onClick={onPreview}
    />
  ) : null

export default compose(
  withSignedUrl,
  withProps(({ file: { originalName } = {} }) => ({
    hasPreview: hasPreview(originalName),
  })),
  withHandlers({
    onPreview: ({ getSignedUrl, file }) => () => {
      getSignedUrl(file.id).then(r => {
        window.open(r.data.getSignedUrl)
      })
    },
  }),
)(PreviewFile)
