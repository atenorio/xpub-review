import {
  parseFileSize,
  fileHasPreview,
  getAllowedMimeTypes,
  getSupportedFileFormats,
} from '../fileUtils'

describe('File utils', () => {
  it('has preview for the correct mimeTypes ', () => {
    const files = [
      {
        mimeType: 'application/pdf',
      },
      {
        mimeType: 'text/plain',
      },
      {
        mimeType: 'image/jpeg',
      },
      { mimeType: 'application/rdf+xml' },
      { mimeType: 'application/vnd.oasis.opendocument.text' },
      { mimeType: 'application/msword' },
      {
        mimeType:
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
      },
    ]

    const result = files.map(fileHasPreview)
    expect(result).toEqual([true, true, true, false, false, false, false])
  })

  it('gets the allowed mimeTypes', () => {
    const allowedFileExtensions = ['jpeg', 'pdf', 'odt']

    const result = getAllowedMimeTypes(allowedFileExtensions)
    expect(result).toEqual([
      'image/jpeg',
      'application/pdf',
      'application/vnd.oasis.opendocument.text',
    ])

    const emptyResult = getAllowedMimeTypes([])
    expect(emptyResult).toEqual([])
  })

  it('gets the supported file formats', () => {
    const allowedFileExtensions = ['jpeg', 'pdf', 'odt']

    const result = getSupportedFileFormats(allowedFileExtensions)
    expect(result).toEqual('JPEG, PDF, ODT')

    const emptyResult = getSupportedFileFormats([])
    expect(emptyResult).toEqual('')
  })

  it('parses the file size', () => {
    const files = [
      { size: 10 },
      { size: 1000 },
      { size: 100000 },
      { size: 10000000 },
      { size: 10000000000 },
    ]

    const result = files.map(parseFileSize)
    expect(result).toEqual(['10 bytes', '1 kB', '100 kB', '10 MB', '10 GB'])
  })
})
