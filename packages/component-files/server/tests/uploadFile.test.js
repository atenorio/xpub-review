const useCases = require('../src/use-cases')

const { uploadManuscriptFile } = useCases

const { decomposeFilename, getFileNumber } = uploadManuscriptFile

describe('decomposeFilename utility function', () => {
  it('should return empty strings for undefined', () => {
    const res = decomposeFilename(undefined)
    expect(res).toEqual(['', ''])
  })
  it('should return correct extensions', () => {
    expect(decomposeFilename('test.pdf')).toEqual(['test', 'pdf'])
    expect(decomposeFilename('test.pdf.docx')).toEqual(['test.pdf', 'docx'])
    expect(decomposeFilename('test...docx')).toEqual(['test..', 'docx'])
    expect(decomposeFilename('test.')).toEqual(['test', ''])
  })
  it('should return correct names', () => {
    expect(decomposeFilename('test.pdf')).toEqual(['test', 'pdf'])
    expect(decomposeFilename('test(2019).pdf')).toEqual(['test(2019)', 'pdf'])
    expect(decomposeFilename('test())).pdf')).toEqual(['test()))', 'pdf'])
    expect(decomposeFilename('.pdf')).toEqual(['', 'pdf'])
  })
})

describe('getFileNumber utility function', () => {
  it('should return correct number', () => {
    expect(
      getFileNumber({
        originalName: 'test.pdf',
        fileName: 'test.pdf',
      }),
    ).toEqual(1)
    expect(
      getFileNumber({
        originalName: 'test.pdf',
        fileName: 'test (1).pdf',
      }),
    ).toEqual(2)
    expect(
      getFileNumber({
        originalName: 'Chimie(2019).pdf',
        fileName: 'Chimie(2019).pdf',
      }),
    ).toEqual(1)
    expect(
      getFileNumber({
        originalName: 'Chimie (2019).pdf',
        fileName: 'Chimie (2019).pdf',
      }),
    ).toEqual(1)
    expect(
      getFileNumber({
        originalName: 'Chimie(2019).pdf',
        fileName: 'Chimie(2019) (2).pdf',
      }),
    ).toEqual(3)
    expect(
      getFileNumber({
        originalName: 'Chimie(2019) (x).pdf',
        fileName: 'Chimie(2019) (x).pdf',
      }),
    ).toEqual(1)
  })
})
