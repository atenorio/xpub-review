const s3service = require('./src/s3Service')
const resolvers = require('./src/resolvers')
const server = require('./src/restServer')

module.exports = { s3service, resolvers, server }
