const uuid = require('uuid')
const { chain } = require('lodash')

const decomposeFilename = (fileName = '') => {
  const ext = fileName.split('.').reverse()[0]
  const name = fileName.replace(`.${ext}`, '')
  return [name, ext]
}

const getFileNumber = file => {
  const [name] = decomposeFilename(file.fileName)
  const [originalName] = decomposeFilename(file.originalName)
  return (
    Number(
      name
        .replace(originalName, '')
        .trim()
        .replace(/[()]/g, ''),
    ) + 1
  )
}

const initialize = ({ models: { File, Manuscript }, s3Service, logEvent }) => ({
  async execute({ entityId, fileInput, fileData, userId }) {
    const { stream, filename, mimetype } = await fileData
    const key = `${entityId}/${uuid.v4()}`
    let parsedName = filename

    const manuscript = await Manuscript.find(entityId, 'files')
    const manuscriptVersions = await Manuscript.findManuscriptsBySubmissionId({
      submissionId: manuscript.submissionId,
      excludedStatus: Manuscript.Statuses.deleted,
      eagerLoadRelations: '[files]',
    })
    const lastFile = chain(manuscriptVersions)
      .flatMap(m => m.files)
      .filter(file => file.originalName === filename)
      .sortBy('created')
      .last()
      .value()

    if (lastFile) {
      const [name, ext] = decomposeFilename(lastFile.originalName)
      const fileNumber = getFileNumber(lastFile)
      parsedName = `${name} (${fileNumber}).${ext}`
    }

    await s3Service.upload({
      key,
      stream,
      mimetype,
      metadata: {
        filename,
        type: fileInput.type,
      },
    })

    const file = new File({
      manuscriptId: entityId,
      commentId: null,
      size: fileInput.size,
      fileName: parsedName,
      providerKey: key,
      mimeType: mimetype,
      originalName: filename,
      type: File.Types[fileInput.type],
    })
    await file.save()

    manuscript.assignFile(file)

    await manuscript.save()
    if (manuscript.status !== 'draft') {
      logEvent({
        userId,
        manuscriptId: entityId,
        action: logEvent.actions.file_added,
        objectType: logEvent.objectType.file,
        objectId: file.id,
      })
    }

    return {
      ...file,
      filename: file.fileName,
    }
  },
})

const authsomePolicies = ['isAuthenticated']

module.exports = {
  initialize,
  getFileNumber,
  authsomePolicies,
  decomposeFilename,
}
