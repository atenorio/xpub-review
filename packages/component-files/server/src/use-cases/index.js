const deleteFileUseCase = require('./deleteFile')
const getSignedUrlUseCase = require('./getSignedURL')
const uploadFileUseCase = require('./uploadFile')
const uploadCommentFile = require('./uploadCommentFile')
const uploadManuscriptFile = require('./uploadManuscriptFile')

module.exports = {
  deleteFileUseCase,
  getSignedUrlUseCase,
  uploadFileUseCase,
  uploadCommentFile,
  uploadManuscriptFile,
}
