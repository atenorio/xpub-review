import React from 'react'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { Modal } from 'component-modal'
import { th } from '@pubsweet/ui-toolkit'
import { compose, withHandlers } from 'recompose'
import { Row, Item, Breadcrumbs, withFetching, ShadowedBox } from '@hindawi/ui'

import { parseError } from '../components/utils'
import { Journals, AdminJournalForm } from '../components/'
import withJournals from '../graphql/withJournalsGQL'

const JournalsPage = ({
  history,
  addJournal,
  editJournal,
  data: { getJournals },
  isFetching,
}) => (
  <Root>
    <Row alignItems="center" justify="space-between">
      <Item alignItems="center" my={6}>
        <Breadcrumbs path="/admin">ADMIN DASHBOARD</Breadcrumbs>
      </Item>
    </Row>
    <CustomShadowedBox>
      <Row mb={6}>
        <Item alignItems="center" justify="flex-end">
          <Modal
            addJournal={addJournal}
            component={AdminJournalForm}
            confirmText="ADD JOURNAL"
            isFetching={isFetching}
            modalKey="addJournal"
            title="Add Journal"
          >
            {showModal => (
              <Button
                data-test-id="add-journal"
                medium
                onClick={showModal}
                primary
                width={36}
              >
                Add Journal
              </Button>
            )}
          </Modal>
        </Item>
      </Row>
      <Journals
        editJournal={editJournal}
        history={history}
        journals={getJournals}
      />
    </CustomShadowedBox>
  </Root>
)

export default compose(
  withFetching,
  withJournals,
  withHandlers({
    addJournal: ({ addJournal }) => (
      input,
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      addJournal({
        variables: {
          input,
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(error => {
          setFetching(false)
          setError(parseError(error))
        })
    },

    editJournal: ({ editJournal }) => (
      {
        id,
        name,
        code,
        issn,
        apc,
        email,
        triageEditorUserId,
        articleTypes,
        activationDate,
        peerReviewModelId,
      },
      { setFetching, hideModal, setError, clearError },
    ) => {
      clearError()
      setFetching(true)
      editJournal({
        variables: {
          id,
          input: {
            name,
            code,
            issn,
            apc,
            email,
            triageEditorUserId,
            articleTypes,
            activationDate,
            peerReviewModelId,
          },
        },
      })
        .then(() => {
          setFetching(false)
          hideModal()
        })
        .catch(error => {
          setFetching(false)
          setError(parseError(error))
        })
    },
  }),
)(JournalsPage)

const Root = styled.div`
  padding: 0 calc(${th('gridUnit')} * 14);
`

const CustomShadowedBox = styled(ShadowedBox)`
  min-width: 100%;
`
