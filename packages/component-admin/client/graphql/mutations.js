import gql from 'graphql-tag'

export const addUserFromAdminPanel = gql`
  mutation addUserFromAdminPanel($input: UserInput) {
    addUserFromAdminPanel(input: $input) {
      token
    }
  }
`

export const editUserFromAdminPanel = gql`
  mutation editUserFromAdminPanel($id: ID!, $input: EditUserInput!) {
    editUserFromAdminPanel(id: $id, input: $input)
  }
`

export const activateUser = gql`
  mutation activateUser($id: ID!) {
    activateUser(id: $id)
  }
`
export const deactivateUser = gql`
  mutation deactivate($id: ID!) {
    deactivateUser(id: $id)
  }
`

export const assignEditorialRole = gql`
  mutation assignEditorialRole($input: AssignEditorialRoleInput!) {
    assignEditorialRole(input: $input)
  }
`

export const assignLeadEditorialAssistant = gql`
  mutation assignLeadEditorialAssistant($id: ID!) {
    assignLeadEditorialAssistant(id: $id)
  }
`

export const addJournal = gql`
  mutation addJournal($input: AddJournalInput!) {
    addJournal(input: $input) {
      id
    }
  }
`

export const editJournal = gql`
  mutation editJournal($id: ID!, $input: EditJournalInput!) {
    editJournal(id: $id, input: $input)
  }
`
export const addSection = gql`
  mutation addSection($journalId: String, $name: String!) {
    addSection(journalId: $journalId, name: $name) {
      id
      name
    }
  }
`
export const editSection = gql`
  mutation editSection($id: ID!, $input: EditSectionInput!) {
    editSection(id: $id, input: $input)
  }
`
export const addSpecialIssue = gql`
  mutation addSpecialIssue($input: AddSpecialIssueInput!) {
    addSpecialIssue(input: $input) {
      id
      name
      startDate
      endDate
    }
  }
`
export const editSpecialIssue = gql`
  mutation editSpecialIssue($id: ID!, $input: EditSpecialIssueInput!) {
    editSpecialIssue(id: $id, input: $input) {
      id
      name
      startDate
      endDate
    }
  }
`
export const cancelSpecialIssue = gql`
  mutation cancelSpecialIssue($id: ID!, $input: CancelSpecialIssueInput) {
    cancelSpecialIssue(id: $id, input: $input)
  }
`
