import gql from 'graphql-tag'
import { userListFragment, journalFragment, userDetails } from './fragments'

export const getUser = gql`
  query getUser($userId: String) {
    getUser(userId: $userId) {
      ...userDetails
    }
  }
  ${userDetails}
`

export const getUsersForAdminPanel = gql`
  query getUsersForAdminPanel($page: Int, $pageSize: Int) {
    getUsersForAdminPanel(page: $page, pageSize: $pageSize) {
      users {
        ...userListFragment
      }
      totalCount
    }
  }
  ${userListFragment}
`

export const getPeerReviewModels = gql`
  query {
    getPeerReviewModels {
      id
      name
    }
  }
`
export const currentUser = gql`
  query {
    currentUser {
      id
      role
    }
  }
`

export const getJournals = gql`
  query {
    getJournals {
      ...journalFragment
    }
  }
  ${journalFragment}
`

export const getJournal = gql`
  query getJournal($journalId: String!) {
    getJournal(journalId: $journalId) {
      ...journalFragment
    }
  }
  ${journalFragment}
`

export const getUsersForRoleAssignment = gql`
  query {
    getUsersForRoleAssignment {
      id
      identities {
        ... on Local {
          name {
            surname
            givenNames
          }
          email
        }
      }
    }
  }
`

export const getEditorialBoard = gql`
  query getEditorialBoard($journalId: String!) {
    getEditorialBoard(journalId: $journalId) {
      id
      role
      sectionName
      specialIssueName
      fullName {
        surname
        givenNames
      }
      email
      isCorresponding
    }
  }
`

export const getArticleTypes = gql`
  query {
    getArticleTypes {
      id
      name
    }
  }
`
