import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { queries } from '../graphql'

import * as mutations from './mutations'

export default compose(
  graphql(mutations.addUserFromAdminPanel, {
    name: 'addUserFromAdminPanel',
    options: {
      refetchQueries: [
        {
          query: queries.getUsersForRoleAssignment,
        },
      ],
    },
  }),
  graphql(mutations.editUserFromAdminPanel, {
    name: 'editUserFromAdminPanel',
    options: {
      refetchQueries: [
        {
          query: queries.getUsersForRoleAssignment,
        },
      ],
    },
  }),
  graphql(mutations.activateUser, {
    name: 'activateUser',
    options: {
      refetchQueries: [
        {
          query: queries.getUsersForRoleAssignment,
        },
      ],
    },
  }),
  graphql(mutations.deactivateUser, {
    name: 'deactivateUser',
    options: {
      refetchQueries: [
        {
          query: queries.getUsersForRoleAssignment,
        },
      ],
    },
  }),
)
