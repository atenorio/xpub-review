import gql from 'graphql-tag'

export const userDetails = gql`
  fragment userDetails on User {
    id
    isActive
    isAdmin
    isRIPE
    identities {
      ... on Local {
        name {
          surname
          givenNames
          title
        }
        country
        email
        aff
        isConfirmed
      }
    }
  }
`

export const userListFragment = gql`
  fragment userListFragment on UserWithLocalIdentity {
    id
    isActive
    isConfirmed
    name {
      surname
      givenNames
    }
    email
    aff
  }
`
export const specialIssueFragment = gql`
  fragment specialIssueFragment on SpecialIssue {
    id
    name
    startDate
    endDate
    isCancelled
    isActive
    expirationDate
    customId
    callForPapers
    leadGuestEditor {
      id
      alias {
        name {
          givenNames
          surname
        }
      }
    }
  }
`

export const journalFragment = gql`
  fragment journalFragment on Journal {
    id
    name
    code
    issn
    apc
    email
    activationDate
    articleTypes {
      id
      name
    }
    triageEditor {
      id
      alias {
        name {
          givenNames
          surname
        }
        email
      }
      user {
        id
      }
    }
    isActive
    peerReviewModel {
      id
      name
      hasTriageEditor
      triageEditorLabel
      academicEditorLabel
      hasFigureheadEditor
      figureheadEditorLabel
    }
    sections {
      id
      name
      sectionEditors {
        id
        alias {
          name {
            givenNames
            surname
          }
        }
      }
      specialIssues {
        ...specialIssueFragment
        section {
          id
          name
        }
      }
    }
    specialIssues {
      ...specialIssueFragment
    }
  }
  ${specialIssueFragment}
`
