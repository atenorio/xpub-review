import React from 'react'
import styled, { css } from 'styled-components'
import { get } from 'lodash'
import { th, override, validationColor } from '@pubsweet/ui-toolkit'

const formatNumber = number =>
  number
    .toString()
    .replace(/\s/g, '')
    .replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1 ')

const APCField = ({
  validationStatus,
  type = 'text',
  value = '',
  label,
  ...props
}) => (
  <Root>
    {label && <Label htmlFor="apc-input">{label}</Label>}
    <InputWrapper>
      <Sign validationStatus={validationStatus} value={value}>
        $
      </Sign>
      <Input
        id="apc-input"
        type={type}
        validationStatus={validationStatus}
        value={formatNumber(value)}
        {...props}
      />
    </InputWrapper>
  </Root>
)

export default APCField

const Root = styled.div`
  display: flex;
  flex-direction: column;
  ${override('ui.TextField')};
`

const Label = styled.label`
  font-size: ${th('fontSizeBaseSmall')};
  line-height: ${th('lineHeightBaseSmall')};
  display: block;
  ${override('ui.Label')};
  ${override('ui.TextField.Label')};
`
const InputWrapper = styled.div`
  display: flex;
`
const colorSign = props => {
  if (get(props, 'validationStatus') === 'error') {
    return css`
      color: ${validationColor};
    `
  } else if (get(props, 'value')) {
    return css`
      color: inherit;
    `
  }
  return css`
    color: ${th('colorFurnitureHue')};
  `
}

const Sign = styled.div`
  font-family: ${th('defaultFont')};
  font-size: ${th('fontSizeBase')};
  padding-left: calc(${th('gridUnit')} * 2);
  display: flex;
  position: absolute;
  align-self: center;
  z-index: 1;

  ${colorSign}
`

const Input = styled.input`
  position: relative;
  font-family: inherit;
  font-size: inherit;
  width: fill-available;
  height: calc(${th('gridUnit')} * 12);

  border: ${th('borderWidth')} ${th('borderStyle')} ${validationColor};
  border-radius: ${th('borderRadius')};
  padding-right: calc(${th('gridUnit')} * 2);
  padding-left: calc(${th('gridUnit')} * 4);

  &::placeholder {
    color: ${th('colorTextPlaceholder')};
  }
  ${override('ui.TextField.Input')};
`
