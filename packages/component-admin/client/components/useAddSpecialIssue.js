import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useAddSpecialIssue = journalId => {
  const [addSpecialIssue] = useMutation(mutations.addSpecialIssue, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleAddSpecialIssue = (values, modalProps) => {
    const { name, startDate, endDate, sectionId, callForPapers } = values
    modalProps.setFetching(true)
    addSpecialIssue({
      variables: {
        input: {
          name,
          endDate,
          startDate,
          journalId,
          sectionId,
          callForPapers,
        },
      },
    })
      .then(() => {
        modalProps.setFetching(false)
        modalProps.hideModal()
      })
      .catch(error => {
        modalProps.setFetching(false)
        modalProps.setError(parseError(error))
      })
  }

  return { handleAddSpecialIssue }
}

export default useAddSpecialIssue
