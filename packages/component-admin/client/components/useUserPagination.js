import { useState } from 'react'
import { useQuery } from 'react-apollo'
import { get } from 'lodash'
import { getUsersForAdminPanel } from '../graphql/queries'

const useUserPagination = itemsPerPage => {
  const [page, setPage] = useState(0)

  const { data, loading, refetch } = useQuery(getUsersForAdminPanel, {
    variables: { page, pageSize: itemsPerPage },
    fetchPolicy: 'cache-and-network',
  })

  const totalUsers = get(data, 'getUsersForAdminPanel.totalCount', 0)
  const users = get(data, 'getUsersForAdminPanel.users', []).map(user => ({
    id: get(user, 'id'),
    givenNames: get(user, 'name.givenNames', ''),
    surname: get(user, 'name.surname', ''),
    aff: get(user, 'aff', ''),
    email: get(user, 'email', ''),
    isActive: get(user, 'isActive', false),
    isConfirmed: get(user, 'isConfirmed', false),
  }))

  const toFirst = () => {
    setPage(0)
  }

  const toLast = () => {
    const floor = Math.floor(totalUsers / itemsPerPage)
    setPage(totalUsers % itemsPerPage ? floor : floor - 1)
  }

  const nextPage = () => {
    setPage(page =>
      page * itemsPerPage + itemsPerPage < totalUsers ? page + 1 : page,
    )
  }

  const prevPage = () => {
    setPage(page => Math.max(0, page - 1))
  }

  return {
    users,
    totalUsers,
    loading,
    page,
    setPage,
    toLast,
    toFirst,
    prevPage,
    nextPage,
    refetchPage: refetch,
  }
}

export default useUserPagination
