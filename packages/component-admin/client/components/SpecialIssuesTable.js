import React, { Fragment } from 'react'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Spinner } from '@pubsweet/ui'
import { ScrollContainer, Row } from '@hindawi/ui'
import SpecialIssueCard from './SpecialIssueCard'

const SpecialIssuesTable = ({
  journalId,
  specialIssues,
  loading,
  sections,
}) => {
  if (loading) return <Spinner />
  return (
    <Fragment>
      {specialIssues.length > 0 && (
        <ScrollContainer>
          {specialIssues.map((specialIssue, index) => (
            <SpecialIssueCard
              journalId={journalId}
              key={specialIssue.id}
              sections={sections}
              specialIssue={specialIssue}
            />
          ))}
        </ScrollContainer>
      )}
      {specialIssues.length === 0 && (
        <Row alignItems="center" height={24} justify="center">
          <EmptyState>No special issues added yet</EmptyState>
        </Row>
      )}
    </Fragment>
  )
}

export default SpecialIssuesTable

const EmptyState = styled.span`
  background-color: ${th('white')};
  font-size: 20px;
  font-family: 'Nunito';
  color: ${th('actionSecondaryColor')};
`
