import React from 'react'
import styled from 'styled-components'

import JournalCard from './JournalCard'

const Journals = ({ journals = [], editJournal, history }) => (
  <JournalsContainer data-test-id="dashboard-list-items">
    {journals.map((j, index) => (
      <JournalCard
        editJournal={editJournal}
        history={history}
        journal={j}
        key={j.id}
      />
    ))}
    <JournalCard ghost />
  </JournalsContainer>
)

const JournalsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`

export default Journals
