import React from 'react'
import { useQuery } from 'react-apollo'
import { isEmpty, flatMap, chain } from 'lodash'
import styled, { css } from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Spinner } from '@pubsweet/ui'
import { useRoles, Text, Icon, TextTooltip, MultiAction } from '@hindawi/ui'
import { Modal } from 'component-modal'

import { queries } from '../graphql'
import useAssignLeadEditorialAssistant from './useAssignLeadEditorialAssistant'

const getFullName = ({ givenNames = '', surname = '' }) =>
  `${givenNames} ${surname}`

const getRoleLabel = (role, roleList, specialIssueName) => {
  if (role === 'triageEditor' && specialIssueName) {
    return roleList.find(el => el.value === 'triageEditorSI').label
  }
  if (role === 'academicEditor' && specialIssueName) {
    return roleList.find(el => el.value === 'academicEditorSI').label
  }
  return roleList.find(el => el.value === role).label
}

const EditorialBoardTable = ({
  sections,
  journalId,
  peerReviewModel,
  journalSpecialIssues,
}) => {
  const { roles } = useRoles(peerReviewModel)
  const { data, loading } = useQuery(queries.getEditorialBoard, {
    variables: { journalId },
    fetchPolicy: 'network-only',
  })

  const {
    handleAssignLeadEditorialAssistant,
  } = useAssignLeadEditorialAssistant(journalId)

  if (loading) return <Spinner />
  const boardList = chain(data)
    .get('getEditorialBoard', [])
    .sortBy(i => getFullName(i.fullName))
    .value()
  const sectionSpecialIssues = flatMap(
    sections,
    section => section.specialIssues,
  )
  const specialIssues = [...sectionSpecialIssues, ...journalSpecialIssues]

  return (
    <Table>
      <thead>
        <Tr>
          <Th />
          <Th>Role</Th>
          <Th>Full Name</Th>
          {!isEmpty(sections) && <Th>Section</Th>}
          {!isEmpty(specialIssues) && <Th>Special Issue</Th>}
          <Th>Email</Th>
        </Tr>
      </thead>
      <tbody>
        {boardList.length ? (
          boardList.map(
            ({
              id,
              role,
              email,
              fullName,
              sectionName,
              isCorresponding,
              specialIssueName,
            }) => (
              <Tr key={`board-table-${id}`}>
                <Td>
                  {role === 'editorialAssistant' && (
                    <Modal
                      cancelText="CANCEL"
                      component={MultiAction}
                      confirmText="ASSIGN"
                      modalKey={`assign-lead-editorial-assistant-${id}`}
                      onConfirm={handleAssignLeadEditorialAssistant(id)}
                      subtitle={`${getFullName(fullName)} (${email})`}
                      title="Assign Lead Editorial Assistant?"
                    >
                      {showModal => (
                        <TextTooltip title="Assign Lead">
                          <Icon
                            data-test-id="assign-lead-editorial-assistant"
                            fontSize="16px"
                            icon={isCorresponding ? 'solidLead' : 'lead'}
                            onClick={showModal}
                          />
                        </TextTooltip>
                      )}
                    </Modal>
                  )}
                </Td>
                <Td>{getRoleLabel(role, roles, specialIssueName)}</Td>
                <Td>{getFullName(fullName)}</Td>
                {!isEmpty(sections) && (
                  <Td>
                    <WithEllipsis>{sectionName} </WithEllipsis>
                  </Td>
                )}
                {!isEmpty(specialIssues) && (
                  <Td>
                    <WithEllipsis>{specialIssueName} </WithEllipsis>
                  </Td>
                )}
                <Td>{email}</Td>
              </Tr>
            ),
          )
        ) : (
          <Tr>
            <NoDataTd colSpan={4}>No User added yet</NoDataTd>
          </Tr>
        )}
      </tbody>
    </Table>
  )
}

const noWrap = props => {
  if (props.noWrap) {
    return css`
      white-space: nowrap;
    `
  }
}

const Table = styled.table`
  font-family: 'Nunito';
  border-collapse: collapse;
  min-width: 100%;
`

const WithEllipsis = styled(Text)`
  max-width: calc(${th('gridUnit')} * 50);
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`

const Th = styled.th`
  border: none;
  height: calc(${th('gridUnit')} * 10);
  text-align: start;
  vertical-align: middle;
`

const Tr = styled.tr`
  border-bottom: 1px solid ${th('colorBorder')};
`

const Td = styled.td`
  border: none;
  height: calc(${th('gridUnit')} * 9);
  text-align: start;
  vertical-align: middle;

  ${noWrap}
`

const NoDataTd = styled.td`
  height: calc(${th('gridUnit')} * 22);
  background-color: ${th('backgroundColor')};
  text-align: center;
  vertical-align: middle;
  font-size: 20px;
  color: ${th('actionSecondaryColor')};
`

export default EditorialBoardTable
