import React from 'react'

import {
  Row,
  Item,
  Label,
  FormModal,
  Textarea,
  validators,
  ValidatedFormField,
} from '@hindawi/ui'

const CancelSpecialIssueModal = ({ handleCancelSpecialIssue, hideModal }) => (
  <FormModal
    cancelText="NO"
    confirmText="YES"
    content={() => (
      <Row mt={6}>
        <Item vertical>
          <Label required>Cancellation reason</Label>
          <ValidatedFormField
            component={Textarea}
            data-test-id="return-reason"
            name="reason"
            validate={[validators.required]}
          />
        </Item>
      </Row>
    )}
    hideModal={hideModal}
    onSubmit={handleCancelSpecialIssue}
    title="Cancel Special Issue?"
  />
)

export default CancelSpecialIssueModal
