import React from 'react'
import { useQuery } from 'react-apollo'
import { get } from 'lodash'
import { SearchableSelect } from '@hindawi/ui'

import { getUsersForRoleAssignment } from '../graphql/queries'

const getName = identity =>
  `${get(identity, 'name.givenNames', '')} ${get(identity, 'name.surname', '')}`

const EditorialRoleSelect = ({
  disabled,
  onChange,
  value,
  validationStatus,
}) => {
  const handleChange = option => onChange(option ? `${option.value}` : '')
  const { data, loading } = useQuery(getUsersForRoleAssignment)
  const triageEditorList = get(data, 'getUsersForRoleAssignment', []).map(
    ({ id, identities }) => ({
      value: id,
      label: getName(identities[0]),
      sublabel: get(identities, '0.email', ''),
    }),
  )

  const selected = triageEditorList.find(
    triageEditor => triageEditor.value === value,
  )
  return (
    <SearchableSelect
      disabled={disabled}
      isLoading={loading}
      onChange={handleChange}
      options={triageEditorList}
      placeholder="Name or Email"
      validationStatus={validationStatus}
      value={selected || ''}
    />
  )
}

export default EditorialRoleSelect
