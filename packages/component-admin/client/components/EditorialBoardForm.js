import React from 'react'
import { useMutation } from 'react-apollo'
import { Formik } from 'formik'
import { Spinner, Button } from '@pubsweet/ui'
import {
  Row,
  Item,
  Menu,
  Icon,
  Label,
  useRoles,
  validators,
  useFetching,
  ValidatedFormField,
} from '@hindawi/ui'
import { isEmpty, flatMap } from 'lodash'
import { parseError } from '../components/utils'
import { mutations, queries } from '../graphql'
import EditorialRoleSelect from './EditorialRoleSelect'

const EditorialBoardForm = ({
  sections,
  journalId,
  peerReviewModel,
  journalSpecialIssues,
}) => {
  const sectionSpecialIssues = flatMap(
    sections,
    section => section.specialIssues,
  ).filter(Boolean)

  const specialIssues = [
    ...journalSpecialIssues,
    ...sectionSpecialIssues,
  ].filter(si => si.isActive && !si.isCancelled)
  let { roles } = useRoles(peerReviewModel)
  if (specialIssues.length === 0) {
    roles = roles.filter(
      r => !['triageEditorSI', 'academicEditorSI'].includes(r.value),
    )
  }

  const sectionOptions = sections.map(s => ({
    label: s.name,
    value: s.id,
  }))
  let specialIssueOptions
  if (specialIssues.length > 0) {
    specialIssueOptions = specialIssues.map(si => ({
      label: si.name,
      value: si.id,
    }))
  }
  const { isFetching, setFetching } = useFetching(false)
  const [assignRole] = useMutation(mutations.assignEditorialRole, {
    refetchQueries: [
      {
        query: queries.getEditorialBoard,
        variables: {
          journalId,
        },
      },
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleSubmit = (values, { resetForm, setFieldError }) => {
    const specialIssueRoles = {
      triageEditorSI: 'triageEditor',
      academicEditorSI: 'academicEditor',
    }
    if (['triageEditorSI', 'academicEditorSI'].includes(values.role)) {
      values.role = specialIssueRoles[values.role]
    }
    const { userId, role, sectionId, specialIssueId } = values

    setFetching(true)
    assignRole({
      variables: {
        input: { userId, role, journalId, sectionId, specialIssueId },
      },
    })
      .then(() => {
        setFetching(false)
        resetForm({})
      })
      .catch(error => {
        setFetching(false)
        setFieldError('userId', parseError(error))
      })
  }
  return (
    <Formik onSubmit={handleSubmit}>
      {({ handleSubmit, resetForm, values }) => (
        <Row alignItems="start" justify="start" mt={1}>
          <Item maxWidth={47} mr={4} vertical>
            <Label mb={1} required>
              Assign Editorial Role
            </Label>
            <ValidatedFormField
              component={Menu}
              data-test-id="role"
              name="role"
              onChange={() => resetForm({})}
              options={roles}
              placeholder="Select Role"
              validate={[validators.required]}
            />
          </Item>
          {!isEmpty(sections) && values.role === 'triageEditor' && (
            <Item maxWidth={111} mr={4} vertical>
              <Label mb={1} required>
                Section
              </Label>
              <ValidatedFormField
                component={Menu}
                data-test-id="sectionId"
                name="sectionId"
                options={sectionOptions}
                placeholder="Select Section"
                validate={[validators.required]}
              />
            </Item>
          )}
          {['triageEditorSI', 'academicEditorSI'].includes(values.role) && (
            <Item maxWidth={111} mr={4} vertical>
              <Label mb={1} required>
                Special Issue
              </Label>
              <ValidatedFormField
                component={Menu}
                data-test-id="specialIssueId"
                name="specialIssueId"
                options={specialIssueOptions}
                placeholder="Select Special Issue"
                validate={[validators.required]}
              />
            </Item>
          )}
          <Item
            data-test-id="user-selector"
            maxWidth={111}
            mr={4}
            mt={5}
            vertical
          >
            <ValidatedFormField
              component={EditorialRoleSelect}
              disabled={
                !isEmpty(sections) && values.role === 'triageEditor'
                  ? !values.sectionId
                  : !values.role
              }
              name="userId"
              validate={[validators.required]}
            />
          </Item>
          <Item maxWidth={27} mt={5}>
            {isFetching ? (
              <Item mt={1}>
                <Spinner />
              </Item>
            ) : (
              <Button invert onClick={handleSubmit} secondary small>
                <Icon icon="addUser" mr={1} />
                Add User
              </Button>
            )}
          </Item>
        </Row>
      )}
    </Formik>
  )
}

export default EditorialBoardForm
