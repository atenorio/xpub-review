import React, { Fragment } from 'react'
import moment from 'moment'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Row, Text, Icon, Item, Label, TextTooltip } from '@hindawi/ui'
import { Modal } from 'component-modal'
import { get } from 'lodash'
import { CancelSpecialIssueModal, AdminSpecialIssueForm } from '../components'
import useCancelSpecialIssue from './useCancelSpecialissue'
import useEditSpecialIssue from './useEditSpecialIssue'

const SpecialIssueCard = ({ specialIssue, journalId, sections }) => {
  const id = get(specialIssue, 'id', '')
  const name = get(specialIssue, 'name', '')
  const startDate = get(specialIssue, 'startDate', '')
  const endDate = get(specialIssue, 'endDate', '')
  const isCancelled = get(specialIssue, 'isCancelled')
  const isActive = get(specialIssue, 'isActive')
  const expirationDate = get(specialIssue, 'expirationDate', '')
  const leadGuestEditor = get(specialIssue, 'leadGuestEditor', {})
  const sectionName = get(specialIssue, 'section.name')
  const customId = get(specialIssue, 'customId')

  const { handleCancelSpecialIssue } = useCancelSpecialIssue(id, journalId)
  const { handleEditSpecialIssue } = useEditSpecialIssue()

  return (
    <Card data-test-id={`special-issue-${id}`} justify="center" mb={5} mt={3}>
      <Row alignItems="baseline">
        <NameWrapper
          alignItems="center"
          display="flex"
          justify-content="center"
        >
          <TextTooltip title={name}>
            <CustomH3 ellipsis mt={1} title={name}>
              {name}
            </CustomH3>
          </TextTooltip>
        </NameWrapper>

        {!isCancelled && isActive && (
          <Modal
            component={CancelSpecialIssueModal}
            handleCancelSpecialIssue={handleCancelSpecialIssue}
            modalKey={`cancelSpecialIssue-${id}`}
          >
            {showModal => (
              <TextTooltip title="Cancel">
                <CustomIcon
                  data-test-id="cancel-special-issue"
                  fontSize="16px"
                  icon="remove"
                  onClick={event => {
                    event.stopPropagation()
                    showModal()
                  }}
                />
              </TextTooltip>
            )}
          </Modal>
        )}
        {!isCancelled && (
          <Modal
            component={AdminSpecialIssueForm}
            confirmText="SAVE"
            editMode
            handleEditSpecialIssue={handleEditSpecialIssue}
            modalKey={specialIssue.id}
            sections={sections}
            specialIssue={specialIssue}
            title="Edit Special Issue"
          >
            {showModal => (
              <CustomIcon
                data-test-id="edit-special-issue"
                fontSize="16px"
                icon="edit"
                onClick={showModal}
              />
            )}
          </Modal>
        )}
      </Row>

      {isCancelled && (
        <Row alignItems="baseline">
          <Item alignItems="flex-start" pb={2} pl={4}>
            <Text error>CANCELLED</Text>
          </Item>
        </Row>
      )}

      <GreyBox>
        <CustomRow justify="flex-start">
          <Item alignItems="flex-start" flex={0} pr={4} vertical>
            <StyledLabel mb={2}>Submission start date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(startDate).format('YYYY-MM-DD')}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Submission end date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(endDate).format('YYYY-MM-DD')}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Expiration date</StyledLabel>
            <Text whiteSpace="nowrap">
              {moment(expirationDate).format('YYYY-MM-DD')}
            </Text>
          </Item>
          <VerticalSeparator />
          <Item alignItems="flex-start" flex={0} pl={4} pr={4} vertical>
            <StyledLabel mb={2}>Special Isusue ID</StyledLabel>
            <Text whiteSpace="nowrap">{customId}</Text>
          </Item>
          {sectionName && (
            <Fragment>
              <VerticalSeparator />
              <Item
                alignItems="flex-start"
                flex={1}
                flexWrap="wrap"
                minWidth={0}
                pl={4}
              >
                <StyledLabel mb={2}>Section</StyledLabel>
                <WithEllipsis>{sectionName}</WithEllipsis>
              </Item>
            </Fragment>
          )}
          <Item alignItems="flex-end" pr={3} vertical>
            <Label mb={2}>Lead Guest Editor</Label>
            {leadGuestEditor ? (
              <Text whiteSpace="nowrap">
                {`${get(leadGuestEditor, 'alias.name.givenNames', '')}
                      ${get(leadGuestEditor, 'alias.name.surname', '')}`}
              </Text>
            ) : (
              <Text>Unassigned</Text>
            )}
          </Item>
        </CustomRow>
      </GreyBox>
    </Card>
  )
}

export default SpecialIssueCard

// #region styles

const CustomIcon = styled(Icon)`
  opacity: 0;
  display: block;
  padding: 0 calc(${th('gridUnit')} * 3) calc(${th('gridUnit')} * 3)
    calc(${th('gridUnit')} * 3);
  align-items: center;
`

const StyledLabel = styled(Label)`
  width: 100%;
  > h4 {
    white-space: nowrap;
  }
`

const WithEllipsis = styled(Text)`
  display: block;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`
const Card = styled.div`
  flex: 1;
  border: 1px solid ${th('disabledColor')};
  box-shadow: none;
  border-radius: 3px;
  padding: 0;
  margin-bottom: calc(${th('gridUnit')} * 2);

  &:hover {
    box-shadow: ${th('shadows.shadowMedium')};

    ${CustomIcon} {
      opacity: 1;
    }
`
const GreyBox = styled.div`
  background-color: ${th('backgroundColor')};
  border-bottom-left-radius: 3px;
  border-bottom-right-radius: 3px;
`
const CustomRow = styled(Row)`
  padding: calc(${th('gridUnit')} * 2) 0 calc(${th('gridUnit')} * 2)
    calc(${th('gridUnit')} * 4);
`
const NameWrapper = styled(Item)`
  padding: calc(${th('gridUnit')} * 2) 0 calc(${th('gridUnit')} * 2)
    calc(${th('gridUnit')} * 4);
`

const CustomH3 = styled(H3)`
  display: inline-block;
`

const VerticalSeparator = styled.div`
  border-left: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 12);
  margin: 0px calc(${th('gridUnit')} * 2);
`

// #endregion
