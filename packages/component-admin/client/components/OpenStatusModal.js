import React from 'react'
import { Button } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { OpenModal, withFetching } from '@hindawi/ui'
import { withHandlers, compose, setDisplayName } from 'recompose'

const OpenStatusModal = ({
  onConfirm,
  isFetching,
  user: { id, firstName = '', lastName = '', isActive },
}) => (
  <OpenModal
    isFetching={isFetching}
    modalKey={`deactivate-${id}`}
    onConfirm={onConfirm}
    subtitle={`${firstName} ${lastName}`}
    title={`Are you sure you want to ${
      !isActive ? 'activate' : 'deactivate'
    } user?`}
  >
    {showModal => (
      <ActivateButton ml={2} mr={2} onClick={showModal} size="small">
        {!isActive ? 'ACTIVATE' : 'DEACTIVATE'}
      </ActivateButton>
    )}
  </OpenModal>
)

export default compose(
  withFetching,
  withHandlers({
    onConfirm: ({ user, onConfirm, ...props }) => modalProps => {
      if (typeof onConfirm === 'function') {
        onConfirm(user, { ...props, ...modalProps })
      }
    },
  }),
  setDisplayName('AdminStatusModal'),
)(OpenStatusModal)

// #region styles
const ActivateButton = styled(Button)`
  width: calc(${th('gridUnit')} * 20);
`
// #endregion
