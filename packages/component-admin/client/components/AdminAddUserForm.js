import React, { Fragment } from 'react'
import { useJournal } from 'component-journal-info'
import { get } from 'lodash'
import { TextField } from '@pubsweet/ui'
import {
  FormModal,
  Item,
  Label,
  Text,
  Menu,
  MenuCountry,
  Row,
  ValidatedFormField,
  ValidatedCheckboxField,
  validators,
} from '@hindawi/ui'

// #region helpers

const validate = values => {
  const errors = {}

  if (get(values, 'email', '') === '') {
    errors.email = 'Required'
  }

  return errors
}
// #endregion

const FormFields = ({ titles }) => (
  <Fragment>
    <Row alignItems="baseline" mt={6}>
      <Item mr={2} vertical>
        <Label required>First Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="first-name-input"
          inline
          name="givenNames"
          validate={[validators.required]}
        />
      </Item>
      <Item ml={2} vertical>
        <Label required>Last Name</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="last-name-input"
          inline
          name="surname"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row mt={1}>
      <Item data-test-id="title-dropdown" mr={2} vertical>
        <Label required>Title</Label>
        <ValidatedFormField
          component={Menu}
          name="title"
          options={titles}
          validate={[validators.required]}
        />
      </Item>
      <Item ml={2} vertical>
        <Label required>Email</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="email-input"
          inline
          name="email"
          validate={[validators.emailValidator]}
        />
      </Item>
    </Row>

    <Row mt={1}>
      <Item data-test-id="country" mr={2} vertical>
        <Label required>Country</Label>
        <ValidatedFormField
          component={MenuCountry}
          data-test-id="country-dropdwon"
          name="country"
          validate={[validators.required]}
        />
      </Item>
      <Item ml={2} vertical>
        <Label required>Affiliation</Label>
        <ValidatedFormField
          component={TextField}
          data-test-id="affiliation-input"
          inline
          name="aff"
          validate={[validators.required]}
        />
      </Item>
    </Row>

    <Row>
      <Item mr={2}>
        <ValidatedCheckboxField data-test-id="is-admin-input" name="isAdmin" />
        <Text fontWeight={700} mr={4}>
          Assign Admin
        </Text>
        <ValidatedCheckboxField data-test-id="is-ripe-input" name="isRIPE" />
        <Text fontWeight={700}>Assign RIPE</Text>
      </Item>
    </Row>
  </Fragment>
)

const AdminAddUserForm = ({ hideModal, user, onCancel, onConfirm }) => {
  const { titles } = useJournal()
  const onSubmit = (values, props) => onConfirm(values, props)
  const onClose = props => {
    if (typeof onCancel === 'function') onCancel(props)
    hideModal()
  }
  return (
    <FormModal
      cancelText="CANCEL"
      confirmText="SAVE USER"
      content={FormFields}
      hideModal={hideModal}
      initialValues={{
        isAdmin: false,
        isRIPE: false,
      }}
      onCancel={onClose}
      onSubmit={onSubmit}
      title="Add user"
      titles={titles}
      validate={validate}
    />
  )
}

export default AdminAddUserForm
