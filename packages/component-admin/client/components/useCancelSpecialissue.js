import { useMutation } from 'react-apollo'
import { mutations, queries } from '../graphql'
import { parseError } from './utils'

const useCancelSpecialIssue = (id, journalId) => {
  const [cancelSpecialIssue] = useMutation(mutations.cancelSpecialIssue, {
    refetchQueries: [
      {
        query: queries.getJournal,
        variables: {
          journalId,
        },
      },
    ],
  })

  const handleCancelSpecialIssue = (
    { reason },
    { setFetching, setError, hideModal },
  ) => {
    setFetching(true)
    cancelSpecialIssue({
      variables: {
        id,
        input: {
          reason,
        },
      },
    })
      .then(() => {
        setFetching(false)
        hideModal()
      })
      .catch(error => {
        setError(false)
        setError(parseError(error))
      })
  }

  return { handleCancelSpecialIssue }
}

export default useCancelSpecialIssue
