import moment from 'moment'

export const setInitialValues = (values = {}) => ({
  id: values.id || '',
  apc: (values.apc || '').toString(),
  name: values.name || '',
  issn: values.issn || '',
  articleTypes: (values.articleTypes || []).map(at => at.id),
  email: values.email || '',
  code: values.code || '',
  activationDate: values.activationDate,
  isActive: values.isActive || false,
  peerReviewModelId: values.peerReviewModel.id || '',
})
export const setSectionInitialValues = (values = {}) => ({
  id: values.id || '',
  name: values.name || '',
})
export const setSpecialIssuesInitialValues = (values = {}) => ({
  id: values.id || '',
  name: values.name || '',
  sectionId: values.section ? values.section.id : '',
  callForPapers: values.callForPapers || '',
  startDate:
    values.startDate || new Date(moment().format('YYYY-MM-DD')).toISOString(),
  endDate:
    values.endDate || new Date(moment().format('YYYY-MM-DD')).toISOString(),
})

export const parseError = e => e.message.replace('GraphQL error: ', '')

export const formatAPC = apc =>
  apc
    .toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    })
    .replace(/,/g, ' ')
