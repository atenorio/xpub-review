import React, { Fragment } from 'react'
import moment from 'moment'
import { H3 } from '@pubsweet/ui'
import styled from 'styled-components'
import { th } from '@pubsweet/ui-toolkit'
import { Modal } from 'component-modal'
import {
  ShadowedBox,
  Text,
  Row,
  Label,
  Item,
  Icon,
  TextTooltip,
} from '@hindawi/ui'

import { get, isNumber } from 'lodash'

import { formatAPC } from '../components/utils'
import { AdminJournalForm } from './index'

const getName = alias =>
  `${get(alias, 'name.givenNames', '')} ${get(alias, 'name.surname', '')}`

const JournalCard = ({ editJournal, journal, ghost, history }) => {
  const id = get(journal, 'id', '')
  const code = get(journal, 'code', '')
  const name = get(journal, 'name', '')
  const issn = get(journal, 'issn', '')
  const apc = get(journal, 'apc', '')
  const email = get(journal, 'email', '')
  const activationDate = get(journal, 'activationDate')
  const articleTypes = get(journal, 'articleTypes', [])
  const triageEditor = get(journal, 'triageEditor', '')
  const isActive = get(journal, 'isActive')
  const triageEditorLabel = get(journal, 'peerReviewModel.triageEditorLabel')
  return (
    <CustomShadowedBox
      data-test-id={`journal-${id}`}
      ghost={ghost}
      justify="center"
      mb={5}
      onClick={() => history.push(`/admin/journals/${id}`)}
    >
      <Row alignItems="baseline" justify="left">
        <NameWrapper
          alignItems="center"
          display="flex"
          justify-content="space-between"
        >
          <TextTooltip title={name}>
            <CustomH3 ellipsis mt={1} title={name}>
              {name}
            </CustomH3>
          </TextTooltip>
          <Item alignItems="center">
            <DotSeparator />
            <CustomText mr={7} title={code}>
              {code}
            </CustomText>
          </Item>

          <Modal
            component={AdminJournalForm}
            confirmText="SAVE"
            editJournal={editJournal}
            editMode
            journal={journal}
            modalKey={id}
            title="Edit Journal"
          >
            {showModal => (
              <TextTooltip title="Edit">
                <CustomIcon
                  data-test-id="edit-journal"
                  fontSize="16px"
                  icon="edit"
                  onClick={event => {
                    event.stopPropagation()
                    showModal()
                  }}
                />
              </TextTooltip>
            )}
          </Modal>
        </NameWrapper>
      </Row>
      <Items>
        <Row alignItems="baseline" justify="left" mb={4}>
          {isActive ? (
            <Fragment>
              <Text customId fontWeight={700}>
                ACTIVE
              </Text>
              {activationDate && (
                <Text pl={1}>{`since ${moment(activationDate).format(
                  'YYYY-MM-DD',
                )}`}</Text>
              )}
            </Fragment>
          ) : (
            <Text error fontWeight={700}>
              INACTIVE
            </Text>
          )}
          {activationDate && !isActive && (
            <Text pl={1}>
              {`until ${moment(activationDate).format('YYYY-MM-DD')}`}
            </Text>
          )}
        </Row>

        {articleTypes && articleTypes.length > 0 && (
          <Row justify="left">
            <Item display="inline-block" mb={4} vertical>
              <Label mb={2}>Article Types</Label>
              {articleTypes.map(articleType => (
                <Fragment key={`${id}-${articleType.id}`}>
                  <TextWithSeparator mr={2} title={articleType.name}>
                    {articleType.name}
                  </TextWithSeparator>
                </Fragment>
              ))}
            </Item>
          </Row>
        )}
      </Items>
      <GreyBox>
        <CustomRow justify="space-between">
          <WithEllipsis vertical>
            <Label mb={2}>Email</Label>
            <Text ellipsis mr={2} title={email}>
              {email}
            </Text>
          </WithEllipsis>

          <Item alignItems="center" justify="flex-end">
            {triageEditor && (
              <Fragment>
                <Item alignItems="flex-end" flex="auto" vertical>
                  <Label mb={2}>{triageEditorLabel}</Label>
                  <Text whiteSpace="nowrap">{getName(triageEditor.alias)}</Text>
                </Item>
                <VerticalSeparator />
              </Fragment>
            )}
            {issn && (
              <Item alignItems="flex-end" flex={0} vertical>
                <Label mb={2}>ISSN</Label>
                <Text whiteSpace="nowrap">{issn}</Text>
              </Item>
            )}
            {issn && isNumber(apc) && <VerticalSeparator />}
            {isNumber(apc) && (
              <Item alignItems="flex-end" flex={0} vertical>
                <Label mb={2}>APC</Label>
                <Text whiteSpace="nowrap">{formatAPC(apc)}</Text>
              </Item>
            )}
          </Item>
        </CustomRow>
      </GreyBox>
    </CustomShadowedBox>
  )
}

export default JournalCard

// #region styles
const CustomIcon = styled(Icon)`
  opacity: 0;
  display: block;
  padding: calc(${th('gridUnit')} * 1) calc(${th('gridUnit')} * 3)
    calc(${th('gridUnit')} * 3) 0;
  align-items: center;
`

const CustomShadowedBox = styled(ShadowedBox)`
  flex: 1 1 calc(${th('gridUnit')} * 144);
  visibility: ${props => (props.ghost ? 'hidden' : 'visible')};
  border: 1px solid ${th('disabledColor')};
  box-shadow: none;
  padding: 0;
  cursor: pointer;

  &:hover {
    box-shadow: ${th('shadows.shadowMedium')};

    ${CustomIcon} {
      opacity: 1;
    }
  }
  @media only screen and (min-width: 1328px) {
    flex: 1 1 49%;

    :nth-child(2n + 1) {
      margin-right: calc(${th('gridUnit')} * 2);
    }

    :nth-child(2n) {
      margin-left: calc(${th('gridUnit')} * 2);
    }
  }
`
const GreyBox = styled.div`
  background-color: ${th('backgroundColor')};
  border-bottom-left-radius: 2%;
  border-bottom-right-radius: 2%;
`
const CustomRow = styled(Row)`
  padding: calc(${th('gridUnit')} * 4);
`
const Items = styled.div`
  margin: calc(${th('gridUnit')} * 4);
`
const NameWrapper = styled(Item)`
  width: 90%;
  padding: calc(${th('gridUnit')} * 4) 0 0 calc(${th('gridUnit')} * 4);
`
const WithEllipsis = styled(Item)`
  display: inline-grid;
`

const CustomText = styled(Text)`
  max-width: 25%;
  line-height: calc(${th('gridUnit')} * 4 + 2);
`

const TextWithSeparator = styled(Text)`
  border-right: 1px solid ${th('colorFurniture')};
  padding: 0 calc(${th('gridUnit')} * 2);

  :first-of-type {
    padding-left: 0;
  }

  :last-of-type {
    border: none;
  }
`

const CustomH3 = styled(H3)`
  display: inline-block;
  max-width: calc(${th('gridUnit')} * 160);
`

const VerticalSeparator = styled.div`
  border-left: 1px solid ${th('colorFurniture')};
  height: calc(${th('gridUnit')} * 8);
  margin: 0px calc(${th('gridUnit')} * 2);
`

const DotSeparator = styled.div`
  align-self: center;
  background-color: ${th('heading.h2Color')};
  border-radius: 50%;
  height: ${th('gridUnit')};
  margin: 0px calc(${th('gridUnit')} * 2);
  width: ${th('gridUnit')};
`
// #endregion
