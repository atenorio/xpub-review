import React, { Fragment } from 'react'
import styled from 'styled-components'
import { H3 } from '@pubsweet/ui'
import { th } from '@pubsweet/ui-toolkit'
import { Box, Tabs } from '@hindawi/ui'

const JournalDetailsTabs = ({ tabButtons, children }) => (
  <CustomBox>
    <Tabs>
      {({ selectedTab, changeTab }) => (
        <Fragment>
          <TabsHeader>
            {tabButtons.map((tab, index) => (
              <TabButton
                key={tab}
                ml={2}
                mr={2}
                onClick={() => changeTab(index)}
                selected={selectedTab === index}
              >
                {renderTabButton(tab)}
              </TabButton>
            ))}
          </TabsHeader>

          {React.Children.toArray(children)[selectedTab]}
        </Fragment>
      )}
    </Tabs>
  </CustomBox>
)

const renderTabButton = (tab, numberOfSubmittedReports) => {
  if (typeof tab === 'function') {
    return React.createElement(tab)
  }
  if (typeof tab === 'string') {
    return (
      <H3 pl={6} pr={6}>
        {tab}
      </H3>
    )
  }
  return tab
}

const CustomBox = styled(Box)`
  border: 1px solid ${th('disabledColor')};
  margin-top: calc(${th('gridUnit')} * 10);
`

const TabsHeader = styled.div`
  align-items: center;
  background-color: ${th('colorBackgroundHue2')};
  box-sizing: border-box;
  display: flex;
  justify-content: flex-start;

  padding: 0 calc(${th('gridUnit')} * 3);
`
const TabButton = styled.div`
  align-items: center;
  border-top: ${props =>
    props.selected
      ? `3px solid ${props.theme.colorFurnitureHue}`
      : '3px solid transparent'};
  background-color: ${props =>
    props.selected ? th('colorBackgroundHue') : 'inherit'};
  box-sizing: border-box;
  cursor: pointer;
  display: flex;
  justify-content: center;
  height: calc(${th('gridUnit')} * 10);
`

export default JournalDetailsTabs
