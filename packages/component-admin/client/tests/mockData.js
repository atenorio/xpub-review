export const articleTypes = [
  {
    id: '22131002-b65a-42ca-8d7a-fd43200c04c0',
    name: 'Review Article',
    __typename: 'ArticleType',
  },
  {
    id: '8dc86d05-24e1-4d8e-955a-91527365b0f1',
    name: 'Research Article',
    __typename: 'ArticleType',
  },
  {
    id: 'c39350fb-97fe-45ad-b893-e4db49bf6075',
    name: 'Letter to the Editor',
    __typename: 'ArticleType',
  },
]

export const users = [
  {
    id: '76373a7c-5397-4346-8b25-7c95772e093d',
    identities: [
      {
        name: {
          givenNames: 'Barbara',
          surname: 'Taylor',
          __typename: 'Name',
        },
        email: 'barbara.taylor+ko@ce.sh',
        __typename: 'Local',
      },
    ],
    __typename: 'User',
  },
  {
    id: 'aa352d30-4650-4f33-8b00-0a480667f96f',
    identities: [
      {
        name: {
          givenNames: 'Hester',
          surname: 'Rowe',
          __typename: 'Name',
        },
        email: 'hester.rowe+lafsosan@mi.je',
        __typename: 'Local',
      },
    ],
    __typename: 'User',
  },
  {
    id: 'aea81888-ea95-4551-b794-71f49daaa5ce',
    identities: [
      {
        name: {
          givenNames: 'Katharine',
          surname: 'Chandler',
          __typename: 'Name',
        },
        email: 'katharine.chandler+ju@isahu.ck',
        __typename: 'Local',
      },
    ],
    __typename: 'User',
  },
]
