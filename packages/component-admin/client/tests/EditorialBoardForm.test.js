import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { cleanup, fireEvent } from '@testing-library/react'

import { render } from './testUtils'
import EditorialBoardForm from '../components/EditorialBoardForm'

const peerReviewModel = {
  name: 'Chief Minus',
  hasTriageEditor: true,
}

const sections = []
const specialIssues = []

describe('EditorialBoardForm component', () => {
  beforeEach(() => {
    cleanup()
  })
  it('should contain all fields', () => {
    const container = render(
      <EditorialBoardForm
        journalSpecialIssues={specialIssues}
        peerReviewModel={peerReviewModel}
        sections={sections}
      />,
    )
    const { getByText, getByTestId } = container

    expect(getByTestId('role-filter')).toBeInTheDocument()
    expect(getByTestId('user-selector')).toBeInTheDocument()
    expect(getByText('Add User')).toBeInTheDocument()
  })
  it('should hide section dropdown when selecting triage editor on PRM without section', async done => {
    const container = render(
      <EditorialBoardForm
        journalSpecialIssues={specialIssues}
        peerReviewModel={peerReviewModel}
        sections={sections}
      />,
    )
    const { getByTestId } = container

    const sectionDropdown = document.querySelector(
      '[data-test-id="sectionId-filter"]',
    )
    expect(sectionDropdown).not.toBeInTheDocument()

    fireEvent.click(getByTestId('role-filter'))
    setTimeout(() => {
      fireEvent.click(getByTestId('role-triageEditor'))
      setTimeout(() => {
        const sectionDropdown = document.querySelector(
          '[data-test-id="sectionId-filter"]',
        )
        expect(sectionDropdown).not.toBeInTheDocument()
        done()
      })
    })
  })
  it('should show section dropdown when selecting triage editor on PRM with section', async done => {
    const peerReviewModel = { name: 'Section Editor', hasTriageEditor: true }
    const sections = [
      {
        id: '1',
        name: 'Computer Engineering',
      },
      {
        id: '2',
        name: 'Technical Support',
      },
    ]
    const container = render(
      <EditorialBoardForm
        journalSpecialIssues={specialIssues}
        peerReviewModel={peerReviewModel}
        sections={sections}
      />,
    )
    const { getByTestId } = container

    const sectionDropdown = document.querySelector(
      '[data-test-id="sectionId-filter"]',
    )
    expect(sectionDropdown).not.toBeInTheDocument()

    fireEvent.click(getByTestId('role-filter'))
    setTimeout(() => {
      fireEvent.click(getByTestId('role-triageEditor'))
      setTimeout(() => {
        expect(getByTestId('sectionId-filter')).toBeInTheDocument()
        done()
      })
    })
  })
  it('should make user selector disabled until role is selected on PRM without section', async done => {
    const container = render(
      <EditorialBoardForm
        journalSpecialIssues={specialIssues}
        peerReviewModel={peerReviewModel}
        sections={sections}
      />,
    )
    const { getByTestId, getByPlaceholderText } = container

    expect(getByPlaceholderText('Name or Email')).toBeDisabled()

    fireEvent.click(getByTestId('role-filter'))
    setTimeout(() => {
      fireEvent.click(getByTestId('role-triageEditor'))
      setTimeout(() => {
        expect(getByPlaceholderText('Name or Email')).not.toBeDisabled()
        done()
      })
    })
  })
  it('should make user selector disabled until role and section are selected on PRM with section', async done => {
    const peerReviewModel = { name: 'Section Editor', hasTriageEditor: true }
    const sections = [
      {
        id: '1',
        name: 'Computer Engineering',
      },
      {
        id: '2',
        name: 'Technical Support',
      },
    ]
    const container = render(
      <EditorialBoardForm
        journalSpecialIssues={specialIssues}
        peerReviewModel={peerReviewModel}
        sections={sections}
      />,
    )
    const { getByTestId, getByPlaceholderText } = container

    expect(getByPlaceholderText('Name or Email')).toBeDisabled()

    fireEvent.click(getByTestId('role-filter'))
    setTimeout(() => {
      fireEvent.click(getByTestId('role-triageEditor'))
      setTimeout(() => {
        expect(getByPlaceholderText('Name or Email')).toBeDisabled()

        fireEvent.click(getByTestId('sectionId-filter'))
        setTimeout(() => {
          fireEvent.click(getByTestId('sectionId-1'))
          setTimeout(() => {
            expect(getByPlaceholderText('Name or Email')).not.toBeDisabled()
            done()
          })
        })
      })
    })
  })
})
