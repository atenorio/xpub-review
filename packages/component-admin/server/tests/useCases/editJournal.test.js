const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Team, ArticleType } = models
fixtures.generateArticleTypes(ArticleType)

const { editJournalUseCase } = require('../../src/use-cases')

const jobsService = {
  scheduleJournalActivation: jest.fn(),
}
const eventsService = {
  publishJournalEvent: jest.fn(),
}
const chance = new Chance()

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
})
describe('Edit journal use case', () => {
  let input
  let journal

  beforeEach(async () => {
    input = generateInput()
    journal = await fixtures.generateJournal({ Journal })
  })
  it('should edit the journal information', async () => {
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })
    expect(journal.name).toEqual(input.name)
  })
  it('updates the article types when they are changed', async () => {
    const articleType = fixtures.getArticleTypeByPeerReview(true)
    const noPeerReviewArticleType = fixtures.getArticleTypeByPeerReview(false)
    const journal = await fixtures.generateJournal({
      properties: { articleTypes: [articleType.id] },
      Journal,
    })
    await dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    input.articleTypes = [noPeerReviewArticleType]

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })
    expect(journal.name).toEqual(input.name)

    const journalArticleTypes = fixtures.getJournalArticleTypesByJournalId(
      journal.id,
    )
    expect(journalArticleTypes).toHaveLength(1)
  })
  it('changes the activation date when a new date is provided', async () => {
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    journal.activationDate = new Date().toISOString()
    const futureDate = new Date()
    futureDate.setDate(futureDate.getDate() + 2)
    input.activationDate = futureDate.toISOString()

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })

    expect(journal.activationDate).toEqual(input.activationDate)
    expect(journal.isActive).toBeFalsy()
    expect(jobsService.scheduleJournalActivation).toHaveBeenCalledTimes(1)
  })
  it('activates the journal if the activation date is set to today', async () => {
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    input.activationDate = new Date().toISOString()

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })

    expect(journal.activationDate).toEqual(input.activationDate)
    expect(journal.isActive).toBeTruthy()
  })
  it('does nothing if the activation date is not changed', async () => {
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    const futureDate = new Date()
    futureDate.setDate(futureDate.getDate() + 2)

    journal.activationDate = futureDate
    journal.isActive = true
    input.activationDate = futureDate

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })

    expect(journal.activationDate).toBeDefined()
    expect(journal.isActive).toBeTruthy()
  })
  it('does nothing if the input date and journal date are undefined and null', async () => {
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    await editJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, adminId, id: journal.id, reqUserId: adminId })

    expect(journal.activationDate).toBeNull()
    expect(journal.isActive).toBeFalsy()
  })
})
