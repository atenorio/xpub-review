const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { addSpecialIssueUseCase } = require('../../src/use-cases')

const chance = new Chance()
const eventsService = {
  publishJournalEvent: jest.fn(),
}

const generateInput = () => ({
  isActive: false,
  name: chance.company(),
  endDate: new Date(chance.date({ year: 2069, string: true })),
  startDate: new Date(),
  callForPapers: chance.paragraph(),
})
const { Team, SpecialIssue } = models
dataService.createGlobalUser({
  models,
  fixtures,
  role: Team.Role.admin,
})

describe('Add special issue use case', () => {
  const { PeerReviewModel, Journal, Section } = models
  let input, jobsService
  let journal
  beforeEach(async () => {
    input = generateInput()
    jobsService = {
      scheduleSpecialIssueActivation: jest.fn(),
    }
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    input.journalId = journal.id

    fixtures.generatePeerReviewModel({
      properties: {
        name: 'Special Issue Chief Minus',
        approvalEditors: [Team.Role.triageEditor],
      },
      PeerReviewModel,
    })
  })

  it('creates a new special issue on journal', async () => {
    const specialIssue = await addSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute(input)

    expect(specialIssue.name).toEqual(input.name)
    expect(specialIssue.journalId).toEqual(input.journalId)
    expect(specialIssue.callForPapers).toEqual(input.callForPapers)
  })

  it('returns an error if the special issue already exists', async () => {
    await fixtures.generateSpecialIssue({ properties: input, SpecialIssue })

    try {
      const specialIssue = await addSpecialIssueUseCase
        .initialize({ models, jobsService, eventsService })
        .execute(input)

      expect(specialIssue.name).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual('Special issue already exists.')
    }
  })

  it('returns an error if the end date is earlier than the start date', async () => {
    input.endDate = new Date(chance.date({ year: 1969, string: true }))

    try {
      const specialIssue = await addSpecialIssueUseCase
        .initialize({ models, jobsService, eventsService })
        .execute(input)

      expect(specialIssue.name).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual('End date must be after start date.')
    }
  })

  it('creates an active special issue if the start date is set to today', async () => {
    input.startDate = new Date().toISOString()

    const specialIssue = await addSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute(input)

    expect(jobsService.scheduleSpecialIssueActivation).not.toHaveBeenCalled()
    expect(specialIssue.isActive).toBe(true)
  })

  it('creates an inactive special issue when the start date is in the future', async () => {
    input.startDate = new Date(
      chance.date({ year: 2069, string: true }),
    ).toISOString()
    input.startDate = new Date(
      chance.date({ year: 2070, string: true }),
    ).toISOString()

    const specialIssue = await addSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute(input)

    expect(specialIssue.activationDate).toEqual(input.activationDate)
    expect(jobsService.scheduleSpecialIssueActivation).toHaveBeenCalledTimes(1)
    expect(specialIssue.isActive).toBe(false)
  })

  it('creates special issue on a section', async () => {
    const section = fixtures.generateSection({
      properties: {
        journalId: journal.id,
      },
      Section,
    })
    input.sectionId = section.id
    input.journalId = undefined

    const specialIssue = await addSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute(input)

    expect(specialIssue.name).toEqual(input.name)
    expect(specialIssue.sectionId).toEqual(input.sectionId)
  })

  it('assigns a random 6 digit id when created', async () => {
    const specialIssue = await addSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute(input)

    expect(specialIssue.customId).toBeDefined()
    expect(specialIssue.customId.length).toEqual(6)
  })
})
