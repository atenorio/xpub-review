process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { models, fixtures } = require('fixture-service')
const { getUsersForRoleAssignmentUseCase } = require('../../src/use-cases')

describe('Get users for role assignment use case', () => {
  it('returns an array', async () => {
    const users = await getUsersForRoleAssignmentUseCase
      .initialize(models)
      .execute()

    expect(Array.isArray(users)).toEqual(true)
  })
  it('returns only active users', async () => {
    const { User, Identity } = models

    await fixtures.generateUser({
      properties: { isConfirmed: true, isActive: true },
      User,
      Identity,
    })

    const users = await getUsersForRoleAssignmentUseCase
      .initialize(models)
      .execute()

    expect(users.length).toEqual(1)
    expect(users[0].isActive).toEqual(true)
  })
})
