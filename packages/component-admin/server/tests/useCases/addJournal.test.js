process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { addJournalUseCase } = require('../../src/use-cases')

const chance = new Chance()
const jobsService = {
  scheduleJournalActivation: jest.fn(),
}
const eventsService = {
  publishJournalEvent: jest.fn(),
}

const generateInput = () => ({
  name: chance.company(),
  code: chance.word().toUpperCase(),
  email: chance.email(),
  issn: `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
    min: 999,
    max: 9999,
  })}`,
  apc: chance.natural(),
  articleTypes: [chance.guid(), chance.guid()],
  isActive: false,
})

describe('Add journal use case', () => {
  let input

  beforeEach(async () => {
    input = generateInput()
  })

  it('creates a new journal', async () => {
    const { Team, PeerReviewModel } = models
    await fixtures.generatePeerReviewModel({
      properties: { name: 'Chief Minus' },
      PeerReviewModel,
    })

    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    const journal = await addJournalUseCase
      .initialize({ models, eventsService })
      .execute({ input, adminId })

    expect(journal.code).toEqual(input.code)
  })

  it('returns an error if the journal already exists', async () => {
    const { Team, Journal, PeerReviewModel } = models
    await fixtures.generatePeerReviewModel({
      properties: { name: 'Chief Minus' },
      PeerReviewModel,
    })
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    await fixtures.generateJournal({ properties: input, Journal })

    try {
      const journal = await addJournalUseCase
        .initialize({ models, eventsService })
        .execute({ input, adminId })

      expect(journal.code).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual('Journal already exists.')
    }
  })
  it('returns an error if the apc is negative', async () => {
    const { Team, PeerReviewModel } = models
    await fixtures.generatePeerReviewModel({
      properties: { name: 'Chief Minus' },
      PeerReviewModel,
    })
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })
    input.apc *= -1

    try {
      const journal = await addJournalUseCase
        .initialize({ models, eventsService })
        .execute({ input, adminId })

      expect(journal.code).toBeUndefined()
    } catch (e) {
      expect(e.message).toEqual(
        `Journal APC ${input.apc} cannot be a negative number.`,
      )
    }
  })
  it('creates an active journal if the activation date is set to today', async () => {
    const { Team, PeerReviewModel } = models
    await fixtures.generatePeerReviewModel({
      properties: { name: 'Chief Minus' },
      PeerReviewModel,
    })
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    input.activationDate = new Date().toISOString()
    const journal = await addJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, userId: adminId })

    expect(jobsService.scheduleJournalActivation).not.toHaveBeenCalled()
    expect(journal.isActive).toBe(true)
  })
  it('creates an inactive journal when the activation date is in the future', async () => {
    const { Team, PeerReviewModel } = models
    await fixtures.generatePeerReviewModel({
      properties: { name: 'Chief Minus' },
      PeerReviewModel,
    })
    const { userId: adminId } = dataService.createGlobalUser({
      models,
      fixtures,
      role: Team.Role.admin,
    })

    input.activationDate = new Date(chance.date({ year: 2069, string: true }))

    const journal = await addJournalUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, userId: adminId })

    expect(journal.activationDate).toEqual(input.activationDate)
    expect(jobsService.scheduleJournalActivation).toHaveBeenCalledTimes(1)
    expect(journal.isActive).toBe(false)
  })
})
