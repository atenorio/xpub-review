process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { deactivateUserUseCase } = require('../../src/use-cases')

const chance = new Chance()

const eventsService = {
  publishUserEvent: jest.fn(),
}
const { Team, User, Identity, TeamMember } = models

describe('Deactivate user from admin panel', () => {
  it('returns an error when the user id is invalid', async () => {
    const result = deactivateUserUseCase
      .initialize({ models, eventsService })
      .execute({ id: chance.guid() })

    return expect(result).rejects.toThrow()
  })
  it('deactivates an user when the user id is valid', async () => {
    let team
    team = fixtures.getTeamByRole(Team.Role.admin)
    if (!team)
      team = fixtures.generateTeam({
        properties: { role: Team.Role.admin },
        Team,
      })

    const admin = fixtures.generateUser({ User, Identity })
    const teamMember = fixtures.generateTeamMember({
      properties: {
        userId: admin.id,
        teamId: team.id,
      },
      TeamMember,
    })
    team.members.push(teamMember)

    const user = fixtures.generateUser({
      properties: { isActive: true },
      User,
      Identity,
    })
    await deactivateUserUseCase
      .initialize({ models, eventsService })
      .execute({ id: user.id, userId: admin.id })

    expect(user.isActive).toBe(false)
  })
  it('does not return an error when the user is already inactive', async () => {
    const user = fixtures.generateUser({
      properties: { isActive: false },
      User,
      Identity,
    })
    const result = deactivateUserUseCase
      .initialize({ models, eventsService })
      .execute({ id: user.id })

    return expect(result).resolves.not.toThrow()
  })
})
