const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { editSpecialIssueUseCase } = require('../../src/use-cases')

const chance = new Chance()
const eventsService = {
  publishJournalEvent: jest.fn(),
}

const generateInput = () => ({
  name: chance.company(),
  endDate: new Date(chance.date({ year: 2030, string: true })),
  startDate: new Date(),
  callForPapers: chance.paragraph(),
})
const { Team, SpecialIssue, PeerReviewModel, Journal, Section } = models
dataService.createGlobalUser({
  models,
  fixtures,
  role: Team.Role.admin,
})

describe('Edit special issue use case', () => {
  let input, jobsService
  let journal
  let specialIssue
  beforeEach(async () => {
    input = generateInput()
    jobsService = {
      scheduleSpecialIssueActivation: jest.fn(),
    }
    const peerReviewModel = fixtures.generatePeerReviewModel({
      properties: {
        approvalEditors: [Team.Role.academicEditor],
      },
      PeerReviewModel,
    })

    journal = fixtures.generateJournal({
      properties: {
        peerReviewModelId: peerReviewModel.id,
      },
      Journal,
    })
    input.journalId = journal.id
    specialIssue = await fixtures.generateSpecialIssue({
      SpecialIssue,
    })
  })

  it('should edit the special issue info', async () => {
    const specialIssueEdited = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })

    expect(specialIssueEdited.name).toEqual(input.name)
    expect(specialIssueEdited.callForPapers).toEqual(input.callForPapers)
    expect(specialIssueEdited.startDate).toEqual(input.startDate)
    expect(specialIssueEdited.endDate).toEqual(input.endDate)
  })
  it('should link to a section and remove journal id', async () => {
    const section = await fixtures.generateSection({ Section })
    input.sectionId = section.id
    const specialIssueEdited = await editSpecialIssueUseCase
      .initialize({ models, jobsService, eventsService })
      .execute({ input, id: specialIssue.id })
    expect(specialIssueEdited.sectionId).toEqual(section.id)
    expect(specialIssueEdited.journalId).toEqual(null)
  })
})
