process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { models, fixtures } = require('fixture-service')

const { activateUserUseCase } = require('../../src/use-cases')

const chance = new Chance()
const { User, Identity } = models

const eventsService = {
  publishUserEvent: jest.fn(),
}

describe('Activate user from admin panel', () => {
  it('returns an error when the user id is invalid', async () => {
    const result = activateUserUseCase
      .initialize({ models, eventsService })
      .execute(chance.guid())

    return expect(result).rejects.toThrow()
  })
  it('activates an user when the user id is valid', async () => {
    const user = fixtures.generateUser({
      properties: { isActive: false },
      User,
      Identity,
    })
    await activateUserUseCase
      .initialize({ models, eventsService })
      .execute(user.id)

    expect(user.isActive).toBe(true)
  })
  it('does not return an error when the user is already active', async () => {
    const user = fixtures.generateUser({
      properties: { isActive: true },
      User,
      Identity,
    })
    const result = activateUserUseCase
      .initialize({ models, eventsService })
      .execute(user.id)

    return expect(result).resolves.not.toThrow()
  })
})
