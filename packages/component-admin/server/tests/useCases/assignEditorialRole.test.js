process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Section, Team, User, Identity, SpecialIssue } = models
const useCases = require('../../src/use-cases')

const chance = new Chance()
const eventsService = {
  publishJournalEvent: jest.fn(),
}

describe('Assign editorial role', () => {
  it('should add a user on a journal', async () => {
    const journal = fixtures.generateJournal({
      Journal,
    })
    const role = chance.pickone(Team.JournalRoles)
    const user = fixtures.generateUser({
      properties: { isActive: true },
      User,
      Identity,
    })

    await useCases.assignEditorialRoleUseCase
      .initialize({ models, eventsService })
      .execute({ input: { userId: user.id, role, journalId: journal.id } })

    const team = fixtures.getTeamByRoleAndJournalId({ role, id: journal.id })
    const teamMember = team.members.find(m => m.userId === user.id)
    expect(teamMember).toBeDefined()
  })
  it('should add a user on a section', async () => {
    const journal = fixtures.generateJournal({
      Journal,
    })
    const section = fixtures.generateSection({
      properties: {
        journalId: journal.id,
      },
      Section,
    })
    section.journal = journal
    const role = Team.Role.triageEditor
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })

    await useCases.assignEditorialRoleUseCase
      .initialize({ models, eventsService })
      .execute({
        input: {
          userId: user.id,
          role,
          sectionId: section.id,
        },
      })

    const team = fixtures.getTeamByRoleAndSectionId({ role, id: section.id })
    const teamMember = team.members.find(m => m.userId === user.id)
    expect(teamMember).toBeDefined()
  })
  it('should add a Lead Guest Editor on a special issue', async () => {
    const journal = fixtures.generateJournal({
      Journal,
    })
    const specialIssue = fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        name: chance.company(),
        endDate: new Date(chance.date({ year: 2069, string: true })),
        startDate: new Date(),
        journalId: journal.id,
      },
      SpecialIssue,
    })
    const role = Team.Role.triageEditor
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })

    await useCases.assignEditorialRoleUseCase
      .initialize({ models, eventsService })
      .execute({
        input: {
          userId: user.id,
          role,
          specialIssueId: specialIssue.id,
        },
      })

    const team = fixtures.getTeamByRoleAndSpecialIssueId({
      role,
      id: specialIssue.id,
    })
    const teamMember = team.members.find(m => m.userId === user.id)
    expect(teamMember).toBeDefined()
  })
  it('should add a corresponding Editorial Assistant if it is the first', async () => {
    const journal = fixtures.generateJournal({
      Journal,
    })
    const role = Team.Role.editorialAssistant
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })

    await useCases.assignEditorialRoleUseCase
      .initialize({ models, eventsService })
      .execute({
        input: {
          userId: user.id,
          role,
          journalId: journal.id,
        },
      })

    const team = fixtures.getTeamByRoleAndJournalId({ role, id: journal.id })
    const teamMember = team.members.find(m => m.userId === user.id)
    expect(teamMember.isCorresponding).toBe(true)
  })
  it('should return an error if the user already has an editorial role on the journal', async () => {
    const journal = await fixtures.generateJournal({ Journal })
    const role = chance.pickone(Team.JournalRoles)

    const teamMember = dataService.createUserOnJournal({
      role,
      models,
      journal,
      fixtures,
    })

    try {
      await useCases.assignEditorialRoleUseCase
        .initialize({ models, eventsService })
        .execute({
          input: {
            userId: teamMember.userId,
            role: chance.pickone([
              Team.Role.academicEditor,
              Team.Role.editorialAssistant,
            ]),
            journalId: journal.id,
          },
        })
      expect(true).toBeFalsy()
    } catch (e) {
      expect(e.message).toEqual(
        `User already has an editorial role on this journal.`,
      )
    }
  })
})
