const Chance = require('chance')
const {
  models,
  fixtures,
  services: { dataService },
} = require('fixture-service')

const { Journal, Section, Team, SpecialIssue, User, Identity } = models
const { getEditorialBoardUseCase } = require('../../src/use-cases')

const chance = new Chance()

describe('Get editorial board use case', () => {
  it('returns the editorial board and staff for a specified journal', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const triageEditorRole = Team.Role.triageEditor

    const triageEditor = dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: triageEditorRole,
    })

    dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: triageEditorRole,
    })

    dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.academicEditor,
    })

    dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: Team.Role.academicEditor,
    })

    const journalUsers = await getEditorialBoardUseCase
      .initialize(models)
      .execute(journal.id)

    expect(journalUsers).toHaveLength(4)
    const journalTriageEditor = journalUsers.find(
      jU => jU.email === triageEditor.alias.email,
    )
    expect(journalTriageEditor.role).toEqual(triageEditorRole)
  })
  it('returns the editorial board and staff for a specified section', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const section = fixtures.generateSection({
      properties: {
        journalId: journal.id,
      },
      Section,
    })
    const otherSection = fixtures.generateSection({
      properties: {
        journalId: journal.id,
      },
      Section,
    })

    dataService.createUserOnSection({
      models,
      section,
      fixtures,
      role: Team.Role.triageEditor,
    })

    dataService.createUserOnSection({
      models,
      section: otherSection,
      fixtures,
      role: Team.Role.triageEditor,
    })

    const sectionUsers = await getEditorialBoardUseCase
      .initialize(models)
      .execute(journal.id)

    expect(sectionUsers.length).toEqual(2)
  })
  it('returns the editorial board and staff for a special issue from journal', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const firstSpecialIssue = fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        name: chance.company(),
        endDate: new Date(chance.date({ year: 2069, string: true })),
        startDate: new Date(),
        journalId: journal.id,
      },
      SpecialIssue,
    })
    const secondSpecialIssue = fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        name: chance.company(),
        endDate: new Date(chance.date({ year: 2069, string: true })),
        startDate: new Date(),
        journalId: journal.id,
      },
      SpecialIssue,
    })

    const firstUser = fixtures.generateUser({ User, Identity })
    dataService.addUserOnSpecialIssue({
      models,
      specialIssue: firstSpecialIssue,
      fixtures,
      role: Team.Role.triageEditor,
      user: firstUser,
    })

    const secondUser = fixtures.generateUser({ User, Identity })
    dataService.addUserOnSpecialIssue({
      models,
      specialIssue: secondSpecialIssue,
      fixtures,
      role: Team.Role.triageEditor,
      user: secondUser,
    })

    const specialIssueUsers = await getEditorialBoardUseCase
      .initialize(models)
      .execute(journal.id)

    expect(specialIssueUsers.length).toEqual(2)
  })
  it('returns the editorial board and staff from special issues, sections and journals', async () => {
    const journal = fixtures.generateJournal({ Journal })
    const triageEditorRole = Team.Role.triageEditor

    dataService.createUserOnJournal({
      models,
      journal,
      fixtures,
      role: triageEditorRole,
    })
    const specialIssue = fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        name: chance.company(),
        endDate: new Date(chance.date({ year: 2069, string: true })),
        startDate: new Date(),
        journalId: journal.id,
      },
      SpecialIssue,
    })
    const specialIssueUser = fixtures.generateUser({ User, Identity })
    const specialIssueAcademicEditor = fixtures.generateUser({ User, Identity })
    const triageEditorSI = dataService.addUserOnSpecialIssue({
      models,
      specialIssue,
      fixtures,
      role: Team.Role.triageEditor,
      user: specialIssueUser,
    })
    triageEditorSI.team.specialIssueName = specialIssue.name
    const academicEditorSI = dataService.addUserOnSpecialIssue({
      models,
      specialIssue,
      fixtures,
      role: Team.Role.academicEditor,
      user: specialIssueAcademicEditor,
    })
    academicEditorSI.team.specialIssueName = specialIssue.name
    const section = fixtures.generateSection({
      properties: {
        journalId: journal.id,
      },
      Section,
    })

    const triageEditorSection = dataService.createUserOnSection({
      models,
      section,
      fixtures,
      role: Team.Role.triageEditor,
    })
    triageEditorSection.team.sectionName = section.name
    const sectionSpecialIssue = fixtures.generateSpecialIssue({
      properties: {
        isActive: false,
        name: chance.company(),
        endDate: new Date(chance.date({ year: 2069, string: true })),
        startDate: new Date(),
        sectionId: section.id,
      },
      SpecialIssue,
    })
    const sectionSpecialIssueUser = fixtures.generateUser({ User, Identity })
    const triageEditorSectionSI = dataService.addUserOnSpecialIssue({
      models,
      specialIssue: sectionSpecialIssue,
      fixtures,
      role: Team.Role.triageEditor,
      user: sectionSpecialIssueUser,
    })
    triageEditorSectionSI.team.specialIssueName = sectionSpecialIssue.name
    const editorialBoardUsers = await getEditorialBoardUseCase
      .initialize(models)
      .execute(journal.id)

    const journalTriageEditor = editorialBoardUsers.find(
      user =>
        user.role === Team.Role.triageEditor &&
        !user.sectionName &&
        !user.specialIssueName,
    )
    expect(journalTriageEditor).toBeDefined()
    const specialIssueTriageEditor = editorialBoardUsers.find(
      user => user.role === Team.Role.triageEditor && user.specialIssueName,
    )
    expect(specialIssueTriageEditor).toBeDefined()

    const specialIssueAE = editorialBoardUsers.find(
      user => user.role === Team.Role.academicEditor && user.specialIssueName,
    )
    expect(specialIssueAE).toBeDefined()

    const sectionTriageEditor = editorialBoardUsers.find(
      user => user.role === Team.Role.triageEditor && user.sectionName,
    )
    expect(sectionTriageEditor).toBeDefined()

    const sectionSpecialIssueTriageEditor = editorialBoardUsers.find(
      user => user.role === Team.Role.triageEditor && user.specialIssueName,
    )
    expect(sectionSpecialIssueTriageEditor).toBeDefined()
  })
})
