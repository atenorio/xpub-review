const moment = require('moment')

module.exports.initialize = ({ Job, Journal, eventsService }) => {
  const jobHandler = async job => {
    try {
      const { journalId } = job.data
      const journal = await Journal.find(journalId)

      journal.updateProperties({ isActive: true })
      await journal.save()

      eventsService.publishJournalEvent({
        journalId: journal.id,
        eventName: 'JournalActivated',
      })

      return job.done()
    } catch (e) {
      throw new Error(e)
    }
  }

  return {
    async scheduleJournalActivation({ journal, teamMemberId }) {
      const params = {
        journalId: journal.id,
      }

      const executionDate = moment(journal.activationDate)
        .add(3, 'hours')
        .toISOString()

      const job = await Job.schedule({
        params,
        jobHandler,
        teamMemberId,
        executionDate,
        journalId: journal.id,
      })

      return job
    },
  }
}
