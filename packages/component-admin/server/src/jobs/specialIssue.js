const logger = require('@pubsweet/logger')
const moment = require('moment')

module.exports.initialize = ({ Job, SpecialIssue, Section, eventsService }) => {
  const jobHandler = async job => {
    try {
      const { specialIssueId } = job.data
      const specialIssue = await SpecialIssue.find(specialIssueId)
      specialIssue.updateProperties({ isActive: true })
      await specialIssue.save()

      logger.info(`${specialIssueId} has been updated`)

      const { sectionId } = specialIssue
      let { journalId } = specialIssue
      let eventName = 'JournalSpecialIssueOpened'

      if (sectionId) {
        ;({ journalId } = await Section.find(sectionId))
        eventName = 'JournalSectionSpecialIssueOpened'
      }

      eventsService.publishJournalEvent({
        journalId,
        eventName,
      })

      return job.done()
    } catch (e) {
      throw new Error(e)
    }
  }
  return {
    async scheduleSpecialIssueActivation({ specialIssue, teamMemberId }) {
      const params = {
        specialIssueId: specialIssue.id,
      }

      const executionDate = moment(specialIssue.startDate)
        .add(3, 'hours')
        .toISOString()

      const job = await Job.schedule({
        params,
        jobHandler,
        teamMemberId,
        executionDate,
        specialIssueId: specialIssue.id,
      })

      return job
    },
  }
}
