module.exports = {
  isDateToday(inputDate) {
    if (!inputDate) {
      return false
    }

    const date = new Date(inputDate)
    const today = new Date()

    return (
      date.getDate() === today.getDate() &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    )
  },
  areDatesEqual(previousDate, currentDate) {
    if (!previousDate || !currentDate) {
      return false
    }
    const prevDate = new Date(previousDate)
    const currDate = new Date(currentDate)

    return (
      prevDate.getDate() === currDate.getDate() &&
      prevDate.getMonth() === currDate.getMonth() &&
      prevDate.getFullYear() === currDate.getFullYear()
    )
  },
}
