const initialize = ({ ArticleType }) => ({
  execute: async () => ArticleType.all(),
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
