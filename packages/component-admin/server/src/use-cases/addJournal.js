const { Promise } = require('bluebird')
const { isDateToday } = require('../dateService/dateService')

const initialize = ({
  models: { Journal, User, Team, JournalArticleType },
  jobsService,
  eventsService,
}) => ({
  execute: async ({ input, userId }) => {
    let journal = {}
    const {
      apc,
      name,
      code,
      issn,
      email,
      articleTypes,
      activationDate,
      peerReviewModelId,
    } = input
    if (apc < 0) {
      throw new ConflictError(`Journal APC ${apc} cannot be a negative number.`)
    }

    journal = await Journal.findUniqueJournal({ code, name, email })
    if (journal) {
      throw new ConflictError(`Journal already exists.`)
    }

    journal = new Journal({
      email,
      name,
      code,
      apc,
      issn,
      activationDate,
      peerReviewModelId,
    })

    await journal.save()
    if (isDateToday(activationDate)) {
      journal.updateProperties({ isActive: true })
      await journal.save()
    } else if (activationDate) {
      const user = await User.find(userId, 'teamMemberships.team')

      const admin = user.teamMemberships.find(
        tm => tm.team.role === Team.Role.admin,
      )

      jobsService.scheduleJournalActivation({ journal, teamMemberId: admin.id })
    }

    await Promise.each(articleTypes, async articleTypeId => {
      const journalArticleType = new JournalArticleType({
        articleTypeId,
        journalId: journal.id,
      })
      await journalArticleType.save()
    })
    eventsService.publishJournalEvent({
      journalId: journal.id,
      eventName: 'JournalAdded',
    })
    return journal.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
