const initialize = ({ models: { User, Team }, useCases, eventsService }) => ({
  execute: async ({ input, id, userId }) => {
    const user = await User.find(id, '[identities, teamMemberships.team]')

    if (input.isAdmin && input.isRIPE) {
      throw new Error(
        'A user cannot be both admin and research integrity publishing editor.',
      )
    }

    if (input.isAdmin !== undefined) {
      await useCases.editAdminUseCase
        .initialize({ Team, eventsService })
        .execute({ input, id, userId, user })
    }
    if (input.isRIPE !== undefined) {
      await useCases.editRIPEUseCase
        .initialize({ Team, eventsService })
        .execute({ input, id, userId, user })
    }

    delete input.isAdmin
    delete input.isRIPE

    const localIdentity = user.getDefaultIdentity()
    localIdentity.updateProperties(input)
    await localIdentity.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserUpdated',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
