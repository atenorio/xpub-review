const initialize = ({ models: { Team }, eventsService }) => ({
  execute: async ({ user }) => {
    const journalAdminTeam = await Team.findOrCreate({
      queryObject: {
        role: Team.Role.admin,
      },
      eagerLoadRelations: 'members',
      options: {
        role: Team.Role.admin,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
      },
    })

    const newAdmin = journalAdminTeam.addMember(user, {
      userId: user.id,
      teamId: journalAdminTeam.id,
    })
    await newAdmin.save()
    await journalAdminTeam.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

module.exports = {
  initialize,
}
