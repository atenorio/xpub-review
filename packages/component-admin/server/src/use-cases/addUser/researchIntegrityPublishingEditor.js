const initialize = ({ models: { Team }, eventsService }) => ({
  execute: async ({ user }) => {
    const ripeTeam = await Team.findOrCreate({
      queryObject: {
        role: Team.Role.researchIntegrityPublishingEditor,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
      },
      eagerLoadRelations: 'members',
      options: {
        role: Team.Role.researchIntegrityPublishingEditor,
        manuscriptId: null,
        journalId: null,
        sectionId: null,
      },
    })

    const newRIPE = ripeTeam.addMember(user, { userId: user.id })
    await newRIPE.save()
    await ripeTeam.save()

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

module.exports = {
  initialize,
}
