const { areDatesEqual, isDateToday } = require('../dateService/dateService')

module.exports = {
  async handleActivationDate({
    Job,
    Team,
    startDate,
    TeamMember,
    jobsService,
    oldStartDate,
    specialIssue,
  }) {
    if (!areDatesEqual(oldStartDate, startDate)) {
      const specialIssueJob = await Job.findOneBy({
        queryObject: {
          specialIssueId: specialIssue.id,
        },
      })
      await Job.cancel(specialIssueJob.id)
      await specialIssueJob.delete()

      if (isDateToday(startDate)) {
        specialIssue.updateProperties({
          isActive: true,
        })
        await specialIssue.save()
      } else {
        const admin = await TeamMember.findOneByRole({ role: Team.Role.admin })
        jobsService.scheduleSpecialIssueActivation({
          specialIssue,
          teamMemberId: admin.id,
        })
      }
    }
  },
}
