const initialize = ({ models: { Section }, eventsService }) => ({
  execute: async ({ id, input }) => {
    const existingSection = await Section.findUniqueSection({
      name: input.name,
    })
    if (existingSection) throw new ConflictError(`Section already exists.`)

    const section = await Section.findOneBy({ queryObject: { id } })
    section.updateProperties(input)
    await section.save()

    eventsService.publishJournalEvent({
      journalId: section.journalId,
      eventName: 'JournalSectionUpdated',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
