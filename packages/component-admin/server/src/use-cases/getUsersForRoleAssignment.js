const initialize = ({ User }) => ({
  async execute() {
    const users = await User.findAll({
      queryObject: { isActive: true },
      eagerLoadRelations: ['identities'],
    })

    return users.map(user => {
      user.identities = [user.getDefaultIdentity()]
      return user.toDTO()
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
