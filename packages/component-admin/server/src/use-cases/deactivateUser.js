const initialize = ({ models: { User, Team }, eventsService }) => ({
  execute: async ({ id, userId }) => {
    const user = await User.find(id)
    if (!user.isActive) return

    const adminTeam = await Team.findOneBy({
      queryObject: { role: Team.Role.admin },
      eagerLoadRelations: 'members',
    })

    const admin = adminTeam.members.find(member => member.userId === user.id)
    if (admin && userId === user.id) {
      throw new ConflictError('Admin cannot be deactivated.')
    } else {
      user.updateProperties({ isActive: false })
      await user.save()
    }

    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserDeactivated',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
