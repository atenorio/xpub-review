const initialize = ({ Team, eventsService }) => ({
  execute: async ({ user, id, input, userId }) => {
    const adminMember = user.getTeamMemberByRole(Team.Role.admin)

    if (!input.isAdmin && adminMember && userId === id) {
      throw new ConflictError('Cannot remove own admin role.')
    }

    if (!input.isAdmin && adminMember && userId !== id) {
      await adminMember.delete()
    }

    if (input.isAdmin && !adminMember) {
      const adminTeam = await Team.findOrCreate({
        queryObject: {
          role: Team.Role.admin,
        },
        eagerLoadRelations: 'members.[user.[identities]]',
        options: { role: Team.Role.admin, manuscriptId: null, journalId: null },
      })

      const newMember = adminTeam.addMember(user, {
        userId: user.id,
        teamId: adminTeam.id,
      })
      await newMember.save()

      eventsService.publishUserEvent({
        userId: user.id,
        eventName: 'UserUpdated',
      })
    }
  },
})

module.exports = {
  initialize,
}
