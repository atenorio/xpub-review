const { handleActivationDate } = require('./setSpecialIssueProps')

const initialize = ({
  models: { SpecialIssue, Job, Team, TeamMember, Section },
  jobsService,
  eventsService,
}) => ({
  execute: async ({ input, id }) => {
    const { startDate, endDate, name, sectionId, callForPapers } = input

    if (endDate <= startDate) {
      throw new ConflictError(`End date must be after start date.`)
    }
    const specialIssue = await SpecialIssue.find(id)
    const oldStartDate = specialIssue.startDate

    if (specialIssue.name !== name) {
      const existingSpecialIssue = await SpecialIssue.findUnique(name)
      if (existingSpecialIssue)
        throw new ConflictError(`Special issue already exists.`)
    }

    if (sectionId)
      specialIssue.updateProperties({
        name,
        endDate,
        startDate,
        sectionId,
        journalId: null,
        callForPapers,
      })
    else {
      specialIssue.updateProperties({
        name,
        endDate,
        startDate,
        callForPapers,
      })
    }
    await specialIssue.save()

    await handleActivationDate({
      Job,
      Team,
      startDate,
      TeamMember,
      jobsService,
      oldStartDate,
      specialIssue,
    })

    if (sectionId) {
      const section = await Section.find(sectionId)

      eventsService.publishJournalEvent({
        journalId: section.journalId,
        eventName: 'JournalSectionSpecialIssuesUpdated',
      })
    } else {
      eventsService.publishJournalEvent({
        journalId: specialIssue.journalId,
        eventName: 'JournalSpecialIssueUpdated',
      })
    }

    return specialIssue.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
