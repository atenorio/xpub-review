const { get } = require('lodash')

const initialize = ({
  models: { Team, TeamMember, User, SpecialIssue, Section },
  eventsService,
}) => ({
  execute: async ({
    input: { userId, role, journalId, sectionId, specialIssueId },
  }) => {
    const user = await User.find(
      userId,
      '[teamMemberships.team.section, identities]',
    )

    if (!specialIssueId) {
      const existingUserJournalMember = user.teamMemberships.find(
        member => member.team.journalId === journalId,
      )
      const exitingUserSectionMember = user.teamMemberships.find(
        member => get(member, 'team.section.journalId') === journalId,
      )
      if (existingUserJournalMember || exitingUserSectionMember) {
        throw new ValidationError(
          `User already has an editorial role on this journal.`,
        )
      }
    } else {
      const existingUserSpecialIssueMember = user.teamMemberships.find(
        member => member.team.specialIssueId === specialIssueId,
      )
      if (existingUserSpecialIssueMember) {
        throw new ValidationError(
          `User already has an editorial role on this special issue.`,
        )
      }
    }

    const options = { role }
    const option = { label: 'journalId', value: journalId }
    if (specialIssueId) {
      option.label = 'specialIssueId'
      option.value = specialIssueId
    }
    if (sectionId) {
      option.label = 'sectionId'
      option.value = sectionId
    }
    options[option.label] = option.value

    const team = await Team.findOrCreate({
      queryObject: options,
      options,
      eagerLoadRelations: 'members',
    })

    let isCorresponding
    if (role === Team.Role.editorialAssistant) {
      const correspondingEditorialAssistant = await TeamMember.findCorrespondingEditorialAssistant(
        journalId,
      )
      if (!correspondingEditorialAssistant) isCorresponding = true
      else isCorresponding = false
    }

    const teamMember = team.addMember(user, {
      teamId: team.id,
      userId: user.id,
      isCorresponding,
    })
    await teamMember.save()
    await team.save()

    const eventObject = await getEventObject({
      journalId,
      sectionId,
      specialIssueId,
      Section,
      SpecialIssue,
    })
    eventsService.publishJournalEvent(eventObject)
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}

const getEventObject = async ({
  specialIssueId,
  sectionId,
  journalId,
  SpecialIssue,
  Section,
}) => {
  const eventObject = { journalId: '', eventName: '' }

  if (!sectionId && !specialIssueId) {
    eventObject.journalId = journalId
    eventObject.eventName = 'JournalEditorAssigned'
    return eventObject
  }

  if (specialIssueId) {
    const specialIssue = await SpecialIssue.find(specialIssueId)
    if (specialIssue.sectionId) {
      const section = await Section.find(specialIssue.sectionId)
      eventObject.journalId = section.journalId
      eventObject.eventName = 'JournalSectionSpecialIssueEditorAssigned'
      return eventObject
    }
    eventObject.journalId = specialIssue.journalId
    eventObject.eventName = 'JournalSpecialIssueEditorAssigned'
    return eventObject
  }

  if (sectionId) {
    const section = await Section.find(sectionId)
    eventObject.journalId = section.journalId
    eventObject.eventName = 'JournalSectionEditorAssigned'
    return eventObject
  }
}
