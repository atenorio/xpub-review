const { isDateToday } = require('../dateService/dateService')

const initialize = ({
  models: { SpecialIssue, TeamMember, Team, PeerReviewModel, Section },
  jobsService,
  eventsService,
}) => ({
  execute: async ({
    name,
    startDate,
    endDate,
    journalId,
    sectionId,
    callForPapers,
  }) => {
    if (endDate <= startDate) {
      throw new ConflictError(`End date must be after start date.`)
    }

    const existingSpecialIssue = await SpecialIssue.findUnique(name)
    if (existingSpecialIssue)
      throw new ConflictError(`Special issue already exists.`)

    const specialIssuePeerReviewModel = await PeerReviewModel.findOneBy({
      queryObject: {
        name: 'Special Issue Chief Minus',
      },
    })

    let specialIssue
    if (sectionId) {
      specialIssue = new SpecialIssue({
        name,
        endDate,
        startDate,
        sectionId,
        callForPapers,
        peerReviewModelId: specialIssuePeerReviewModel.id,
      })
    } else {
      specialIssue = new SpecialIssue({
        name,
        endDate,
        startDate,
        journalId,
        callForPapers,
        peerReviewModelId: specialIssuePeerReviewModel.id,
      })
    }
    await specialIssue.save()

    if (isDateToday(startDate)) {
      specialIssue.updateProperties({ isActive: true })
      await specialIssue.save()
    } else if (startDate) {
      const admin = await TeamMember.findOneByRole({ role: Team.Role.admin })
      jobsService.scheduleSpecialIssueActivation({
        specialIssue,
        teamMemberId: admin.id,
      })
    }

    if (sectionId) {
      eventsService.publishJournalEvent({
        journalId,
        eventName: 'JournalSectionSpecialIssueAdded',
      })
    } else {
      eventsService.publishJournalEvent({
        journalId,
        eventName: 'JournalSpecialIssueAdded',
      })
    }
    return specialIssue.toDTO()
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
