const { Promise } = require('bluebird')

const initialize = ({ Journal, Section, Team, SpecialIssue }) => ({
  execute: async journalId => {
    const journal = await Journal.find(journalId)
    const sections = await Section.findAllByJournal({
      journalId: journal.id,
    })
    const journalSpecialIssues = await SpecialIssue.findAllByJournal({
      journalId: journal.id,
    })
    let sectionSpecialIssues = []
    await Promise.each(sections, async section => {
      sectionSpecialIssues = [
        ...sectionSpecialIssues,
        ...(await SpecialIssue.findAllBySection({ sectionId: section.id })),
      ]
    })
    const specialIssues = [...journalSpecialIssues, ...sectionSpecialIssues]
    const teamReducer = (acc, team) => {
      const members = team.members.map(member => ({
        id: member.id,
        role: team.role,
        sectionName: team.sectionName,
        specialIssueName: team.specialIssueName,
        email: member.alias.email,
        isCorresponding: member.isCorresponding,
        fullName: {
          surname: member.alias.surname,
          givenNames: member.alias.givenNames,
          title: member.alias.title,
        },
      }))

      return [...acc, ...members]
    }

    const journalTeams = await Team.findAllByJournal({
      journalId: journal.id,
    })
    let sectionsTeams = []
    await Promise.each(sections, async section => {
      sectionsTeams = [
        ...sectionsTeams,
        ...(await Team.findAllBySection({ sectionId: section.id })),
      ]
    })
    let specialIssuesTeams = []
    await Promise.each(
      specialIssues,
      async specialIssue =>
        (specialIssuesTeams = [
          ...specialIssuesTeams,
          ...(await Team.findAllBySpecialIssue({
            specialIssueId: specialIssue.id,
          })),
        ]),
    )
    return [...journalTeams, ...sectionsTeams, ...specialIssuesTeams]
      .filter(
        team =>
          [
            Team.Role.triageEditor,
            Team.Role.academicEditor,
            Team.Role.editorialAssistant,
          ].includes(team.role) && !team.manuscriptId,
      )
      .reduce(teamReducer, [])
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
