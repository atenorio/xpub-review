const initialize = ({ models: { SpecialIssue, Section }, eventsService }) => ({
  execute: async ({ id, input: { reason } }) => {
    const specialIssue = await SpecialIssue.find(id)

    if (!specialIssue.isActive) {
      throw new Error(`Inactive special issues can't be cancelled.`)
    }

    specialIssue.updateProperties({
      isCancelled: true,
      cancelReason: reason,
    })
    await specialIssue.save()
    if (specialIssue.sectionId) {
      const section = await Section.find(specialIssue.sectionId)
      eventsService.publishJournalEvent({
        journalId: section.journalId,
        eventName: 'JournalSectionSpecialIssueUpdated',
      })
    } else {
      eventsService.publishJournalEvent({
        journalId: specialIssue.journalId,
        eventName: 'JournalSpecialIssueUpdated',
      })
    }
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
