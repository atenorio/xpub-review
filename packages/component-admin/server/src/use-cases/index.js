const getArticleTypesUseCase = require('./getArticleTypes')
const getPeerReviewModelsUseCase = require('./getPeerReviewModels')

const getJournalsUseCase = require('./getJournals')
const addJournalUseCase = require('./addJournal')
const editJournalUseCase = require('./editJournal')

const addSectionUseCase = require('./addSection')
const editSectionUseCase = require('./editSection')

const addSpecialIssueUseCase = require('./addSpecialIssue')
const editSpecialIssueUseCase = require('./editSpecialIssue')
const cancelSpecialIssueUseCase = require('./cancelSpecialIssue')

const getUsersForAdminPanelUseCase = require('./getUsersForAdminPanel')
const addUserFromAdminPanelUseCase = require('./addUserFromAdminPanel')
const addAdminFromAdminPanelUseCase = require('./addUser/admin')
const addRIPEFromAdminPanelUseCase = require('./addUser/researchIntegrityPublishingEditor')
const editUserFromAdminPanelUseCase = require('./editUserFromAdminPanel')
const editAdminUseCase = require('./editUser/admin')
const editRIPEUseCase = require('./editUser/researchIntegrityPublishingEditor')
const activateUserUseCase = require('./activateUser')
const deactivateUserUseCase = require('./deactivateUser')

const getEditorialBoardUseCase = require('./getEditorialBoard')
const getUsersForRoleAssignmentUseCase = require('./getUsersForRoleAssignment')
const assignEditorialRoleUseCase = require('./assignEditorialRole')
const assignLeadEditorialAssistantUseCase = require('./assignLeadEditorialAssistant')

module.exports = {
  editRIPEUseCase,
  editAdminUseCase,
  addJournalUseCase,
  addSectionUseCase,
  getJournalsUseCase,
  editJournalUseCase,
  editSectionUseCase,
  activateUserUseCase,
  deactivateUserUseCase,
  getArticleTypesUseCase,
  addSpecialIssueUseCase,
  editSpecialIssueUseCase,
  getEditorialBoardUseCase,
  cancelSpecialIssueUseCase,
  assignEditorialRoleUseCase,
  getPeerReviewModelsUseCase,
  addRIPEFromAdminPanelUseCase,
  getUsersForAdminPanelUseCase,
  addUserFromAdminPanelUseCase,
  addAdminFromAdminPanelUseCase,
  editUserFromAdminPanelUseCase,
  getUsersForRoleAssignmentUseCase,
  assignLeadEditorialAssistantUseCase,
}
