const uuid = require('uuid')

const initialize = ({
  useCases,
  createSSOUser,
  eventsService,
  notification: notificationService,
  models: { User, Identity, Team },
}) => ({
  execute: async input => {
    if (input.isAdmin && input.isRIPE) {
      throw new Error(
        'User cannot be both admin and research integrity publishing editor.',
      )
    }

    const userIdentity = await Identity.findOneByEmail(input.email)
    if (userIdentity) {
      throw new ConflictError('User already exists in the database.')
    }
    const newUserDetails = {
      defaultIdentity: 'local',
      isActive: true,
      isSubscribedToEmails: true,
      confirmationToken: uuid.v4(),
      unsubscribeToken: uuid.v4(),
      agreeTc: true,
    }

    const user = new User(newUserDetails)
    await user.save()

    const newIdentityDetails = {
      type: 'local',
      isConfirmed: false,
      passwordHash: null,
      givenNames: input.givenNames,
      surname: input.surname,
      email: input.email,
      aff: input.aff,
      title: input.title,
      userId: user.id,
      country: input.country,
    }
    const identity = new Identity(newIdentityDetails)
    await identity.save()
    await user.assignIdentity(identity)

    let useCase
    if (input.isAdmin) useCase = 'addAdminFromAdminPanelUseCase'
    if (input.isRIPE) useCase = 'addRIPEFromAdminPanelUseCase'

    if (useCase) {
      await useCases[useCase]
        .initialize({ models: { Team }, eventsService })
        .execute({ user })
    } else await user.save()

    if (process.env.KEYCLOAK_SERVER_URL) {
      await createSSOUser({
        identity: newIdentityDetails,
        user: newUserDetails,
      })
    } else {
      await notificationService.notifyUserAddedByAdmin({
        user,
        identity,
        isAdmin: input.isAdmin,
        isRIPE: input.isRIPE,
      })
    }
    eventsService.publishUserEvent({
      userId: user.id,
      eventName: 'UserAdded',
    })
  },
})

const authsomePolicies = ['admin']

module.exports = {
  initialize,
  authsomePolicies,
}
