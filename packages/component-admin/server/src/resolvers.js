const models = require('@pubsweet/models')
const { createSSOUser } = require('component-sso')
const { withAuthsomeMiddleware } = require('helper-service')

const useCases = require('./use-cases')
const notification = require('./notifications/notification')
const JournalJobsService = require('./jobs/journal')
const SpecialIssueJobsService = require('./jobs/specialIssue')

const events = require('component-events')

const resolvers = {
  Query: {
    async getUsersForAdminPanel(_, { page, pageSize }, ctx) {
      return useCases.getUsersForAdminPanelUseCase
        .initialize(models)
        .execute(page, pageSize)
    },
    async getJournals(_, { input }, ctx) {
      return useCases.getJournalsUseCase.initialize(models).execute()
    },
    async getUsersForRoleAssignment(_, ctx) {
      return useCases.getUsersForRoleAssignmentUseCase
        .initialize(models)
        .execute()
    },
    async getArticleTypes() {
      return useCases.getArticleTypesUseCase.initialize(models).execute()
    },
    async getEditorialBoard(_, { journalId }, ctx) {
      return useCases.getEditorialBoardUseCase
        .initialize(models)
        .execute(journalId)
    },
    async getPeerReviewModels(_, { input }, ctx) {
      return useCases.getPeerReviewModelsUseCase.initialize(models).execute()
    },
  },
  Mutation: {
    async addUserFromAdminPanel(_, { input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.addUserFromAdminPanelUseCase
        .initialize({
          notification,
          models,
          useCases,
          eventsService,
          createSSOUser,
        })
        .execute(input)
    },
    async activateUser(_, { id }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.activateUserUseCase
        .initialize({ models, eventsService })
        .execute(id)
    },
    async deactivateUser(_, { id }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.deactivateUserUseCase
        .initialize({ models, eventsService })
        .execute({ id, userId: ctx.user })
    },
    async editUserFromAdminPanel(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.editUserFromAdminPanelUseCase
        .initialize({ models, useCases, eventsService })
        .execute({ input, id, userId: ctx.user })
    },
    async addJournal(_, { input }, ctx) {
      const { Job, Journal } = models
      const eventsService = events.initialize({ models })

      const jobsService = JournalJobsService.initialize({
        Job,
        Journal,
        eventsService,
      })

      return useCases.addJournalUseCase
        .initialize({ models, jobsService, eventsService })
        .execute({ input, userId: ctx.user })
    },
    async editJournal(_, { id, input }, ctx) {
      const { Job, Journal } = models
      const eventsService = events.initialize({ models })

      const jobsService = JournalJobsService.initialize({
        Job,
        Journal,
        eventsService,
      })

      return useCases.editJournalUseCase
        .initialize({ models, jobsService, eventsService })
        .execute({ input, id, reqUserId: ctx.user })
    },
    async assignEditorialRole(_, { input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.assignEditorialRoleUseCase
        .initialize({ models, eventsService })
        .execute({ input })
    },
    async addSection(_, { name, journalId }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.addSectionUseCase
        .initialize({ models, eventsService })
        .execute({ name, journalId })
    },
    async editSection(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })

      return useCases.editSectionUseCase
        .initialize({ models, eventsService })
        .execute({ id, input })
    },
    async addSpecialIssue(_, { input }, ctx) {
      const { Job, Section, SpecialIssue } = models
      const eventsService = events.initialize({ models })

      const jobsService = SpecialIssueJobsService.initialize({
        Job,
        Section,
        SpecialIssue,
        eventsService,
      })

      return useCases.addSpecialIssueUseCase
        .initialize({ models, jobsService, eventsService })
        .execute(input)
    },
    async cancelSpecialIssue(_, { id, input }, ctx) {
      const eventsService = events.initialize({ models })
      return useCases.cancelSpecialIssueUseCase
        .initialize({ models, eventsService })
        .execute({ id, input })
    },
    async editSpecialIssue(_, { id, input }, ctx) {
      const { Job, Section, SpecialIssue } = models
      const eventsService = events.initialize({ models })

      const jobsService = SpecialIssueJobsService.initialize({
        Job,
        Section,
        SpecialIssue,
        eventsService,
      })

      return useCases.editSpecialIssueUseCase
        .initialize({ models, jobsService, eventsService })
        .execute({ input, id })
    },
    async assignLeadEditorialAssistant(_, { id }, ctx) {
      return useCases.assignLeadEditorialAssistantUseCase
        .initialize({ models })
        .execute({ id })
    },
  },
}

module.exports = withAuthsomeMiddleware(resolvers, useCases)
