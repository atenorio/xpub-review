import gql from 'graphql-tag'

export const loadReviewerSuggestions = gql`
  query loadReviewerSuggestions($manuscriptId: String!) {
    loadReviewerSuggestions(manuscriptId: $manuscriptId) {
      id
      email
      givenNames
      surname
      aff
      profileUrl
      numberOfReviews
      isInvited
    }
  }
`
