import React from 'react'
import 'jest-styled-components'
import '@testing-library/jest-dom/extend-expect'
import { waitForElement, fireEvent, cleanup } from '@testing-library/react'
import PublonsTable from '../components/PublonsTable'
import { render } from './testUtils'

// eslint-disable-next-line jest/no-disabled-tests
describe.skip('Publons Table ', () => {
  beforeEach(() => {
    cleanup()
  })
  const canInvitePublons = true
  it('Should render all reviews', async done => {
    jest.setTimeout(30000)

    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="558131fe-b308-548b-8e6c-750c59380466"
      />,
    )
    let firstReviewer
    let secondReviewer

    try {
      firstReviewer = await waitForElement(() =>
        getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
      )
      secondReviewer = await waitForElement(() =>
        getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc149'),
      )
    } catch (error) {
      console.error(error)
    }
    setTimeout(() => {
      expect(firstReviewer.textContent).toEqual('Lora Brent')
      expect(
        getByTestId('aff-9484df8e-e5c0-58d8-bc4e-0642409fc349').textContent,
      ).toEqual('Alaska Air Group, Inc.')
      expect(
        getByTestId('numberOfReviews-9484df8e-e5c0-58d8-bc4e-0642409fc349')
          .textContent,
      ).toEqual('34')
      expect(
        getByTestId(
          'reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349',
        ),
      ).toBeInTheDocument()

      expect(secondReviewer.textContent).toEqual('Charles Lottie')
      expect(
        getByTestId('aff-9484df8e-e5c0-58d8-bc4e-0642409fc149').textContent,
      ).toEqual('Corning Inc.')
      expect(
        getByTestId('numberOfReviews-9484df8e-e5c0-58d8-bc4e-0642409fc149')
          .textContent,
      ).toEqual('55')
      expect(
        getByTestId(
          'reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc149',
        ),
      ).toBeInTheDocument()
      done()
    })
  }, 30000)

  it('Should open modal after inviting one reviewer', async done => {
    jest.setTimeout(30000)

    const { getByTestId, getByText } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="558131fe-b308-548b-8e6c-750c59380466"
      />,
    )
    try {
      await waitForElement(() =>
        getByTestId('name-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
      )
    } catch (error) {
      console.error(error)
    }

    expect(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    ).toBeInTheDocument()

    fireEvent.click(
      getByTestId('reviewerPublonsButton-9484df8e-e5c0-58d8-bc4e-0642409fc349'),
    )
    setTimeout(() => {
      expect(getByText('Send Invitation to Review?')).toBeInTheDocument()
      expect(getByText('SEND')).toBeInTheDocument()
      expect(getByText('BACK')).toBeInTheDocument()
      done()
    })
  }, 30000)

  it('Should return error if there are no reviewers', async done => {
    jest.setTimeout(30000)

    const { getByTestId } = render(
      <PublonsTable
        canInvitePublons={canInvitePublons}
        manuscriptId="6638-2324"
      />,
    )
    try {
      await waitForElement(() => getByTestId('error-empty-state'))
    } catch (error) {
      console.error(error)
    }
    setTimeout(() => {
      expect(getByTestId('error-empty-state')).toBeInTheDocument()
      expect(getByTestId('error-empty-state')).toHaveTextContent(
        'There are no reviewer suggestions to display',
      )
      done()
    })
  }, 30000)
})
