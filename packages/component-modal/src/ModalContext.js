import { createContext } from 'react'

export default createContext({
  modalKey: null,
  showModal: props => () => {},
  hideModal: () => {},
})
