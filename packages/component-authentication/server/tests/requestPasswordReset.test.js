process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const { fixtures, models } = require('fixture-service')

const { User, Identity } = models
const requestPassword = require('../src/use-cases/requestPasswordReset')

const notificationService = {
  sendForgotPasswordEmail: jest.fn(),
}

describe('request password reset use case', () => {
  it('returns an error when the user has already requested a password reset', async () => {
    const user = fixtures.generateUser({
      properties: {
        passwordResetTimestamp: Date.now(),
      },
      User,
      Identity,
    })

    try {
      await requestPassword
        .initialize(notificationService, models)
        .execute(user.identities[0].email)
    } catch (e) {
      expect(e.message).toEqual('A password reset has already been requested.')
    }
  })

  it('returns success when the user is inactive', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: false,
      },
      User,
      Identity,
    })

    const result = requestPassword
      .initialize(notificationService, models)
      .execute(user.identities[0].email)
    return expect(result).resolves.not.toThrow()
  })

  it('sets the password reset properties when the body is correct', async () => {
    const user = fixtures.generateUser({
      properties: { isActive: true },
      User,
      Identity,
    })

    await requestPassword
      .initialize(notificationService, models)
      .execute(user.identities[0].email)

    expect(user.passwordResetToken).not.toBeNull()
    expect(user.passwordResetTimestamp).not.toBeNull()
  })

  it('returns success if the email is non-existent', async () => {
    const email = 'email@domain.com'

    const result = requestPassword
      .initialize(notificationService, models)
      .execute(email)

    return expect(result).resolves.not.toThrow()
  })
})
