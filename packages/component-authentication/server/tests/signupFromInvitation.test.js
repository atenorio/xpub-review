process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { fixtures, models, services } = require('fixture-service')

const { User, Identity } = models
const { signUpFromInvitationUseCase } = require('../src/use-cases')

const chance = new Chance()

const generateInput = () => ({
  givenNames: chance.first(),
  surname: chance.first(),
  title: chance.pickone(['mr', 'mrs', 'miss', 'ms', 'dr', 'prof']),
  country: chance.country(),
  aff: chance.company(),
  password: 'Password1!',
  email: chance.email(),
  agreeTc: true,
  confirmationToken: chance.guid(),
})

describe('signupFromInvitation use case', () => {
  let input

  beforeEach(() => {
    input = generateInput()
  })

  it('return success', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
        isConfirmed: false,
      },
      User,
      Identity,
    })
    input.email = user.identities[0].email
    input.confirmationToken = user.confirmationToken

    const result = await signUpFromInvitationUseCase
      .initialize(services.tokenService, models)
      .execute(input)
    expect(result.token).toBeDefined()
  })

  it('return an error when confirmation token is not valid', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
        isConfirmed: false,
      },
      User,
      Identity,
    })
    input.email = user.identities[0].email

    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Invalid confirmation token.')
    }
  })

  it('return error for password too weak', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: true,
      },
      User,
      Identity,
    })
    input.email = user.identities[0].email

    input.password = 'weak-very-weak'
    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual(
        'Password is too weak. Please check password requirements.',
      )
    }
  })

  it('return an error if the user is deactivated', async () => {
    const user = fixtures.generateUser({
      properties: {
        isActive: false,
      },
      User,
      Identity,
    })
    input.email = user.identities[0].email
    input.confirmationToken = user.confirmationToken

    const result = signUpFromInvitationUseCase
      .initialize(services.tokenService, models)
      .execute(input)

    return expect(result).rejects.toThrow('Something went wrong.')
  })

  it('return error when identity does not exist', async () => {
    input.email = 'email@domain.com'
    try {
      await signUpFromInvitationUseCase
        .initialize(services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Something went wrong.')
    }
  })
})
