process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
process.env.SUPPRESS_NO_CONFIG_WARNING = true

const Chance = require('chance')
const { fixtures, models, services } = require('fixture-service')

const { signUpUseCase } = require('../src/use-cases')

const chance = new Chance()

const notificationService = {
  notifyUserOnSignUp: jest.fn(),
}

const generateInput = () => ({
  givenNames: chance.first(),
  surname: chance.first(),
  title: chance.pickone(['mr', 'mrs', 'miss', 'ms', 'dr', 'prof']),
  country: chance.country(),
  affiliation: chance.company(),
  password: 'Password1!',
  email: chance.email(),
  agreeTc: true,
})

describe('signup use case', () => {
  let input

  beforeEach(() => {
    input = generateInput()
  })

  it('returns a new user', async () => {
    const result = await signUpUseCase
      .initialize(notificationService, services.tokenService, models)
      .execute(input)

    expect(result.token).toBeDefined()
  })

  it('returns an error when the user is logged in', async () => {
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, models)
        .execute(input, chance.hash())
    } catch (e) {
      expect(e.message).toEqual('Cannot sign up while logged in.')
    }
  })

  it('returns an error when agree T&C is false', async () => {
    input.agreeTC = false
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('Terms & Conditions must be read and approved.')
    }
  })

  it('returns an error when the password is weak', async () => {
    input.password = 'weak-password'
    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual(
        'Password is too weak. Please check password requirements.',
      )
    }
  })

  it('returns an error if the user already exists in the db', async () => {
    await signUpUseCase
      .initialize(notificationService, services.tokenService, models)
      .execute(input)

    const initialUsersLength = fixtures.users.length

    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('User already exists.')
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })

  it('returns an error if the user already exists in the db (uppercase email)', async () => {
    await signUpUseCase
      .initialize(notificationService, services.tokenService, models)
      .execute(input)

    const initialUsersLength = fixtures.users.length
    input.email = input.email.toUpperCase()

    try {
      await signUpUseCase
        .initialize(notificationService, services.tokenService, models)
        .execute(input)
    } catch (e) {
      expect(e.message).toEqual('User already exists.')
      expect(fixtures.users.length).toEqual(initialUsersLength)
    }
  })
})
