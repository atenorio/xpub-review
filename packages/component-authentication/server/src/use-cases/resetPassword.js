const config = require('config')

const initialize = (logger, { User, Identity }) => ({
  execute: async ({ email, password, token }) => {
    if (!config.passwordStrengthRegex.test(password))
      throw new ValidationError(
        'Password is too weak. Please check password requirements.',
      )

    const identity = await Identity.findOneByEmail(email)
    if (!identity) return

    const user = await User.find(identity.userId)
    if (!user) return

    if (token !== user.passwordResetToken) {
      logger.error(
        `invite pw reset tokens do not match: REQ ${token} vs. DB ${user.passwordResetToken}`,
      )
      throw new Error('Invalid request.')
    }

    user.passwordResetToken = null
    user.passwordResetTimestamp = null

    await user.save()

    identity.updateProperties({
      isConfirmed: true,
    })
    const passwordHash = await Identity.hashPassword(password)

    identity.passwordHash = passwordHash
    await identity.save()
  },
})

const authsomePolicies = ['isUnauthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
