const { v4 } = require('uuid')
const config = require('config')

const hoursUntilNextPasswordReset = config.get(
  'password-reset.hoursUntilNextRequest',
)
const initialize = (notificationService, { User, Identity }) => ({
  execute: async email => {
    const identity = await Identity.findOneByEmail(email)
    if (!identity) return

    const user = await User.find(identity.userId, 'identities')
    if (!user.isActive) return

    if (user.passwordResetTimestamp) {
      const resetDate = new Date(user.passwordResetTimestamp)
      const hoursPassed = Math.floor(
        (new Date().getTime() - resetDate) / (60 * 60 * 1000),
      )
      if (hoursPassed < hoursUntilNextPasswordReset) {
        throw new ConflictError('A password reset has already been requested.')
      }
    }

    user.passwordResetToken = v4()
    user.passwordResetTimestamp = new Date().toISOString()

    await user.save()

    await notificationService.sendForgotPasswordEmail({
      user,
      identity: user.getDefaultIdentity(),
    })
  },
})

const authsomePolicies = ['isUnauthenticated']

module.exports = {
  initialize,
  authsomePolicies,
}
