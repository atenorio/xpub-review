import React from 'react'
import { get } from 'lodash'
import { Spinner } from '@pubsweet/ui'
import { useQuery } from 'react-apollo'
import { Redirect, Route } from 'react-router-dom'
import { useKeycloak } from 'component-sso/client'

import { queries } from './graphql'
import { setToken, removeToken } from './utils'

const toLogin = location => ({
  pathname: '/login',
  state: {
    from: location,
  },
})

const AuthenticatedComponent = ({
  component: Component,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')

  if (!window.localStorage.getItem('token')) {
    return <Redirect to={toLogin(location)} />
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    return <Redirect to={toLogin(location)} />
  }

  return (
    <Route
      render={routerProps => (
        <Component currentUser={currentUser} {...routerProps} />
      )}
      {...rest}
    />
  )
}

const AuthenticatedKeycloakComponent = ({
  component: Component,
  keycloak,
  location,
  ...rest
}) => {
  const { loading, data } = useQuery(queries.currentUser)
  const currentUser = get(data, 'currentUser')

  if (!window.localStorage.getItem('token')) {
    keycloak.login()
    return null
  }

  if (loading) return <Spinner />

  if (!currentUser) {
    removeToken()
    keycloak.logout()
    return null
  }

  return (
    <Route
      render={routerProps => (
        <Component currentUser={currentUser} {...routerProps} />
      )}
      {...rest}
    />
  )
}

const AuthenticatedKeycloakRoute = ({
  keycloak,
  component: Component,
  location,
  ...rest
}) => {
  if (keycloak.authenticated) {
    if (!window.localStorage.getItem('token')) {
      setToken(keycloak.token)
    }
  } else {
    removeToken()
    keycloak.login()
  }
  return (
    <AuthenticatedKeycloakComponent
      component={Component}
      keycloak={keycloak}
      location={location}
      {...rest}
    />
  )
}

const AuthenticatedRoute = ({ component: Component, location, ...rest }) => {
  const keycloak = useKeycloak()
  if (keycloak) {
    return (
      <AuthenticatedKeycloakRoute
        component={Component}
        keycloak={keycloak}
        location={location}
        {...rest}
      />
    )
  }
  return (
    <AuthenticatedComponent
      component={Component}
      location={location}
      {...rest}
    />
  )
}

export default AuthenticatedRoute
