A login form.

```js
<LoginForm loginUser={(values, props) => console.log('login', values)} />
```

Login request pending.

```js
<LoginForm
  isFetching
  loginUser={(values, props) => console.log('login', values)}
/>
```

Login form with error.

```js
<LoginForm
  fetchingError="Oops! Something went wrong..."
  loginUser={(values, props) => console.log('login', values)}
/>
```
