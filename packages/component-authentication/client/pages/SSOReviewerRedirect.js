import React, { useEffect } from 'react'
import { Loader, parseSearchParams } from '@hindawi/ui'

const SSOReviewRedirect = ({ location, history }) => {
  useEffect(() => {
    const { submissionId, invitationId, manuscriptId } = parseSearchParams(
      location.search,
    )

    history.push({
      pathname: `/emails/accept-review`,
      search: `?invitationId=${invitationId}&manuscriptId=${manuscriptId}&submissionId=${submissionId}`,
    })
  }, [history, location.search])

  return <Loader />
}

export default SSOReviewRedirect
