import React from 'react'
import { Button, H2, Spinner } from '@pubsweet/ui'
import { compose, lifecycle, withState } from 'recompose'
import { Row, Text, ShadowedBox } from '@hindawi/ui'

import { parseSearchParams, setToken } from '../utils'

import withAuthenticationGQL from '../graphql'

const loading = `Loading...`
const confirmTitle = `Welcome to Hindawi!`
const confirmSubtitle = `Your account has been successfully confirmed.`
const errorTitle = `Something went wrong. Please try again.`

const ConfirmAccount = ({ message: { title, subtitle }, history }) => (
  <ShadowedBox center mt={5}>
    <H2>{title}</H2>
    <Row mb={title !== errorTitle ? 2 : 0} mt={2}>
      <Text>{subtitle}</Text>
    </Row>
    {title !== errorTitle && (
      <Row>
        {title === loading ? (
          <Spinner />
        ) : (
          <Button onClick={() => history.replace('/')} primary>
            Go to Dashboard
          </Button>
        )}
      </Row>
    )}
  </ShadowedBox>
)

export default compose(
  withAuthenticationGQL,
  withState('message', 'setConfirmMessage', {
    title: loading,
  }),
  lifecycle({
    componentDidMount() {
      const { location, confirmUser, setConfirmMessage } = this.props
      const { confirmationToken, userId } = parseSearchParams(location.search)
      confirmUser({
        variables: {
          input: {
            userId,
            token: confirmationToken,
          },
        },
      })
        .then(r => {
          setConfirmMessage({
            title: confirmTitle,
            subtitle: confirmSubtitle,
          })
          setToken(r.data.confirmAccount.token)
        })
        .catch(err => {
          setConfirmMessage({
            title: errorTitle,
            subtitle: err.message,
          })
        })
    },
  }),
)(ConfirmAccount)
