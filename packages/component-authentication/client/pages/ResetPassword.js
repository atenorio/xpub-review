import { compose, withHandlers } from 'recompose'
import { withFetching } from '@hindawi/ui'

import withAuthenticationGQL from '../graphql'
import { ResetPasswordForm } from '../components'

export default compose(
  withFetching,
  withAuthenticationGQL,
  withHandlers({
    requestResetPassword: ({
      history,
      setError,
      setFetching,
      requestPasswordReset,
    }) => (values, formProps) => {
      setFetching(true)
      requestPasswordReset({
        variables: values,
      })
        .then(r => {
          setFetching(false)
          history.push('/info-page', {
            path: '/login',
            buttonText: 'Go to login screen',
            title: 'Reset Password Email Sent',
          })
        })
        .catch(e => {
          setFetching(false)
          setError(e.message)
        })
    },
  }),
)(ResetPasswordForm)
