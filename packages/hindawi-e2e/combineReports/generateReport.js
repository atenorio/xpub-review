const combine = require('./combine.js')

const data = combine.combineMochaAwesomeReports()
const path = require('path')
const uuidv1 = require('uuid/v1')
const rimraf = require('rimraf')
const shell = require('shelljs')

const uuid = uuidv1()
const date = Date.now()

combine.writeReport(data, uuid)

shell.exec(
  `../../node_modules/.bin/marge ./${uuid}.json --saveHtml --reportFilename=${date}.html --reportDir=Reports `,
  (code, stdout, stderr) => {
    if (stderr) throw stderr
    rimraf(path.join(__dirname, '..', 'mochawesome-report'), () => {})
    rimraf(path.join(__dirname, '..', `${uuid}.json`), () => {})
  },
)
