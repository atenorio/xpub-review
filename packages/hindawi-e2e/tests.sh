#!/bin/bash

x=0

echo "Select what tests you want to run"
echo "1. Clear database"
echo "2. Create users from fixtures"
echo "3. Run integration tests for peer review main flow"
echo "4. Run ALL integration tests for peer review"
echo "5. Run user actions flow tests"
echo "6. Run bugs & new features tests"
echo "7. Run regression gql tests"
echo "8. Quit"

function0() {
    echo "=============================================================================================="
    echo "Please select another action"
    echo "1. Clear database"
    echo "2. Create users from fixtures"
    echo "3. Run integration tests for peer review main flow"
    echo "4. Run ALL integration tests for peer review"
    echo "5. Run user actions flow tests"
    echo "6. Run bugs & new features tests"
    echo "7. Run regression gql tests "
    echo "8. Quit"
}

while [ $x = 0 ]
do
  read answer
  case "$answer" in
    1)
    echo "Clearing database ... "
    yarn cypress run --spec "cypress/integration/db-scripts/clearDatabase.spec.js" --reporter "../../node_modules/mochawesome"
    sleep 3
    echo "Database cleared"
    echo "Running migrations"
    cd "../app"
    npx pubsweet migrate
    function0
    ;;
    2)
    echo "Creating users ..."
    yarn cypress run --spec "cypress/integration/db-scripts/createUser.spec.js" --reporter "../../node_modules/mochawesome"
    sleep 3
    echo "Users have been created!"
    function0
    ;;
    3)
    echo "Running peer review main flow ..."
    yarn cypress run --spec "cypress/integration/peerReview/MainFlow.spec.js" --reporter "../../node_modules/mochawesome" --reporter-options "overwrite=false,reportTitle=report.html,showPassed=true"
    echo "Generating Report"
    node ./combineReports/generateReport.js
    sleep 3 
    function0
    ;;
    4)
    echo "Running ALL peer review flows ..."
    yarn cypress run --spec "cypress/integration/peerReview/*" --reporter "../../node_modules/mochawesome" --reporter-options "overwrite=false,reportTitle=report.html,showPassed=true"
    echo "Generating Report"
    node ./combineReports/generateReport.js
    sleep 3 
    function0
    ;;
    5)
    echo "Running user actions flows ... "
    yarn cypress run --spec "cypress/integration/user-flow/*" --reporter "../../node_modules/mochawesome" --reporter-options "overwrite=false,reportTitle=report.html,showPassed=true"
    echo "Generating Report"
    node ./combineReports/generateReport.js
    sleep 3 
    function0
    ;;
    6)
    echo "Running bugs & new features ... "
    yarn cypress run --spec "cypress/integration/bugs/*" --reporter "../../node_modules/mochawesome" --reporter-options "overwrite=false,reportTitle=report.html,showPassed=true"
    sleep 3 
    function0
    ;;
    7)
    echo "Running regression for gql ... "
    yarn cypress run --spec "cypress/integration/regressionGql/*" --reporter "../../node_modules/mochawesome" --reporter-options "overwrite=false,reportTitle=report.html,showPassed=true"
    sleep 3 
    function0
    ;;
    8)
    echo "Exiting ..."
    x=1
    ;;
    *)
    clear
    echo "That is not an option"
    sleep 3
    x=1
    ;;
  esac


done