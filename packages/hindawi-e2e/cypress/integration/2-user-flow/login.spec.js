describe('Login', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
    cy.fixture('users/author').as('author')
  })

  it('Login successfully as user', function loginUser() {
    const { author } = this
    cy.visit('/')

    cy.get('[data-test-id="login-email"]').type(
      Cypress.env('email') + author.username,
    )
    cy.get('[data-test-id="login-password"]').type(Cypress.env('password'))
    cy.get('[data-test-id="login-button"]').click()

    cy.contains('Submit')
  })

  it('Login failure as user', () => {
    cy.visit('/')

    cy.get('[data-test-id="login-email"]').type('user@test.com')
    cy.get('[data-test-id="login-password"]').type('password123')
    cy.get('[data-test-id="login-button"]').click()

    cy.contains('Wrong username or password')
  })

  it('login via api', function loginApiGql() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('[data-test-id="login-button"]').should('not.be.visible')
  })
})
