describe('Author submits manuscript from the frontend', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
  })

  it('Author submits manuscript from UI', function authorSubmitsManuscript() {
    const { author, fragment } = this

    cy.submitManuscript({ fragment, author })
  })

  it('Author creates draft from UI', function authorCreateDraft() {
    const { author, fragment } = this

    cy.createDraft({ fragment, author })
  })
})
