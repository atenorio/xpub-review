describe('Re-subscribe email notifications', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
  })

  it('Re-subscribe email notifications', function unsubscribe() {
    const { author } = this

    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get('[data-test-id="admin-menu-button"]').click()
    cy.get('[data-test-id="admin-dropdown-profile"]').click()

    cy.get('[type="button"]')
      .contains('Unsubscribe')
      .click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-test-id="modal-root"]').should('not.exist')
    cy.get('[type="button"]')
      .contains('Unsubscribe')
      .click()

    cy.get('[data-test-id="modal-confirm"]').click()
    cy.get('[type="button"]')
      .contains('Unsubscribe')
      .click()

    cy.get('[data-test-id="modal-confirm"]').click()

    cy.get('[type="button"]')
      .contains('Re-subscribe')
      .click()
    cy.get('[data-test-id="modal-cancel"]').click()
    cy.get('[data-test-id="modal-root"]').should('not.exist')

    cy.get('[type="button"]')
      .contains('Re-subscribe')
      .wait(1000)
      .click()

    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[type="button"]').contains('Unsubscribe')
  })
})
