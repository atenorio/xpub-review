describe('Create author', () => {
  beforeEach(() => {
    cy.fixture('users/author').as('author')
    cy.visit('/')
  })

  it('Create succesfully a new author account', function createAuthor() {
    const { author } = this

    cy.get('[to="/signup"]')
      .click()
      .url()
      .should('include', '/signup')

    cy.get('[data-test-id="first-name"]')
      .type(author.firstName)
      .get('[data-test-id="last-name"]')
      .type(author.lastName)
      .get('div[role="listbox"]')
      .first()
      .click()
      .get('div[role="option"]')
      .contains(author.title)
      .click()
      .get('[data-test-id="country-menu"]')
      .click()
      .get('div[role="option"]')
      .contains(author.country)
      .click()
      .get('[data-test-id="affiliation"]')
      .type(author.affiliation)

      .get('.custom-checkbox')
      .get('input[name="agreeTc"]')
      .check({ force: true })
      .get('[data-test-id="form-submit"]')
      .click()
      .url()
      .should('include', '/signup')

      .get('[data-test-id="sign-up-email"]')
      .type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      .get('[data-test-id="password"]')
      .type(`${Cypress.env('password')}`)
      .get('[data-test-id="confirm-password"]')
      .type(`${Cypress.env('password')}`)
      .get('[data-test-id="form-submit"]')
      .click()
    cy.get('[data-test-id="manuscript-submit"]').should('be.visible')
    cy.contains('Your account is not confirmed. Please check your email.')
  })
})
