describe('A new Author on a manuscript is assigned no 2 in the list', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')

    cy.clearLocalStorage()
  })

  it('Author adds more authors to a draft', async function checkAuthorNo() {
    const { fragment, author } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/').wait(3000)

    cy.get('[type="button"]')
      .contains('Submit')
      .click()
      .wait(3000)
    cy.get('[data-test-id="journal-select"] button').click()
    cy.contains(fragment.journalName).click({ force: true })

    cy.wait(3000)
    cy.getByText('NEXT STEP').click({ force: true })

    cy.get('[data-test-id="agree-checkbox"] input')
      .should('be.visible')
      .check({ force: true })
    cy.get('[type="button"]')
      .contains('NEXT STEP')
      .click()

    await cy.location().then(loc => {
      Cypress.env(
        'manuscriptId',
        loc.pathname
          .replace('/submit', '')
          .split('/')
          .pop(),
      )
    })
    cy.log(Cypress.env('manuscriptId'))

    fragment.authors.forEach(author => {
      cy.get('[data-test-id="submission-add-author"] button').click()
      cy.get('[data-testid="new-user"]').should('contain', '#2 Author')
      cy.get('[data-test-id="email-author"]').type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
      cy.get('[data-test-id="surname-author"]').type(author.lastName)
      cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)

      cy.get('[data-test-id="country-author"]').type(author.country)
      cy.get('[role="option"]')
        .contains(author.country)
        .click({ force: true })

      cy.get('[data-test-id="save-author-1"]').click({ force: true })
    })
    cy.wait(2000)
  })

  it('Admin edits manuscript before EQS to add a new Author', function editManuscript() {
    const { admin, fragment } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
      .get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .click({ force: true })
    cy.get('[data-test-id="journal-select"] button').click()
    cy.contains(fragment.journalName).click({ force: true })

    cy.wait(3000)
    cy.getByText('NEXT STEP').click({ force: true })

    cy.get('[data-test-id="agree-checkbox"] input')
      .should('be.visible')
      .check({ force: true })
    cy.get('[type="button"]')
      .contains('NEXT STEP')
      .click()

    cy.wait(2000)

    cy.get('[data-test-id="submission-title"] input')
      .clear()
      .type('Fragment 2 - modified')

    cy.get('[data-test-id="submission-type"] button').click()
    cy.contains('Review Article').click({ force: true })

    cy.get('[data-test-id="submission-abstract"] textarea')
      .clear()
      .type('802.11B-modified')

    fragment.authors.forEach(author => {
      cy.get('[data-test-id="submission-add-author"] button').click()

      cy.get('[data-test-id="email-author"]').type(
        `${Cypress.env('email') +
          Math.random()
            .toString(22)
            .substring(8)}@thinslices.com`,
      )
      cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
      cy.get('[data-test-id="surname-author"]').type(author.lastName)
      cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)
      cy.get('[data-testid="new-user"]').should('contain', '#3 Author')
      cy.get('[data-test-id="country-author"]').type(author.country)
      cy.get('[role="option"]')
        .contains(author.country)
        .click({ force: true })

      cy.get('.icn_icn_save').click()
    })
  })
})
