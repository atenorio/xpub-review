describe('Create users into DB', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('users/ea').as('ea')
  })
  it('Create Triage Editor', function createEiC() {
    const { triageEditor } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + triageEditor.email,
      givenName: triageEditor.firstName,
      surname: triageEditor.lastName,
      role: 'triageEditor',
    }).should('equal', true)
  })
  it('Create EA', function createEA() {
    const { ea } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + ea.email,
      givenName: ea.firstName,
      surname: ea.lastName,
      role: 'editorialAssistant',
    }).should('equal', true)
  })
  it('Create AcademicEditor', function createAcademicEditor() {
    const { academicEditor } = this
    cy.task('createGlobalUser', {
      email: Cypress.env('email') + academicEditor.email,
      givenName: academicEditor.firstName,
      surname: academicEditor.lastName,
      role: 'academicEditor',
    }).should('equal', true)
  })
  it('Create Author', function createAuthor() {
    const { author } = this
    cy.task('createUser', {
      email: Cypress.env('email') + author.email,
      givenName: author.firstName,
      surname: author.lastName,
    }).should('equal', true)
  })
  it('Create reviewers', function createReviewer() {
    const { reviewer } = this
    reviewer.forEach(reviewer => {
      cy.task('createUser', {
        email: Cypress.env('email') + reviewer.email,
        givenName: reviewer.firstName,
        surname: reviewer.lastName,
      }).should('equal', true)
    })
  })
})
