const Chance = require('chance')

const chance = new Chance()

const formatAPC = apc =>
  apc
    .toLocaleString('en-US', {
      style: 'currency',
      currency: 'USD',
      minimumFractionDigits: 0,
      maximumFractionDigits: 0,
    })
    .replace(/,/g, ' ')

describe('Admin adds journal', () => {
  let name, code, issn, email

  beforeEach(() => {
    name = `[Auto] Journal ${chance.word()}`
    code = chance.syllable()
    issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    email = `${Cypress.env('email')}${code}@thinslices.com`
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Journals').click()
        cy.get('[data-test-id="add-journal"]').click()
      })
  })

  it("Doesn't accept input with non-alphanumberic characters", () => {
    cy.get('[data-test-id="apc-input"]').click()
    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id="apc-input"]')
      .type('!')
      .as('input')
    cy.get('.erron-wrapper')
      .contains('Invalid number')
      .as('error')

    const wrongString = '§=`[];\'\\,./±!@#$%^&*()_+~{}:"|<>?'

    wrongString.split('').forEach(l => {
      cy.get('@input')
        .clear()
        .type(l)
        .get('@error')
        .contains('Invalid number')
        .wait(200)
    })
  })

  it("Doesn't accept input with empty string", () => {
    cy.get('[data-test-id="apc-input"]').type(' ')
    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)
    cy.get('.erron-wrapper').contains('Required')
  })

  it("Accepts the number 0 and doesn't break the journal card", () => {
    const apc = 0

    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)
      .get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1) // first journal- is the logo
      .contains('APC')
  })

  it('Correctly formats the currency', () => {
    const apc = 1234

    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)
    cy.get('[data-test-id="prm-input-filter"]').click()
    cy.findByText('Associate Editor').click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()
      .get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1) // first journal- is the logo
      .contains(`${formatAPC(apc)}`)
  })
})
