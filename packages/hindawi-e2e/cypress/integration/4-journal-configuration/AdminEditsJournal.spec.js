const Chance = require('chance')

const chance = new Chance()

describe('Admin edits a journal', () => {
  beforeEach(() => {
    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
        cy.get('[data-test-id="admin-menu-button"]').click()
        cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
        cy.contains('Journals').click()
      })
  })

  it('Adds an active journal', () => {
    const todayDate = Cypress.moment().format('YYYY-MM-DD')

    const name = `[Auto] Journal ${chance.word()}`
    const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
    const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    const apc = `${chance.natural({ min: 100, max: 1999 })}`
    const email = `${Cypress.env('email')}${code}@thinslices.com`

    cy.get('[data-test-id="add-journal"]').click()
    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)
      .get('span')
      .contains('Active for submissions')
      .click()
      .get('[data-test-id="calendar-button"]')
      .contains(todayDate)

    cy.get('[data-test-id="prm-input-filter"]').click()
    cy.findByText('Section Editor').click()

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(0)
      .click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()

    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1) // first journal- is the logo
      .contains(name)
      .get('span')
      .contains('ACTIVE')
  })

  it('Admin edits an active existing journal', () => {
    const name = `[Edited]-${chance.word()}`
    const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
    const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    const apc = `${chance.natural({ min: 100, max: 1999 })}`
    const email = `${Cypress.env('email')}${code}@thinslices.com`

    cy.wait(2000)
    cy.get('[data-test-id^="journal-"]').eq(1)
    cy.get('span')
      .eq(4)
      .click()

    cy.get('[data-test-id="journal-name-input"]')
      .clear()
      .type(name)
      .get('[data-test-id="code-input"]')
      .clear()
      .type(code)
      .get('[data-test-id="issn-input"]')
      .clear()
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .clear()
      .type(apc)
      .get('[data-test-id="email-input"]')
      .clear()
      .type(email)

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(2)
      .click()
    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(2000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .get('h3')
      .contains(name)
  })

  it('Adds an inactive journal', () => {
    const name = `[Auto] Journal ${chance.word()}`
    const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
    const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    const apc = `${chance.natural({ min: 100, max: 1999 })}`
    const email = `${Cypress.env('email')}${code}@thinslices.com`

    cy.get('[data-test-id="add-journal"]').click()
    cy.get('[data-test-id="journal-name-input"]')
      .type(name)
      .get('[data-test-id="code-input"]')
      .type(code)
      .get('[data-test-id="issn-input"]')
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .type(apc)
      .get('[data-test-id="email-input"]')
      .type(email)

    cy.get('[data-test-id="prm-input-filter"]').click()
    cy.findByText('Single Tier Academic Editor').click()

    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(0)
      .click()
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(1)
      .click()

    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(1000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .findByText(name)
      .should('be.visible')
  })

  it('Admin edits an inactive existing journal', () => {
    const todayDate = Cypress.moment().format('YYYY-MM-DD')

    const name = `[Edited]-${chance.word()}`
    const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
    const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
      min: 999,
      max: 9999,
    })}`
    const apc = `${chance.natural({ min: 100, max: 1999 })}`
    const email = `${Cypress.env('email')}${code}@thinslices.com`

    cy.wait(2000)
    cy.get('[data-test-id^="journal-"]').eq(1)
    cy.get('span')
      .eq(4)
      .click()

    cy.get('[data-test-id="journal-name-input"]')
      .clear()
      .type(name)
      .get('[data-test-id="code-input"]')
      .clear()
      .type(code)
      .get('[data-test-id="issn-input"]')
      .clear()
      .type(issn)
      .get('[data-test-id="apc-input"]')
      .clear()
      .type(apc)
      .get('[data-test-id="email-input"]')
      .clear()
      .type(email)
      .get('span')
      .contains('Active for submissions')
      .click()
      .get('[data-test-id="calendar-button"]')
      .contains(todayDate)
    cy.get(`[data-test-id="article-types-box"]`)
      .find('label')
      .eq(2)
      .click()
    cy.get('[data-test-id="modal-confirm"]')
      .click()
      .wait(2000)

    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .get('h3')
      .contains(name)
      .get('span')
      .contains('ACTIVE')
  })
})
