describe('Admin adds editorial role', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
    cy.fixture('users/user').as('user')

    cy.fixture('users/admin')
      .as('admin')
      .then(admin => {
        cy.loginApi(admin.email, admin.password, true).visit('/')
      })
  })
  it('Admin adds a triage editor', function addTriageEditor() {
    const { user, admin } = this
    cy.createUser({ admin, user })
    Cypress.env('newUser', user)
    cy.log(Cypress.env('userEmail'))
    cy.createJournal({ admin })
    cy.log(Cypress.env('userEmail'))
    cy.visit('/admin/journals')
    cy.wait(2000)
    cy.get('[data-test-id^="journal-"]')
      .eq(1)
      .click()
    cy.wait(2000)
    cy.findByText('Assign Editorial Role').click()
    cy.get('[data-test-id="role-filter"]').click()
    cy.get('[data-test-id="role-triageEditor"]').click()
    cy.log(Cypress.env('userEmail'))
    cy.get('input[placeholder="Name or Email"]').type(
      `${Cypress.env('userEmail')}{enter}`,
    )
    cy.findByText('Add User').click()
    cy.contains(Cypress.env('userEmail'))
    cy.findByText('No results found').should('be.visible')
  })
})
