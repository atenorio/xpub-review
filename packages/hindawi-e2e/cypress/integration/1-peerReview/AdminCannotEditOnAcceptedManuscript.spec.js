describe('Admin cannot edit on Accepted manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { reviewer, academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
  })

  it('Give a review for publish as Reviewer', function minorRevisionReview() {
    const { reviewer } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.submitReview('Publish')
  })

  it('AcademicEditor makes recommendation to publish', function academicEditorMakesRecommendationToPublish() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.academicEditorMakesRecommendation('Publish')
  })

  it('Triage Editor makes decision to publish', function triageEditorMakesDecisioToPublish() {
    const { triageEditor } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.triageEditorMakesDecision('Publish')
  })

  it('Manuscript approved by EQA', function approveManuscriptByEQA() {
    const { admin } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.approveManuscriptByEQA({ admin })
  })

  it('Admin tries to edit manuscript in Accepted status', function editManuscriptApi() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit(
      `/submit/${Cypress.env('submissionId')}/${Cypress.env('manuscriptId')}`,
    )
    return cy.location().then(loc => {
      cy.expect(loc.pathname).contain('/404')
    })
  })
})
