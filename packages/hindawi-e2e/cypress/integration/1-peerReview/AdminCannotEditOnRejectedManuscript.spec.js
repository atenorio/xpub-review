describe('Admin cannot edit Rejected manuscript', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.respondToInvitationAsAcademicEditor('yes')
  })

  it('AcademicEditor makes recommendation to reject', function academicEditorMakesRecommendationToReject() {
    const { academicEditor } = this

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
  })

  it('Triage Editor makes decision to reject', function triageEditorMakesDecisioToReject() {
    const { triageEditor } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.triageEditorMakesDecision('Reject')
  })

  it('Admin tries to edit manuscript in Rejected status', function editManuscriptApi() {
    const { admin } = this

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.get('[data-test-id="button-qa-manuscript-edit"]').should('not.exist')
    cy.visit(
      `/submit/${Cypress.env('submissionId')}/${Cypress.env('manuscriptId')}`,
    )
    cy.url().should('have', '/404')
  })
})
