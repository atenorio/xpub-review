describe(`AcademicEditor doesn't see files on latest manuscript if pending`, () => {
  beforeEach(() => {
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('manuscripts/statuses').as('statuses')
    cy.fixture('models/updatedFragment').as('updatedFragment')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Should invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Should invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.inviteReviewer({ reviewer })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Should hide files from pending reviewers and accept', function reviewerDoesntSeeFiles() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.get('.icn_icn_preview').should('not.be.visible')
    cy.get('.icn_icn_downloadZip').should('not.be.visible')
    cy.get('[data-test-id="files-tab"]').should('not.be.visible')

    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Major Revision')
    cy.reload()
    cy.checkStatus(statuses.reviewCompleted.reviewer)
  })

  it('AcademicEditor makes recommendation for major revision', function majorRevisionRecommendation() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.academicEditorMakesRecommendation('Major Revision')

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.triageEditor)
  })

  it('Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.checkStatus(statuses.underReview.author)
  })

  it('Should hide files from pending reviewers only on version 2', function reviewerDoesntSeeFiles() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.reload()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.get('.icn_icn_preview').should('not.exist')
    cy.get('.icn_icn_downloadZip').should('not.exist')
    cy.get('[data-test-id="files-tab"]').should('not.exist')

    cy.contains('Version 2').click()
    cy.contains('Version 1').click()

    cy.get('.icn_icn_preview').should('be.visible')
    cy.get('.icn_icn_downloadZip').should('be.visible')
    cy.get('[data-test-id="files-tab"]').should('be.visible')
  })
})
