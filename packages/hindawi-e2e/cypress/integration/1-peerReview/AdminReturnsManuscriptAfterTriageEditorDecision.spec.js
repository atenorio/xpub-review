describe('Admin returns manuscript after Triage Editor decision', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, academicEditor, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')

    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
  })

  it('AcademicEditor makes recommendation to publish', function academicEditorMakesRecommendationToPublish() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.academicEditorMakesRecommendation('Publish')
    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.triageEditor)
  })

  it('Triage Editor makes decision to publish', function triageEditorMakesDecisioToPublish() {
    const { triageEditor, statuses } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.triageEditor)
    cy.contains('Publish')
    cy.triageEditorMakesDecision('Publish')
    cy.checkStatus(statuses.inQA.triageEditor)
  })

  it('Manuscript rejected by EQA', function adminRejectManuscript() {
    const { admin, triageEditor } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.rejectManuscriptByEQA()
    cy.get('[data-test-id="journal-logo"]').click()

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.contains('Publish')
    cy.contains('Return to Academic Editor')
    cy.contains('Reject')

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.contains('Your Editorial Decision').click()
    cy.contains('Please select').click()
    cy.get('[role="option"]').contains('Publish')
    cy.contains('Return to Academic Editor')
    cy.contains('Reject')
  })
})
