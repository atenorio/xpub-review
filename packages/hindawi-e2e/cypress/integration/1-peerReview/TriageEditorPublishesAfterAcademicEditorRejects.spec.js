describe('Triage Editor publishes after AcademicEditor rejects', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit(`/`).wait(2000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')
    cy.wait(4000)

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/').wait(2000)
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.inviteReviewer({ reviewer, academicEditor })
    cy.wait(3000)
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, academicEditor, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')
    cy.reload()
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')
    cy.wait(3000)

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
  })

  it('AcademicEditor rejects manuscript ', function AcademicEditorrejectsManuscript() {
    const { academicEditor, triageEditor, admin, statuses, author } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    return cy.academicEditorMakesRecommendation('Reject').then(() => {
      cy.wait(3000)
      cy.get('[data-test-id="contextual-box-editorial-comments"]')
        .contains('Editorial Comments')
        .should('be.visible')

      cy.loginApi(triageEditor.email, triageEditor.password)
      cy.visit('/')
      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.triageEditor)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-author'),
        )
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-triageEditor'),
        )

      cy.loginApi(admin.email, admin.password, true)
      cy.visit('/')
      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.checkStatus(statuses.pendingApproval.admin)
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`)
        .should('be.visible')
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-author'),
        )
        .should(
          'contain',
          Cypress.env('academicEditorRecommendationText-triageEditor'),
        )

      cy.loginApi(author.email, author.password)
      cy.visit('/')
      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()
      cy.get(`[data-test-id="contextual-box-editorial-comments"]`).should(
        'not.be.visible',
      )
    })
  })

  it('Triage Editor makes decision to publish', function triageEditorMakesDecisioToPublish() {
    const { triageEditor, statuses, admin, author } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.triageEditor)

    cy.get('[data-test-id="contextual-box-editorial-comments"]')
      .should('be.visible')
      .contains('Reject')

    cy.triageEditorMakesDecision('Publish')
    cy.checkStatus(statuses.inQA.triageEditor)

    cy.loginApi(author.email, author.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.inQA.author)

    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.inQA.admin)
    cy.get('[data-test-id="contextual-box-editorial-comments"]')
      .should('be.visible')
      .contains('Publish')
  })
})
