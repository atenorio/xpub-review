describe('Request Manuscript Revision Triage Editor', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/author').as('author')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')
  })

  it('Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPI()
  })

  it('Triage Editor successfully request manuscript revision from author', function requestManuscriptRevision() {
    const { triageEditor, statuses, author, updatedFragment } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.submitted.triageEditor)

    return cy.triageEditorMakesDecision('Request Revision').then(() => {
      cy.checkStatus(statuses.revisionRequested.triageEditor)

      cy.loginApi(author.email, author.password)
      cy.visit('/')

      cy.checkStatus(statuses.revisionRequested.author)

      cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
        .should('be.visible')
        .click()

      cy.get('[data-test-id="contextual-box-editorial-comments"]')
        .contains('Editorial Comments')
        .should('be.visible')

      cy.authorSubmitRevision({ updatedFragment })
    })
  })
})
