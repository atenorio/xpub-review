describe('Manuscript main flow', () => {
  beforeEach(() => {
    cy.fixture('users/admin').as('admin')
    cy.fixture('users/triageEditor').as('triageEditor')
    cy.fixture('users/academicEditor').as('academicEditor')
    cy.fixture('users/author').as('author')
    cy.fixture('users/reviewer').as('reviewer')
    cy.fixture('models/fragment').as('fragment')
    cy.fixture('models/updatedFragment').as('updatedFragment')
    cy.fixture('manuscripts/statuses').as('statuses')

    cy.clearLocalStorage()
    // cy.pause()
  })

  it('1. Successfully submits a manuscript', () => {
    cy.createManuscriptViaAPInoEQS(`${Cypress.env('manuscriptId')}`)
  })

  it('2. Successfully approve EQS', function approveEQA() {
    const { admin, statuses, fragment } = this

    cy.approveManuscriptByEQS({ admin, fragment, statuses })
  })

  it('3. Invite AcademicEditor as Triage Editor', function inviteAcademicEditor() {
    const { triageEditor, academicEditor, statuses } = this
    cy.inviteAcademicEditor({ triageEditor, academicEditor })
    cy.checkStatus(statuses.academicEditorInvited.triageEditor)

    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')
    cy.wait(2000)

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
  })

  it('4. Should accept invitation as AcademicEditor', function respondToInvitationAsAcademicEditor() {
    const { statuses, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.academicEditorInvited.academicEditor)
    cy.respondToInvitationAsAcademicEditor('yes')

    cy.checkStatus(statuses.academicEditorAssigned.academicEditor)
  })

  it('5. Invite reviewers as AcademicEditor', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('6. Give a review for major revision as Reviewer', function majorRevisionReview() {
    const { reviewer, academicEditor, statuses } = this

    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')

    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Major Revision')

    cy.checkStatus(statuses.reviewCompleted.reviewer)

    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.checkStatus(statuses.reviewCompleted.academicEditor)
  })

  it('7. AcademicEditor makes recommendation for major revision', function majorRevisionRecommendation() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.academicEditorMakesRecommendation('Major Revision')

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.triageEditor)
  })

  it('8. Author submit major revision', function majorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.checkStatus(statuses.underReview.author)
  })

  // Minor Revision
  it('9. Give a review for minor revision as Reviewer', function minorRevisionReview() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[1].email, reviewer[1].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)
    cy.respondToInvitationAsReviewer('yes')

    cy.checkStatus(statuses.underReview.reviewer)
    cy.submitReview('Minor Revision')

    cy.checkStatus(statuses.reviewCompleted.reviewer)
  })

  it('10. AcademicEditor makes recommendation for minor revision', function minorRevisionRecommendation() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()
    cy.academicEditorMakesRecommendation('Minor Revision')

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
      .should('be.visible')
      .click()

    cy.checkStatus(statuses.revisionRequested.triageEditor)
  })

  it('11. Author submit minor revision', function minorRevisionSubmission() {
    const { author, statuses, updatedFragment } = this
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.checkStatus(statuses.revisionRequested.author)
    cy.authorSubmitRevision({ updatedFragment })
    cy.visit('/')
    cy.get(`[data-test-id="dashboard-list-items"] > div`)
      .first()
      .click()
    return cy.location().then(loc => {
      Cypress.env(
        'manuscriptId',
        loc.pathname
          .replace('/details', '')
          .split('/')
          .pop(),
      )
    })
  })

  // Publish
  it('12. Invite reviewers as AcademicEditor for another version', function inviteReviewer() {
    const { statuses, reviewer, academicEditor } = this
    cy.loginApi(academicEditor.username, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.inviteReviewer({ reviewer, academicEditor })
    cy.checkStatus(statuses.reviewersInvited.academicEditor)
  })

  it('13. Give a review for publish as Reviewer', function submitPublishReview() {
    const { reviewer, statuses } = this
    cy.loginApi(reviewer[2].email, reviewer[2].password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)

      .should('be.visible')
      .click()

    cy.checkStatus(statuses.reviewersInvited.reviewer)

    cy.respondToInvitationAsReviewer('yes')
    cy.checkStatus(statuses.underReview.reviewer)

    cy.submitReview('Publish')

    cy.checkStatus(statuses.reviewCompleted.reviewer)
  })

  it('14. AcademicEditor makes recommendation to publish', function academicEditorMakesRecommendationToPublish() {
    const { academicEditor, triageEditor, statuses } = this
    cy.loginApi(academicEditor.email, academicEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()

    cy.academicEditorMakesRecommendation('Publish')
    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)

      .should('be.visible')
      .click()

    cy.checkStatus(statuses.pendingApproval.triageEditor)
  })

  it('15. Triage Editor makes decision to publish', function triageEditorMakesDecisioToPublish() {
    const { triageEditor, statuses } = this

    cy.loginApi(triageEditor.email, triageEditor.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.pendingApproval.triageEditor)

    cy.get('[data-test-id="contextual-box-editorial-comments"]')
      .should('be.visible')
      .contains('Publish')

    cy.triageEditorMakesDecision('Publish')

    cy.checkStatus(statuses.inQA.triageEditor)
  })

  it('16. Manuscript approved by EQA', function approveManuscriptByEQA() {
    const { admin, author, statuses } = this
    cy.loginApi(admin.email, admin.password, true)
    cy.visit('/')
    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.approveManuscriptByEQA({ admin })
    cy.loginApi(author.email, author.password)
    cy.visit('/')

    cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`).click()
    cy.checkStatus(statuses.accepted.author)
  })
})
