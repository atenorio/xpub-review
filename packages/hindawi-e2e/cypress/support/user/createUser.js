const createUser = ({ admin, user }) => {
  cy.loginApi(admin.email, admin.password, true)
  const userEmail = `${Cypress.env('email') +
    Math.random()
      .toString(22)
      .substring(8)}@hindawi.com`
  Cypress.env('userEmail', userEmail)
  cy.visit(`/admin/users`)
    .url()
    .should('include', '/admin/users')
    .get('[data-test-id="add-user"]')
    .click()

    .get('[name="givenNames"]')
    .type(user.firstName)

    .get('[name="surname"]')
    .type(user.lastName)

    .get('[role="listbox"]')
    .eq(0)
    .click()
    .get('[role="option"]')
    .contains(user.title)
    .click()

    .get('[name="email"]')
    .clear()
    .type(userEmail)

    .get('[role="listbox"]')
    .eq(1)
    .click()
    .get('[role="option"]')
    .contains(user.country)
    .click()

    .get('[name="aff"]')
    .type(user.affiliation)

    .then($body => {
      if (user.isAdmin)
        cy.get('[type="checkbox"]')
          .first()
          .click({ force: true })
    })

    .get('[type="button"]')
    .contains('SAVE USER')
    .click()
}

Cypress.Commands.add('createUser', createUser)
