const editManuscript = fragment => {
  cy.get('[type="button"]')
    .eq(0)
    .click()
  cy.get('[role="option"]')
    .contains('Bioinorganic Chemistry and Applications')
    .click()
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()
  cy.wait(3000)
  cy.get('[data-test-id="agree-checkbox"] input')
    .should('be.visible')
    .check({ force: true })
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()

  cy.wait(2000)

  cy.get('[data-test-id="submission-title"] input')
    .clear()
    .type('Fragment 2 - modified')

  cy.get('[data-test-id="submission-type"] button').click()
  cy.contains('Review Article').click({ force: true })

  cy.get('[data-test-id="submission-abstract"] textarea')
    .clear()
    .type('802.11B-modified')

  fragment.authors.forEach(author => {
    cy.get('[data-test-id="submission-add-author"] button').click()
    cy.get('[data-test-id="email-author"]').type(
      `${Cypress.env('email') +
        Math.random()
          .toString(22)
          .substring(8)}@thinslices.com`,
    )
    cy.get('[data-test-id="givenNames-author"]').type(author.firstName)
    cy.get('[data-test-id="surname-author"]').type(author.lastName)
    cy.get('[data-test-id="affiliation-author"]').type(author.affiliation)

    cy.get('[data-test-id="country-author"]').type(author.country)
    cy.get('[role="option"]')
      .contains(author.country)
      .click({ force: true })

    cy.get('.icn_icn_save').click()
  })
  cy.findByText('Yes').click()
  cy.get('[name="meta.conflictOfInterest"]').type(fragment.conflicts.message)
  cy.get('[name="meta.dataAvailability"]').type(
    fragment.conflicts.dataAvailabilityMessage,
  )
  cy.get('[name="meta.fundingStatement"]').type(
    fragment.conflicts.fundingMessage,
  )

  cy.wait(2000)
  cy.get('[type="button"]')
    .contains('NEXT STEP')
    .click()

  cy.contains('Main Manuscript')
  cy.log(Cypress.env('manuscriptId'))
  cy.location().then(loc => {
    const manuscriptId = loc.pathname
      .replace('/submit', '')
      .split('/')
      .pop()
    cy.uploadFileViaAPI({
      entityId: manuscriptId,
      fileType: 'supplementary',
      onResponse: () => {},
    })
    cy.uploadFileViaAPI({
      entityId: manuscriptId,
      fileType: 'coverLetter',
      onResponse: () => {},
    })
  })
  cy.wait(3000)
}

Cypress.Commands.add('editManuscript', editManuscript)
