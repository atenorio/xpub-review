export { default as submitManuscript } from './submitManuscript'
export { default as approveManuscriptByEQS } from './approveManuscriptByEQS'
export { default as inviteAcademicEditor } from './inviteAcademicEditor'
export { default as checkStatus } from './checkStatus'
export { default as inviteReviewer } from './inviteReviewer'
export { default as rejectManuscriptByEQS } from './rejectManuscriptByEQS'
export { default as authorSubmitRevision } from './authorSubmitRevision'
export {
  default as respondToInvitationAsAcademicEditor,
} from './respondToInvitationAsAcademicEditor'
export {
  default as respondToInvitationAsReviewer,
} from './respondToInvitationAsReviewer'
export { default as submitReview } from './submitReview'
export {
  default as triageEditorMakesDecision,
} from './triageEditorMakesDecision'
export {
  default as academicEditorMakesRecommendation,
} from './academicEditorMakesRecommendation'
export {
  default as resendAcademicEditorInvite,
} from './resendAcademicEditorinvite'
export {
  default as resendAcademicEditorInviteApi,
} from './resendAcademicEditorinviteApi'
export {
  default as triageEditorRevokesAcademicEditor,
} from './triageEditorRevokesAcademicEditor'
export { default as approveManuscriptByEQA } from './approveManuscriptByEQA'
export { default as editManuscript } from './editManuscript'
export { default as createDraft } from './createDraft'
export { default as rejectManuscriptByEQA } from './rejectManuscriptByEQA'
export { default as addJournal } from './addJournal'
