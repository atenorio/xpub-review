const approveManuscriptByEQA = () => {
  cy.get('[data-test-id="button-qa-manuscript-technical-checks"]')
    .should('be.visible')
    .click()
  cy.contains('div', 'Editorial decision').should('be.visible')
  cy.get('[data-test-id="accept-button"]').click()

  cy.contains('Are you sure you want to accept this manuscript?')
  cy.contains('div', 'Accept manuscript').should('be.visible')
  cy.get('[data-test-id="modal-confirm"]').click()

  cy.contains('Manuscript decision submitted. Thank you!')
}

Cypress.Commands.add('approveManuscriptByEQA', approveManuscriptByEQA)
