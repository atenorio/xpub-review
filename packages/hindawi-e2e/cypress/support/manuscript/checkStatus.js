const checkStatus = status => {
  cy.get('[data-test-id="manuscript-status"]').should('contain', status)
}

Cypress.Commands.add('checkStatus', checkStatus)
