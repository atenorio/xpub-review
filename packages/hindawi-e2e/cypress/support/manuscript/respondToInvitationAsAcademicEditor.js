const respondToInvitationAsAcademicEditor = response => {
  cy.get(`[value=${response}]`).click()
  cy.contains('Respond to Invitation').click()
  cy.get(`[data-test-id="modal-confirm"]`).click()
}
Cypress.Commands.add(
  'respondToInvitationAsAcademicEditor',
  respondToInvitationAsAcademicEditor,
)
