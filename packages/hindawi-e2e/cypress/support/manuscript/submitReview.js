const submitReview = recommendation => {
  cy.findByText('Your Report').click()
  cy.findByText('Choose in the list').click()
  cy.findByText(recommendation).click()

  cy.get('[data-test-id="form-report-textarea"]')
    .click()
    .type(Math.random().toString(22))
  cy.get('[data-test-id="submit-reviewer-report"]').click()
  cy.findByText('Progress Saved')
  cy.get('[data-test-id="modal-confirm"]').click()
}

Cypress.Commands.add('submitReview', submitReview)
