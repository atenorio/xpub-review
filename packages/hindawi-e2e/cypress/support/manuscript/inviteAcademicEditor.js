const inviteAcademicEditor = ({ triageEditor, academicEditor }) => {
  cy.loginApi(triageEditor.email, triageEditor.password)
  cy.visit(`/`)

  cy.get(`[data-test-id="manuscript-${Cypress.env('manuscriptId')}"]`)
    .should('be.visible')
    .click()
  cy.contains('Assign Academic Editor').click()
  cy.get('[data-test-id="manuscript-assign-academic-editor-filter"]').type(
    Cypress.env('email') + academicEditor.email,
    { force: true },
  )
  cy.get(`[data-test-id^="manuscript-assign-academic-editor-invite"]`)
    .contains('INVITE')
    .click()
  cy.get('[data-test-id="modal-confirm"]').click()
}
Cypress.Commands.add('inviteAcademicEditor', inviteAcademicEditor)
