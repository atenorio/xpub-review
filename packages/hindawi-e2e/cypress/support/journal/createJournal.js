const Chance = require('chance')

const chance = new Chance()

const name = `[Auto] Journal ${chance.word()}`
const code = `${chance.syllable()}${chance.natural({ min: 1, max: 99 })}`
const issn = `${chance.natural({ min: 999, max: 9999 })}-${chance.natural({
  min: 999,
  max: 9999,
})}`
const apc = `${chance.natural({ min: 100, max: 1999 })}`
const email = `${Cypress.env('email')}${code}@thinslices.com`
// const todayDate = Cypress.moment().format('YYYY-MM-DD')

const createJournal = ({ admin }) => {
  cy.loginApi(admin.email, admin.password, true).visit('/')
  cy.get('[data-test-id="admin-menu-button"]').click()
  cy.get('[data-test-id="admin-dropdown-dashboard"]').click()
  cy.contains('Journals').click()
  cy.get('[data-test-id="add-journal"]').click()

  cy.get('[data-test-id="journal-name-input"]')
    .type(name)
    .get('[data-test-id="code-input"]')
    .type(code)
    .get('[data-test-id="issn-input"]')
    .type(issn)
    .get('[data-test-id="apc-input"]')
    .type(apc)
    .get('[data-test-id="email-input"]')
    .type(email)
    .get('span')
    .contains('Active for submissions')
    .click()
  // .get('[data-test-id="calendar-button"]')
  // .contains(todayDate)
  // .click()
  // .get('.react-calendar')
  // .get('.react-calendar__month-view')
  // .click()

  cy.get('[data-test-id="prm-input-filter"]').click()
  cy.findByText('Section Editor').click()
  cy.get(`[data-test-id="article-types-box"]`)
    .find('label')
    .eq(0)
    .click()
  cy.get(`[data-test-id="article-types-box"]`)
    .find('label')
    .eq(1)
    .click()
    .get('[data-test-id="modal-confirm"]')
    .click()
    .wait(1000)
}
Cypress.Commands.add('createJournal', createJournal)
